//
//  OfferCollectionViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/19/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class OfferCollectionViewCell: UICollectionViewCell {
    @IBOutlet var offerPriceLabel: UILabel!
    
    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var typeOfOffLabel: UILabel!
    @IBOutlet var myview: UIView!
    
}
