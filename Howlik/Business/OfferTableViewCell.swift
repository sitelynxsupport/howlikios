//
//  OfferTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/19/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {
    
    @IBOutlet var offercollectionviewHeight: NSLayoutConstraint!
    @IBOutlet var offerCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      //  if let flowLayout = offerCollectionView.collectionViewLayout as? UICollectionViewFlowLayout { flowLayout.estimatedItemSize = CGSize( width: 1 , height: 1) }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
