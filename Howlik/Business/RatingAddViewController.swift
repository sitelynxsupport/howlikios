//
//  RatingAddViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/15/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Cosmos
import UITextViewPlaceholder
import FontAwesome_swift
import SCLAlertView

class RatingAddViewController: UIViewController,UITextViewDelegate {
 var bizTitle = ""
    var id = ""
    var status = ""
     let buisnessapiClass = ApiListViewcontroller()
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var crossbutton: UIButton!
    
    @IBOutlet var reviewtext: UITextView!
    @IBOutlet var dollarrating: CosmosView!
    @IBOutlet var starrating: CosmosView!
    var apikey = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        reviewtext.becomeFirstResponder()
//reviewtext.addDoneButtonOnKeyboard()
      addDoneactioButtonOnKeyboard()
 titleLabel.text = bizTitle
        reviewtext.delegate = self
    
        
        reviewtext.placeholder = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.*"
     

    }
 
    @IBAction func closeAction(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        }
            reviewtext.becomeFirstResponder()
          dollarrating.becomeFirstResponder()
          starrating.becomeFirstResponder()
        starrating.shouldHideToolbarPlaceholder = false
       
        crossbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.timesCircle, textColor: UIColor.red, size: CGSize(width: 80, height: 80)), for: UIControlState.normal)
        crossbutton.tintColor = UIColor.gray
             textViewDidChange(reviewtext)
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addDoneactioButtonOnKeyboard()
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
       
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Post Review", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .done, target: self, action:   #selector(RatingAddViewController.doneButtonAction))
        let items = [flexSpace, done]
        //  let items = [flexSpace]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
         reviewtext.inputAccessoryView = doneToolbar
   
    }
    @objc func doneButtonAction()
    {
          UserDefaults.standard.set(reviewtext.text, forKey: "Postedreview")
       let rating = starrating.rating
             let expense = dollarrating.rating
    apiCalling(apikey:self.apikey,biz_id:id,review:reviewtext.text,rating:rating,expense:expense)
         self.resignFirstResponder()
         dismiss(animated: true, completion: nil)
    }
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
     
        textViewDidChange(reviewtext)

}
    @objc(textViewDidChange:) func textViewDidChange(_ textView: UITextView) {
            reviewtext.becomeFirstResponder()
    }
    func apiCalling(apikey:String,biz_id:String,review:String,rating:Double,expense:Double){
        let scriptUrl = buisnessapiClass.Postareviewforbusiness
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)&review=\(review)&rating=\(rating)&expense=\(expense)"
       
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
          
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                  
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.status == "success"{
                            
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.resignFirstResponder()
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Your reviews will be posted publicly using your Howlik name and picture", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                            
                           
                        }else{
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.resignFirstResponder()
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("It looks like something is wrong!\n You can't add reviews to your own business and business that you have already reviewed", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
}

//extension UITextView{
//
//    @IBInspectable var doneAccessory: Bool{
//        get{
//            return self.doneAccessory
//        }
//        set (hasDone) {
//            if hasDone{
//                addDoneButtonOnKeyboard()
//            }
//        }
//    }
//
//    func addDoneButtonOnKeyboard()
//    {
//        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
//        doneToolbar.barStyle = .default
//
//        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let done: UIBarButtonItem = UIBarButtonItem(title: "donefgtyty", style: .done, target: self, action: #selector(self.doneButtonsAction))
//
//     let items = [flexSpace, done]
//      //  let items = [flexSpace]
//        doneToolbar.items = items
//        doneToolbar.sizeToFit()
//
//        self.inputAccessoryView = doneToolbar
//    }
//
//    @objc func doneButtonsAction()
//    {
//        self.resignFirstResponder()
//    }
//}

