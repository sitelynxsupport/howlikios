//
//  ReportView.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/24/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class ReportView: UIView {

    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var reportLabel: UILabel!
    @IBOutlet var commentsText: UITextView!
  
    @IBOutlet var mainview: UIView!
    @IBOutlet var cancelOutlet: UIButton!
    
    @IBOutlet var lastView: UIView!
    @IBOutlet var okOutlet: UIButton!
    override init(frame: CGRect){ // for using custom viw in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
        
        commonInit()
    }
    private func commonInit(){
        //  we are going to do stuff here
        //        Bundle.main.loadNibNamed("replyView", owner: self, options: nil)
        //        addSubview(replyView)
        //        replyView.frame = self.bounds
        //        replyView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        let nibView = Bundle.main.loadNibNamed("ReportView", owner: self, options: nil)![0] as! UIView
        nibView.frame = self.bounds;
        self.addSubview(nibView)
    }
    

}
