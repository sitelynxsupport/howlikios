//
//  ReviewTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Cosmos

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet var discriptionlabel: UILabel!
    @IBOutlet var dollarview: CosmosView!
    @IBOutlet var ratingview: CosmosView!
    @IBOutlet var reviewpersonname: UILabel!
    @IBOutlet var reviewimage: UIImageView!
    @IBOutlet var cosmosview: UIView!
    @IBOutlet var datelabel: UILabel!
    
    @IBOutlet var heighcon: NSLayoutConstraint!
    @IBOutlet var viewAllbutton: UIButton!
    @IBOutlet var reviewLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
