//
//  4actionTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class actionTableViewCell: UITableViewCell {
    @IBOutlet var imgbutton: UIImageView!
    
    @IBOutlet var labelheight: NSLayoutConstraint!
    @IBOutlet var optionlabel: UILabel!
    @IBOutlet var actionbutton: UIButton!
    @IBOutlet var titlelabel: UILabel!
    @IBOutlet var cosmosview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
