//
//  addgiftcardViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/22/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FontAwesome_swift
import Alamofire
import TTGSnackbar
import SCLAlertView

class addgiftcardViewController: UIViewController,NVActivityIndicatorViewable,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {
    var biz_id = ""
    var apikey = ""
    var language_code = ""
    var country_code = ""
      var pricebook_Array = [pricesclass]()
     var value = ""
    var val = false
    var currency_code = ""
    var lan_id = ""
    var sender_id = ""
    //var cancelButton = UIBarButtonItem()
  //  var doneButton = UIBarButtonItem()
  //  var spaceButton = UIBarButtonItem()
  //  let toolBar = UIToolbar()
    
    var myPickerView : UIPickerView!
     let buisnessapiClass = ApiListViewcontroller()
    @IBOutlet var quantityview: UIView!
    @IBOutlet var bangroundview: UIView!
    @IBOutlet var purchaseoutlet: UIButton!
    @IBOutlet var messageTextfield: UITextField!
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var reciepenttextfield: UITextField!
    @IBOutlet var sendersname: UITextField!
    @IBOutlet var pickerview: UIPickerView!
    
    @IBOutlet var quantityoutlet: UITextField!
    @IBOutlet var totallabel: UILabel!
   
    @IBOutlet var pricelabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
         SetBackBarButtonCustom()
         if  UserDefaults.standard.string(forKey:"apikey") != nil{
         bangroundview.isHidden = true
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton("OK", action: { // create button on alert
                print("click click") // action on click
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError("ERROR", subTitle: "Please  Register to create profile!")
         }else{
           bangroundview.isHidden = false
      //  pickerview.isHidden = true
        quantityoutlet.delegate = self
       
        // Do any additional setup after loading the view.
        self.navigationItem.title = "ADD GIFT CARD"
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        sendersname.setBottomBorder()
        reciepenttextfield.setBottomBorder()
        emailTextfield.setBottomBorder()
        messageTextfield.setBottomBorder()
       
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            
          
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.country_code = UserDefaults.standard.string(forKey: "country_code")!
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
GiftcertificateapiCalling(apikey:apikey,biz_id:biz_id,country_code:country_code,language_code:language_code)
            
        }else{
            print("%%%%%%%%%%%%%%dgfrdytr%%%%%%%%%%%%%%%%%")
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            
            
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton("OK", action: { // create button on alert
                print("click click") // action on click
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError("ERROR", subTitle: "Please  Register to create profile!")
        }
        
        
        if  UserDefaults.standard.string(forKey:"senderid") != nil{
            
            self.sender_id = UserDefaults.standard.string(forKey: "senderid")!
        }
        // Do any additional setup after loading the view.
        purchaseoutlet.backgroundColor = .clear
        purchaseoutlet.layer.cornerRadius = 20
        purchaseoutlet.layer.borderWidth = 1
        purchaseoutlet.layer.borderColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
        purchaseoutlet.setTitleColor(UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0), for: UIControlState())
        totallabel.layer.cornerRadius = 5.0
        totallabel.layer.borderColor = UIColor.lightGray.cgColor
        totallabel.layer.borderWidth = 0.5
        //totallabel.layer.masksToBounds = false

        quantityoutlet.setBorder()
        pricelabel.layer.cornerRadius = 5.0
        pricelabel.layer.borderColor = UIColor.lightGray.cgColor
        pricelabel.layer.borderWidth = 0.5
       // quantityoutlet.layer.masksToBounds = false
      
     
    quantityview.addBottomBorderWithColor(color: UIColor.black, width: 0.5)
        }
    }
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        quantityoutlet.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
      //  toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            toolBar.tintColor = UIColor.red
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(addgiftcardViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(addgiftcardViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        quantityoutlet.inputAccessoryView = toolBar
        
    }
    @objc func doneClick() {
    quantityoutlet.resignFirstResponder()
    }
    @objc func cancelClick() {
        quantityoutlet.resignFirstResponder()
    }
  
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUp(quantityoutlet)
    }
    func GiftcertificateapiCalling(apikey:String,biz_id:String,country_code:String,language_code:String){
        self.startactvityIndicator()
      //  personarray.removeAll()
        let scriptUrl = buisnessapiClass.LoadCreateGiftCertificate
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id,"language_code": language_code,"country_code": country_code]
        print(parameters)
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                        if response["prices"] == nil {
                         
                        self.bangroundview.isHidden = true
                        let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false // hide default button
                        )
                    let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                    alert.addButton("OK", action: { // create button on alert
                        self.resignFirstResponder()
                        self.dismiss(animated: true, completion: nil)
                                    })
                    alert.showInfo("ALERT", subTitle: "No Gift Card Available")
                            
                                                 
                      }else{
                        
      
                     let infos = response.object(forKey: "prices")!

                        

                        for info in (infos as! [[String:Any]]){
                            var priceElement = pricesclass()
                            let contry2 = info as! NSDictionary
                            if  (contry2["id"] as? String) != nil {
                               let id = contry2["id"] as! String
                                priceElement.price_id = id
                                
                                
                            }
                            if  (contry2["price"] as? String) != nil {
                              let price = contry2["price"] as! String
                                 priceElement.price = price
                                
                                
                            }
                            self.pricebook_Array.append(priceElement)
                            
                            }
                            let currency = response.object(forKey: "currency")!
                            let contry = currency as! NSDictionary
                            if  (contry["currency_code"] as? String) != nil {
                                self.currency_code = contry["currency_code"] as! String
                                let values = (self.pricebook_Array[0].price)
                                
                                self.quantityoutlet.text = values
                                self.totallabel.text = String(Int(self.pricelabel.text!)! * Int(values)!)
                                
                                
                            }
                      }
                    
                        
                        
                        DispatchQueue.main.async
                            {
                             
                            //    self.pickerview.reloadAllComponents()
                               
                              
                                
                                self.stopActivityIndicator()
                                
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
 func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pricebook_Array.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pricebook_Array[row].price
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     //   quantityoutlet.setTitle(pricebook_Array[row].price, for: UIControlState())
        quantityoutlet.text = pricebook_Array[row].price
        self.totallabel.text = String(Int(self.pricelabel.text!)! * Int(self.quantityoutlet.text!)!)
       
    }
    
    @IBAction func priceAction(_ sender: Any) {

        if (sendersname.text?.isEmpty)!{
               alertsnack(message: "sendersname is required..")
        }else if (reciepenttextfield.text?.isEmpty)!{
               alertsnack(message: "reciepent is required..")
            
        }else if (emailTextfield.text?.isEmpty)!{
               alertsnack(message: "email is required..")
        }else if (messageTextfield.text?.isEmpty)!{
               alertsnack(message: "message is required..")
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "paypal") as! paypalViewController
            vc.biz_id = biz_id
            vc.curr_code = currency_code
            vc.lan_id = language_code
            vc.gift_amount = quantityoutlet.text!
            vc.recipient_name = reciepenttextfield.text!
            vc.curr_code = currency_code
            vc.recipient_email = reciepenttextfield.text!
            vc.recipient_message = messageTextfield.text!
            vc.recipient_email = reciepenttextfield.text!
            vc.recipient_message = messageTextfield.text!
            vc.sender_name = sendersname.text!
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
       
        }
    }
    func alertsnack(message: String){
        
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = "Done"
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    
    func posttableapiCalling(apikey:String,biz_id:String,curr_code:String,lan_id:String,gift_amount:String,recipient_name:String,gift_quantity:String,recipient_email:String,recipient_message:String,sender_name:String,sender_id:String,pay_now:String){
       /* self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.PostCreateGiftCertificate
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)&curr_code=\(curr_code)&lan_id=\(lan_id)&gift_amount=\(gift_amount)&recipient_name=\(recipient_name)&gift_quantity=\(gift_quantity)&recipient_email=\(recipient_email)&recipient_message=\(recipient_message)&sender_name=\(sender_name)&sender_id=\(sender_id)&pay_now=\(pay_now)"
        print(">>>>>>>>>")
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    print(convertedJsonIntoArray)
                 //   self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                //    print(self.status)
                    
                    
                }
                
                
                DispatchQueue.main.async
                    {
                    //    if self.status == "success"{
                            // self.stopActivityIndicator()
                            
//                            self.stopActivityIndicator()
//                            let appearance = SCLAlertView.SCLAppearance(
//                                showCloseButton: false // hide default button
//                            )
//                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
//                            alert.addButton("OK", action: { // create button on alert
//                                self.resignFirstResponder()
//                                self.dismiss(animated: true, completion: nil)
//                            })
//                            alert.showSuccess("SUCCESS", subTitle: "Booking addeded successfully")
//
//
//                        }else{
//                            self.stopActivityIndicator()
//                            let appearance = SCLAlertView.SCLAppearance(
//                                showCloseButton: false // hide default button
//                            )
//                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
//                            alert.addButton("OK", action: { // create button on alert
//                                self.dismiss(animated: true, completion: nil)
//                            })
//                            alert.showError("ERROR", subTitle: "Oops...It looks like something is wrong!\nbooking not available.")
//                        }
//
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
        
    }*/
//
//        let myUrl = URL(string: "http://www.howlik.com/api/giftpay/index.php");
//
//        var request = URLRequest(url:myUrl!)
//
//        request.httpMethod = "POST"// Compose a query string
//
//        let postString = "apikey=apikey&biz_id=biz_id&curr_code=curr_code&lan_id=lan_id&gift_amount=gift_amount&recipient_name=recipient_name&gift_quantity=gift_quantity&recipient_email=recipient_email&gift_quantity=gift_quantity&recipient_message=recipient_message&sender_name=sender_name&sender_id=sender_id&pay_now=pay_now";
//
//        request.httpBody = postString.data(using: String.Encoding.utf8);
//
//        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
//
//            if error != nil
//            {
//                print("error=\(error)")
//                return
//            }
//
//            // You can print out response object
//            print("response = \(response)")
//
//            //Let's convert response sent from a server side script to a NSDictionary object:
//            do {
//                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
//
//                if let parseJSON = json {
//
//                    // Now we can access value of First Name by its key
//                    let firstNameValue = parseJSON["firstName"] as? String
//                    print("firstNameValue: \(firstNameValue)")
//                }
//            } catch {
//                print(error)
//            }
//        }
//        task.resume()
        
        
        
       
    }
}
extension UIView {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
}
