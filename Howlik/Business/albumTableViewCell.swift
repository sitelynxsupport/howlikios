//
//  albumTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class albumTableViewCell: UITableViewCell  {
    @IBOutlet var viewallLabel: UIButton!
    
    @IBOutlet var photosLabel: UILabel!
    @IBOutlet var imageview: UIImageView!
    
    @IBOutlet var albumcollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
      
        viewallLabel.setTitle(NSLocalizedString("View all", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
           photosLabel.text = NSLocalizedString("Photos", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // MARK: UICollectionViewDataSource
    
}
