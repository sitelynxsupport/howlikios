//
//  amentiesViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/20/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift
import NVActivityIndicatorView

class amentiesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable{
var apikey = ""
    var daynumberArray = [String]()
    var startTimearray = [String]()
    var endTimearray = [String]()
     var valueArray = [String]()
var informationArray = [informationprpertyclass]()
    var businessInfoArray = [String]()
    
  
    @IBOutlet var nextOutlets: UIButton!
    
 
    @IBOutlet var informationTable: UITableView!
    let buisnessapiClass = ApiListViewcontroller()
    var language_code = ""
     var country_code = ""
    override func viewDidLoad() {
        super.viewDidLoad()
          if  UserDefaults.standard.string(forKey:"apikey") != nil{
           self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        self.country_code = UserDefaults.standard.string(forKey: "country_code")!
        self.language_code = UserDefaults.standard.string(forKey: "language_code")!
LoadcreatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code)
        }
         // self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        self.navigationItem.title = NSLocalizedString("New Business", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        nextOutlets.layer.cornerRadius = 10.0
        nextOutlets.setTitle(NSLocalizedString("Next", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func LoadcreatebuisnessapiCalling(apikey:String,language_code:String,country_code:String){
        self.startactvityIndicator()
        self.informationArray.removeAll()
        let scriptUrl = buisnessapiClass.LoadCreateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        let informations = response.object(forKey: "informations")!
                   
                        for information in (informations as! [[String:Any]]){
                               let Infokey = informationprpertyclass()
                            let contry1 = information as NSDictionary
                            if  (contry1["info_id"] as? String) != nil {
                                let info_id = contry1["info_id"] as! String
                                Infokey.info_id = String(info_id)
                            }else{
                                let info_id = "nil"
                              Infokey.info_id = String(info_id)
                            }
                            
                            if  (contry1["info_title"] as? String) != nil {
                                let info_title = contry1["info_title"] as! String
                                Infokey.info_title = info_title
                            }else{
                                let info_title = "nil"
                                Infokey.info_title = info_title
                            }
                            Infokey.isChecked = false
                            self.informationArray.append(Infokey)
                        }
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                
                               self.informationTable.reloadData()
                                self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  informationArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! amentiesTableViewCell
            cell.selectionStyle = .none
        cell.propertiesLabel.text = informationArray[indexPath.row].info_title
        let val = informationArray[indexPath.row].isChecked
        
        cell.checkboxOutlet.tag = indexPath.row
       cell.checkboxOutlet.addTarget(self, action:#selector(self.checkView(_:)), for:.touchUpInside)
       
       
        if(val == true){
            cell.checkboxOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
            cell.checkboxOutlet.tintColor = UIColor.red
         
        }else{
          
            cell.checkboxOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
               cell.checkboxOutlet.tintColor = UIColor.green
        }
        
         return cell
    }
    @objc func checkView(_ sender: UIButton){

        if informationArray[sender.tag].isChecked == false{
            informationArray[sender.tag].isChecked = true
        }else{
              informationArray[sender.tag].isChecked = false
        }
        self.informationTable.reloadData()
    }
    
    @IBAction func nextAction(_ sender: Any) {
        businessInfoArray.removeAll()
        valueArray.removeAll()
        
        for i in 0..<informationArray.count{
              businessInfoArray.append(informationArray[i].info_id)
              if (informationArray[i].isChecked == true)
              {
              
                valueArray.append("1")
              }else{
                 valueArray.append("0")
            }
        }
        UserDefaults.standard.set(businessInfoArray, forKey: "id")
            UserDefaults.standard.set(valueArray, forKey: "value")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "finish") as! finishBusinessViewController
        vc.startTimearray = startTimearray
        vc.endTimearray = endTimearray
        vc.daynumberArray = daynumberArray
        let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
        self.present(navController, animated: true, completion: nil)
    }
    
}
