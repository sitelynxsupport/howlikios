//
//  amentieseditViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/6/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift
import NVActivityIndicatorView

class amentieseditViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
    var apikey = ""
    var daynumberArray = [String]()
    var startTimearray = [String]()
    var endTimearray = [String]()
    var informationArray = [informationprpertyclass]()
    var businessInfoArray = [String]()
    var id = ""
    var valueInfoArray = [String]()
    var status = ""
    @IBOutlet var nextOutlets: UIButton!
    
    
    @IBOutlet var informationTable: UITableView!
    let buisnessapiClass = ApiListViewcontroller()
    var language_code = ""
    var country_code = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        self.navigationItem.title = NSLocalizedString("EDIT BUSINESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nextOutlets.setTitle(NSLocalizedString("UPDATE", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        SetBackBarButtonCustom()
       
    }
    override func viewDidAppear(_ animated: Bool) {
        self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        self.id = UserDefaults.standard.string(forKey: "biz_id")!
        LoadcreatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code)
        
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func LoadupdatebuisnessapiCalling(apikey:String,language_code:String,country_code:String,biz_id:String){
        self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.LoadUpdateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code,"biz_id":biz_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                      
                      let business = response.object(forKey: "business")!
                        let contry1 = business as! NSDictionary
                     
                        
                  if  (contry1["more_info"] as? NSArray) != nil {
                            let more_infos = contry1["more_info"] as! NSArray
                    
                    for i in 0..<self.informationArray.count{
                      
                    for more_info in (more_infos as! [[String:Any]]){
                       
                       
                       
                                let contry2 = more_info as NSDictionary
                                if  (contry2["id"] as? NSInteger) != nil {
                                    let id = contry2["id"] as! NSInteger
                                  
                                    if String(id) == self.informationArray[i].info_id{
                                          let value = contry2["value"] as! String
                                       
                                        if value == "1"{
                                        
                                            self.informationArray[i].isChecked = true
                                               self.informationArray[i].value = String(1)
                                        }
                                    }
                                    
                                }
                    }
                    }
                        }
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                self.stopActivityIndicator()
                                   self.informationTable.reloadData()
                              
                                
                               
                                
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func LoadcreatebuisnessapiCalling(apikey:String,language_code:String,country_code:String){
        self.startactvityIndicator()
        self.informationArray.removeAll()
        let scriptUrl = buisnessapiClass.LoadCreateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                      
                        let informations = response.object(forKey: "informations")!
                      
                        for information in (informations as! [[String:Any]]){
                            let Infokey = informationprpertyclass()
                            let contry1 = information as NSDictionary
                            if  (contry1["info_id"] as? String) != nil {
                                let info_id = contry1["info_id"] as! String
                                Infokey.info_id = String(info_id)
                            }else{
                                let info_id = "nil"
                                Infokey.info_id = String(info_id)
                            }
                            
                            if  (contry1["info_title"] as? String) != nil {
                                let info_title = contry1["info_title"] as! String
                                Infokey.info_title = info_title
                            }else{
                                let info_title = "nil"
                                Infokey.info_title = info_title
                            }
                            Infokey.isChecked = false
                            Infokey.value = String(0)
                            self.informationArray.append(Infokey)
                        }
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                
                             
                                self.LoadupdatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code,biz_id:self.id)
                                self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  informationArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! amentiesTableViewCell
        cell.propertiesLabel.text = informationArray[indexPath.row].info_title
        let val = informationArray[indexPath.row].isChecked
        
        cell.checkboxOutlet.tag = indexPath.row
        cell.checkboxOutlet.addTarget(self, action:#selector(self.checkView(_:)), for:.touchUpInside)
        
        
        if(val == true){
            cell.checkboxOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
            cell.checkboxOutlet.tintColor = UIColor.red
            
        }else{
            
            cell.checkboxOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
            cell.checkboxOutlet.tintColor = UIColor.green
        }
        
        return cell
    }
    @objc func checkView(_ sender: UIButton){
        
        if informationArray[sender.tag].isChecked == false{
            informationArray[sender.tag].isChecked = true
            informationArray[sender.tag].value = String(1)
        }else{
            informationArray[sender.tag].isChecked = false
            informationArray[sender.tag].value = String(0)
        }
        self.informationTable.reloadData()
    }
    
    @IBAction func nextAction(_ sender: Any) {
        businessInfoArray.removeAll()
        valueInfoArray.removeAll()
        
        for i in 0..<informationArray.count{
            //if (informationArray[i].isChecked == true)
            //{
                businessInfoArray.append(informationArray[i].info_id)
              valueInfoArray.append(informationArray[i].value)
            ///}
        }
        
        var bigDic: NSMutableDictionary = NSMutableDictionary()
        var objectArray = NSMutableArray()
        var dic : NSMutableDictionary = NSMutableDictionary()
        for i in 0..<businessInfoArray.count
        {
            
            dic = NSMutableDictionary()
            dic.setValue(businessInfoArray[i], forKey: "id")
            dic.setValue(valueInfoArray[i], forKey: "value")
            objectArray.add(dic)
        }
        
        bigDic.setValue(apikey, forKey: "apikey")
        bigDic.setValue(id, forKey: "biz_id")
        bigDic.setValue(objectArray, forKey: "biz_infos")
       
        //Convert to Data
        
        let jsonData = try! JSONSerialization.data(withJSONObject: bigDic, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        // print(jsonData)
        
        //Convert back to string. Usually only do this for debugging
        
        if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
            
            //ethaann nink veenda string
            
            // json aayit kittum
            
          
            let scriptUrl = buisnessapiClass.PostUpdateBusinessInformations
            let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
            let myUrl = URL(string: urlWithParams);
            var request = URLRequest(url:myUrl!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request)
            {
                data, response, error in
                
                if error != nil
                {
                    print("error=\(error)")
                    DispatchQueue.main.async
                        {
                            print("server down")
                    }
                    return
                }
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
                do{
                    
                    if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                        
                    {
                        
                        self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                   
                        
                    }
                    DispatchQueue.main.async
                        {
                            
                            
                            if self.status == "success"{
                                self.stopActivityIndicator()
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "bsnsedit1") as! bsnsedit1ViewController
                                let navController = UINavigationController(rootViewController: vc)
                                self.present(navController, animated: true, completion: nil)
                                
                                //self.imageUploading(selectedImage:self.selectedImage)
                            }else{
                                self.stopActivityIndicator()
                            }
                            
                            
                    }//dispatch ends
                    
                }//do ends
                catch let error as NSError {
                    print(error.localizedDescription)
                    
                }
                
            }
            
            
            
           
            
            task.resume()
            
            
        }
        
        
        
        
    }
    

}
