//
//  bookingViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/28/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
class bookingViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITableViewDelegate,UITableViewDataSource {
  
    
@IBOutlet var runkeeperSwitch4: DGRunkeeperSwitch!
     @IBOutlet var fromtime: UITextField!
      @IBOutlet var totime: UITextField!
    
    @IBOutlet var mytable: UITableView!
    @IBOutlet var priceTextfield: UITextField!
    
    @IBOutlet var slotAction: UIButton!
    @IBOutlet var minheight: NSLayoutConstraint!
    @IBOutlet var slotheight: NSLayoutConstraint!
    
    @IBOutlet var tableheight: NSLayoutConstraint!
    @IBOutlet var maxheight: NSLayoutConstraint!
    @IBOutlet var slotTextfield: UITextField!
    
    @IBOutlet var titleview2: UIView!
    @IBOutlet var titleview1: UIView!
    @IBOutlet var minTextfield: UITextField!
    
    @IBOutlet var view1: UIView!
    @IBOutlet var maximumTextfield: UITextField!
    
    @IBOutlet var numberpicker: UIPickerView!
    @IBOutlet var view2: UIView!
    @IBOutlet var priceheight: NSLayoutConstraint!
    @IBOutlet var submitOutlet: UIButton!
    @IBOutlet var Tabletextfield: UITextField!
    
    
    @IBOutlet var from1Textfield: UITextField!
    
    @IBOutlet var toTextdfield: UITextField!
    
    @IBOutlet var addthisTableOutlet: UIButton!
    var pickerindex = Int()
    
    var pickerData: [String] = [String]()
    var cancelButton = UIBarButtonItem()
    var doneButton = UIBarButtonItem()
    var spaceButton = UIBarButtonItem()
    let toolBar = UIToolbar()
    var slotArray = [timeslotobject]()
    var tableArray = [tablebookingClass]()
            var buttonIndex = 0
    @IBOutlet var tableOutlet: UIButton!
    @IBOutlet var maximumOutlet: UIButton!
    @IBOutlet var minimumOutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
      mytable.isHidden = true
        titleview2.isHidden = true

        self.navigationItem.title = NSLocalizedString("EDIT BUSINESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        // Do any additional setup after loading the view.
        // Input data into the Array:
        self.numberpicker.delegate = self
        self.numberpicker.dataSource = self
        pickerData = ["1", "2", "3", "4", "5", "6","7","8","9","10"]
        numberpicker.backgroundColor = UIColor.lightGray
  
        doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(BusinesshourViewController.doneClick))
        spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        doneButton.tintColor = UIColor.green
        toolBar.isUserInteractionEnabled = true
        view2.isHidden = true
        runkeeperSwitch4.titles = [NSLocalizedString("TIME SLOT BOOKING", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), NSLocalizedString("TABLE BOOKING", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
           runkeeperSwitch4.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        runkeeperSwitch4.selectedBackgroundColor = .white
        runkeeperSwitch4.titleColor = .white
        runkeeperSwitch4.selectedTitleColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        runkeeperSwitch4.titleFont = UIFont(name: "HelveticaNeue-Light", size: 14.0)
        fromtime.setBorder()
        totime.setBorder()
        priceTextfield.setBorder()
        slotTextfield.setBorder()
        minTextfield.setBorder()
        maximumTextfield.setBorder()
         Tabletextfield.setBorder()
        submitOutlet.layer.cornerRadius = 10.0
         submitOutlet.backgroundColor =  UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        numberpicker.isHidden = true
        view1.backgroundColor = UIColor.white
        titleview1.backgroundColor = UIColor.white
        from1Textfield.setBorder()
            toTextdfield.setBorder()
        addthisTableOutlet.layer.cornerRadius = 10.0
          addthisTableOutlet.backgroundColor =  UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        view2.backgroundColor = UIColor.white
         titleview2.backgroundColor = UIColor.white
      toolBar.backgroundColor = UIColor.lightGray
        mytable.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func slotActionbutton(_ sender: Any) {
        pickerindex = 0
            numberpicker.isHidden = false
        
        
       
        
    }
    @IBAction func switchAction(_ sender: Any) {
        if runkeeperSwitch4.selectedIndex == 0{
            buttonIndex = 0
            view1.backgroundColor = UIColor.white
            view1.isHidden = false
             view2.isHidden = true
             titleview2.isHidden = true
            mytable.isHidden = true
        }else{
         
            buttonIndex = 1
             let screenSize: CGRect = UIScreen.main.bounds
             view2.isHidden = false
              titleview2.isHidden = false
               mytable.isHidden = true
            
    //        view2 = UIView(frame: CGRect(x:0, y:10,width: view1.frame.width , height: 197))
            // view1.isHidden = true
            //view1.addSubview(view2)
            
        }
        
    }
    
    @IBAction func TableAction(_ sender: Any) {
         pickerindex = 1
            numberpicker.isHidden = false
        
    }
    @IBAction func maximumAction(_ sender: Any) {
         pickerindex = 2
            numberpicker.isHidden = false
    }
    @IBAction func minimumAction(_ sender: Any) {
         pickerindex = 3
            numberpicker.isHidden = false
    }
    @IBAction func addthisAction(_ sender: Any) {
        let slotclass = tablebookingClass()
        var Availability = ""
        Availability = from1Textfield.text! + "-" + toTextdfield.text!
        var Seats = ""
        Seats = minimumOutlet.currentTitle! + "-" + maximumOutlet.currentTitle!
        slotclass.Availability = Availability
        slotclass.Seats  = Seats
        slotclass.Tables = tableOutlet.currentTitle!
        tableArray.append(slotclass)
        self.mytable.reloadData()
        mytable.isHidden = false
        
    }
    
    @IBAction func submitAction(_ sender: Any) {
        let slotclass = timeslotobject()
        slotclass.fromtime = fromtime.text!
        slotclass.totime = totime.text!
        slotclass.price = priceTextfield.text!
        slotclass.slots = slotAction.currentTitle!
        slotArray.append(slotclass)
        self.mytable.reloadData()
        mytable.isHidden = false
     
     
       
    }
    @IBAction func fromtimeAction(_ sender: Any) {
   
   
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        fromtime.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerfromValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(bookingViewController.cancelfromdateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        fromtime.inputAccessoryView = toolBar
        
    }
  
    @objc func cancelfromdateClick() {
        fromtime.text = ""
        fromtime.resignFirstResponder()
        
    }
    @objc func datePickerfromValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "HH:mm a"
        
        // Apply date format
        fromtime.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    
    
    @objc func doneClick() {
        
        fromtime.resignFirstResponder()
        totime.resignFirstResponder()
        from1Textfield.resignFirstResponder()
          toTextdfield.resignFirstResponder()
       
    }
    
  
    
    @IBAction func toTimeAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        totime.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickertoValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title:NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(bookingViewController.canceltodateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        totime.inputAccessoryView = toolBar
        
    }
    
    @objc func canceltodateClick() {
        totime.text = ""
        totime.resignFirstResponder()
        
    }
    @objc func datePickertoValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "HH:mm a"
        
        // Apply date format
        totime.text = dateFormatter.string(from: sender.date)
        
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return pickerData.count
    }
   
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerindex == 0{
        slotAction.setTitle(pickerData[row], for: UIControlState())
        }else if pickerindex == 1{
            Tabletextfield.placeholder = ""
              tableOutlet.setTitle(pickerData[row], for: UIControlState())
        }else if pickerindex == 2{
               maximumTextfield.placeholder = ""
            maximumOutlet.setTitle(pickerData[row], for: UIControlState())
        }else {
                 minTextfield.placeholder = ""
              minimumOutlet.setTitle(pickerData[row], for: UIControlState())
        }
        numberpicker.isHidden = true
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if buttonIndex == 0{
        return  slotArray.count
    }
       return  tableArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if buttonIndex == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! timeslotTableViewCell
               cell.backgroundColor = UIColor.clear
      cell.fromtimelabel.text = slotArray[indexPath.row].fromtime
          cell.totimelabel.text = slotArray[indexPath.row].totime
          cell.pricelabel.text = slotArray[indexPath.row].price
          cell.slotlabel.text = slotArray[indexPath.row].slots
        cell.deleteimage.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.trash, textColor: UIColor.green, size: CGSize(width: 45, height: 45)), for: UIControlState.normal)
        cell.deleteimage.tintColor = UIColor.red
            cell.deleteimage.tag = indexPath.row;
            cell.deleteimage.addTarget(self, action:#selector(self.buttondelView(_:)), for:.touchUpInside)
               return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "secondcell", for: indexPath) as! tablebookingTableViewCell
               cell.backgroundColor = UIColor.clear
            cell.availabilitylabel.text = tableArray[indexPath.row].Availability
            cell.slotlabel.text = tableArray[indexPath.row].Seats
            cell.tablelabel.text = tableArray[indexPath.row].Tables
            cell.deleteOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.trash, textColor: UIColor.green, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
            cell.deleteOutlet.tintColor = UIColor.red
            cell.deleteOutlet.tag = indexPath.row;
            cell.deleteOutlet.addTarget(self, action:#selector(self.buttondeltableView(_:)), for:.touchUpInside)
               return cell
        }
        
     
    }
    @objc func buttondelView(_ sender: UIButton)
    {
        slotArray.remove(at: sender.tag)
        mytable.reloadData()
    }
    @objc func buttondeltableView(_ sender: UIButton)
    {
         tableArray.remove(at: sender.tag)
          mytable.reloadData()
    }
    @IBAction func fromtime2Action(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        from1Textfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerfrom2ValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title:NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(bookingViewController.cancelfrom2dateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        from1Textfield.inputAccessoryView = toolBar
        
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    
    @objc func cancelfrom2dateClick() {
        from1Textfield.text = ""
        from1Textfield.resignFirstResponder()
        
    }
    @objc func datePickerfrom2ValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "HH:mm a"
        
        // Apply date format
        from1Textfield.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    
    
    @IBAction func totime2Action(_ sender: Any) {
          let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        toTextdfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerto2ValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(bookingViewController.cancelto2dateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toTextdfield.inputAccessoryView = toolBar
        
    }
    
    @objc func cancelto2dateClick() {
        toTextdfield.text = ""
        toTextdfield.resignFirstResponder()
        
    }
    @objc func datePickerto2ValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "HH:mm a"
        
        // Apply date format
        toTextdfield.text = dateFormatter.string(from: sender.date)
        
        
    }
    
}
extension UITextField {
    func setBorder() {
        self.borderStyle = .none
        self.backgroundColor = UIColor.clear
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 5.0
        let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        
       
       
    }
}
