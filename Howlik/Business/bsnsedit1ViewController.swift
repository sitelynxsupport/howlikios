//
//  bsnsedit1ViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/26/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import FontAwesome_swift
import Foundation
import SDWebImage
import XLActionController
import GoogleMaps
class bsnsedit1ViewController: UIViewController,NVActivityIndicatorViewable,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet var catagoryviewLabel: UILabel!
    
    @IBOutlet var amentiesviewLabel: UILabel!
    @IBOutlet var discountsandOffersviewLabel: UILabel!
    @IBOutlet var giftcertifcateviewLabel: UILabel!
    @IBOutlet var bokingviewLabel: UILabel!
   
    @IBOutlet var mobileviewLabel: UILabel!
    @IBOutlet var websiteviewLabel: UILabel!
    @IBOutlet var adreesviewLabel: UILabel!
    @IBOutlet var keywordviewlabel: UILabel!
    @IBOutlet var businessinfoviewLabel: UILabel!
    @IBOutlet var discountbutton: UIButton!
    
    @IBOutlet var amentiesbutton: UIButton!
    @IBOutlet var mapview: GMSMapView!
    @IBOutlet var giftcertificateOutlet: UISwitch!
    
    @IBOutlet var bookingOutlet: UISwitch!
    @IBOutlet var mycollection: UICollectionView!
    @IBOutlet var bsnsimage: UIImageView!
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var mobileLabel: UILabel!
    
    @IBOutlet var keywordLabel: UILabel!
    @IBOutlet var catogoryLabel: UILabel!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    
    var status = ""
    var id = ""
    var label = UILabel()
    var statusvalue  = Int()
    var pendingFlag = Bool()
var language_code = ""
var country_code = ""
var apikey = ""
    var address1 = ""
     var address2 = ""
    var category_id = ""
    var phone = ""
    var email = ""
    var titles = ""
    var catagoryname = ""
      var Keywordname = ""
    var swiftArray = [String]()
    var scrollindex = 0
    private var index: Int = 0
    var dataSource = [String]()
    var descriptions = ""
    var lat = ""
     var lon = ""

let buisnessapiClass = ApiListViewcontroller()
    override func viewDidLoad() {
        super.viewDidLoad()
         if UserDefaults.standard.string(forKey: "bookingStatus") != nil{
        if UserDefaults.standard.string(forKey: "bookingStatus")! == "true"{
                bookingOutlet.isOn = true
        }else{
             bookingOutlet.isOn = false
        }
         }else{
              UserDefaults.standard.set("false", forKey: "bookingStatus")
        }
       
        self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        self.country_code = UserDefaults.standard.string(forKey: "country_code")!
        self.language_code = UserDefaults.standard.string(forKey: "language_code")!
        id = UserDefaults.standard.string(forKey: "biz_id")!;
        self.swiftArray.removeAll()
        self.dataSource.removeAll()
        LoadupdatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code,biz_id:id)
       
        // Do any additional setup after loading the view.
        self.navigationItem.title =  NSLocalizedString("EDIT BUSINESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       discountbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.plus, textColor: UIColor.red, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
        discountbutton.tintColor = UIColor.red
        amentiesbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.plus, textColor: UIColor.red, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
        amentiesbutton.tintColor = UIColor.red
        SetBackBarButtonCustom()
      languageSet()
      //  dataSource = ["city","no-image02","unnamed"]
        
       
    }
   
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
   
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func languageSet(){
        businessinfoviewLabel.text = NSLocalizedString("BusinessInfo", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           catagoryviewLabel.text = NSLocalizedString("Category", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           keywordviewlabel.text = NSLocalizedString("Keywords", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
               adreesviewLabel.text = NSLocalizedString("Address", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            mobileviewLabel.text = NSLocalizedString("Mobile", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           websiteviewLabel.text = NSLocalizedString("Website", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           bokingviewLabel.text = NSLocalizedString("Bookings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         giftcertifcateviewLabel.text = NSLocalizedString("GiftCertificate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           amentiesviewLabel.text = NSLocalizedString("Amenities", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          discountsandOffersviewLabel.text = NSLocalizedString("Discounts and Offers", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func LoadupdatebuisnessapiCalling(apikey:String,language_code:String,country_code:String,biz_id:String){
        self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.LoadUpdateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code,"biz_id":biz_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                     
                        let business = response.object(forKey: "business")!
                        let contry1 = business as! NSDictionary
                      
                        if  (contry1["category_id"] as? String) != nil {
                            self.category_id = contry1["category_id"] as! String
                           
                        }
                        if  (contry1["keywords"] as? NSArray) != nil {
                      let keywords = contry1["keywords"] as! NSArray
                           
                            self.swiftArray = keywords as NSArray as! [String]
                        
                        }
                      //
                       
                        
                        if  (contry1["address1"] as? String) != nil {
                            self.address1 = contry1["address1"] as! String
                            
                        }
                        if  (contry1["address2"] as? String) != nil {
                            self.address2 = contry1["address2"] as! String
                            
                        }
                        if  (contry1["description"] as? String) != nil {
                            self.descriptions = contry1["description"] as! String
                            
                        }
                       
                        if  (contry1["phone"] as? String) != nil {
                            self.phone = contry1["phone"] as! String
                            
                        }
                        if  (contry1["lat"] as? String) != nil {
                            self.lat = contry1["lat"] as! String
                            
                        }
                        if  (contry1["lon"] as? String) != nil {
                            self.lon = contry1["lon"] as! String
                            
                        }
                        if  (contry1["title"] as? String) != nil {
                            self.titles = contry1["title"] as! String
                            
                        }
                        if  (contry1["biz_email"] as? String) != nil {
                            self.email = contry1["web"] as! String
                            
                        }
                        
                        let keywords = response.object(forKey: "keywords")!
                        
                        for keyword in (keywords as! [[String:Any]]){
                              let contry2 = keyword as NSDictionary
                             if  (contry2["id"] as? String) != nil {
                                  let id = contry2["id"] as! String
                              
                                
                        
                            for i in self.swiftArray{
                                
                                if id == i{
                                
                                    if  (contry2["name"] as? String) != nil{
                                      let name = contry2["name"] as! String
                                    
                                    self.Keywordname = name + "," + self.Keywordname
                                    }
                                }
                            }
                            }
                        }
                        
                        let categories = response.object(forKey: "categories")!
                        
                        for categorie in (categories as! [[String:Any]]){
                            let contry3 = categorie as NSDictionary
                               if  (contry3["code"] as? String) != nil {
                            let code = contry3["code"] as! String
                         
                            if code == self.category_id{
                                    if  (contry3["name"] as? String) != nil {
                                self.self.catagoryname = contry3["name"] as! String
                                }
                                
                               
                            }
                            }
                        }
                        if  (contry1["biz_images"] as? NSArray) != nil {
                            let biz_images = contry1["biz_images"] as! NSArray
                            for biz_image in (biz_images as! [[String:Any]]){
                                let contry4 = biz_image as NSDictionary
                                if  (contry4["filename"] as? String) != nil {
                                    let filename = contry4["filename"] as! String
                                    let image =  "http://www.howlik.com/"  +  filename
                                    self.dataSource.append(image)
                                    if  (contry4["id"] as? String) != nil {
                                        let id = contry4["id"] as! String
                                    }
                                }
                          
                        }
                       
                        
                        
                            
                            
                        }
                       
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                self.stopActivityIndicator()
                         self.catogoryLabel.text  = self.catagoryname
                                self.keywordLabel.text  = self.Keywordname
                                self.addressLabel.text = self.address1
                                     self.mobileLabel.text = self.phone
                                  self.emailLabel.text = self.email
                                self.titleLabel.text = self.titles
                                
                              
                                UIView.animate(withDuration: 2.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                                    
                                    Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
                                    self.mycollection.reloadData()
                                    
                                }, completion: nil)
                                self.loadmap()
                                
                            /*    UIView.animate(withDuration: 10.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                                    
                                    for i in 0..<self.self.dataSource.count{
                                        //self.self.bsnsimage.sd_setImage(with: URL(string:self.dataSource[i]), placeholderImage:
                                          //  UIImage(named: "no-image02"))
                                        self.bsnsimage.image = UIImage(named:self.dataSource[i])
                                    }
                                    
                                }, completion: nil)*/
//                                Timer.scheduledTimer(withTimeInterval: 10, repeats: true){_ in
//                                   for i in 0..<self.self.dataSource.count{
//                                    self.self.bsnsimage.sd_setImage(with: URL(string:self.dataSource[i]), placeholderImage:
//                                          UIImage(named: "user"))
//                                }
//                                    
//                                }
                                
                                
                             
                                
                               

                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func loadmap(){
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2DMake((lat as NSString).doubleValue,(lon as NSString).doubleValue)
        
        ///View for Marker
        let DynamicView = UIView(frame: CGRect(x:0, y:0, width:50, height:50))
        DynamicView.backgroundColor = UIColor.clear
        
        //Pin image view for Custom Marker
        let imageView = UIImageView()
        imageView.frame  = CGRect(x:0, y:0, width:50, height:20)
        // imageView.image = UIImage(named:"LocationPin")
        imageView.image = (UIImage.fontAwesomeIcon(name:  FontAwesome.mapMarker, textColor: UIColor.red,size: CGSize(width: 50, height: 20)))
        
        //Adding pin image to view for Custom Marker
        DynamicView.addSubview(imageView)
        
        UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        marker.icon = imageConverted
        marker.map = mapview
        
mapview.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 11)
    }
    @objc func scrollAutomatically(_ timer: Timer) {
        if  dataSource.count != 0{
        if scrollindex < dataSource.count{
            
            mycollection.scrollToItem(at: IndexPath(item: scrollindex, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
             scrollindex += 1
            
         //   mycollection.reloadData()
           
        }else{
         
            scrollindex = 0
              mycollection.scrollToItem(at: IndexPath(item: scrollindex, section: 0), at: UICollectionViewScrollPosition.left, animated: false)
            
        }
        }
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! bsnseditCollectionViewCell
      
 cell.bsnsImage.sd_setImage(with: URL(string: dataSource[indexPath.row]), placeholderImage:
           UIImage(named: "no-image02"))
      
        
        return cell
        
    }
    
    
    
    
    @IBAction func bookingAction(_ sender: Any) {
        if bookingOutlet.isOn == true{
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "booking") as! bookingViewController
//            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
//            self.present(navController, animated: true, completion: nil)
            UserDefaults.standard.set("true", forKey: "bookingStatus")
            statusvalue = 1
            bookingEnableApiCalling(apikey:apikey,biz_id:id,status:"1")
        }else{
              UserDefaults.standard.set("false", forKey: "bookingStatus")
            statusvalue = 0
             bookingEnableApiCalling(apikey:apikey,biz_id:id,status:"0")
        }
    }
   
    func bookingEnableApiCalling(apikey:String,biz_id:String,status:String){
        let scriptUrl = buisnessapiClass.EnableDisableBusinessBooking
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)&status=\(status)"
      
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                   
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                    
            
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.pendingFlag == true{
                        if (self.status == "success") {
                            if self.statusvalue == 1 {
                            self.createAlertbox(text : NSLocalizedString("Booking Enabled", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                
                          
                            }else{
                                self.createAlertbox(text : NSLocalizedString("Booking Disabled", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                              
                       }
                            
                        }else{
                           
                            self.createAlertbox(text : NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }
                        }
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    func createAlertbox(text : String){
        let h = UIScreen.main.bounds.height
        label = UILabel.init(frame: CGRect(x: 10, y: h / 2, width: view.frame.width - 20, height: 50))
        view.addSubview(label)
        label.layer.cornerRadius = 10.0
        label.layer.masksToBounds = true
        //label.center = view.center
       // label.center.y = view.center.y - 70
        label.text = text
        label.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
         label.textColor = UIColor.white
        label.textAlignment = .center
        UIView.animate(withDuration: 0.5, delay: 2.0, options: [], animations: {
            
            self.label.alpha = 0.0
            
        }) { (finished: Bool) in
            
            self.label.isHidden = true
        }
    }
    @IBAction func editAction(_ sender: Any) {
       self.ActionSheets()
        
    }
    func ActionSheets(){
        let actionSheet = TwitterActionController()
        // set up a header title
        actionSheet.headerData = NSLocalizedString("What would you like to edit?", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        // Add some actions, note that the first parameter of `Action` initializer is `ActionData`.
        actionSheet.addAction(Action(ActionData(title:  NSLocalizedString("Basic Info", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subtitle: "", image: UIImage(named: "information")!), style: .default, handler: { action in
            // do something useful
            UserDefaults.standard.set(self.swiftArray, forKey: "swiftArray")
            UserDefaults.standard.set(self.dataSource, forKey: "dataSource")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "editview") as! editviewViewController
          vc.id = self.id
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(Action(ActionData(title: NSLocalizedString("Timing", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subtitle: "", image: UIImage(named: "timing")!), style: .default, handler: { action in
            // do something useful
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "bsnshouredit") as! businesshoureditViewController
            vc.id = self.id
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(Action(ActionData(title:  NSLocalizedString("Address", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subtitle: "", image: UIImage(named: "address")!), style: .default, handler: { action in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "addresseditfinish") as! AddressfinaleditViewController
            vc.id = self.id
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        }))
        actionSheet.addAction(Action(ActionData(title: NSLocalizedString("MoreInfo", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subtitle: "", image: UIImage(named: "more_informations")!), style: .default, handler: { action in
            // do something useful
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "amentiesedit") as! amentieseditViewController
            vc.id = self.id
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        }))
        // present actionSheet like any other view controller
        present(actionSheet, animated: true, completion: nil)
    }

    
    @IBAction func giftcertificateAction(_ sender: Any) {
    }
    
    @IBAction func discountAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "discounts") as! discountsViewController
        vc.id = self.id
        let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
        self.present(navController, animated: true, completion: nil)
        
    }
    
}
