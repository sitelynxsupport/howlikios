//
//  buisnessCollectionViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/13/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class buisnessCollectionViewCell: UICollectionViewCell {
    @IBOutlet var bookingCount: UIButton!
    
    @IBOutlet var bookingviewAction: UIButton!
    @IBOutlet var locationImage: UIImageView!
    @IBOutlet var seenImage: UIImageView!
    @IBOutlet var bsnsView: UIView!
    @IBOutlet var moreButton: UIButton!
    @IBOutlet var bookingLabel: UILabel!
    @IBOutlet var bsnsImage: UIImageView!
    @IBOutlet var bsnsnameLabel: UILabel!
    @IBOutlet var seenLabel: UILabel!
    
    @IBOutlet var datelabel: UILabel!
    @IBOutlet var statelabel: UILabel!
}
