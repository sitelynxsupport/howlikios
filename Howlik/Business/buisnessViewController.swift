//
//  buisnessViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/13/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import FontAwesome_swift
import NVActivityIndicatorView
import WSTagsField

class buisnessViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable {
    
    
    @IBOutlet var floatingButton: UIButton!
    let buisnessapiClass = ApiListViewcontroller()
    var city_code = ""
    var country_code = ""
    var language_code = ""
    var apikey = ""
   var value = 0
    var id = ""
    var buttonindex = Int()
    var status = ""
    fileprivate let tagfield  = WSTagsField()
    var pendingFlag = false
    var customview = UIView()
    @IBOutlet var mycollectionView: UICollectionView!
    var buisnessElementArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        floatingButton.layer.shadowColor = UIColor.black.cgColor
        floatingButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        floatingButton.layer.masksToBounds = false
        floatingButton.layer.shadowRadius = 1.0
        floatingButton.layer.shadowOpacity = 0.5
        floatingButton.layer.cornerRadius = floatingButton.frame.width / 2
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
              self.country_code = UserDefaults.standard.string(forKey: "country_code")!
               self.language_code = UserDefaults.standard.string(forKey: "language_code")!
            LoadbuisnessapiCalling(apikey:apikey,city_code:city_code,country_code:country_code)
        }else{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
               // action on click
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
           })
            alert.showError(NSLocalizedString("ERROR", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("PleaseRegistertoviewBusinesses", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
       
 
            }
        floatingButton.isHidden = false

        }
   
        // Do any additional setup after loading the view.
      
        
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
      
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 30, height: 30)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tabBarController?.tabBar.items?[3].title = NSLocalizedString("Business", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }
    func LoadbuisnessapiCalling(apikey:String,city_code:String,country_code:String){
     
        buisnessElementArray.removeAllObjects()
             let scriptUrl = buisnessapiClass.OwnBusinessDetails
        //let myUrl = URL(string: scriptUrl);
          let parameters: Parameters = ["apikey": apikey,"city_code": city_code,"country_code":country_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        //  if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: retrivedResult, options: []) as? [String:Any]{
                     
                         if (response["business"]) != nil {
                        let business = response.object(forKey: "business")!
                        if (business as AnyObject).count == 0 {
                              self.createeventNothingview()
                        }else{
                          self.mycollectionView.isHidden = false
                        for busines in (business as! [[String:Any]]){
                            let mybusiness = buisnessClass()
                            let contry1 = busines as NSDictionary
                            if  (contry1["biz_booking"] as? NSInteger) != nil {
                                let biz_booking = contry1["biz_booking"] as! NSInteger
                                mybusiness.biz_booking = String(biz_booking)
                            }else{
                                let biz_booking = "nil"
                                mybusiness.biz_booking = biz_booking
                            }
                            
                            if  (contry1["biz_date"] as? String) != nil {
                                let biz_date = contry1["biz_date"] as! String
                                
                             
                                mybusiness.biz_date = biz_date
                            }else{
                                let biz_date = "nil"
                                mybusiness.biz_date = biz_date
                            }
                            if  (contry1["biz_id"] as? String) != nil {
                                let biz_id = contry1["biz_id"] as! String
                                mybusiness.biz_id = biz_id
                            }
                            else{
                                let biz_id = "nil"
                                 mybusiness.biz_id = biz_id
                            }
                            
                            if  (contry1["biz_title"] as? String) != nil {
                                let biz_title = contry1["biz_title"] as! String
                                mybusiness.biz_title = biz_title
                            }
                            else{
                                let biz_title = "nil"
                                mybusiness.biz_title = biz_title
                            }
                            if  (contry1["city_title"] as? String) != nil {
                                let city_title = contry1["city_title"] as! String
                                mybusiness.city_title = city_title
                            }
                            else{
                                let city_title = "nil"
                                mybusiness.city_title = city_title
                            }
                            if  (contry1["location_title"] as? String) != nil {
                                let location_title = contry1["location_title"] as! String
                                mybusiness.location_title = location_title
                            }
                            else{
                                let location_title = "nil"
                                mybusiness.location_title = location_title
                            }
                            if  (contry1["biz_image"] as? String) != nil {
                                let biz_image = contry1["biz_image"] as! String
                                mybusiness.biz_image = "http://www.howlik.com/" +  biz_image
                            }
                            if  (contry1["biz_visits"] as? String) != nil {
                                let biz_visits = contry1["biz_visits"] as! String
                                mybusiness.biz_visits = String(biz_visits)
                            }
                            else{
                                let biz_visits = "nil"
                                mybusiness.biz_visits = biz_visits
                            }
                            
                            self.buisnessElementArray.add(mybusiness)
                            
                        }
                      }
                       }
                        
                        // self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {

                                self.mycollectionView.reloadData()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    
    func createeventNothingview(){
        let screenSize: CGRect = UIScreen.main.bounds
        customview  = UIView(frame: CGRect(x: 0, y: 140, width: Int(screenSize.width) , height: Int(screenSize.height) - 400 ))
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:140,width: customview.frame.width , height: 100));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 100));
       
        let defaults = UserDefaults.standard
           if defaults.string(forKey: "userType") == "3" {
            label.text =  NSLocalizedString("BussinesNathing", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           }else{
             label.text =  NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        }
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 5
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
         view1.addSubview(label)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 7
        layout.minimumLineSpacing = 7
     mycollectionView!.collectionViewLayout = layout
        
    }
    func pendinbuisnessapiCalling(apikey:String,language_code:String){
        // buisnessElementArray.removeAllObjects()
        self.startactvityIndicator()
            buisnessElementArray.removeAllObjects()
        let scriptUrl = buisnessapiClass.PendingBusinessList
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"language_code":language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        //  if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: retrivedResult, options: []) as? [String:Any]{
                     
                          if (response["business"]) != nil {
                        let business = response.object(forKey: "business")!
                        if (business as AnyObject).count == 0 {
                            self.createeventNothingview()
                        }else{
                        for busines in (business as! [[String:Any]]){
                            let mybusiness = buisnessClass()
                            let contry1 = busines as NSDictionary
                            if  (contry1["biz_booking"] as? NSInteger) != nil {
                                let biz_booking = contry1["biz_booking"] as! NSInteger
                                mybusiness.biz_booking = String(biz_booking)
                            }else{
                                let biz_booking = "nil"
                                mybusiness.biz_booking = biz_booking
                            }
                            
                            if  (contry1["biz_date"] as? String) != nil {
                                let biz_date = contry1["biz_date"] as! String
                                
                                
                                mybusiness.biz_date = biz_date
                            }else{
                                let biz_date = "nil"
                                mybusiness.biz_date = biz_date
                            }
                            if  (contry1["biz_id"] as? String) != nil {
                                let biz_id = contry1["biz_id"] as! String
                                mybusiness.biz_id = biz_id
                            }
                            else{
                                let biz_id = "nil"
                                mybusiness.biz_id = biz_id
                            }
                            
                            if  (contry1["biz_title"] as? String) != nil {
                                let biz_title = contry1["biz_title"] as! String
                                mybusiness.biz_title = biz_title
                            }
                            else{
                                let biz_title = "nil"
                                mybusiness.biz_title = biz_title
                            }
                            if  (contry1["city_title"] as? String) != nil {
                                let city_title = contry1["city_title"] as! String
                                mybusiness.city_title = city_title
                            }
                            else{
                                let city_title = "nil"
                                mybusiness.city_title = city_title
                            }
                            if  (contry1["location_title"] as? String) != nil {
                                let location_title = contry1["location_title"] as! String
                                mybusiness.location_title = location_title
                            }
                            else{
                                let location_title = "nil"
                                mybusiness.location_title = location_title
                            }
                            if  (contry1["biz_image"] as? String) != nil {
                                let biz_image = contry1["biz_image"] as! String
                                mybusiness.biz_image = "http://www.howlik.com/" +  biz_image
                            }
                            if  (contry1["biz_visits"] as? String) != nil {
                                let biz_visits = contry1["biz_visits"] as! String
                                mybusiness.biz_visits = String(biz_visits)
                            }
                            else{
                                let biz_visits = "nil"
                                mybusiness.biz_visits = biz_visits
                            }
                            
                            self.buisnessElementArray.add(mybusiness)
                            
                        }
                   }
                        }
                        
                        // self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                  self.mycollectionView.reloadData()
                                self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
      
        return 2
      
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (section == 0) {
            return 1
  }
      return buisnessElementArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 if indexPath.section ==  0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! busnsviewCollectionViewCell
     UIApplication.shared.statusBarStyle = .lightContent
    
    cell.bsnsimgview.image = UIImage(named:"business_cover")
   
    cell.runkeeperSwitch4.tag = indexPath.row;
    
    cell.runkeeperSwitch4.addTarget(self, action:#selector(self.buttonView(_:)), for:.valueChanged)
   

            
            return cell
       }else{
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mycell", for: indexPath) as! buisnessCollectionViewCell
        let myElement:buisnessClass = self.buisnessElementArray.object(at: indexPath.row) as! buisnessClass
    if value == 0{
        cell.bookingLabel.text =  NSLocalizedString("Bookings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            cell.bookingCount.setTitle(myElement.biz_booking, for: UIControlState())
        cell.bookingCount.layer.cornerRadius =    cell.bookingCount.frame.width / 2
        cell.bookingCount.layer.borderWidth = 1.0
        cell.bookingCount.layer.borderColor = UIColor.black.cgColor
        cell.bookingCount.layer.masksToBounds = true
        }else{
        cell.bookingCount.setTitle("", for: UIControlState())
        cell.bookingLabel.text = ""
        cell.bookingCount.layer.cornerRadius =   cell.bookingCount.frame.width / 2
        cell.bookingCount.layer.borderWidth = 1.0
        cell.bookingCount.layer.borderColor = UIColor.clear.cgColor
        cell.bookingCount.layer.masksToBounds = true
    }
            cell.bsnsImage.sd_setImage(with: URL(string:myElement.biz_image), placeholderImage:
                    UIImage(named: "no-image02"))
            cell.bsnsnameLabel.text = myElement.biz_title
            cell.seenLabel.text = myElement.biz_visits
            cell.statelabel.text = myElement.location_title
            cell.datelabel.text = myElement.biz_date
            cell.seenImage.image = UIImage.fontAwesomeIcon(name: FontAwesome.eye, textColor: UIColor.black, size: CGSize(width: 22, height: 22))
            cell.locationImage.image = UIImage.fontAwesomeIcon(name: FontAwesome.mapMarker, textColor: UIColor.black, size: CGSize(width: 22, height: 22))
        
            cell.moreButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.ellipsisH, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
    cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
     cell.bookingviewAction.tag = indexPath.row
    cell.bookingviewAction.addTarget(self, action:#selector(self.bookView(_:)), for:.touchUpInside)
       
        cell.bsnsImage.sd_setImage(with: URL(string:myElement.biz_image), placeholderImage:
            UIImage(named: "no-image02"))
        cell.bsnsnameLabel.text = myElement.biz_title
        cell.seenLabel.text = myElement.biz_visits
        cell.statelabel.text = myElement.location_title
        cell.datelabel.text = myElement.biz_date
        cell.seenImage.image = UIImage.fontAwesomeIcon(name: FontAwesome.eye, textColor: UIColor.black, size: CGSize(width: 22, height: 22))
        cell.locationImage.image = UIImage.fontAwesomeIcon(name: FontAwesome.mapMarker, textColor: UIColor.black, size: CGSize(width: 22, height: 22))
        cell.moreButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.ellipsisH, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
        cell.moreButton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
   

            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section ==  0 {
            //let width = (self.view.frame.size.width)
            //let height = 30
          return CGSize(width: view.frame.size.width, height: 140);
        
        }else{
            
            let width = (self.view.frame.size.width - 12 * 2.8) / 2//some width
           
            let height = (width * 3.6 / 2.65  )//ratio
         return CGSize(width: width, height: height);
      }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         floatingButton.isHidden = false
//        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
//        {
//            print("up")
//            floatingButton.isHidden = false
//        }
//        else
//        {
//            floatingButton.isHidden = true
//            print("down")
//        }
    }
    @objc func buttonView(_ sender: DGRunkeeperSwitch)
    {
          print("valueChanged: \(sender.selectedIndex)")
        if sender.selectedIndex == 0{
             value = 0
            self.mycollectionView.isHidden = false
            self.customview.isHidden = true
         pendingFlag = true
             LoadbuisnessapiCalling(apikey:apikey,city_code:city_code,country_code:country_code)
        }else{
            value = 1
            self.mycollectionView.isHidden = false
            self.customview.isHidden = true
            pendingFlag = false
            pendinbuisnessapiCalling(apikey:apikey,language_code:language_code)
            
        }
    }
    @objc func bookView(_ sender: UIButton)
    {
          let myElement:buisnessClass = self.buisnessElementArray.object(at: sender.tag) as! buisnessClass
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "businessbooking") as! businessbookingviewViewController
        vc.biz_id = myElement.biz_id
        let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
        self.present(navController, animated: true, completion: nil)
    }
    @objc func moreView(_ sender: UIButton)
    {
       let myElement:buisnessClass = self.buisnessElementArray.object(at: sender.tag) as! buisnessClass
        id = myElement.biz_id
        buttonindex = sender.tag
        ActionSheet()
    }
    func ActionSheet(){
        let alertController = UIAlertController(title: "Action Sheet", message: NSLocalizedString("Whatwouldyouliketodo", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: .actionSheet)

        let editButton = UIAlertAction(title:   NSLocalizedString("Edit", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (action) -> Void in
           
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "bsnsedit1") as! bsnsedit1ViewController
            vc.id = self.id
            vc.pendingFlag = self.pendingFlag
                    UserDefaults.standard.set(self.id, forKey: "biz_id")
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        })
        
        let  deleteButton = UIAlertAction(title: NSLocalizedString("Deleteforever", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .destructive, handler: { (action) -> Void in
          
         
            let id  = self.id
         
            self.DeleteapiCalling(apikey:self.apikey,biz_id:id)
            
            self.mycollectionView.reloadData()
            
        })


        let cancelButton = UIAlertAction(title:  NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel, handler: { (action) -> Void in
         
        })


        alertController.addAction(editButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)

        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
    func DeleteapiCalling(apikey:String,biz_id:String){
        self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.DeleteBusiness
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)"
        
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
           
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                  
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
             
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if (self.status == "success") {
                            self.stopActivityIndicator()
                               self.buisnessElementArray.removeObject(at: self.buttonindex)
                            self.mycollectionView.reloadData()
                           
                        }else{
                            self.stopActivityIndicator()
                        }
                        
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    @IBAction func businessAddAction(_ sender: Any) {
  
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "userType") != "2" {
            let alert = UIAlertController(title: "Howlik", message: NSLocalizedString("BussinesNathing", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }
                return
            }))
            self.present(alert, animated: true, completion: nil)
        }
     
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "bsnsAdd") as! BusnsAddViewController
        let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
        self.present(navController, animated: true, completion: nil)
    }
    
    
    
}
