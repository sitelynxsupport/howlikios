//
//  businessHoursHeadingCell.swift
//  Howlik
//
//  Created by phenomtec on 02/05/19.
//  Copyright © 2019 Shrishti Informatics. All rights reserved.
//

import UIKit

class businessHoursHeadingCell: UITableViewCell {

    @IBOutlet weak var background: UIView!
    @IBOutlet var workinghourlabel: UILabel!
    @IBOutlet var businesshourlabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
