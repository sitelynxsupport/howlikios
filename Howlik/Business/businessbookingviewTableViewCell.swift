//
//  businessbookingviewTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class businessbookingviewTableViewCell: UITableViewCell {
    @IBOutlet var bookingtypelabel: UILabel!
    @IBOutlet var namelabel: UILabel!
    @IBOutlet var timelabel: UILabel!
    @IBOutlet var datelabel: UILabel!
    
    @IBOutlet var statusOutlet: UIButton!
    @IBOutlet var smallview: UIView!
    @IBOutlet var approvedlabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
