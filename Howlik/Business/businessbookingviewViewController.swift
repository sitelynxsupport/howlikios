//
//  businessbookingviewViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift

class businessbookingviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var bookingtable: UITableView!
    
var apikey = ""
var biz_id = ""
var bookingArray = [businessbookingviewclass]()
var booking_type  = ""
    var book_date  = ""
let buisnessapiClass = ApiListViewcontroller()
    var businessstatusclass = businessstatusClass()
       let alert = emailValidateViewController()
var language_code = ""
    var sr_people = ""
    var sr_date = ""
    var sr_time = ""
    var status1 = ""
     var statustype = ""
    var bookingid = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print("********************")
        print(biz_id)
          print("********************")
       
        self.bookingtable.separatorStyle = UITableViewCellSeparatorStyle.none
        self.navigationItem.title = NSLocalizedString("Bookings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
       
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        SetBackBarButtonCustom()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
            BookeventloadapiCalling(apikey:apikey,biz_id:biz_id,language_code:language_code)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func BookeventloadapiCalling(apikey:String,biz_id:String,language_code:String){
        // self.startactvityIndicator()
        bookingArray.removeAll()
        let scriptUrl = buisnessapiClass.OwnedBusinessBookings
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id,"language_code":language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                       let booking = response.object(forKey: "booking")!
                       
//                        // let currencyDictionary = ((currency as! NSArray)[0])  as! NSDictionary
//                        // print(currencyDictionary)
                        for book in (booking as! [[String:Any]]){
                            let bookElement = businessbookingviewclass()
                            let contry3 = book as NSDictionary
                            if  (contry3["name"] as? String) != nil {
                             let name = contry3["name"] as! String
                                bookElement.name = name
                                
                            }
                            if  (contry3["book_date"] as? String) != nil {
                                self.book_date = contry3["book_date"] as! String
                                bookElement.book_date = self.book_date
                            }
                            if  (contry3["extraInfo"] as? NSDictionary) != nil {
                                let extraInfo = contry3["extraInfo"] as! NSDictionary
                                if  (extraInfo["sr_time"] as? String) != nil {
                                    self.sr_time = extraInfo["sr_time"] as! String
                                    bookElement.sr_time = self.sr_time
                                    
                                }
                                if  (extraInfo["sr_people"] as? String) != nil {
                                    self.sr_people = extraInfo["sr_people"] as! String
                                    
                                    
                                }
                                if  (extraInfo["sr_date"] as? String) != nil {
                                    self.sr_date = extraInfo["sr_date"] as! String
                                }
                                if self.sr_date.count > 0{
                                    if  self.booking_type  == "5"{
                                        
                                        bookElement.srdetails  =  self.sr_people + " at " + self.sr_time + " "  + self.sr_date
                                    }else{
                                        bookElement.srdetails = self.book_date + " at " + self.sr_time
                                    }
                                }else{
                                    bookElement.srdetails = "Date not available...."
                                }
                               
                              
                                
                            }
                            
                            if  (contry3["book_type"] as? String) != nil {
                                self.booking_type = contry3["book_type"] as! String
                                
                            }
                            if  (contry3["approved"] as? String) != nil {
                                let approved = contry3["approved"] as! String
                                bookElement.approved = approved
                            }
                            if  (contry3["city_name"] as? String) != nil {
                                let city_name = contry3["city_name"] as! String
                                bookElement.city_name = city_name
                            }
                          
                            if  (contry3["email"] as? String) != nil {
                                let email = contry3["email"] as! String
                                bookElement.email = email
                            }
                           
                            if  (contry3["mobile"] as? String) != nil {
                                let mobile = contry3["mobile"] as! String
                                bookElement.mobile = mobile
                            }
                            if  (contry3["notes"] as? String) != nil {
                                let notes = contry3["notes"] as! String
                                bookElement.notes = notes
                            }
                            if  (contry3["title"] as? String) != nil {
                                let title = contry3["title"] as! String
                                bookElement.title = title
                            }
                            if  (contry3["id"] as? String) != nil {
                                let id = contry3["id"] as! String
                                bookElement.id = id
                            }
                            
                            self.bookingArray.append(bookElement)
                        }

                        
                        
                        DispatchQueue.main.async
                            {
                               
                                self.bookingtable.reloadData()
                                print("&&&&&&&&&&&&&&&&&&&&&&")
                                print(self.bookingArray.count)
                                print("&&&&&&&&&&&&&&&&&&&&&&")
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookingArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! businessbookingviewTableViewCell
        cell.selectionStyle = .none
        cell.separatorInset = .zero
           cell.smallview.layer.shadowColor = UIColor.black.cgColor
          cell.smallview.layer.shadowOpacity = 0.5
          cell.smallview.layer.shadowOffset = CGSize.zero
          cell.smallview.layer.shadowRadius = 2.0
   cell.namelabel.text = bookingArray[indexPath.row].name
         cell.datelabel.text = bookingArray[indexPath.row].book_date
           cell.timelabel.text = bookingArray[indexPath.row].sr_time
        if  self.booking_type == "3"{
            cell.bookingtypelabel.text = NSLocalizedString("TIME SLOT BOOKING", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          
        }else{
              cell.bookingtypelabel.text = NSLocalizedString("TABLE BOOKING", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        }
        if  bookingArray[indexPath.row].approved == "1"{
          
            cell.approvedlabel.text = NSLocalizedString("Approved", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            cell.approvedlabel.textColor = UIColor.green
           cell.statusOutlet.isUserInteractionEnabled  = false
        }else if bookingArray[indexPath.row].approved == "2"{
           
            cell.approvedlabel.text = NSLocalizedString("Declined", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
             cell.approvedlabel.textColor = UIColor.red
                cell.statusOutlet.isUserInteractionEnabled  = false
        }else{
            cell.approvedlabel.text = NSLocalizedString("Waiting", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            cell.approvedlabel.textColor = UIColor(red: 224/255.0, green: 147/255.0, blue: 8/255.0, alpha: 1.0)
                cell.statusOutlet.isUserInteractionEnabled  = true
            cell.statusOutlet.tag = indexPath.row
              cell.statusOutlet.addTarget(self, action: #selector(statusAction), for: .touchUpInside)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
        
    }
    @objc  func statusAction(sender: UIButton!) {
        businessstatusclass  = businessstatusClass(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
        self.view.addSubview(businessstatusclass)
        businessstatusclass.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        businessstatusclass.lastView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
         businessstatusclass.busineessTitle.text = bookingArray[sender.tag].title
        businessstatusclass.businessPlaceLabel.text = bookingArray[sender.tag].city_name
        businessstatusclass.businessDateandTimeLabel.text = bookingArray[sender.tag].srdetails
         businessstatusclass.bookedbyLabel.text = NSLocalizedString("Booked By", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            businessstatusclass.bookedby.isHidden = true
        businessstatusclass.nameLabel.text = NSLocalizedString("Name", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         businessstatusclass.name.text =  bookingArray[sender.tag].name
         businessstatusclass.mobileLabel.text = NSLocalizedString("Mobile", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        businessstatusclass.mobile.text = bookingArray[sender.tag].mobile
         businessstatusclass.messageLabel.text = NSLocalizedString("Message", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         businessstatusclass.message.text = bookingArray[sender.tag].notes
          businessstatusclass.emailLabel.text = NSLocalizedString("Email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         businessstatusclass.email.text =  bookingArray[sender.tag].email
        bookingid =  bookingArray[sender.tag].id
        businessstatusclass.rejectOutlet.layer.cornerRadius = 15.0
        businessstatusclass.acceptOutlet.layer.cornerRadius = 15.0
        businessstatusclass.rejectOutlet.addTarget(self, action: #selector(rejectbuttonAction), for: .touchUpInside)
        businessstatusclass.acceptOutlet.addTarget(self, action: #selector(acceptbuttonAction), for: .touchUpInside)
        // rplyView.backgroundColor = UIColor.red
    }
    func createReportView(){
       
    
        
    }
    
    @objc  func rejectbuttonAction(sender: UIButton!) {
     statustype = "2"
         apiCalling(Apikey:apikey,booking_id:bookingid,status:"2")
        
    }
    @objc  func acceptbuttonAction(sender: UIButton!) {
      statustype = ""
        apiCalling(Apikey:apikey,booking_id:bookingid,status:"1")
    }
    func apiCalling(Apikey:String,booking_id:String,status:String){
        let scriptUrl = buisnessapiClass.AcceptRejectBookings
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "Apikey=\(Apikey)&booking_id=\(booking_id)&status=\(status)"
        print(">>>>>>>>>")
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("################################")
            print(responseString)
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    print(convertedJsonIntoArray)
                    self.status1 = (convertedJsonIntoArray["status"] as! NSString) as String
                    print(self.status1)
                    if self.status1 == "success"{
                        
                        }
                    
                        
                        
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if (self.status1 == "success") {
                            
                            if self.statustype == "2"{
                                self.alert.Successalert(SubTitle: NSLocalizedString("Declined", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                self.businessstatusclass.isHidden = true
                                
                            }else{
                                  self.alert.Successalert(SubTitle: NSLocalizedString("Accepted", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                self.businessstatusclass.isHidden = true
                            }
                            
                        }else{
                            //self.stopActivityIndicator()
                            self.alert.Failurealert()
                            self.businessstatusclass.isHidden = true
                        }
                        
                        self.BookeventloadapiCalling(apikey:self.apikey,biz_id:self.biz_id,language_code:self.language_code)
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    
}
