//
//  businesshoureditViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/6/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import NVActivityIndicatorView
import Alamofire

class businesshoureditViewController: UIViewController,NVActivityIndicatorViewable {
    @IBOutlet var nextOutlet: UIButton!
    
    @IBOutlet var view1: UIView!
    var apikey = ""
    @IBOutlet var view2: UIView!
    
    @IBOutlet var view3: UIView!
    @IBOutlet var view4: UIView!
    @IBOutlet var view5: UIView!
    @IBOutlet var view6: UIView!
    @IBOutlet var view7: UIView!
    var status = ""
    
    @IBOutlet var sunLabel: UILabel!
    @IBOutlet var satLabel: UILabel!
    @IBOutlet var friLabel: UILabel!
    @IBOutlet var thuLabel: UILabel!
    @IBOutlet var wedLabel: UILabel!
    @IBOutlet var tueLabel: UILabel!
    @IBOutlet var monLabel: UILabel!
    @IBOutlet var monstartTme: UITextField!
    @IBOutlet var monendTme: UITextField!
    @IBOutlet var tuestartTme: UITextField!
    @IBOutlet var tueendTme: UITextField!
    @IBOutlet var wedstartTme: UITextField!
    @IBOutlet var wedendTme: UITextField!
    @IBOutlet var thustartTme: UITextField!
    @IBOutlet var thuendTme: UITextField!
    @IBOutlet var fristartTme: UITextField!
    @IBOutlet var friendTme: UITextField!
    @IBOutlet var satstartTme: UITextField!
    @IBOutlet var satendTme: UITextField!
    @IBOutlet var sunstartTme: UITextField!
    @IBOutlet var sunendTme: UITextField!
    var language_code = ""
    var country_code = ""
    var cancelButton = UIBarButtonItem()
    var doneButton = UIBarButtonItem()
    var spaceButton = UIBarButtonItem()
    let toolBar = UIToolbar()
    var buttonIndex = Int()
    var startTimearray = [String]()
    var endTimearray = [String]()
    var daynumberArray = [String]()
    let buisnessapiClass = ApiListViewcontroller()
    var id = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        // Do any additional setup after loading the view.
       
        
        view1.setviewBottomBorder()
        view2.setviewBottomBorder()
        view3.setviewBottomBorder()
        view4.setviewBottomBorder()
        view5.setviewBottomBorder()
        view6.setviewBottomBorder()
        view7.setviewBottomBorder()
        monstartTme.setgrayBottomBorder()
        monendTme.setgrayBottomBorder()
        tuestartTme.setgrayBottomBorder()
        tueendTme.setgrayBottomBorder()
        wedstartTme.setgrayBottomBorder()
        wedendTme.setgrayBottomBorder()
        thustartTme.setgrayBottomBorder()
        thuendTme.setgrayBottomBorder()
        fristartTme.setgrayBottomBorder()
        friendTme.setgrayBottomBorder()
        satstartTme.setgrayBottomBorder()
        satendTme.setgrayBottomBorder()
        sunstartTme.setgrayBottomBorder()
        sunendTme.setgrayBottomBorder()
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        // Do any additional setup after loading the view.
        
        doneButton = UIBarButtonItem(title: NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self, action: #selector(BusinesshourViewController.doneClick))
        spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        doneButton.tintColor = UIColor.green
        toolBar.isUserInteractionEnabled = true
        self.navigationItem.title = NSLocalizedString("EDIT BUSINESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        langSet()
        self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        LoadupdatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code,biz_id:id)
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func langSet(){
        monLabel.text = NSLocalizedString("MON", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        tueLabel.text = NSLocalizedString("TUE", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         wedLabel.text = NSLocalizedString("WED", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         thuLabel.text = NSLocalizedString("THU", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         friLabel.text = NSLocalizedString("FRI", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         satLabel.text = NSLocalizedString("SAT", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         sunLabel.text = NSLocalizedString("SUN", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        monstartTme.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        tuestartTme.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        wedstartTme.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        thustartTme.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        fristartTme.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        satstartTme.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        sunstartTme.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        monendTme.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        tueendTme.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        wedendTme.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        thuendTme.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        friendTme.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        satendTme.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        sunendTme.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        nextOutlet.setTitle(NSLocalizedString("UPDATE", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
    }
    func LoadupdatebuisnessapiCalling(apikey:String,language_code:String,country_code:String,biz_id:String){
        self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.LoadUpdateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code,"biz_id":biz_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                   
                        let business = response.object(forKey: "business")!
                        let contry1 = business as! NSDictionary
                    
                       // let biz_hours = response.object(forKey: "biz_hours")!
                        if  (contry1["biz_hours"] as? NSArray) != nil {
                            let biz_hours = contry1["biz_hours"] as! NSArray
                        for biz_hour in (biz_hours as! [[String:Any]]){
                            let contry2 = biz_hour as NSDictionary
                            if  (contry2["biz_days"] as? String) != nil {
                                let biz_days = contry2["biz_days"] as! String
                                
                                if biz_days == "0"{
                                      let biz_end_hours = contry2["biz_end_hours"] as! String
                                      let biz_start_hours = contry2["biz_start_hours"] as! String
                                    self.monstartTme.text = biz_start_hours
                                    self.monendTme.text = biz_end_hours
                                }else if  biz_days == "1"{
                                    let biz_end_hours = contry2["biz_end_hours"] as! String
                                    let biz_start_hours = contry2["biz_start_hours"] as! String
                                    self.tuestartTme.text = biz_start_hours
                                    self.tueendTme.text = biz_end_hours
                                }else if  biz_days == "2"{
                                    let biz_end_hours = contry2["biz_end_hours"] as! String
                                    let biz_start_hours = contry2["biz_start_hours"] as! String
                                    self.wedstartTme.text = biz_start_hours
                                    self.wedendTme.text = biz_end_hours
                                }else if  biz_days == "3"{
                                    let biz_end_hours = contry2["biz_end_hours"] as! String
                                    let biz_start_hours = contry2["biz_start_hours"] as! String
                                    self.thustartTme.text = biz_start_hours
                                    self.thuendTme.text = biz_end_hours
                                }else if  biz_days == "4"{
                                    let biz_end_hours = contry2["biz_end_hours"] as! String
                                    let biz_start_hours = contry2["biz_start_hours"] as! String
                                    self.fristartTme.text = biz_start_hours
                                    self.friendTme.text = biz_end_hours
                                }else if  biz_days == "5"{
                                    let biz_end_hours = contry2["biz_end_hours"] as! String
                                    let biz_start_hours = contry2["biz_start_hours"] as! String
                                    self.satstartTme.text = biz_start_hours
                                    self.satendTme.text = biz_end_hours
                                }else if  biz_days == "6"{
                                    let biz_end_hours = contry2["biz_end_hours"] as! String
                                    let biz_start_hours = contry2["biz_start_hours"] as! String
                                    self.sunstartTme.text = biz_start_hours
                                    self.sunendTme.text = biz_end_hours
                                }
                            }
                        }
                        }
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                    self.stopActivityIndicator()
//                                self.catogoryLabel.text  = self.catagoryname
//                                self.keywordLabel.text  = self.Keywordname
//                                self.addressLabel.text = self.address1
//                                self.mobileLabel.text = self.phone
//                                self.emailLabel.text = self.email
//                                self.titleLabel.text = self.titles
                                
                              
                                
                             
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    @IBAction func NextAction(_ sender: Any) {
        self.startactvityIndicator()
        if(monstartTme.text?.isEmpty == false && monendTme.text?.isEmpty == false){
            startTimearray.append(monstartTme.text!)
            endTimearray.append(monendTme.text!)
            daynumberArray.append("0")
        } else{
            startTimearray.append("0")
            endTimearray.append("0")
            daynumberArray.append("0")
        }
        if(tuestartTme.text?.isEmpty == false && tueendTme.text?.isEmpty == false){
            startTimearray.append(tuestartTme.text!)
            endTimearray.append(tueendTme.text!)
            daynumberArray.append("1")
        }
        else{
            startTimearray.append("0")
            endTimearray.append("0")
            daynumberArray.append("1")
        }
        if(wedstartTme.text?.isEmpty == false && wedendTme.text?.isEmpty == false){
            startTimearray.append(wedstartTme.text!)
            endTimearray.append(wedstartTme.text!)
            daynumberArray.append("2")
        }else{
            startTimearray.append("0")
            endTimearray.append("0")
            daynumberArray.append("2")
        }
        if(thustartTme.text?.isEmpty == false && thuendTme.text?.isEmpty == false){
            startTimearray.append(tuestartTme.text!)
            endTimearray.append(tueendTme.text!)
            daynumberArray.append("3")
        }else{
            startTimearray.append("0")
            endTimearray.append("0")
            daynumberArray.append("3")
        }
        if(fristartTme.text?.isEmpty == false && friendTme.text?.isEmpty == false){
            startTimearray.append(fristartTme.text!)
            endTimearray.append(friendTme.text!)
            daynumberArray.append("4")
        }else{
            startTimearray.append("0")
            endTimearray.append("0")
            daynumberArray.append("4")
        }
        if(satstartTme.text?.isEmpty == false && satendTme.text?.isEmpty == false){
            startTimearray.append(satstartTme.text!)
            endTimearray.append(satendTme.text!)
            daynumberArray.append("5")
        }else{
            startTimearray.append("0")
            endTimearray.append("0")
            daynumberArray.append("5")
        }
        if(sunstartTme.text?.isEmpty == false && sunendTme.text?.isEmpty == false){
            startTimearray.append(sunstartTme.text!)
            endTimearray.append(sunendTme.text!)
            daynumberArray.append("6")
        }else{
            startTimearray.append("0")
            endTimearray.append("0")
            daynumberArray.append("6")
        }
        
        var bigDic: NSMutableDictionary = NSMutableDictionary()
        var objectArray = NSMutableArray()
        var dic : NSMutableDictionary = NSMutableDictionary()
        for i in 0..<startTimearray.count
        {
            
            dic = NSMutableDictionary()
            dic.setValue(startTimearray[i], forKey: "biz_start_hours")
            dic.setValue(endTimearray[i], forKey: "biz_end_hours")
            dic.setValue(daynumberArray[i], forKey: "biz_days")
            objectArray.add(dic)
        }
        
    
        bigDic.setValue(objectArray, forKey: "biz_hours")
        bigDic.setValue(apikey, forKey: "apikey")
        bigDic.setValue(id, forKey: "biz_id")
        
        
        
        //Convert to Data
        
        let jsonData = try! JSONSerialization.data(withJSONObject: bigDic, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        
        //Convert back to string. Usually only do this for debugging
        
        if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
            
            //ethaann nink veenda string
            
            // json aayit kittum
       
            let scriptUrl = buisnessapiClass.PostUpdateBusinessHours
            let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
            let myUrl = URL(string: urlWithParams);
            var request = URLRequest(url:myUrl!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request)
            {
                data, response, error in
                
                if error != nil
                {
                    print("error=\(error)")
                    DispatchQueue.main.async
                        {
                            print("server down")
                    }
                    return
                }
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
          
                do{
                    
                    if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                        
                    {
                        
                        self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                     
                        
                    }
                    DispatchQueue.main.async
                        {
                            
                            
                            if self.status == "success"{
                                self.stopActivityIndicator()
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "bsnsedit1") as! bsnsedit1ViewController
                                let navController = UINavigationController(rootViewController: vc)
                                self.present(navController, animated: true, completion: nil)
                                
                                //self.imageUploading(selectedImage:self.selectedImage)
                            }else{
                                self.stopActivityIndicator()
                            }
                            
                            
                    }//dispatch ends
                    
                }//do ends
                catch let error as NSError {
                    print(error.localizedDescription)
                    
                }
                
            }
            
            
            
           
            
            task.resume()
            
            
        }
        
    
    
        
        
      
      
    }
    
    @IBAction func textAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        monstartTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.canceldateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        monstartTme.inputAccessoryView = toolBar
        
    }
    
    @objc func canceldateClick() {
        monstartTme.text = ""
        monstartTme.resignFirstResponder()
        
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        monstartTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    
    
    @objc func doneClick() {
        
        monstartTme.resignFirstResponder()
        monendTme.resignFirstResponder()
        tuestartTme.resignFirstResponder()
        tueendTme.resignFirstResponder()
        wedstartTme.resignFirstResponder()
        wedendTme.resignFirstResponder()
        thustartTme.resignFirstResponder()
        thuendTme.resignFirstResponder()
        fristartTme.resignFirstResponder()
        friendTme.resignFirstResponder()
        satstartTme.resignFirstResponder()
        satendTme.resignFirstResponder()
        sunstartTme.resignFirstResponder()
        sunendTme.resignFirstResponder()
        // timepickerFromOut.resignFirstResponder()
    }
    
    @IBAction func monendtAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        monendTme.inputView = datePickerView
        datePickerView.locale = Locale(identifier: "en_US")
        datePickerView.addTarget(self, action: #selector(datePickermonendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelmonenddateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        monendTme.inputAccessoryView = toolBar
    }
    @objc func cancelmonenddateClick() {
        monendTme.text = ""
        monendTme.resignFirstResponder()
        
    }
    @objc func datePickermonendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
         dateFormatter.locale = Locale(identifier: "en_US")
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        monendTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    
    
    @IBAction func tueStartAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        tuestartTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickertuestartValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.canceltuestartdateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        tuestartTme.inputAccessoryView = toolBar
    }
    @objc func canceltuestartdateClick() {
        tuestartTme.text = ""
        tuestartTme.resignFirstResponder()
        
    }
    @objc func datePickertuestartValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        tuestartTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    @IBAction func tueEndAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        tueendTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickertueendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.canceltueenddateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        tueendTme.inputAccessoryView = toolBar
    }
    @objc func canceltueenddateClick() {
        tueendTme.text = ""
        tueendTme.resignFirstResponder()
        
    }
    @objc func datePickertueendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        tueendTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    @IBAction func wedStartaction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        wedstartTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerwedstartValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelwedstartdateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        wedstartTme.inputAccessoryView = toolBar
    }
    @objc func cancelwedstartdateClick() {
        wedstartTme.text = ""
        wedstartTme.resignFirstResponder()
        
    }
    @objc func datePickerwedstartValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        wedstartTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    @IBAction func wedEndzAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        wedendTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerwedendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelwedenddateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        wedendTme.inputAccessoryView = toolBar
    }
    @objc func cancelwedenddateClick() {
        wedendTme.text = ""
        wedendTme.resignFirstResponder()
        
    }
    @objc func datePickerwedendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        wedendTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    
    @IBAction func TuestartAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        thustartTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerthustartValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title:NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelthustartdateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        thustartTme.inputAccessoryView = toolBar
    }
    @objc func cancelthustartdateClick() {
        thustartTme.text = ""
        thustartTme.resignFirstResponder()
        
    }
    @objc func datePickerthustartValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        thustartTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    @IBAction func ThuAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        thuendTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerthuendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelthuenddateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        thuendTme.inputAccessoryView = toolBar
    }
    @objc func cancelthuenddateClick() {
        thuendTme.text = ""
        thuendTme.resignFirstResponder()
        
    }
    @objc func datePickerthuendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        thuendTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    @IBAction func friStartAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        fristartTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerfriStartValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelfriStartdateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        fristartTme.inputAccessoryView = toolBar
    }
    @objc func cancelfriStartdateClick() {
        fristartTme.text = ""
        fristartTme.resignFirstResponder()
        
    }
    @objc func datePickerfriStartValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        fristartTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    @IBAction func FRiEndAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        friendTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerfriendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelfrienddateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        friendTme.inputAccessoryView = toolBar
    }
    @objc func cancelfrienddateClick() {
        friendTme.text = ""
        friendTme.resignFirstResponder()
        
    }
    @objc func datePickerfriendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        friendTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    @IBAction func satstartAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        satstartTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickersatstartValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelsatstartdateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        satstartTme.inputAccessoryView = toolBar
    }
    @objc func cancelsatstartdateClick() {
        satstartTme.text = ""
        satstartTme.resignFirstResponder()
        
    }
    @objc func datePickersatstartValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        satstartTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    @IBAction func satEndAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        satendTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickersatendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelsatenddateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        satendTme.inputAccessoryView = toolBar
    }
    @objc func cancelsatenddateClick() {
        satendTme.text = ""
        satendTme.resignFirstResponder()
        
    }
    @objc func datePickersatendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        satendTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    @IBAction func sunStartAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sunstartTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickersunstartValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelsunstartdateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        sunstartTme.inputAccessoryView = toolBar
    }
    @objc func cancelsunstartdateClick() {
        sunstartTme.text = ""
        sunstartTme.resignFirstResponder()
        
    }
    @objc func datePickersunstartValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        sunstartTme.text = dateFormatter.string(from: sender.date)
        
        
    }
    @IBAction func sunendAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        sunendTme.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerssunendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(BusinesshourViewController.cancelsunenddateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        sunendTme.inputAccessoryView = toolBar
    }
    @objc func cancelsunenddateClick() {
        sunendTme.text = ""
        sunendTme.resignFirstResponder()
        
    }
    @objc func datePickerssunendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "hh:mm a"
        
        // Apply date format
        sunendTme.text = dateFormatter.string(from: sender.date)
        
        
    }

    

  

}
