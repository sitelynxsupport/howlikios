//
//  businessstatusClass.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/31/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class businessstatusClass: UIView {
    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var mainview: UIView!
    @IBOutlet var lastView: UIView!
    @IBOutlet var busineessTitle: UILabel!
    @IBOutlet var businessPlaceLabel: UILabel!
    @IBOutlet var businessDateandTimeLabel: UILabel!
    @IBOutlet var bookedbyLabel: UILabel!
    @IBOutlet var bookedby: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var message: UILabel!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var mobile: UILabel!
    @IBOutlet var mobileLabel: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var acceptOutlet: UIButton!
    @IBOutlet var rejectOutlet: UIButton!
    override init(frame: CGRect){ // for using custom viw in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
        
        commonInit()
    }
    private func commonInit(){
        //  we are going to do stuff here
        //        Bundle.main.loadNibNamed("replyView", owner: self, options: nil)
        //        addSubview(replyView)
        //        replyView.frame = self.bounds
        //        replyView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        let nibView = Bundle.main.loadNibNamed("businessstatusClass", owner: self, options: nil)![0] as! UIView
        nibView.frame = self.bounds;
        self.addSubview(nibView)
    }

}
