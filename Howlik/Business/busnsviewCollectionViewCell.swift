//
//  busnsviewCollectionViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/13/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class busnsviewCollectionViewCell: UICollectionViewCell {
    @IBOutlet var bsnsimgview: UIImageView!
     @IBOutlet var runkeeperSwitch4: DGRunkeeperSwitch!
   
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
      
        runkeeperSwitch4.titles = [NSLocalizedString("Mybusinesses", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Pendingapprovals", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
        runkeeperSwitch4.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        runkeeperSwitch4.selectedBackgroundColor = .white
        runkeeperSwitch4.titleColor = .white
        runkeeperSwitch4.selectedTitleColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        runkeeperSwitch4.titleFont = UIFont(name: "HelveticaNeue-Light", size: 17.0)
        
    }

    
}
