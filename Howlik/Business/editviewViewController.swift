//
//  editviewViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/1/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import NVActivityIndicatorView
import Alamofire
import UITextViewPlaceholder
import TTGSnackbar


class editviewViewController: UIViewController,NVActivityIndicatorViewable,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource  {
      let textViewRecognizer = UITapGestureRecognizer()
      let buisnessapiClass = ApiListViewcontroller()
    var address1 = String()
    var address2 = String()
    var phone = String()
    var titles = String()
    var email = String()
    var Keywordname =  String()
    var catagoryname = String()
    var descriptions = String()
      var swiftArray = [String]()
       var dataSource = [String]()
    var apikey = ""
    var height = Int()
var addview = UIView()
    var addedView = UIView()
    var  dashImage = ["city","city","city","city","city","city","city","city","city"]
    var myTableView = UITableView()
    var newview = UIView()
    var language_code  = ""
    var country_code = ""
    var CtagoryCodeArray = [String]()
    var CtagorynameArray = [String]()
    var keywordidArray = [String]()
    var keywordnameArray = [String]()
    var catagoryid = ""
    var status = ""
    var buttonIndex = Int()
    var cellIndex = Int()
    var clickIndex = 0
    var keyWordArray = [SingleKeyword]()
    var valueArray = [String]()
    var tableHeightConstraint: NSLayoutConstraint!
    var tickValue = ""
    var tickvalueArray = [String]()
    var val = false
    var lar = [String]()
    var id = ""
    var category_id = ""
      //  let textfield = TextField()
    var tableheightindicator = Int()
    @IBOutlet var catogoryOutlet: UIButton!
    @IBOutlet var selectCatagoryTextfield: UITextField!
    @IBOutlet var keywordoutlet: UIButton!
    @IBOutlet var selectKeywordTextfield: UITextField!
    @IBOutlet var keywordlabel: UILabel!
    @IBOutlet var buisnessTitleTextfield: UITextField!
    
    @IBOutlet var discriptionTextfield: UITextView!
    @IBOutlet var phoneNumberTextfield: UITextField!
    
    @IBOutlet var NextOutlet: UIButton!
    @IBOutlet var websiteTextfield: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.id = UserDefaults.standard.string(forKey: "biz_id")!
        textViewRecognizer.addTarget(self, action: #selector(tappedTextView(_:)))
        keywordlabel.addGestureRecognizer(textViewRecognizer)
     
        
        
        //keywordoutlet.backgroundColor = .clear
       
        SetBackBarButtonCustom()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{

        self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        self.country_code = UserDefaults.standard.string(forKey: "country_code")!
        self.language_code = UserDefaults.standard.string(forKey: "language_code")!
        }
        discriptionTextfield.layer.borderColor = UIColor.lightGray.cgColor
        discriptionTextfield.layer.borderWidth = 1.0;
        discriptionTextfield.backgroundColor = UIColor.clear
        discriptionTextfield.layer.cornerRadius = 4
        discriptionTextfield.layer.masksToBounds = false
        selectCatagoryTextfield.setBorder()
      
        phoneNumberTextfield.setBorder()
        websiteTextfield.setBorder()
        buisnessTitleTextfield.setBorder()
        buisnessTitleTextfield.setLeftPaddingPoints(5.0)
        websiteTextfield.setLeftPaddingPoints(5.0)
        phoneNumberTextfield.setLeftPaddingPoints(5.0)
        selectKeywordTextfield.setLeftPaddingPoints(5.0)
        // Do any additional setup after loading the view.
        discriptionTextfield.delegate = self
        catogoryOutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        NextOutlet.layer.cornerRadius = 10.0
        keywordlabel.layer.borderWidth = 0.5
        keywordlabel.layer.cornerRadius = 5
        keywordlabel.layer.borderColor = UIColor.lightGray.cgColor
        self.navigationItem.title = NSLocalizedString("EDIT BUSINESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        //"EDIT BUSINESS"
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        selectCatagoryTextfield.setLeftPaddingPoints(5.0)
        selectKeywordTextfield.setLeftPaddingPoints(5.0)
        buisnessTitleTextfield.setLeftPaddingPoints(5.0)
        phoneNumberTextfield.setLeftPaddingPoints(5.0)
        websiteTextfield.setLeftPaddingPoints(5.0)
        if  UserDefaults.standard.string(forKey:"language_code") == "en"{
            catogoryOutlet.contentHorizontalAlignment = .left
           
        }else{
            catogoryOutlet.contentHorizontalAlignment = .right
        }
self.LoadupdatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code,biz_id:self.id)

    }
    
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if discriptionTextfield.text == ""{
     
             discriptionTextfield.placeholder = ""
          
        }else{
                discriptionTextfield.placeholder = NSLocalizedString("Enter Description", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          
        }
          createTableview()
    }
     @objc func tappedTextView(_ sender: UITapGestureRecognizer) {
  

        if catogoryOutlet.currentTitle == nil{
            showAlertButtonTapped()
        }else{
        selectKeywordTextfield.placeholder = ""
        buttonIndex = 1
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
           
            self.height =  Int(self.tableviewHeight())
            
     //    self.newview = UIView(frame: CGRect(x: 15, y: 3, width: screenSize.width - 30 , height: screenSize.height - 100 ))
            if tableheightindicator == 0{
                self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
            }else{
                  self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
                newview.center =  view.center
            }
            
        self.addview.addSubview(newview)
        
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label);
        let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 65, y:newview.frame.height - 45,width: 60  , height:  40));
        newview.addSubview(button);
        // button.backgroundColor = UIColor.green
        button.layer.cornerRadius = 3.0
        button.setTitle(NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        label.textColor = UIColor.black
        label.isHighlighted = true
        newview.backgroundColor  = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        myTableView.register(UINib(nibName: "BsnstableView", bundle: nil), forCellReuseIdentifier: "myIdentifier")
        label.text = NSLocalizedString("Select Keywords", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        newview.isHidden =  true
        myTableView.isHidden =  true
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        addview.layer.cornerRadius = 10;
        addview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
        addview.isHidden = false
        newview.isHidden = false
        myTableView.isHidden = false
        myTableView.reloadData()
     
        }
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
        
        
        
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    
    func showAlertButtonTapped() {
       
        // create the alert
        let alert = UIAlertController(title: NSLocalizedString("ALERT", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), message: NSLocalizedString("SelectCategory", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
 func LoadupdatebuisnessapiCalling(apikey:String,language_code:String,country_code:String,biz_id:String){
        self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.LoadUpdateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code,"biz_id":biz_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                     
                        let business = response.object(forKey: "business")!
                        let contry1 = business as! NSDictionary
                        
                        if  (contry1["category_id"] as? String) != nil {
                            self.category_id = contry1["category_id"] as! String
                          
                            self.catagoryid = self.category_id
                           
                        }
                        if  (contry1["keywords"] as? NSArray) != nil {
                            let keywords = contry1["keywords"] as! NSArray
                            
                            self.swiftArray = keywords as NSArray as! [String]
                         
                        }
                        if  (contry1["description"] as? String) != nil {
                            self.descriptions = contry1["description"] as! String
                            
                        }
                        if  (contry1["phone"] as? String) != nil {
                            self.phone = contry1["phone"] as! String
                            
                        }
                        if  (contry1["title"] as? String) != nil {
                            self.titles = contry1["title"] as! String
                            
                        }
                        if  (contry1["biz_email"] as? String) != nil {
                            self.email = contry1["web"] as! String
                            
                        }
                        
                        let keywords = response.object(forKey: "keywords")!
                        
                        for keyword in (keywords as! [[String:Any]]){
                            let contry2 = keyword as NSDictionary
                            if  (contry2["id"] as? String) != nil {
                                let id = contry2["id"] as! String
                                for i in self.swiftArray{
                                    if id == i{
                                        if  (contry2["name"] as? String) != nil{
                                            let name = contry2["name"] as! String
                                            
                                            self.Keywordname = name + "," + self.Keywordname
                                        }
                                    }
                                }
                            }
                        }
                        
                        let categories = response.object(forKey: "categories")!
                        
                        for categorie in (categories as! [[String:Any]]){
                            let contry3 = categorie as NSDictionary
                            if  (contry3["code"] as? String) != nil {
                                let code = contry3["code"] as! String
                                
                                if code == self.category_id{
                                    if  (contry3["name"] as? String) != nil {
                                        self.self.catagoryname = contry3["name"] as! String
                                    }
                                    
                                    
                                }
                            }
                        }
                        
                        
                        DispatchQueue.main.async
                            {
                                self.stopActivityIndicator()
                                self.LoadcatogorybuisnessapiCalling(apikey:apikey,language_code:language_code,category_id:self.catagoryid)
                                self.catogoryOutlet.setTitle( self.catagoryname, for: UIControlState())
                              //   self.keywordoutlet.setTitle(self.Keywordname, for: UIControlState())
                                self.keywordlabel.text = self.Keywordname
                               
                                self.discriptionTextfield.text = self.descriptions
                                if self.discriptionTextfield.text != ""{
                                     self.discriptionTextfield.placeholder = ""
                                }
                                self.phoneNumberTextfield.text = self.phone
                                self.websiteTextfield.text = self.email
                                self.buisnessTitleTextfield.text = self.titles
                                self.discriptionTextfield.placeholder = ""

    }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
   
    
 @IBAction func keywordActionButton(_ sender: Any) {
    
   
   
    
    }
    @objc func okView(_ sender: UIButton)
    {
       tickValue = ""
        
        for i in 0..<keyWordArray.count{
            if (keyWordArray[i].isChecked == true){
                //tickvalueArray.append(keyWordArray[i].keywordName)
           tickValue = keyWordArray[i].keywordName + "," + tickValue
            }
        }

        keywordlabel.text = tickValue
         //keywordoutlet.setTitle(tickValue, for: UIControlState())
         addview.isHidden = true
        newview.isHidden = true
        tickvalueArray.removeAll()

        
    }
    
    @IBAction func catogoryButtonAction(_ sender: Any) {
        
        selectCatagoryTextfield.placeholder = ""
        buttonIndex = 0
        createTableview()
    LoadcreatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code)
        addview.isHidden = false
        newview.isHidden = false
        myTableView.isHidden = false
        
    }
    @IBAction func bsnsAction(_ sender: Any) {
        
        tickvalueArray.removeAll()
        if (catogoryOutlet!.currentTitle == nil){
            alertsnack(message: NSLocalizedString("SelectCategory", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }
           
        else if (keywordlabel!.text?.isEmpty)!{
            alertsnack(message:NSLocalizedString("Select Keywords", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }
         
        else if (buisnessTitleTextfield!.text?.isEmpty)! {
            alertsnack(message: NSLocalizedString("Need Business Title", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
           
        }else if (discriptionTextfield!.text?.isEmpty)! {
            alertsnack(message: NSLocalizedString("Enter Description", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (phoneNumberTextfield!.text?.isEmpty)! {
            alertsnack(message: NSLocalizedString("Phone number required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (websiteTextfield!.text?.isEmpty)! {
            alertsnack(message: NSLocalizedString("Website is required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else{
            for i in 0..<keyWordArray.count{
                if (keyWordArray[i].isChecked == true){
                    tickvalueArray.append(keyWordArray[i].keywordID)
                }else{
                  
                }
                
                
                
            }
          
            nextApicalling()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if buttonIndex  == 0{
            return CtagorynameArray.count
        }else{
            
            return keyWordArray.count
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if buttonIndex  == 0{
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.selectionStyle = .none
        
            cell.textLabel?.text = self.CtagorynameArray[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! BsnstableView
                cell.selectionStyle = .none
      
            cell.keywordButton.tag = indexPath.row
            cellIndex = indexPath.row
            cell.keywordButton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
            cell.keywordLabel.text = keyWordArray[indexPath.row].keywordName
            let val = keyWordArray[indexPath.row].isChecked
            
            if(val == true){
                cell.keywordButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.keywordButton.tintColor = UIColor.red
                
            }else{
                
                cell.keywordButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.keywordButton.tintColor = UIColor.green
            }
            
            
            return cell
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if buttonIndex == 0{
        catogoryOutlet.setTitle(self.CtagorynameArray[indexPath.row], for: UIControlState())
            catagoryid = CtagoryCodeArray[indexPath.row]
              keywordlabel.text = ""
        }else{
            cellIndex = indexPath.row
          
            
        }
    
    LoadcatogorybuisnessapiCalling(apikey:apikey,language_code:language_code,category_id:catagoryid)
        myTableView.reloadData()
        addview.isHidden = true
        newview.isHidden = true
        myTableView.isHidden = true
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    @objc func moreView(_ sender: UIButton)
    {
        if  keyWordArray[sender.tag].isChecked == false{
            keyWordArray[sender.tag].isChecked = true
         
            
            
        }else{
            keyWordArray[sender.tag].isChecked = false
      

        }
        
        
        myTableView.reloadData()
    }
    
    
    func LoadcreatebuisnessapiCalling(apikey:String,language_code:String,country_code:String){
        self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.LoadCreateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                       
                        let categories = response.object(forKey: "categories")!
                       
                        for categorie in (categories as! [[String:Any]]){
                            let contry1 = categorie as NSDictionary
                            if  (contry1["code"] as? String) != nil {
                                let code = contry1["code"] as! String
                                self.CtagoryCodeArray.append(String(code))
                            }else{
                                let code = "nil"
                                self.CtagoryCodeArray.append(String(code))
                            }
                            
                            if  (contry1["name"] as? String) != nil {
                                let name = contry1["name"] as! String
                                self.CtagorynameArray.append(name)
                            }else{
                                let name = "nil"
                                self.CtagorynameArray.append(name)
                            }
                      
                        }
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                           
                                self.myTableView.reloadData()
                                self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
 
    func LoadcatogorybuisnessapiCalling(apikey:String,language_code:String,category_id:String){
        self.startactvityIndicator()
        tickvalueArray.removeAll()
        keyWordArray.removeAll()
        let scriptUrl = buisnessapiClass.GenerateKeywordsfromCategory
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&language_code=\(language_code)&category_id=\(category_id)"
     
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
             //   print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                 
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                    print(self.status)
                    if self.status == "success"{
                        if  (convertedJsonIntoArray["keywords"] as? NSArray) != nil {
                            let keywords = convertedJsonIntoArray["keywords"] as! NSArray
                        
                          //  self.keyWordArray.removeAll()
                            for keyword in  (keywords as! [[String:Any]])
                            {
                                let contry1 = keyword as NSDictionary
                                let keyObj = SingleKeyword()
                                
                                if  (contry1["id"] as? String) != nil {
                                    let id = contry1["id"] as! String
                                    keyObj.keywordID = String(id)
                                 
                                    if self.swiftArray.contains(id){
                                      
                                         keyObj.isChecked = true
                                        
                                    }else{
                                         keyObj.isChecked = false
                                    }
                                    
                                }
                                if  (contry1["name"] as? String) != nil {
                                    let name = contry1["name"] as! String
                                    keyObj.keywordName = name
                                }
                                //keyObj.isChecked = false
                                self.keyWordArray.append(keyObj)
                            }
                            
                        }
                        
                        
                    }else{
                        
                    }
                    
                    
                    
                    DispatchQueue.main.async
                        {
                           
                            self.myTableView.reloadData()
                            
                            self.stopActivityIndicator()
                            
                            
                            
                    }
                    
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    
   
    func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        self.addview.addSubview(newview)
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 8, width: newview.frame.width, height: newview.frame.height))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
      //  view1.backgroundColor  = UIColor.lightGray
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        // if buttonIndex == 0{
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        label.text = NSLocalizedString("SelectCategory", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        newview.isHidden =  true
        addview.isHidden = true
        myTableView.isHidden =  true
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
    }
    func alertsnack(message: String){
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    func nextApicalling(){
        self.startactvityIndicator()
        var createdArray = NSMutableArray()
        var bigDic: NSMutableDictionary = NSMutableDictionary()
        var dic : NSMutableDictionary = NSMutableDictionary()
        bigDic.setValue(catagoryid, forKey: "category_id")
        bigDic.setValue(discriptionTextfield.text, forKey: "biz_desc")
        bigDic.setValue(apikey, forKey: "apikey")
        bigDic.setValue(id, forKey: "biz_id")
        bigDic.setValue(buisnessTitleTextfield.text, forKey: "biz_title")
        bigDic.setValue(phoneNumberTextfield.text, forKey: "phone")
        bigDic.setValue(websiteTextfield.text, forKey: "website")
        bigDic.setValue(language_code, forKey: "langauge_code")
        bigDic.setValue(country_code, forKey: "country_code")
        bigDic.setValue(tickvalueArray, forKey: "keywords")
        
        do {
            
            
            
            //Convert to Data
            
            let jsonData = try! JSONSerialization.data(withJSONObject: bigDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            
         
            
            //Convert back to string. Usually only do this for debugging
            
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                let scriptUrl = buisnessapiClass.PostUpdateBusinessGeneralDetails
                let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
                let myUrl = URL(string: urlWithParams);
                var request = URLRequest(url:myUrl!)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpBody = jsonData
                let task = URLSession.shared.dataTask(with: request)
                {
                    data, response, error in
                    
                    if error != nil
                    {
                        print("error=\(error)")
                        DispatchQueue.main.async
                            {
                                print("server down")
                        }
                        return
                    }
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                 
                    do{
                        
                        if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                            
                        {
                            
                            self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                            
                            
                        }
                        DispatchQueue.main.async
                            {
                                
                                
                                if self.status == "success"{
                                    self.stopActivityIndicator()
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "bsnsedit1") as! bsnsedit1ViewController
                                    let navController = UINavigationController(rootViewController: vc)
                                    self.present(navController, animated: true, completion: nil)
                                    
                                }else{
                                    self.stopActivityIndicator()
                                }
                                
                                
                        }//dispatch ends
                        
                    }//do ends
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    
                }
                
                
                
               
                
                task.resume()
                
                
            }
            
        }
        
        
       
        
    }
    
    func tableviewHeight() -> Double{
        
        let screenSize: CGRect = UIScreen.main.bounds
        let maximumHeight = screenSize.height - 100
        if (Double(44.0)  *  Double(keyWordArray.count) >  Double(maximumHeight)){
          
            tableheightindicator = 0
            return Double(maximumHeight)
            
        }else{
            tableheightindicator = 1
            return(44.0  * Double(keyWordArray.count) + 55.0 + 50.0)
            
            
        }
        
    }

}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
