//
//  finishBusinessViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/20/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import FontAwesome_swift
import NVActivityIndicatorView
import TTGSnackbar

class finishBusinessViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,GMSPlacePickerViewControllerDelegate,NVActivityIndicatorViewable {
    let buisnessapiClass = ApiListViewcontroller()
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    var locationid = ""
    var status = ""
        var cityid = ""
    var Placename = ""
    var Placeaddress = ""
    var placelattitude = Double()
    var placlongitude = Double()
    var  locationArray = [locationClass]()
     var  cityArray = [cityClass]()
    var val = false
    @IBOutlet var locationOutlet: UIButton!
    
    @IBOutlet var cityOutlet: UIButton!
    @IBOutlet var addresslineTextfield: UITextField!
    
    @IBOutlet var Addressline2Textfield: UITextField!
    
    @IBOutlet var LocationTextfield: UITextField!
    
    @IBOutlet var zipcodeTextfield: UITextField!
    
    @IBOutlet var pinLocationtextfield: UITextField!
    @IBOutlet var cityTextfield: UITextField!
    
    @IBOutlet var mapOutlet: UIButton!
    @IBOutlet var finishOutlet: UIButton!
   var  language_code = ""
    var  country_code = ""
    var apikey = ""
    var buttonIndex = Int()
    var location_code = ""
    var category_id = ""
    var keywords = [String]()
    var biz_title = ""
    var biz_desc = ""
    var phone = ""
    var website = ""
    var biz_hours = [String: String]()
    var id = [String]()
      var value = [String]()
    var daynumberArray = [String]()
    var startTimearray = [String]()
    var endTimearray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          if  UserDefaults.standard.string(forKey:"apikey") != nil{
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.country_code = UserDefaults.standard.string(forKey: "country_code")!
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
        }
       //  self.location_code = UserDefaults.standard.string(forKey: "location_code")!
         self.category_id = UserDefaults.standard.string(forKey: "category_id")!
        
         self.biz_title = UserDefaults.standard.string(forKey: "biz_title")!
         self.biz_desc = UserDefaults.standard.string(forKey: "biz_desc")!
         self.phone = UserDefaults.standard.string(forKey: "phone")!
        self.website = UserDefaults.standard.string(forKey: "website")!
     
      //  self.biz_hours = UserDefaults.standard.dictionary(forKey: "biz_hours") as! [String : String]
        self.keywords = UserDefaults.standard.array(forKey: "keywords") as! [String]
        self.id = UserDefaults.standard.array(forKey: "id") as! [String]
        self.value = UserDefaults.standard.array(forKey: "value") as! [String]
        
        finishOutlet.layer.cornerRadius = 10.0
         LoadcreatebuisnessapiCalling(apikey:apikey,language_code:language_code,country_code:country_code)
        self.navigationItem.title = NSLocalizedString("New Business", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        addresslineTextfield.setBorder()
        Addressline2Textfield.setBorder()
        LocationTextfield.setBorder()
        zipcodeTextfield.setBorder()
        pinLocationtextfield.setBorder()
        cityTextfield.setBorder()
      setLanguage()
        if  UserDefaults.standard.string(forKey:"language_code") == "en"{
            cityOutlet.contentHorizontalAlignment = .left
                mapOutlet.contentHorizontalAlignment = .left
        }else{
            cityOutlet.contentHorizontalAlignment = .right
                mapOutlet.contentHorizontalAlignment = .right
        }
    }
    func setLanguage(){
        addresslineTextfield.placeholder = NSLocalizedString("Addressline01", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        Addressline2Textfield.placeholder = NSLocalizedString("Addressline02", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        LocationTextfield.placeholder = NSLocalizedString("Location", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        cityTextfield.placeholder = NSLocalizedString("City", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        zipcodeTextfield.placeholder = NSLocalizedString("ZipCode", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        pinLocationtextfield.placeholder = NSLocalizedString("PinLocation", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        finishOutlet.setTitle(NSLocalizedString("Finish", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }

    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func LoadcreatebuisnessapiCalling(apikey:String,language_code:String,country_code:String){
    //    self.informationArray.removeAll()
        self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.LoadCreateBusiness
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code":country_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                      
                    let locations = response.object(forKey: "locations")!
                   
                            for location in (locations as! [[String:Any]]){
                                 let Infokey = locationClass()
                        
                            let contry1 = location as NSDictionary
                            if  (contry1["id"] as? String) != nil {
                                let id = contry1["id"] as! String
                                Infokey.id = id
                            }else{
                                let id = "nil"
                                Infokey.id = id
                            }
                            
                            if  (contry1["title"] as? String) != nil {
                                let title = contry1["title"] as! String
                                Infokey.title = title
                            }else{
                                let title = "nil"
                                Infokey.title = title
                            }
                           
                            self.locationArray.append(Infokey)
                        }
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                self.myTableView.reloadData()
                                self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        addview.addSubview(newview)
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        //view1.backgroundColor  = UIColor.lightGray
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
         if buttonIndex == 0{
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        label.text = NSLocalizedString("Select Location", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            
         }else{
            myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "mycell")
            label.text = NSLocalizedString("selectacity", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           
    }
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        addview.isHidden = false
        newview.isHidden =  false
        myTableView.isHidden =  false
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
    }

    @IBAction func FinishAction(_ sender: Any) {
       
        if (addresslineTextfield!.text?.isEmpty)! {
         
               alertsnack(message: NSLocalizedString("Address1 is required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        } else if (locationOutlet!.currentTitle == nil){
            alertsnack(message: NSLocalizedString("Pleaseselectalocation", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (cityOutlet!.currentTitle == nil){
              alertsnack(message: NSLocalizedString("selectacity", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else if (zipcodeTextfield!.text?.isEmpty)! {
             alertsnack(message: NSLocalizedString("Zip Code is required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else if (mapOutlet!.currentTitle == nil) {
            alertsnack(message: NSLocalizedString("PinLocation", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
           self.apiCalling()
        }
        
    }
    
    @IBAction func locationAction(_ sender: Any) {
        buttonIndex = 0
        
        createTableview()
    }
    

    @IBAction func cityAction(_ sender: Any) {
        if locationOutlet.currentTitle == nil{
              showAlertButtonTapped()
        }else{
          buttonIndex = 1
          createTableview()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if buttonIndex  == 0{
           return locationArray.count
    }else{
            
            return cityArray.count
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if buttonIndex  == 0{
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                cell.selectionStyle = .none
        
            cell.textLabel?.text = locationArray[indexPath.row].title
            return cell
        }else{
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath)
                cell.selectionStyle = .none
            
            cell.textLabel?.text = cityArray[indexPath.row].title
            return cell
            
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if buttonIndex == 0{
            locationOutlet.setTitle(self.locationArray[indexPath.row].title, for: UIControlState())
            locationid = locationArray[indexPath.row].id
            LocationTextfield.placeholder = ""
            addview.isHidden = true
            newview.isHidden = true
            myTableView.isHidden = true
            CityapiCalling(apikey:apikey,language_code:language_code,locationid:locationid)
        }else{
            cityOutlet.setTitle(self.cityArray[indexPath.row].title, for: UIControlState())
            cityid = cityArray[indexPath.row].id
            cityTextfield.placeholder = ""
            addview.isHidden = true
            newview.isHidden = true
            myTableView.isHidden = true
        }
        
    }
    func CityapiCalling(apikey:String,language_code:String,locationid:String){
        self.startactvityIndicator()

        let scriptUrl = buisnessapiClass.GenerateCityfromCountry
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&language_code=\(language_code)&location_code=\(locationid)"
      
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                 
                self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                
                    if self.status == "success"{
                        if  (convertedJsonIntoArray["cities"] as? NSArray) != nil {
                            let cities = convertedJsonIntoArray["cities"] as! NSArray
                          
                            self.cityArray.removeAll()
                            for citie in  (cities as! [[String:Any]])
                            {
                                let contry1 = citie as NSDictionary
                                let keyObj = cityClass()
                                
                                if  (contry1["id"] as? String) != nil {
                                    let id = contry1["id"] as! String
                                    keyObj.id = String(id)
                                    
                                }
                                if  (contry1["title"] as? String) != nil {
                                    let title = contry1["title"] as! String
                                    keyObj.title = title
                                }
                               
                                self.cityArray.append(keyObj)
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    
                    DispatchQueue.main.async
                        {
                            self.myTableView.reloadData()
                            self.stopActivityIndicator()
                           
                            
                            
                            
                    }
                    
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    
    
    @IBAction func LocationCommunication(_ sender: Any) {
        let  config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        
        present(placePicker, animated: true, completion: nil)
        placePicker.delegate = self
        
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace)
    {
        
      
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        Placename = place.name
        placelattitude = place.coordinate.latitude
        placlongitude = place.coordinate.longitude
        let total = String(placelattitude) + "\n" + String(placlongitude)
        mapOutlet.setTitle(total, for: UIControlState())
        pinLocationtextfield.placeholder = ""
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
      
    }
    func apiCalling(){
        self.startactvityIndicator()
        
        var createdArray = NSMutableArray()
        
        var bigDic: NSMutableDictionary = NSMutableDictionary()
        var objectArray = NSMutableArray()
        var bizinfoArray = NSMutableArray()
        var dic : NSMutableDictionary = NSMutableDictionary()
         var bizinfodic : NSMutableDictionary = NSMutableDictionary()
        for i in 0..<startTimearray.count
        {
            
            dic = NSMutableDictionary()
            dic.setValue(startTimearray[i], forKey: "biz_start_hours")
            dic.setValue(endTimearray[i], forKey: "biz_end_hours")
            dic.setValue(daynumberArray[i], forKey: "biz_days")
            objectArray.add(dic)
        }
        for i in 0..<value.count
        {
            
            bizinfodic = NSMutableDictionary()
            bizinfodic.setValue(id[i], forKey: "id")
            bizinfodic.setValue(value[i], forKey: "value")
            bizinfoArray.add(bizinfodic)
        }
    
 
        
        bigDic.setValue(addresslineTextfield.text, forKey: "address_one")
        bigDic.setValue(apikey, forKey: "apikey")
        bigDic.setValue(Addressline2Textfield.text, forKey: "address_two")
        bigDic.setValue(language_code, forKey: "langauge_code")
         bigDic.setValue(country_code, forKey: "country_code")
        bigDic.setValue(objectArray, forKey: "biz_hours")
        bigDic.setValue(objectArray, forKey: "biz_hours")
        bigDic.setValue(biz_desc, forKey: "biz_desc")
        bigDic.setValue(bizinfoArray, forKey: "biz_info")
        
        bigDic.setValue(biz_title, forKey: "biz_title")
        bigDic.setValue(category_id, forKey: "category_id")
        bigDic.setValue(cityid, forKey: "city_code")
        
        bigDic.setValue(keywords, forKey: "keywords")
        bigDic.setValue(placelattitude, forKey: "latitude")
        bigDic.setValue(placlongitude, forKey: "longitude")
        
        bigDic.setValue(phone, forKey: "phone")
        bigDic.setValue(website, forKey: "website")
        bigDic.setValue(zipcodeTextfield.text, forKey: "zip_code")
           bigDic.setValue(locationid, forKey: "location_code")

        do {
            
            
            
            //Convert to Data
            
            let jsonData = try! JSONSerialization.data(withJSONObject: bigDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            // print(jsonData)
            
            //Convert back to string. Usually only do this for debugging
            
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                
                //ethaann nink veenda string
                
                // json aayit kittum
                
                let scriptUrl = buisnessapiClass.PostCreateBusiness
                let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
                let myUrl = URL(string: urlWithParams);
                var request = URLRequest(url:myUrl!)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpBody = jsonData
                let task = URLSession.shared.dataTask(with: request)
                {
                    data, response, error in
                    
                    if error != nil
                    {
                        print("error=\(error)")
                        DispatchQueue.main.async
                            {
                                print("server down")
                        }
                        return
                    }
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                   
                    do{
                        
                        if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                            
                        {
                            
                            self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                          
                            
                        }
                        DispatchQueue.main.async
                            {
                                

                                if self.status == "success"{
                                            self.stopActivityIndicator()
//                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "busss") as! buisnessViewController
//                                let navController = UINavigationController(rootViewController: vc)
//                                self.present(navController, animated: true, completion: nil)
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
                                    if let vcs = tabbarVC.viewControllers,
                                        
                                        let nc = vcs.first as? UINavigationController,
                                        let pendingOverVC = nc.topViewController as? buisnessViewController {
                                        
                                        
                                    }
                                    //    let navController = UINavigationController(rootViewController: vcs)
                                    
                                    self.present(tabbarVC, animated: false, completion: nil)
                                    
                                    
                                //self.imageUploading(selectedImage:self.selectedImage)
                                }else{
                                      self.stopActivityIndicator()
                                }
                                
                                
                        }//dispatch ends
                        
                    }//do ends
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    
                }
                
                
                
               
                
                task.resume()
                
                
            }
            
        }
        
    }
    func alertsnack(message: String){
    
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = "Done"
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }

    func showAlertButtonTapped() {
        
        // create the alert
        let alert = UIAlertController(title: "ALERT", message: NSLocalizedString("Select Location", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title:  NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}
