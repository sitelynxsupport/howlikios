//
//  getDirectionViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/19/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import GoogleMaps

class getDirectionViewController: UIViewController,GMSMapViewDelegate {
    
    @IBOutlet var mapview: GMSMapView!
    var latitude = ""
    var longitude = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SetBackBarButtonCustom()
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getDirection()
//        let button = UIButton(frame: CGRect(x: mapview.frame.width - 100, y:mapview.frame.height - 100, width: 50, height: 50))
//        button.setImage(UIImage(named : "icons8-route-48"), for: UIControlState())
//        button.setTitleColor(.red, for: .normal)
//        button.backgroundColor = UIColor.green
//        button.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
//        mapview.addSubview(button)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func getDirection(){
        let marker = GMSMarker()
      
        marker.position = CLLocationCoordinate2DMake((latitude as NSString).doubleValue,(longitude as NSString).doubleValue)
        
        ///View for Marker
        let DynamicView = UIView(frame: CGRect(x:0, y:0, width:50, height:50))
        DynamicView.backgroundColor = UIColor.clear
        
        //Pin image view for Custom Marker
        let imageView = UIImageView()
        imageView.frame  = CGRect(x:0, y:0, width:50, height:50)
        // imageView.image = UIImage(named:"LocationPin")
        imageView.image = (UIImage.fontAwesomeIcon(name:  FontAwesome.mapMarker, textColor: UIColor.red,size: CGSize(width: 50, height: 50)))
        
        //Adding pin image to view for Custom Marker
        DynamicView.addSubview(imageView)
        
        UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        marker.icon = imageConverted
        marker.map = mapview
        mapview.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 16)
        mapview.delegate = self
        print("You tapped a button")
       
    
       
    
    }
    @objc func handleTap(_ sender: UIButton) {
        let DEVICE_LAT =  UserDefaults.standard.double(forKey: "deviceLatitude")
        let DEVICE_LONG =  UserDefaults.standard.double(forKey: "deviceLongitude")
        print("hjelooooooo")
        print(DEVICE_LAT)
        print(DEVICE_LONG)
        let googleURL = NSURL(string: "comgooglemaps://")
        
        if(UIApplication.shared.canOpenURL(googleURL! as URL)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=\(DEVICE_LAT),\(DEVICE_LONG)&daddr=\(Float(latitude)!),\(Float(longitude)!)&directionsmode=driving")!, options: [:], completionHandler: nil)
        }
       
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        let button = UIButton(frame: CGRect(x: mapview.frame.width - 60, y:mapview.frame.height - 60, width: 50, height: 50))
        button.setImage(UIImage(named : "icons8-route-48"), for: UIControlState())
        button.setTitleColor(.red, for: .normal)
        button.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
        mapview.addSubview(button)
         return true
    }
}
