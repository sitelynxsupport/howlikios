//
//  hourTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/19/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class hourTableViewCell: UITableViewCell {
    
    @IBOutlet var endTimeTextfield: UITextField!
    
    @IBOutlet var dayLabel: UILabel!
  
    @IBOutlet var startTimeTextfieldOutlet: UITextField!
    @IBOutlet var mainview: UIView!
   
    

    var cancelButton = UIBarButtonItem()
    var doneButton = UIBarButtonItem()
    var spaceButton = UIBarButtonItem()
    let toolBar = UIToolbar()
    override func awakeFromNib() {
        super.awakeFromNib()
        
           startTimeTextfieldOutlet.layer.borderWidth = 1.0
            endTimeTextfield.layer.borderWidth = 1.0
        startTimeTextfieldOutlet.layer.borderColor = UIColor.lightGray.cgColor
          endTimeTextfield.layer.borderColor = UIColor.lightGray.cgColor
      //  startTimeTextfieldOutlet.layer.masksToBounds = true
        //  endTimeTextfield.layer.cornerRadius = 5.0
        //   endTimeTextfield.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func starttimeAction(_ sender: Any) {
      
    }
 
    
    
    @IBAction func endTimeAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        endTimeTextfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerendValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self,action:#selector(hourTableViewCell.cancelendtimeClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        endTimeTextfield.inputAccessoryView = toolBar
        
    }
    @objc func datePickerendValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "HH:mm a"
        
        // Apply date format
        endTimeTextfield.text = dateFormatter.string(from: sender.date)
       
        
    }
    @objc func cancelendtimeClick() {
        endTimeTextfield.text = ""
        endTimeTextfield.resignFirstResponder()
        
    }
    
}
