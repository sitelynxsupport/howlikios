//
//  mapTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import GoogleMaps

class mapTableViewCell: UITableViewCell {

    @IBOutlet var maplabel: UILabel!
    @IBOutlet var mapview: GMSMapView!
    
    typealias openGoogleMapTapped = (mapTableViewCell) -> Void
    var openGoogleMapTappedAction : openGoogleMapTapped?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func googleMapsBtn(_ sender: Any) {
        openGoogleMapTappedAction?(self)
    }
    
    func setOpenGoogleMapAction(success:openGoogleMapTapped?) {
        openGoogleMapTappedAction = success
    }
    
    
}
