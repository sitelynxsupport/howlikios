//
//  paypalViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/26/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView
import FontAwesome_swift
class paypalViewController: UIViewController,UIWebViewDelegate,NVActivityIndicatorViewable{
    @IBOutlet var webview: UIWebView!
 let buisnessapiClass = ApiListViewcontroller()
    var biz_id = ""
    var apikey = ""
    var language_code = ""
    var country_code = ""
    var curr_code = ""
    var lan_id = ""
    var sender_id = ""
    var recipient_name = ""
    var gift_quantity = ""
    var gift_amount = ""
    var recipient_email = ""
    var recipient_message = ""
    var sender_name = ""
     var pay_now = ""
    
//
//    override func loadView() {
//        let webConfiguration = WKWebViewConfiguration()
//        webview = WKWebView(frame: .zero, configuration: webConfiguration)
//        webview.uiDelegate = self
//        view = webview
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ADD PAYPAL"
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
       
        
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        }
        if  UserDefaults.standard.string(forKey:"senderid") != nil{
            
            self.sender_id = UserDefaults.standard.string(forKey: "senderid")!
        }
        // Do any additional setup after loading the view.
     
        posttableapiCalling(apikey:apikey,biz_id:biz_id,curr_code:curr_code,lan_id:lan_id,gift_amount:gift_amount,recipient_name:recipient_name,gift_quantity:"1",recipient_email:recipient_email,recipient_message:recipient_message,sender_name:sender_name,sender_id:sender_id,pay_now:"pay_now")
        
        
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
func posttableapiCalling(apikey:String,biz_id:String,curr_code:String,lan_id:String,gift_amount:String,recipient_name:String,gift_quantity:String,recipient_email:String,recipient_message:String,sender_name:String,sender_id:String,pay_now:String){
     //   self.startactvityIndicator()
    let urlstring = buisnessapiClass.PostCreateGiftCertificateb
    let url = URL(string: urlstring)!
    let request = NSMutableURLRequest(url: url as URL)
    request.httpMethod = "POST"
    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    let query :[String:String] = ["apikey":apikey,"biz_id":biz_id,"curr_code":curr_code,"lan_id":lan_id,"gift_amount":gift_amount,"recipient_name":recipient_name,"gift_quantity":"1","recipient_email":recipient_email,"recipient_message":recipient_message,"sender_name":sender_name,"sender_id":sender_id,"pay_now":"pay_now"]
    let baseurl = URL(string:urlstring)!
    let postString = (baseurl.withQueries(query)!).query
    request.httpBody = postString?.data(using: .utf8)
    webview.loadRequest(request as URLRequest)
    
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading{
            return
        }
        self.stopActivityIndicator()
        print("Done loading")
    }
   
}
extension URL{
    func withQueries(_ queries:[String:String]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.queryItems = queries.flatMap{URLQueryItem(name: $0.0, value: $0.1)}
        return components?.url
    }
    
    func justQueries(_ queries:[String:String]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.queryItems = queries.flatMap{URLQueryItem(name: $0.0, value: $0.1)}
        return components?.url
    }
    
}


