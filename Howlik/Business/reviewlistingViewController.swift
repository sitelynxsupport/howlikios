//
//  reviewlistingViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/16/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Cosmos
import FontAwesome_swift
import Alamofire
import SDWebImage
import NVActivityIndicatorView

class reviewlistingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
    let buisnessapiClass = ApiListViewcontroller()
    var reviews_Array = [reviewClass]()
    var biz_id = ""
    var apikey  = ""
    var city_code = ""
    var country_code = ""
    var onpage = 1
    var shouldCheckMore = true
    
    @IBOutlet var reviewtableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        }
        self.navigationItem.title = NSLocalizedString("Reviews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        // Do any additional setup after loading the view.
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        
        dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.reviews_Array.removeAll()
        self.LoadupdatebuisnessapiCalling(apikey:apikey,biz_id:biz_id,page: onpage)
  
    }
    
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 30, height: 30)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    
    func LoadupdatebuisnessapiCalling(apikey:String,biz_id:String,page:Int){
        //  self.startactvityIndicator()
        startactvityIndicator()
        let scriptUrl = buisnessapiClass.SingleBusinessReviewListing
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id,"page":page]
     
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                     var freshArray = [reviewClass]()
//                        let business = response.object(forKey: "business")!
                        
                      
                        
                        let reviews =  response.object(forKey: "reviews")!
                        
                        for review in (reviews as! [[String:Any]]){
                            let reviewsElement = reviewClass()
                            let contry3 = review as NSDictionary
                            if  (contry3["expense"] as? String) != nil {
                                let expense = contry3["expense"] as! String
                                reviewsElement.expense = String(expense)
                                
                            }
                            if  (contry3["rating"] as? String) != nil {
                                let rating = contry3["rating"] as! String
                                reviewsElement.rating = rating
                            }
                            if  (contry3["date"] as? String) != nil {
                                let date = contry3["date"] as! String
                                reviewsElement.date = date
                            }
                            if  (contry3["id"] as? NSInteger) != nil {
                                let id = contry3["id"] as! NSInteger
                                reviewsElement.id = String(id)
                            }
                            if  (contry3["person"] as? String) != nil {
                                let person = contry3["person"] as! String
                                reviewsElement.person = person
                            }
                            if  (contry3["photo"] as? String) != nil {
                                let photo = contry3["photo"] as! String
                                let image = "http://www.howlik.com/" + photo
                                reviewsElement.photo = image
                            }
                            if  (contry3["rating"] as? String) != nil {
                                let rating = contry3["rating"] as! String
                                reviewsElement.rating = rating
                            }
                            if  (contry3["review"] as? String) != nil {
                                let review = contry3["review"] as! String
                                reviewsElement.review = review
                            }
                            freshArray.append(reviewsElement)
                            self.reviews_Array.append(reviewsElement)
                        }
                        
                        DispatchQueue.main.async
                            {
                                if(freshArray.count == 0){
                                    self.shouldCheckMore = false
                                }
                                self.reviewtableview.reloadData()
                                self.stopActivityIndicator()
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return reviews_Array.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == reviews_Array.count - 1 {
            if shouldCheckMore {
                onpage = onpage+1
                self.LoadupdatebuisnessapiCalling(apikey:apikey,biz_id:biz_id,page: onpage)
            }
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! revielistTableViewCell
        cell.reviewimage.sd_setImage(with: URL(string:reviews_Array[indexPath.row].photo), placeholderImage:UIImage(named: "no-image02"))
        cell.reviewimage.layer.cornerRadius = 10.0
        cell.reviewimage.layer.masksToBounds = true
        cell.ratingview.settings.updateOnTouch = false
        cell.ratingview.settings.fillMode = .precise
        cell.ratingview.settings.updateOnTouch = false
        cell.ratingview.settings.fillMode = .precise
        cell.reviewpersonname.text = reviews_Array[indexPath.row].person
        cell.datelabel.text = reviews_Array[indexPath.row].date
        cell.discriptionlabel.text = reviews_Array[indexPath.row].review
        cell.ratingview.rating =  (reviews_Array[indexPath.row].rating as NSString).doubleValue
        cell.dollarview.rating = (reviews_Array[indexPath.row].expense as NSString).doubleValue
        return cell
    }
    
}
