//
//  singleImageTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/8/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Cosmos
class singleImageTableViewCell: UITableViewCell {
    
    @IBOutlet var reviews: UILabel!
    @IBOutlet var businessTitle: UILabel!
    @IBOutlet var workingstatus: UILabel!
    
    @IBOutlet var workinglabel: UILabel!
    
    @IBOutlet var cosmosdollar: CosmosView!
    @IBOutlet var cosmosrating: CosmosView!
    @IBOutlet var transview: UIView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
