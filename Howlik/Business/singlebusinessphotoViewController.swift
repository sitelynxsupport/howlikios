//
//  singlebusinessphotoViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Cosmos
import FontAwesome_swift
import Alamofire
import SDWebImage
class singlebusinessphotoViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
var biz_index = Int()
    let buisnessapiClass = ApiListViewcontroller()
    var image_Array = [imageClass]()
    var biz_id = ""
    var apikey  = ""
    var city_code = ""
    var country_code = ""
    var imgarray = [String]()

    
    @IBOutlet var singlephotocollectioview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
       
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        }
        
        if biz_index == 0 {
              self.navigationItem.title = "1" + " " + NSLocalizedString("Out of", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") + " " + String(imgarray.count)

        }else{
           
            self.navigationItem.title = String(biz_index + 1) + " " + NSLocalizedString("Out of", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") + " " + String(imgarray.count)

        }
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        // Do any additional setup after loading the view.
        
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        
        dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
      //  self.LoadupdatebuisnessapiCalling(apikey:apikey,biz_id:biz_id,country_code:country_code, city_code:city_code)
        self.singlephotocollectioview.reloadData()
          self.singlephotocollectioview.isPagingEnabled   = true
        self.singlephotocollectioview.scrollToItem(at:IndexPath(item: self.biz_index, section: 0), at: .left, animated: true)
     
      
    }

    func LoadupdatebuisnessapiCalling(apikey:String,biz_id:String,country_code:String,city_code:String){
        //  self.startactvityIndicator()
        
        image_Array.removeAll()
        
        let scriptUrl = buisnessapiClass.SingleBusinessDetails
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id,"country_code":country_code,"city_code":city_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
            
                        let business = response.object(forKey: "business")!
                  
                        
                        
                        let images =  response.object(forKey: "images")!
                        
                        
                        for image in (images as! [[String:Any]]){
                            var imageElement = imageClass()
                            let contry3 = image as NSDictionary
                            if  (contry3["id"] as? String) != nil {
                                
                                let id = contry3["id"] as! String
                                imageElement.id = String(id)
                            }
                            if  (contry3["image"] as? String) != nil {
                                let image = contry3["image"] as! String
                                let foto = "http://www.howlik.com/" + image
                                imageElement.image = foto
                            }
                            
                            
                            self.image_Array.append(imageElement)
                        }
                        
                        
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                
                              
                               
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
  
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgarray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! singlephotoCollectionViewCell
        
        cell.singlephoto.sd_setImage(with: URL(string:imgarray[indexPath.row]),
                                   placeholderImage:UIImage(named: "no-image02"))
        
        return cell
    }
    //set paging for collectionview
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let frameSize = collectionView.frame.size
        return CGSize(width: frameSize.width - 10, height: frameSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let indexPath = singlephotocollectioview.indexPathsForVisibleItems.first {
            self.navigationItem.title = String(indexPath.row + 1) + " " + NSLocalizedString("Out of", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") + " " + String(imgarray.count)
        }
    }
  
}
