//
//  singlebusinessviiewViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/8/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import ParallaxHeader
import SnapKit
import Alamofire
import FontAwesome_swift
import GoogleMaps
import SDWebImage
import SCLAlertView
import Photos

class singlebusinessviiewViewController: UIViewController,UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate,GMSMapViewDelegate {
    
    @IBOutlet var view1: UITableView!
    @IBOutlet var HeaderView: UIView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    var previousScrollOffset = CGFloat()
    let maxHeaderHeight: CGFloat = 172;
    let minHeaderHeight: CGFloat = 0;
    let absoluteTop: CGFloat = 0;
    var buttonIndex = Int()
   let myScrollView = UIScrollView()
    var storeLat = ""
    var storelon = ""
    var biz_id = ""
    var apikey  = ""
    var city_code = ""
    var country_code = ""
    var phone = ""
    var web = ""
    var addview = UIView()
    var myTableView = UITableView()
    var newview = UIView()
      var biz_Array = [singlebusinessClass]()
    var offerArray = [offerClass]()
      var image_Array = [imageClass]()
    var hour_Array = [hourClass]()
      var ament_Array = [amentClass]()
       let buisnessapiClass = ApiListViewcontroller()
    var reviews_Array = [reviewClass]()
       var location_Array = [singlelocationClass]()
    var biz_title = ""
      let imageView = UIImageView()
    var labelWidth = CGFloat()
    var day = ""
    var hourz = ""
    var status = ""
    var nof_reviews = ""
      var avg_ratings = ""
    var id = ""
    var own_business = ""
    var avg_expense = ""
    var location_title = ""
    var latitude = ""
      var longitude = ""
       var bangroundview = UIView()
       var customView = UIView()
    var locationid  = ""
     var sourceType = Int()
    var event_imageName = ""
   var booking = ""
      var booking_type = ""
    var activityclass = emailValidateViewController()
      var tablecatheightindicator =  Int()
 var dayarray = [String]()
    var buttonName = [NSLocalizedString("GetDirection", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Call", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Website", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Gift Cards", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
       var buttonImage = ["b_direction","b_call","b_website","b_gift"]
    var button = UIButton()
     let imagePicker = UIImagePickerController()
      var selectedImage = UIImage()
  var biz_url = ""
    var reportview = ReportView()
    var avlDayTitleArray = [String:String]()
    var hourDayTitle = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view1.register(UINib(nibName: "businessHoursHeadingCell", bundle: nil), forCellReuseIdentifier: "businessHoursHeading")
        self.myTableView.register(UINib(nibName: "businessHoursHeadingCell", bundle: nil), forCellReuseIdentifier: "businessHoursHeading")
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        imageView.isUserInteractionEnabled = true
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.country_code = UserDefaults.standard.string(forKey: "country_code")!
           // ,country_code:country_code
          
        }
//        dayarray = ["0","1","2","3","4","5","6"]
//        
//        for i in 0..<self.dayarray.count{
//            let hourElement = hourClass()
//            if self.dayarray[i] == "0"{
//                hourElement.biz_days = "Monday"
//                hourElement.biz_start_hours = "Closed"
//                hourElement.biz_end_hours = ""
//            }else if self.dayarray[i] == "1"{
//                hourElement.biz_days = "Tuesday"
//                hourElement.biz_start_hours = "Closed"
//                hourElement.biz_end_hours = ""
//            }else if self.dayarray[i] == "2"{
//                hourElement.biz_days = "Wednesday"
//                hourElement.biz_start_hours = "Closed"
//                hourElement.biz_end_hours = ""
//            }else if self.dayarray[i] == "3"{
//                hourElement.biz_days = "Thursday"
//                hourElement.biz_start_hours = "Closed"
//                hourElement.biz_end_hours = ""
//            }else if self.dayarray[i] == "4"{
//                hourElement.biz_days = "Friday"
//                hourElement.biz_start_hours = "Closed"
//                hourElement.biz_end_hours = ""
//            }else if self.dayarray[i] == "5"{
//                hourElement.biz_days = "Saturday"
//                hourElement.biz_start_hours = "Closed"
//                hourElement.biz_end_hours = ""
//            }else if self.dayarray[i] == "6"{
//                hourElement.biz_days = "Sunday"
//                hourElement.biz_start_hours = "Closed"
//                hourElement.biz_end_hours = ""
//            }
//            self.hour_Array.append(hourElement)
//        }
       self.LoadupdatebuisnessapiCalling(apikey:apikey,biz_id:biz_id,city_code:city_code)
         navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
   newview.isHidden = true
          button.isHidden = true
       // view1.estimatedRowHeight = 44.0
       // view1.rowHeight = UITableViewAutomaticDimension
      // UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
          self.imageView.isUserInteractionEnabled = true
    }
  
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    override func viewDidAppear(_ animated: Bool) {
          createview()
        newview.isHidden = true
          button.isHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
          self.imageView.isUserInteractionEnabled = true
      
      
    }
func LoadupdatebuisnessapiCalling(apikey:String,biz_id:String,city_code:String){
  activityclass.startactvityIndicator()
    //,country_code:String
    biz_Array.removeAll()
    image_Array.removeAll()
    reviews_Array.removeAll()
    ament_Array.removeAll()
    hour_Array.removeAll()
    self.location_Array.removeAll()
    self.offerArray.removeAll()
    
        let scriptUrl = buisnessapiClass.SingleBusinessDetails
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id,"country_code":country_code,"city_code":city_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        var dayElements = Set<String>()
                        if response["biz_url"] != nil{
                    self.biz_url = response.object(forKey: "biz_url")! as! String
                        }
                   // if response["business"] != nil{
                   let business = response.object(forKey: "business")!
                       // print(business)
                          var homeElement = singlebusinessClass()
                        let contry1 = business as! NSDictionary
                        if  (contry1["biz_image"] as? String) != nil {
                     let biz_image = contry1["biz_image"] as! String
                          let image = "http://www.howlik.com/" + biz_image
                            homeElement.imageUrl = image
                        }
                       
                      if  (contry1["biz_title"] as? String) != nil {
                            self.biz_title = contry1["biz_title"] as! String
                        
                        }
                        
                        if  (contry1["id"] as? String) != nil {
                            self.id = String(contry1["id"] as! String)
                          
                            
                        }
                        if  (contry1["own_business"] as? String) != nil {
                            self.own_business = String(contry1["own_business"] as! String)
                         
                            
                        }
                        
                        
                        if  (contry1["lat"] as? String) != nil {
                            self.storeLat = contry1["lat"] as! String

                        }
                        
                        if  (contry1["booking"] as? String) != nil {
                            self.booking = contry1["booking"] as! String
                            
                        }
                        if  (contry1["booking_type"] as? String) != nil {
                            self.booking_type = contry1["booking_type"] as! String
                            
                        }
                        if  (contry1["lon"] as? String) != nil {
                            self.storelon = contry1["lon"] as! String

                        }
                        if  (contry1["phone"] as? String) != nil {
                            self.phone = contry1["phone"] as! String
                            
                        }
                        if  (contry1["web"] as? String) != nil {
                            self.web = contry1["web"] as! String
                            
                        }
                        if  (contry1["location_title"] as? String) != nil {
                            self.location_title = contry1["location_title"] as! String
                            
                        }
                        if  (contry1["nof_reviews"] as? NSInteger) != nil {
                            self.nof_reviews = String((contry1["nof_reviews"] as!  NSInteger))
                            
                            
                        }else{
                              self.nof_reviews  = "***************"
                        }
                        if  (contry1["avg_ratings"] as? Float) != nil {
                            self.avg_ratings = String(contry1["avg_ratings"] as! Float)
                            
                        }else{
                            
                        }
                        if  (contry1["avg_expense"] as? Float) != nil {
                            self.avg_expense = String(contry1["avg_expense"] as! Float)
                          
                        }else{
                           
                        }
                    
                      
                        if  (contry1["biz_hours_ios"] as? NSArray) != nil {
                            let biz_hours_ios = contry1["biz_hours_ios"] as! NSArray
                            for biz_hour in (biz_hours_ios as! [[String:Any]]){
                                let hourElement = hourClass()
                                let contry3 = biz_hour as NSDictionary
                              if  (contry3["biz_days"] as? String) != nil {
                                    let biz_days = contry3["biz_days"] as! String
                               if biz_days == "0"{
                                        hourElement.biz_days = "Monday"
                                  }
                                if biz_days == "1"{
                                           hourElement.biz_days = "Tuesday"
                                    }
                                if biz_days == "2"{
                                        hourElement.biz_days = "Wednesday"
                                    }
                                if biz_days == "3"{
                                        hourElement.biz_days = "Thursday"
                                    }
                                if biz_days == "4"{
                                        hourElement.biz_days = "Friday"
                                    }
                                if biz_days == "5"{
                                        hourElement.biz_days = "Saturday"
                                 }
                                if biz_days == "6"{
                                        hourElement.biz_days = "Sunday"
                                }
                                    if  (contry3["biz_start_hours"] as? String) != "" {
                                        let biz_start_hours = contry3["biz_start_hours"] as! String
                                         hourElement.biz_start_hours = biz_start_hours
                                    }else{
                                        hourElement.biz_start_hours = "Closed"
                                }
                                    if  (contry3["biz_end_hours"] as? String) != "" {
                                        let biz_end_hours = contry3["biz_end_hours"] as! String
                                        hourElement.biz_end_hours = biz_end_hours
                                    }else{
                                        hourElement.biz_end_hours = ""
                                }
                                
                     
                                    
                                 self.hour_Array.append(hourElement)
                           }
                                
                               
                                for index in 0..<self.hour_Array.count{
                                    var eachDayTimeArray = [String]()
                                    var day = String()
                                    self.hour_Array.forEach({ (eachDay) in
                                        day = eachDay.biz_days
                                        if(eachDay.biz_days == self.hour_Array[index].biz_days){
                                            eachDayTimeArray.append(eachDay.biz_start_hours + "-" + eachDay.biz_end_hours)
                                        }
                                    })
                                    self.avlDayTitleArray[day] = eachDayTimeArray.joined(separator:"\n")
                                    dayElements.insert(day)
                                }
                               
                                 self.hourDayTitle = Array(dayElements)
                                let weekDayNumbers = [
                                    "Sunday": 0,
                                    "Monday": 1,
                                    "Tuesday": 2,
                                    "Wednesday": 3,
                                    "Thursday": 4,
                                    "Friday": 5,
                                    "Saturday": 6,
                                ]
                                self.hourDayTitle.sort(by: { (weekDayNumbers[$0] ?? 7) < (weekDayNumbers[$1] ?? 7) })
                            }

                          
                        }
                    
                        let moreinfo = response.object(forKey: "moreinfo")!
                        for moreinf in (moreinfo as! [[String:Any]]){
                             var amentElement = amentClass()
                            let contry2 = moreinf as NSDictionary
                            if  (contry2["id"] as? String) != nil {
                                let id1 = contry2["id"] as! String
                           
                        if  (contry1["more_info"] as? NSArray) != nil {
                            let more_infos = contry1["more_info"] as! NSArray
                            for more_info in (more_infos as! [[String:Any]]){
                                let contry3 = more_info as NSDictionary
                                if  (contry3["id"] as? NSInteger) != nil {
                                    let id = contry3["id"] as! NSInteger
                                   
                                    if String(id) == id1{
                                        if  (contry3["value"] as? String) != nil {
                                            let value = contry3["value"] as! String
                                         
                                            if value == "1"{
                                                   amentElement.value = "NO"
                                            }else{
                                                  amentElement.value = "YES"
                                            }
                                        }
                              if  (contry2["title"] as? String) != nil {
                                            let title = contry2["title"] as? String
                                        amentElement.title = title!
                                
                                      }
                                        self.ament_Array.append(amentElement)
                                    }
                                   
                                }
    
                                    
                               
                            }
                                }
                            }
                            
                        }

            
                            let locations = response.object(forKey: "locations")!
                            for location in (locations as! [[String:Any]]){
                                var locElement = singlelocationClass()
                                let contry3 = location as NSDictionary
                                if  (contry3["address_1"] as? String) != nil {
                                    let address_1 = contry3["address_1"] as! String
                                        locElement.address_1 = address_1
                                }
                                    if  (contry3["address_2"] as? String) != nil {
                                        let address_2 = contry3["address_2"] as! String
                                        locElement.address_2 = address_2
                                    }
                                    if  (contry3["phone"] as? String) != nil {
                                        let phone = contry3["phone"] as! String
                                        locElement.phone = phone
                                    }
                                if  (contry3["location_id"] as? String) != nil {
                                    let location_id = contry3["location_id"] as! String
                                    locElement.location_id = location_id
                                }
                                
                                if  (contry3["lat"] as? String) != nil {
                                    let latitudes = contry3["lat"] as! String
                                    locElement.latitude = latitudes
                                }
                                if  (contry3["lon"] as? String) != nil {
                                    let longitudes = contry3["lon"] as! String
                                    locElement.longitude = longitudes
                                        }
                                      self.location_Array.append(locElement)
                            
                                    
                                }
                            
                           
                            
                            
                      
                        let businessOpen = response.object(forKey: "businessOpen")!
                        let contry2 = businessOpen as! NSDictionary
                        if  (contry2["day"] as? String) != nil {
                            self.day = contry2["day"] as! String
                         
                        }
                        if  (contry2["hourz"] as? String) != nil {
                          self.hourz = contry2["hourz"] as! String
                         
                            
                        }
                        if  (contry2["status"] as? String) != nil {
                        self.status = contry2["status"] as! String
                            
                            
                        }
                        
                        
                        let reviews =  response.object(forKey: "reviews")!
                      
                            for review in (reviews as! [[String:Any]]){
                                var reviewsElement = reviewClass()
                                let contry3 = review as NSDictionary
                                if  (contry3["expense"] as? String) != nil {
                                    let expense = contry3["expense"] as! String
                                    reviewsElement.expense = String(expense)
                                   
                                }
                                if  (contry3["rating"] as? String) != nil {
                                    let rating = contry3["rating"] as! String
                                    reviewsElement.rating = rating
                                }
                                if  (contry3["date"] as? String) != nil {
                                    let date = contry3["date"] as! String
                                    reviewsElement.date = date
                                }
                                if  (contry3["id"] as? NSInteger) != nil {
                                    let id = contry3["id"] as! NSInteger
                                    reviewsElement.id = String(id)
                                }
                                if  (contry3["person"] as? String) != nil {
                                    let person = contry3["person"] as! String
                                    reviewsElement.person = person
                                }
                                if  (contry3["photo"] as? String) != nil {
                                    let photo = contry3["photo"] as! String
                                     let image = "http://www.howlik.com/" + photo
                                    reviewsElement.photo = image
                                }
                                if  (contry3["rating"] as? String) != nil {
                                    let rating = contry3["rating"] as! String
                                    reviewsElement.rating = rating
                                }
                                if  (contry3["review"] as? String) != nil {
                                    let review = contry3["review"] as! String
                                    reviewsElement.review = review
                                }
                                 self.reviews_Array.append(reviewsElement)
                            }
                        
                        let offers =  response.object(forKey: "offers")!
                        
                        for offer in (offers as! [[String:Any]]){
                            var offerElement = offerClass()
                            let contry3 = offer as NSDictionary
                            if  (contry3["offertype"] as? String) != nil {
                                let offertype = contry3["offertype"] as! String
                                offerElement.offertype = offertype
                                
                            }
                            if  (contry3["percent"] as? String) != nil {
                                let percent = contry3["percent"] as! String
                                offerElement.percent = percent
                            }
                            if  (contry3["content"] as? String) != nil {
                                let content = contry3["content"] as! String
                                offerElement.content = content
                            }
                           
                           
                            self.offerArray.append(offerElement)
                        }
                        
                      
                      let images =  response.object(forKey: "images")!
               
                        
                            for image in (images as! [[String:Any]]){
                                 var imageElement = imageClass()
                                let contry3 = image as NSDictionary
                                if  (contry3["id"] as? String) != nil {
                                   
                                    let id = contry3["id"] as! String
                                    imageElement.id = String(id)
                                }
                                if  (contry3["image"] as? String) != nil {
                                    let image = contry3["image"] as! String
                                    let foto = "http://www.howlik.com/" + image
                                    imageElement.image = foto
                                }
                              
                           
                             self.image_Array.append(imageElement)
                        }
                        self.biz_Array.append(homeElement)
                      
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                              
                                
                                self.createtableview()
                                
                                
                               
                               // self.imageView.isUserInteractionEnabled = true
                                self.imageView.sd_setImage(with: URL(string:homeElement.imageUrl), placeholderImage:UIImage(named: "no-image02"))
                               
                                self.imageView.contentMode = .scaleAspectFill
                           
                                self.imageView.blurView.setup(style: UIBlurEffectStyle.dark, alpha: 0).enable()
                                self.view1.parallaxHeader.view = self.imageView
                                self.view1.parallaxHeader.height = 250
                                self.view1.parallaxHeader.minimumHeight = 40
                                self.view1.parallaxHeader.mode = .centerFill
                             //   self.view1.backgroundColor = UIColor.red
                                //self.imageView.isUserInteractionEnabled = true
                                self.imageView.blurView.vibrancyContentView?.backgroundColor =  UIColor.red
                              self.imageView.backgroundColor =  UIColor.red
                                self.view1.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
                                    //update alpha of blur view on top of image view
                                    self.imageView.blurView.setup(style: UIBlurEffectStyle.dark, alpha: 1).enable()
                                    parallaxHeader.view.blurView.alpha = 1 - parallaxHeader.progress
                                }

                                // Label for vibrant text
                                let vibrantLabel = UILabel()
                                vibrantLabel.text = self.biz_title
                                vibrantLabel.font = UIFont.systemFont(ofSize: 20.0)
                                vibrantLabel.sizeToFit()
                                vibrantLabel.textAlignment = .center
                      
                                let btnLeftMenu: UIButton = UIButton()
                       
                                btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
                             btnLeftMenu.addTarget(self, action: #selector(self.onClcikBack), for: UIControlEvents.touchUpInside)
                              btnLeftMenu.frame = CGRect(x: 0, y: 10, width: 40, height: 40)
                                self.imageView.addSubview(btnLeftMenu)

                                 self.view1.parallaxHeader.view.addSubview(btnLeftMenu)
                           
                                self.view1.reloadData()
                                self.activityclass.stopActivityIndicator()
                            
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    @objc func onClcikBack()
    {
      
        dismiss(animated: true, completion: nil)
        
    }
    func createtableview(){
        return
//        let screenSize: CGRect = UIScreen.main.bounds
//        let window = UIApplication.shared.keyWindow!
//        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
//        window.addSubview(addview)
//        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        let height =  Int(tableviewcatagoryHeight())
//        if tablecatheightindicator == 0{
//            self.bangroundview = UIView(frame: CGRect(x: 15, y: 40, width: Int(screenSize.width - 30) , height: height ))
//        }else{
//            self.bangroundview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
//            bangroundview.center =  view.center
//        }
//       // self.bangroundview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
//        self.addview.addSubview(bangroundview)
//
//        self.myTableView = UITableView(frame: CGRect(x: 0, y: 18, width: bangroundview.frame.width, height: bangroundview.frame.height))
//        myTableView.dataSource = self
//        myTableView.delegate = self
//        myTableView.register(UINib(nibName: "location", bundle: nil), forCellReuseIdentifier: "myIdentifier")
//        bangroundview.addSubview(myTableView)
//        self.myTableView.alwaysBounceVertical = false
//        bangroundview.layer.cornerRadius = 10;
//       // bangroundview.layer.masksToBounds = true
//        myTableView.layer.cornerRadius = 10
//        addview.layer.cornerRadius = 10;
//        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: bangroundview.frame.width , height: 55));
//        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
//        view1.addSubview(label)
//        label.textColor = UIColor.black
//        label.isHighlighted = true
//        view1.backgroundColor  = UIColor.white
//        label.font = UIFont.boldSystemFont(ofSize: 20)
//        self.myTableView.tableHeaderView = view1
//       // addview.layer.masksToBounds = true
//          label.text =  NSLocalizedString("Select Location", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
//        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
//        view1.layer.masksToBounds = true
//        addview.isHidden = false
//        newview.isHidden = false
//        myTableView.isHidden = false
//         myTableView.separatorInset = .zero
//        myTableView.reloadData()
//         myTableView.separatorInset = .zero
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == myTableView{
              return 1
        }
       
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == myTableView{
            return location_Array.count
        }else{
        if section == 0{
            return 1
        }
        else if section == 1 {
            if offerArray.count >= 1{
                 return 1
            }else{
                return 0
            }
           
        }else if section == 2 {
        return 1
        }  else if section == 3 {
              return 1
            
        } else if section == 4 {
              return buttonImage.count
        }else if section == 5{
           return 1
        }else if section == 6{
            return 1
        }else if section == 7{
            if reviews_Array.count > 5 {
                return 5
            }else{
                return reviews_Array.count
            }
        }
        else  if section == 8{
            if(hourDayTitle.count == 0){
                return 0
            }else{
                return hourDayTitle.count+1
            }
            
        }else{
            return ament_Array.count
        }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == myTableView{
           let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! location
                cell.selectionStyle = .none
            cell.addresslabel.text = location_Array[indexPath.row].address_1 + "," + location_Array[indexPath.row].address_2
             cell.phonenumberlabel.text  = location_Array[indexPath.row].phone
            cell.imageview.image = UIImage.fontAwesomeIcon(name: FontAwesome.mapMarker, textColor: UIColor.red, size: CGSize(width: 60, height: 60))
            cell.imageview.tintColor = UIColor.red
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }else{
        if indexPath.section == 0{
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! singleImageTableViewCell
                cell.selectionStyle = .none
        cell.businessTitle.text = self.biz_title
        cell.workingstatus.text = self.status
        cell.workinglabel.text = self.hourz
        cell.workinglabel.isHidden = true  
            cell.cosmosdollar.settings.updateOnTouch = false
            cell.cosmosdollar.settings.fillMode = .precise
            cell.cosmosrating.settings.updateOnTouch = false
            cell.cosmosrating.settings.fillMode = .precise
        cell.cosmosdollar.rating = (avg_expense as NSString).doubleValue
            cell.cosmosrating.rating = (avg_ratings as NSString).doubleValue
        cell.cosmosdollar.settings.updateOnTouch = false
        // Show only fully filled stars
         cell.cosmosdollar.settings.fillMode = .full
        // Other fill modes: .half, .precise
        // Change the size of the stars
         cell.cosmosdollar.settings.starSize = 20
        // Set the distance between stars
         cell.cosmosdollar.settings.starMargin = 5

   cell.reviews.text =  self.nof_reviews + NSLocalizedString("Reviews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell11", for: indexPath) as! OfferTableViewCell
            cell.selectionStyle = .none
            cell.offerCollectionView.tag = 1
            cell.backgroundColor = UIColor.groupTableViewBackground
              cell.offerCollectionView.reloadData()
    
            return cell
        }
        else if indexPath.section == 2 {
        
            let cell = tableView.dequeueReusableCell(withIdentifier:"cellone",for: indexPath) as! ratingTableViewCell
            cell.selectionStyle = .none
            cell.checkinOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.mapMarker, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
            cell.checkinOutlet.setTitle(NSLocalizedString("Location", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
            cell.AddfotoOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.camera, textColor: UIColor.red, size: CGSize(width: 20, height: 20)), for: UIControlState.normal)
            cell.AddfotoOutlet.setTitle(NSLocalizedString("AddPhoto", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
            cell.checkinOutlet.tag = indexPath.row
             cell.checkinOutlet.addTarget(self, action:#selector(self.LocationbuttonView(_:)), for:.touchUpInside)
            cell.ratingAddoutlet.tag = indexPath.row
            cell.ratingAddoutlet.addTarget(self, action:#selector(self.ratingAdd(_:)), for:.touchUpInside)
            cell.AddfotoOutlet.tag = indexPath.row
            cell.AddfotoOutlet.addTarget(self, action:#selector(self.AddFotoAction(_:)), for:.touchUpInside)
              return cell
     

        }else if indexPath.section == 3  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! mapTableViewCell
              cell.selectionStyle = .none
          
     
         let marker = GMSMarker()
         
            marker.position = CLLocationCoordinate2DMake((latitude as NSString).doubleValue,(longitude as NSString).doubleValue)
            
            ///View for Marker
            let DynamicView = UIView(frame: CGRect(x:0, y:0, width:50, height:50))
            DynamicView.backgroundColor = UIColor.clear
            
            //Pin image view for Custom Marker
            let imageView = UIImageView()
            imageView.frame  = CGRect(x:0, y:0, width:40, height:40)
           // imageView.image = UIImage(named:"LocationPin")
            imageView.image = (UIImage.fontAwesomeIcon(name:  FontAwesome.mapMarker, textColor: UIColor.red,size: CGSize(width: 40, height: 40)))
            
            //Adding pin image to view for Custom Marker
            DynamicView.addSubview(imageView)
            
            UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
            DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            marker.icon = imageConverted
            marker.map = cell.mapview
            
            cell.mapview.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 16)
            
            cell.maplabel.text =   self.location_title
            cell.setOpenGoogleMapAction { (success) in
                if((self.storeLat != "" ) && (self.storelon != "")){
                        if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(self.storeLat),\(self.storelon)&directionsmode=driving") {
                            UIApplication.shared.open(url, options: [:])
                        } else {
                            NSLog("Can't use comgooglemaps://");
                    }
                }
                
//                if (UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)) {
//
//                    UIApplication.shared.open(URL(string:  "comgooglemaps://?saddr=&daddr=\(place.latitude),\(place.longitude)&directionsmode=driving")!, options: Any, completionHandler: { (success) in
//                        <#code#>
//                    })
//                }
//
                
                    
                 else {
                    NSLog("Can't use comgooglemaps://");
                    let alert = UIAlertController(title: "Google Maps Launch Error ", message: "Google Maps is not installed on your device, try again after downloading GoogleMaps", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            print("default")
                            
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                            
                        }}))
                    self.present(alert, animated: true, completion: nil)
                }
            
            }
           
            return cell
        }else if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! actionTableViewCell
              cell.selectionStyle = .none
            cell.imgbutton.image = UIImage(named: buttonImage[indexPath.row])
               cell.titlelabel.text = buttonName[indexPath.row]
            cell.actionbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronRight, textColor: UIColor.red, size: CGSize(width: 20, height: 20)), for: UIControlState.normal)
            cell.actionbutton.tintColor = UIColor.gray
            if indexPath.row == 0{
             //   cell.labelheight.multiplier = 1.0
                let newMultiplier:CGFloat = 1.05
                cell.labelheight = cell.labelheight.setMultiplier(multiplier: newMultiplier)
                   cell.optionlabel.text = ""
            }
            else if indexPath.row == 1{
                let newMultiplier:CGFloat = 0.88
                cell.labelheight = cell.labelheight.setMultiplier(multiplier: newMultiplier)
                cell.optionlabel.text = phone
            }
            else if indexPath.row == 2{
                let newMultiplier:CGFloat = 0.88
                cell.labelheight = cell.labelheight.setMultiplier(multiplier: newMultiplier)
                 cell.optionlabel.text = web
            }else{
                    cell.optionlabel.text = ""
                let newMultiplier:CGFloat = 1.1
                cell.labelheight = cell.labelheight.setMultiplier(multiplier: newMultiplier)
                
            }
            
             return cell
        }else if indexPath.section == 5 {
               let cell = tableView.dequeueReusableCell(withIdentifier: "cell5", for: indexPath) as! albumTableViewCell
              cell.selectionStyle = .none
             cell.albumcollectionView.tag = 2
            cell.albumcollectionView.reloadData()
                return cell
        }else if indexPath.section == 6  {
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell9", for: indexPath) as! locationbuttonTableViewCell
              cell.selectionStyle = .none
            cell.locationbuttonOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.shareAlt, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
            
            cell.locationbuttonOutlet.tintColor = UIColor.red
            
            cell.locationbuttonOutlet.setTitle(NSLocalizedString("Share", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
            cell.reportOutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.exclamationTriangle, textColor: UIColor.red, size: CGSize(width: 20, height: 20)), for: UIControlState.normal)
            cell.reportOutlet.setTitle(NSLocalizedString("Report", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
             cell.reportOutlet.tintColor = UIColor.red
            //  cell.reportOutlet.tintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
            cell.reportOutlet.tag = indexPath.row
            cell.reportOutlet.addTarget(self, action:#selector(self.reportAction(_:)), for:.touchUpInside)
            cell.locationbuttonOutlet.tag = indexPath.row
            cell.locationbuttonOutlet.addTarget(self, action:#selector(self.shareAction(_:)), for:.touchUpInside)
            return cell
        }else if indexPath.section == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell6", for: indexPath) as! ReviewTableViewCell
              cell.selectionStyle = .none
        cell.reviewimage.sd_setImage(with: URL(string:reviews_Array[indexPath.row].photo), placeholderImage:UIImage(named: "no-image02"))
            cell.reviewimage.layer.cornerRadius = 10.0
               cell.reviewimage.layer.masksToBounds = true
              cell.ratingview.settings.updateOnTouch = false
               cell.ratingview.settings.fillMode = .precise
            cell.ratingview.settings.updateOnTouch = false
            cell.ratingview.settings.fillMode = .precise
            cell.reviewpersonname.text = reviews_Array[indexPath.row].person
            cell.datelabel.text = reviews_Array[indexPath.row].date
             cell.discriptionlabel.text = reviews_Array[indexPath.row].review
            cell.ratingview.rating =  (reviews_Array[indexPath.row].rating as NSString).doubleValue
          
            cell.dollarview.rating = (reviews_Array[indexPath.row].expense as NSString).doubleValue
          
            if indexPath.row == 0 {
                cell.reviewLabel.text = NSLocalizedString("Reviews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                cell.viewAllbutton.isHidden = false
                cell.heighcon.constant = 45
            }else{
                cell.reviewLabel.text = ""
                cell.viewAllbutton.isHidden = true
                cell.heighcon.constant = 0
            }
            return cell
        }else if indexPath.section == 8{
            if indexPath.row == 0{
                let hcell = tableView.dequeueReusableCell(withIdentifier: "businessHoursHeading", for: indexPath) as! businessHoursHeadingCell
                hcell.businesshourlabel.text = NSLocalizedString("Business Hours", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                hcell.workinghourlabel.text = NSLocalizedString("More Information", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                hcell.selectionStyle = .none
                hcell.background?.frame.size.width = tableView.frame.width
                return hcell
            }else{
                let  tcell = tableView.dequeueReusableCell(withIdentifier: "cell7", for: indexPath) as! busnshourTableViewCell
                tcell.Timeview.isHidden = false
                tcell.daylabel.text = hourDayTitle[indexPath.row-1]
                tcell.workinghours.text = avlDayTitleArray[hourDayTitle[indexPath.row-1]]
                if avlDayTitleArray[hourDayTitle[indexPath.row-1]]?.contains("Closed") ?? false{
                    tcell.workinghours.textColor  = UIColor.red
                     tcell.workinghours.text = tcell.workinghours.text?.replacingOccurrences(of: "-", with: "")
                }
                tcell.selectionStyle = .none
                return tcell
            }
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell8", for: indexPath) as! amentviewTableViewCell
              cell.selectionStyle = .none
            cell.amentieslabel.text = ament_Array[indexPath.row].title
            cell.amentiesOutput.text = ament_Array[indexPath.row].value
            if indexPath.row == 0{
              cell.viewlabel.text =  NSLocalizedString("Amenities", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                cell.heightcon.constant = 40.0
                cell.lineview.isHidden = false
            }else{
                   cell.viewlabel.text = ""
                  cell.heightcon.constant = 0.0
                cell.lineview.isHidden = true
                
            }
            return cell
        
    }
        }
    }
  
//    private func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
//        if marker.title != nil {
//
//            let mapViewHeight = mapView.frame.size.height
//            let mapViewWidth = mapView.frame.size.width
//
//
//            let container = UIView()
//            container.frame = CGRectMake(mapViewWidth - 100, mapViewHeight - 63, 65, 35)
//            container.backgroundColor = UIColor.white
//            self.view.addSubview(container)
//
//            let googleMapsButton = UIButton()
//            googleMapsButton.setTitle("", forState: .Normal)
//            googleMapsButton.setImage(UIImage(named: "icons8-navigation-30"), forState: .Normal)
//            googleMapsButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
//            googleMapsButton.frame = CGRectMake(mapViewWidth - 80, mapViewHeight - 70, 50, 50)
//            googleMapsButton.addTarget(self, action: "markerClick:", forControlEvents: .TouchUpInside)
//            googleMapsButton.gps = String(marker.position.latitude) + "," + String(marker.position.longitude)
//            googleMapsButton.title = marker.title
//            googleMapsButton.tag = 0
//
//            let directionsButton = CustomButton()
//
//            directionsButton.setTitle("", forState: .Normal)
//            directionsButton.setImage(UIImage(named: "googlemapsdirection"), forState: .Normal)
//            directionsButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
//            directionsButton.frame = CGRectMake(mapViewWidth - 110, mapViewHeight - 70, 50, 50)
//            directionsButton.addTarget(self, action: "markerClick:", forControlEvents: .TouchUpInside)
//            directionsButton.gps = String(marker.position.latitude) + "," + String(marker.position.longitude)
//            directionsButton.title = marker.title
//            directionsButton.tag = 1
//            self.view.addSubview(googleMapsButton)
//            self.view.addSubview(directionsButton)
//        }
//        return true
//    }

//    func markerClick(sender: CustomButton) {
//        let fullGPS = sender.gps
//        let fullGPSArr = fullGPS!.characters.split{$0 == ","}.map(String.init)
//
//        let lat1 : NSString = fullGPSArr[0]
//        let lng1 : NSString = fullGPSArr[1]
//
//
//        let latitude:CLLocationDegrees =  lat1.doubleValue
//        let longitude:CLLocationDegrees =  lng1.doubleValue
//
//        if (UIApplication.sharedApplication().openURL(NSURL(string:"comgooglemaps://")!)) {
//            if (sender.tag == 1) {
//                UIApplication.sharedApplication().openURL(NSURL(string:
//                    "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
//            } else if (sender.tag == 0) {
//                UIApplication.sharedApplication().openURL(NSURL(string:
//                    "comgooglemaps://?center=\(latitude),\(longitude)&zoom=14&views=traffic")!)
//            }
//
//        } else {
//            let regionDistance:CLLocationDistance = 10000
//            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
//            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
//            var options = NSObject()
//            if (sender.tag == 1) {
//                options = [
//                    MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
//                    MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span),
//                    MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
//                ]
//            } else if (sender.tag == 0) {
//                options = [
//                    MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
//                    MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
//                ]
//            }
//
//            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
//            let mapItem = MKMapItem(placemark: placemark)
//            mapItem.name = sender.title
//            mapItem.openInMapsWithLaunchOptions(options as? [String : AnyObject])
//        }
//    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if tableView == myTableView{
         
            self.latitude = location_Array[indexPath.row].latitude
             self.longitude = location_Array[indexPath.row].longitude
            self.locationid = location_Array[indexPath.row].location_id
            self.addview.isHidden = true
            self.bangroundview.isHidden = true
            self.myTableView.isHidden = true
              self.view1.reloadData()
         }else{
        if indexPath.section == 4{
            if indexPath.row == 0{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "getdirection") as! getDirectionViewController
                vc.latitude = latitude
                 vc.longitude = longitude
                let navController = UINavigationController(rootViewController: vc)
                self.present(navController, animated: true, completion: nil)
            }
          else if indexPath.row == 1{
             makeCall(buttonIndex:self.phone)
           }else if (indexPath.row == 2){
            if let url = URL(string: web) {
                UIApplication.shared.open(url, options: [:])
            }
            }else if (indexPath.row == 3){
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "addgiftcard") as? addgiftcardViewController
                vc?.biz_id = id
                self.navigationController?.pushViewController(vc!, animated: true)

            }
        }
        }
    }

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if tableView == myTableView{
        return 90
    }else{
          if indexPath.section == 0{
            return 120
          }else if indexPath.section == 1{
             return 120
          }else if indexPath.section == 2{
            return 99
          }else if indexPath.section == 3{
            return 190
          }else if indexPath.section == 4{
                return 50
          }else if indexPath.section == 5{
              return 200
    }else if indexPath.section == 6{
        return 65
    }else if indexPath.section == 7{
            if indexPath.row == 0{
                return 176
            }
            return 116
    }else if indexPath.section == 8{
            if  indexPath.row == 0{
             return 75
            }else{
                var eachDayTimeArray = [String]()
                hour_Array.forEach({ (eachDay) in
                    if(eachDay.biz_days == hour_Array[indexPath.row-1].biz_days){
                        if(eachDay.biz_end_hours == ""){
                            eachDayTimeArray.append(eachDay.biz_start_hours)
                        }else{
                            eachDayTimeArray.append(eachDay.biz_start_hours + "-" + eachDay.biz_end_hours)
                        }
                    }
                })
                let stringHeight = eachDayTimeArray.joined(separator:"\n").height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.systemFont(ofSize: 15))
                return stringHeight + 20
                
            }
          }else{
            if  indexPath.row == 0{
                   return 100
            }
                return 40
    }
            
    }
    
   
   
  
}
    @objc func ratingAdd(_ sender: UIButton){
        if self.own_business == "1"{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                
            })
          alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else{
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ratingadd") as! RatingAddViewController
   //    let navController = UINavigationController(rootViewController: vc)
        vc.bizTitle = biz_title
        vc.id = id
        self.present(vc, animated: true, completion: nil)
        }
    }
       @objc func LocationbuttonView(_ sender: UIButton){
        createtableview()
        
    
        
    }
    @objc func AddFotoAction(_ sender: UIButton){
 if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
        showActionSheet()
        }else{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton("OK", action: { // create button on alert
               // action on click
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Please Register to view More!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }
        
    }
    @objc func shareAction(_ sender: UIButton){
        SharethroughSocialmedia()
        
    }
    @objc func reportAction(_ sender: UIButton){
        createReportView()
        
    }
    func createReportView(){
        
      reportview  = ReportView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
        self.view.addSubview(reportview)
        reportview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
     reportview.lastView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
      //   reportview.Contentview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
         reportview.commentsText.layer.borderColor = UIColor.lightGray.cgColor
         reportview.commentsText.layer.borderWidth = 1.0;
         reportview.commentsText.backgroundColor = UIColor.clear
         reportview.commentsText.layer.cornerRadius = 4
         reportview.commentsText.layer.masksToBounds = false
        //  sendMessageView.tolabel.text = Toname
        reportview.mainview.layer.cornerRadius = 10.0
        reportview.reportLabel.text = NSLocalizedString("Report", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         reportview.commentLabel.text = NSLocalizedString("Comments", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        reportview.okOutlet.setTitle(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
          reportview.cancelOutlet.setTitle(NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        reportview.okOutlet.addTarget(self, action: #selector(sendbuttonAction), for: .touchUpInside)
        reportview.cancelOutlet.addTarget(self, action: #selector(cancelbuttonAction), for: .touchUpInside)
        // rplyView.backgroundColor = UIColor.red
       
    }
  
    @objc  func sendbuttonAction(sender: UIButton!) {
         ReportBusinessapiCalling(apikey:apikey,biz_id:id,reason:reportview.commentsText.text!)
    }
    @objc  func cancelbuttonAction(sender: UIButton!) {
            reportview.isHidden = true
        
    }
    func ReportBusinessapiCalling(apikey:String,biz_id:String,reason:String){
        activityclass.startactvityIndicator()
        let scriptUrl = buisnessapiClass.ReportBusiness
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)&reason=\(reason)"
       
      
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                   
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
              
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.status == "success"{
                            self.activityclass.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.resignFirstResponder()
                                self.reportview.isHidden = true
                                
                              
                            })
                            alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Reported", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                            
                        }else{
                            self.activityclass.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.reportview.isHidden = true
                            })
                            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }
                        
                        
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            sourceType = 1
            //   let myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            sourceType = 2
            // let myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func showActionSheet() {
        //   currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Camera", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Gallery", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if sourceType == 1 {
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                selectedImage = image
                imageUploading(selectedImage:selectedImage)
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                selectedImage = image
                if let asset = PHAsset.fetchAssets(withALAssetURLs: [info[UIImagePickerControllerReferenceURL] as! URL],
                                                   options: nil).firstObject {
                  
                    
                    PHImageManager.default().requestImageData(for: asset, options: nil, resultHandler: { _, _, _, info in
                        
                        if let fileName = (info?["PHImageFileURLKey"] as? NSURL)?.lastPathComponent {
                         
                            self.self.event_imageName = fileName
                            //do sth with file name
                        }
                    })
                }
                imageUploading(selectedImage:selectedImage)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func makeCall(buttonIndex:String){
        let number = buttonIndex
      //  let result = number.removingWhitespaces()
        let result = number.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://" + "\(result)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
 
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
        {

createview()
            if(view1.contentOffset.y <= 0.0)
                
            {
             
                        
                    self.navigationController?.setNavigationBarHidden(true, animated: false)
               //  createview()
              
                
                

            }
          
          
           

            print("up")
         
     
        }
        else
        {
            createview()
            if(view1.contentOffset.y >= 40.0)
            {

              
                        
                        
                       
                       self.navigationController?.setNavigationBarHidden(false, animated: false)
                        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
                        self.navigationController?.navigationBar.shadowImage = nil
                        navigationController?.navigationBar.barTintColor =  UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
                        
                        self.navigationController?.navigationBar.isTranslucent = false
                      self.SetBackBarButtonCustom()
                  self.navigationItem.title = self.biz_title
                  self.view1.parallaxHeader.minimumHeight = 0
                button.isHidden = false
                newview.isHidden = false
                    createview()

             
            }
           
           
           
            print("down")
     

        }
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
     if collectionView.tag == 2{
        return image_Array.count
        }else{
            return offerArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 2
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath as IndexPath) as! imageCollectionViewCell
            //  cell.imageview.image = UIImage(named:image_Array[indexPath.row].image)
            cell.imageview.sd_setImage(with: URL(string:image_Array[indexPath.row].image),
                                  placeholderImage:UIImage(named: "no-image02"))
            cell.imageview.layer.cornerRadius = 10.0
                cell.imageview.layer.masksToBounds = true
            return cell
        }else{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offercell", for: indexPath as IndexPath) as! OfferCollectionViewCell
                   // cell.myview.backgroundColor = UIColor.blue
            cell.myview.layer.cornerRadius = 8.0
            cell.myview.layer.shadowColor = UIColor.gray.cgColor
            cell.myview.layer.shadowOpacity = 1
            cell.myview.layer.shadowOffset = CGSize.zero
            cell.myview.layer.shadowRadius = 2
            cell.myview.backgroundColor = UIColor.white
            cell.typeOfOffLabel.text = offerArray[indexPath.row].offertype
           
         
            if offerArray[indexPath.row].offertype == "Price Off" {
                cell.offerPriceLabel.text = offerArray[indexPath.row].percent
            }else{
                 cell.offerPriceLabel.text = offerArray[indexPath.row].percent + "%"
            }
            cell.offerPriceLabel.textColor = UIColor.green
    
         
            cell.contentLabel.text = offerArray[indexPath.row].content
            labelWidth = cell.contentLabel.intrinsicContentSize.width
            
          cell.contentLabel.preferredMaxLayoutWidth = labelWidth
           
             return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 2
        {

        
            return 5.0
        }else{
            return 3.0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2
        {
            return CGSize(width: 130.0, height: 130.0)
            
            
        }else{
            
            if labelWidth <= 90{
                 return CGSize(width: 99, height: 99)
            }else{
                return CGSize(width: labelWidth, height: 99)
            }
        }
       
    }
  
    @IBAction func viewallphotoAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "viewallphoto") as! viewallsinglebusinessphotoViewController
        vc.biz_id = id
        let navController = UINavigationController(rootViewController: vc)
        
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func viewallreviewall(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "reviewlist") as! reviewlistingViewController
        vc.biz_id = id
        let navController = UINavigationController(rootViewController: vc)
       
        self.present(navController, animated: true, completion: nil)
    }
    
    func createview(){
        let screenSize: CGRect = UIScreen.main.bounds
        self.newview = UIView(frame: CGRect(x: 8, y: screenSize.height - 60, width: screenSize.width - 16 , height: 60 ))
        self.view.addSubview(newview)
        self.newview.backgroundColor = UIColor.white
 
    button = UIButton.init(frame: CGRect(x:8, y:8,width: newview.frame.width - 16  , height:  42));
        newview.addSubview(button);
        button.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
          //  UIColor(red: 102/255.0, green: 178/255.0, blue: 255/255.0, alpha: 1.0)
        button.layer.cornerRadius = 3.0
        button.setTitle(NSLocalizedString("BookanAppointment", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        newview.isHidden = false
        button.isHidden = false
     
        
    }
    @objc func okView(_ sender: UIButton)
    {
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
        if booking  == "0"{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                self.resignFirstResponder()
                self.dismiss(animated: true, completion: nil)
            })
           
            alert.showInfo(NSLocalizedString("ALERT", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString(" There is no Booking available", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") )
        }else{
            if  booking_type == "3"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "timebooking") as! timebookingViewController
               vc.biz_id = id
            vc.biz_title = biz_title
                vc.biz_loc = self.locationid 
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
            }else if  booking_type == "5"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "tablebooking") as! tablebookingViewController
                vc.biz_id = id
                vc.biz_title = biz_title
                vc.biz_loc = self.locationid
                let navController = UINavigationController(rootViewController: vc)
                self.present(navController, animated: true, completion: nil)
            }
        }
        }else{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton("OK", action: { // create button on alert
               // action on click
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Please Register to book Business", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
           
        }
    }
    func imageUploading(selectedImage:UIImage){
        activityclass.startactvityIndicator()
        let body =  [
            
            "apikey": "\(apikey)",
         "biz_id": "\(id)"]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImageJPEGRepresentation(selectedImage, 0.5)!, withName: "image", fileName: "image", mimeType: "image/jpeg")
            for (key, value) in body {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: buisnessapiClass.UploadBusinessImage){
            (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    //print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseString { response in
                  
                
                    if let JSON = response.result.value {
                      
                        
                        self.parseData(JSONData: response.data!)
                    }
                }
                //   self.stopActivityIndicator()
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                self.activityclass.stopActivityIndicator()
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false // hide default button
                )
                let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                     self.activityclass.stopActivityIndicator()
                })
                alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                print(encodingError)
                //    self.stopActivityIndicator()
            }
            
            
        }
        
        
        //     }
        // }else{
        //      self.stopActivityIndicator()
        //  }
    }

    
    func parseData(JSONData: Data) {
        do {
            let readableJSON = try JSONSerialization.jsonObject(with: JSONData, options:.allowFragments) as! [String: Any]
            
            let status = readableJSON["status"] as! String
            
          
            if status == "success"{
                if status == "success"{
                    self.activityclass.stopActivityIndicator()
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false // hide default button
                    )
                    let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                    alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                        self.resignFirstResponder()
                     //  self.dismiss(animated: true, completion: nil)
                    })
                    alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Image uploaded successfully", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                    
                }else{
                           self.activityclass.stopActivityIndicator()
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false // hide default button
                    )
                    let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                    alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                        
                    })
                    
                    alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                }
            }
            
        }
        catch {
            print(error)
        }
    }
    func SharethroughSocialmedia(){
        let text = NSLocalizedString("Hey I am using this awesome app for exploring local business", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        
        // set up activity view controller
        let textToShare = [ biz_url ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    func tableviewcatagoryHeight() -> Double{
        
        let screenSize: CGRect = UIScreen.main.bounds
        let maximumHeight = screenSize.height - 100
      
        if (Double(94.0)  *  Double(location_Array.count) >  Double(maximumHeight)){
            tablecatheightindicator = 0
            return Double(maximumHeight)
            
        }else{
            
            tablecatheightindicator = 1
            return(44.0  * Double(location_Array.count) + 55.0 + 50.0)
            
            
        }
        
    }
    
}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
    
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
}
}

