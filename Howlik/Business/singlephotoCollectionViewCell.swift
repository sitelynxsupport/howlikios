//
//  singlephotoCollectionViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class singlephotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var singlephoto: UIImageView!
}
