//
//  tablebookingViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/21/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import TTGSnackbar
import NVActivityIndicatorView
import FontAwesome_swift
class tablebookingViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable{
var biz_id = ""
var biz_title = ""
var biz_loc = ""
var apikey = ""
var language_code = ""
var country_code = ""
var val = false
var status = ""
var tsb_id = ""
var personcount = ""
var cancelButton = UIBarButtonItem()
var doneButton = UIBarButtonItem()
var spaceButton = UIBarButtonItem()
let toolBar = UIToolbar()
    var addview = UIView()
    var newview = UIView()
    var myTableView = UITableView()
    let buisnessapiClass = ApiListViewcontroller()
    @IBOutlet var messagetextfield: UITextField!
    @IBOutlet var mobiletextfield: UITextField!
    @IBOutlet var emailtextfield: UITextField!
    @IBOutlet var nametextfield: UITextField!
    @IBOutlet var persontextfield: UITextField!
    @IBOutlet var timetextfield: UITextField!
    @IBOutlet var datetextfield: UITextField!
    @IBOutlet var findmytableoutlet: UIButton!
    @IBOutlet var firstbangroundview: UIView!
    
    @IBOutlet var bangroundview: UIView!
    @IBOutlet var personoutlet: UIButton!
    var max_seat = ""
      var min_seat = ""
    var personarray = [String]()
    var numberarray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        nametextfield.setBottomBorder()
         emailtextfield.setBottomBorder()
         mobiletextfield.setBottomBorder()
         messagetextfield.setBottomBorder()
        datetextfield.setBorder()
            timetextfield.setBorder()
            persontextfield.setBorder()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.country_code = UserDefaults.standard.string(forKey: "country_code")!
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
            TableslotapiCalling(apikey:apikey,biz_id:biz_id)
        }
        // Do any additional setup after loading the view.
      findmytableoutlet.backgroundColor = .clear
        findmytableoutlet.layer.cornerRadius = 20
        findmytableoutlet.layer.borderWidth = 1
       findmytableoutlet.layer.borderColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
         findmytableoutlet.setTitleColor(UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0), for: UIControlState())
        self.navigationItem.title = NSLocalizedString("Add table booking", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        doneButton = UIBarButtonItem(title: NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self, action: #selector(tablebookingViewController.doneClick))
        spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        doneButton.tintColor = UIColor.green
        toolBar.isUserInteractionEnabled = true

        
        let shadowSizes : CGFloat = 0.2
        let shadowPaths = UIBezierPath(rect: CGRect(x: -shadowSizes / 2,
                                                   y: -shadowSizes / 2,
                                                   width: self.firstbangroundview.frame.size.width + shadowSizes,
                                                   height: self.firstbangroundview.frame.size.height + shadowSizes))
        self.firstbangroundview.layer.masksToBounds = false
        self.firstbangroundview.layer.shadowColor = UIColor.black.cgColor
        self.firstbangroundview.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.firstbangroundview.layer.shadowOpacity = 0.2
        self.firstbangroundview.layer.shadowPath = shadowPaths.cgPath
    }
    
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func StarttimeAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        timetextfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(timePickerValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title:  NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(tablebookingViewController.canceltimeClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        timetextfield.inputAccessoryView = toolBar
        
    }
    
    @objc func canceltimeClick() {
        timetextfield.text = ""
        timetextfield.resignFirstResponder()
        
    }
    @objc func timePickerValueChanged(sender:UIDatePicker) {
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "HH:mm a"
        
        // Apply date format
        timetextfield.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    
   
      
    
    @IBAction func StartdateAction(_ sender: Any) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        datetextfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title:NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(tablebookingViewController.canceldateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        datetextfield.inputAccessoryView = toolBar
        
        }
        @objc func canceldateClick() {
            datetextfield.text = ""
            datetextfield.resignFirstResponder()
            
        }
        @objc func datePickerValueChanged(sender:UIDatePicker) {
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            // Apply date format
            datetextfield.text = dateFormatter.string(from: sender.date)
            
        }
        @objc func doneClick() {
            
            datetextfield.resignFirstResponder()
            timetextfield.resignFirstResponder()
            // timepickerFromOut.resignFirstResponder()
        }
func TableslotapiCalling(apikey:String,biz_id:String){
     self.startactvityIndicator()
        personarray.removeAll()
        let scriptUrl = buisnessapiClass.LoadTableReservation
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                        
                        if response["info"] != nil{

                       let infos = response.object(forKey: "info")!
                        for info in (infos as! [[String:Any]]){
                            var timeElement = timebookclass()
                            let contry2 = info as! NSDictionary
                            if  (contry2["max_seat"] as? String) != nil {
                                self.max_seat = contry2["max_seat"] as! String
                                
                            }
                            if  (contry2["min_seat"] as? String) != nil {
                                self.min_seat = contry2["min_seat"] as! String
                                
                                
                            }
                           
                            
                            
                        }
                        }
                        
                        DispatchQueue.main.async
                            {
                                for i in  1..<6{
                                    let value = String(i) + "Person"
                                    self.personarray.append(value)
                                    self.numberarray.append(String(i))
                                    
                                }
          
                           self.myTableView.reloadData()
                       self.stopActivityIndicator()
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    
    @IBAction func personAction(_ sender: Any) {
    }
    
    
    func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        self.addview.addSubview(newview)
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 8, width: newview.frame.width, height: newview.frame.height))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        //  view1.backgroundColor  = UIColor.lightGray
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        // if buttonIndex == 0{
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        label.text = NSLocalizedString("Number of persons", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
        newview.isHidden =  true
        addview.isHidden = true
        myTableView.isHidden =  true
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personarray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    cell.selectionStyle = .none
       // cell.separatorInset = .zero
        cell.textLabel?.text = personarray[indexPath.row]
       
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        personcount = numberarray[indexPath.row]
        personoutlet.setTitle(personarray[indexPath.row], for: UIControlState())
        newview.isHidden =  true
        addview.isHidden = true
        myTableView.isHidden =  true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    @IBAction func personButtonAction(_ sender: Any) {
       
        createTableview()

     
        addview.isHidden = false
        newview.isHidden = false
        myTableView.isHidden = false
        myTableView.reloadData()
    }
    
    func CheckTableAvailabilityapiCalling(apikey:String,biz_id:String,sr_date:String,sr_time:String,sr_people:String){
      
  self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.CheckTableAvailability
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
 let postString = "apikey=\(apikey)&biz_id=\(biz_id)&sr_date=\(sr_date)&sr_time=\(sr_time)&sr_people=\(sr_people)"
        print(">>>>>>>>>")
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    print(convertedJsonIntoArray)
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                print(self.status)
                    
                    
                }
                
                
                DispatchQueue.main.async
                    {
                       if self.status == "success"{
                    self.stopActivityIndicator()
                        self.posttableapicalling()
                        
                        
                            
                        }else{
                        self.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Bookings for this business in currently disabled!\nTry again later", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
       
   
    @IBAction func findmytableAction(_ sender: Any) {
        CheckTableAvailabilityapiCalling(apikey:apikey,biz_id:biz_id,sr_date:datetextfield.text!,sr_time:timetextfield.text!,sr_people:personcount)
    }
    
    func posttableapicalling(){
        if (nametextfield!.text?.isEmpty)! {
            
            alertsnack(message: NSLocalizedString("Enter your name", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else if (emailtextfield!.text?.isEmpty)! {
            alertsnack(message:NSLocalizedString("EnterEmail", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (mobiletextfield!.text?.isEmpty)! {
            alertsnack(message:NSLocalizedString("Phone number required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        
            
        }else if (messagetextfield!.text?.isEmpty)! {
             alertsnack(message:NSLocalizedString("Enter message", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
       
            
        }else{
             posttableapiCalling(tsb_name:nametextfield.text!,tsb_email:emailtextfield.text!,tsb_mobile:mobiletextfield.text!,tsb_message:messagetextfield.text!,apikey:apikey,language_code:language_code,country_code:country_code,biz_id:biz_id,biz_loc:biz_loc,tsb_id:tsb_id,sr_date:datetextfield.text!,sr_time:timetextfield.text!,sr_people:personcount)
    }
    }
    func alertsnack(message: String){
        
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
func posttableapiCalling(tsb_name:String,tsb_email:String,tsb_mobile:String,tsb_message:String,apikey:String,language_code:String,country_code:String,biz_id:String,biz_loc:String,tsb_id:String,sr_date:String,sr_time:String,sr_people:String){
      self.startactvityIndicator()
    let scriptUrl = buisnessapiClass.PostTableReservation
    
    let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
    
    let myUrl = URL(string: urlWithParams);
    
    var request = URLRequest(url:myUrl!)
    let postString = "apikey=\(apikey)&biz_id=\(biz_id)&sr_date=\(sr_date)&sr_time=\(sr_time)&sr_people=\(sr_people)&tsb_name=\(tsb_name)&tsb_email=\(tsb_email)&tsb_mobile=\(tsb_mobile)&tsb_message=\(tsb_message)&language_code=\(language_code)&country_code=\(country_code)&biz_loc=\(biz_loc)&tsb_id=\(tsb_id)"
    print(">>>>>>>>>")
    print(postString)
    request.httpBody = postString.data(using: .utf8)
    
    request.httpMethod = "POST"
    let task = URLSession.shared.dataTask(with: request)
    {
        data, response, error in
        
        if error != nil
        {
            print("error=\(error)")
            DispatchQueue.main.async
                {
                    print("server down")
            }
            return
        }
        
        do{
            
            if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                
            {
                print(convertedJsonIntoArray)
                self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                print(self.status)
                
                
            }
            
            
            DispatchQueue.main.async
                {
                    if self.status == "success"{
                        // self.stopActivityIndicator()
                        
                        self.stopActivityIndicator()
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false // hide default button
                        )
                        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                            self.resignFirstResponder()
                            self.dismiss(animated: true, completion: nil)
                        })
                        alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("SuccessfullyAdded", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        
                       
                    }else{
                        self.stopActivityIndicator()
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false // hide default button
                        )
                        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                            self.dismiss(animated: true, completion: nil)
                        })
                        alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                    }
                    
                    
            }
            
        }
        catch let error as NSError {
            print(error.localizedDescription)
            
        }
        
        
    }
    task.resume()
    
    
    }

}
