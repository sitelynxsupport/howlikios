//
//  timebookingViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/19/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift
import SCLAlertView
import TTGSnackbar
import NVActivityIndicatorView
class timebookingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
  
    
    @IBOutlet var tableview: UITableView!
    var window = tablebookingView()
    var timebook_Array = [timebookclass]()
    var apikey = ""
       var biz_id = ""
    var biz_title  = ""
    var tm_from = ""
     var tm_to = ""
    var biz_loc = ""
    var language_code = ""
    var country_code = ""
    var tsb_id = ""
    var status = ""
      var val = false
    let buisnessapiClass = ApiListViewcontroller()
    override func viewDidLoad() {
        super.viewDidLoad()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.country_code = UserDefaults.standard.string(forKey: "country_code")!
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
            TimeslotapiCalling(apikey:apikey,biz_id:biz_id)
        }
        // Do any additional setup after loading the view.
        
        window =   (Bundle.main.loadNibNamed("tablebookingView", owner: self, options: nil)?.first as? tablebookingView)!
  
        self.navigationItem.title = NSLocalizedString("Add time booking", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
       
        
    }
    
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timebook_Array.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! timebookingTableViewCell
        cell.selectionStyle = .none
          cell.separatorInset = .zero
        cell.timelabel.text = timebook_Array[indexPath.row].time
          cell.ratelabel.text = timebook_Array[indexPath.row].rate
           cell.statuslabel.text = timebook_Array[indexPath.row].status
        let value = timebook_Array[indexPath.row].alerts
        if value == "proceed"{
        
            cell.isUserInteractionEnabled = false
            cell.view1.backgroundColor = UIColor.lightGray
           // cell.view1.backgroundColor = UIColor.groupTableViewBackground
        }else{
                cell.isUserInteractionEnabled = true
        }
        cell.addbuttonoutlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.plus, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        cell.addbuttonoutlet.tag = indexPath.row;
        cell.addbuttonoutlet.addTarget(self, action:#selector(self.addtable(_:)), for:.touchUpInside)
        cell.addbuttonoutlet.tintColor = UIColor.green
        cell.view1.layer.shadowColor = UIColor.black.cgColor
          cell.view1.layer.shadowOpacity = 1
          cell.view1.layer.shadowOffset = CGSize.zero
          cell.view1.layer.shadowRadius = 5
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            return 100
    }
    @objc func addtable(_ sender: UIButton)
    {
      
         self.tsb_id = timebook_Array[sender.tag].tsb_id
          let windows = UIApplication.shared.keyWindow!
        window =   (Bundle.main.loadNibNamed("tablebookingView", owner: self, options: nil)?.first as? tablebookingView)!
window.frame = windows.frame
        window.frame = CGRect(x: 0, y: 0, width:view.frame.width , height:view.frame.height)
              view.addSubview(window)
    window.insiseview.frame =  CGRect(x: 20, y: 0, width:window.frame.width - 40 , height:window.frame.height)
     window.insiseview.center = window.center
         window.addSubview(window.insiseview)
        window.emailtextfield.setBottomBorder()
              window.nametextfild.setBottomBorder()
          window.mobiletextfield.setBottomBorder()
         window.messagetextfield.setBottomBorder()
        window.canceloutlet.backgroundColor = .clear
        window.canceloutlet.layer.cornerRadius = 20
        window.canceloutlet.layer.borderWidth = 1
        window.canceloutlet.layer.borderColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
        window.bookslotoutlet.backgroundColor = .clear
        window.bookslotoutlet.layer.cornerRadius = 20
        window.bookslotoutlet.layer.borderWidth = 1
        window.bookslotoutlet.layer.borderColor =  UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
        window.bookslotoutlet.setTitleColor(UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0), for: UIControlState())
         window.canceloutlet.setTitleColor(UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0), for: UIControlState())
       
        window.bookslotoutlet.addTarget(self, action:#selector(self.bookbuttonView(_:)), for:.touchUpInside)
        window.canceloutlet.addTarget(self, action:#selector(self.cancelbuttonView(_:)), for:.touchUpInside)
     
    }
    @objc func bookbuttonView(_ sender: UIButton)
    {
     
        let name = window.nametextfild.text
        let email = window.emailtextfield.text
        let mobile = window.mobiletextfield.text
        let message = window.messagetextfield.text
       
        if (window.nametextfild!.text?.isEmpty)! {
        
            alertsnack(message: NSLocalizedString("Enter your name", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else if (window.emailtextfield!.text?.isEmpty)! {
            alertsnack(message: NSLocalizedString("EnterEmail", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
    
        }else if (window.mobiletextfield!.text?.isEmpty)! {
            alertsnack(message: NSLocalizedString("Phone number required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (window.messagetextfield!.text?.isEmpty)! {
            alertsnack(message: NSLocalizedString("Enter message", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else{
            apiCalling(tsb_name:name!,tsb_email:email!,tsb_mobile:mobile!,tsb_message:message!,apikey:apikey,language_code:language_code,country_code:country_code,biz_id:biz_id,biz_loc:biz_loc,tsb_id:tsb_id)
        }
    }
    func alertsnack(message: String){
        
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }

    @objc func cancelbuttonView(_ sender: UIButton)
    {
     
          dismiss(animated: true, completion: nil)
    }
  
func apiCalling(tsb_name:String,tsb_email:String,tsb_mobile:String,tsb_message:String,apikey:String,language_code:String,country_code:String,biz_id:String,biz_loc:String,tsb_id:String){
    startactvityIndicator()
        let scriptUrl = buisnessapiClass.PostTimeSlotReservation
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)&country_code=\(country_code)&tsb_email=\(tsb_email)&language_code=\(language_code)&biz_loc=\(biz_loc)&tsb_id=\(tsb_id)&tsb_name=\(tsb_name)&tsb_mobile=\(tsb_mobile)&tsb_message=\(tsb_message)"
        print(">>>>>>>>>")
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("################################")
            print(responseString)
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    print(convertedJsonIntoArray)
              self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                    //print(self.status)
                    
                    
                }
                
                
                DispatchQueue.main.async
                    {
                    if self.status == "success"{
                            self.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.resignFirstResponder()
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Booking Added", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                            
                        }else{
                         self.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Booking Added", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    func TimeslotapiCalling(apikey:String,biz_id:String){
  self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.LoadTimeReservation
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                       
                        if response["info"] != nil{
                        let infos = response.object(forKey: "info")!
                        for info in (infos as! [[String:Any]]){
                            var timeElement = timebookclass()
                        let contry2 = info as! NSDictionary
                        if  (contry2["tm_from"] as? String) != nil {
                            self.tm_from = contry2["tm_from"] as! String
                            
                        }
                        if  (contry2["tm_to"] as? String) != nil {
                            self.tm_to = contry2["tm_to"] as! String
                            
                            
                        }
                            timeElement.time = self.tm_from + " - " + self.tm_to
                        if  (contry2["price"] as? String) != nil {
                            let price = contry2["price"] as! String
                        timeElement.rate = price
                        }
                        if  (contry2["status"] as? NSInteger) != nil {
                            let status = contry2["status"] as! NSInteger
                            print("*^^^^^^^^^^^^^^^**********GT*")
                            if String(status) == "0"{
                                 timeElement.status = "Open"
                                
                            }else  if String(status) == "1"{
                                 timeElement.status = "Passed"
                            }else{
                                   timeElement.status = "Full"
                            }
                            
                        }
                            if  (contry2["alerts"] as? String) != nil {
                                let alerts = contry2["alerts"] as! String
                                 timeElement.alerts = String(alerts)
                            }
                            if  (contry2["tsb_id"] as? String) != nil {
                                let tsb_id = contry2["tsb_id"] as! String
                                timeElement.tsb_id = String(tsb_id)
                            }
                            self.timebook_Array.append(timeElement)
                        
                        
                        }
                        }
                        
                        DispatchQueue.main.async
                            {
                                
                                
                                self.tableview.reloadData()
                                self.stopActivityIndicator()
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    

}
