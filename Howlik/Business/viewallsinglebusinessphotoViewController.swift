//
//  viewallsinglebusinessphotoViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/16/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Cosmos
import FontAwesome_swift
import Alamofire
import SDWebImage
class viewallsinglebusinessphotoViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    let buisnessapiClass = ApiListViewcontroller()
    var image_Array = [imageClass]()
    var biz_id = ""
    var apikey  = ""
    var city_code = ""
    var country_code = ""
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
var imgArray = [String]()
    @IBOutlet var photocollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        }
        //self.navigationItem.title = NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        // Do any additional setup after loading the view.
    }
    
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        
        dismiss(animated: true, completion: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
          self.photocollection.delegate = self
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        photocollection!.collectionViewLayout = layout
        self.LoadupdatebuisnessapiCalling(apikey:apikey,biz_id:biz_id,country_code:country_code, city_code:city_code)
      
        
    }
    
    func LoadupdatebuisnessapiCalling(apikey:String,biz_id:String,country_code:String,city_code:String){
        //  self.startactvityIndicator()
       imgArray.removeAll()
        image_Array.removeAll()
       
        let scriptUrl = buisnessapiClass.SingleBusinessDetails
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id,"country_code":country_code,"city_code":city_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                 
                        let business = response.object(forKey: "business")!
                    
                       
                        
                        let images =  response.object(forKey: "images")!
                        
                        
                        for image in (images as! [[String:Any]]){
                            var imageElement = imageClass()
                            let contry3 = image as NSDictionary
                            if  (contry3["id"] as? String) != nil {
                                
                                let id = contry3["id"] as! String
                                imageElement.id = String(id)
                            }
                            if  (contry3["image"] as? String) != nil {
                                let image = contry3["image"] as! String
                                let foto = "http://www.howlik.com/" + image
                                imageElement.image = foto
                                self.imgArray.append(foto)
                            }
                            
                            
                            self.image_Array.append(imageElement)
                        }
                      
                        
                        
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                
                            
                             
                                self.photocollection.reloadData()
                            
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return image_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! viewallphotoCollectionViewCell
    
 cell.bsnsphoto.sd_setImage(with: URL(string:image_Array[indexPath.row].image),
                                   placeholderImage:UIImage(named: "no-image02"))
 
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        
        return 0.0
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "singlephoto") as! singlebusinessphotoViewController
        vc.biz_index = indexPath.row
        vc.biz_id = biz_id
        vc.imgarray = imgArray
        let navController = UINavigationController(rootViewController: vc)
        
        self.present(navController, animated: true, completion: nil)
    }

  
}
