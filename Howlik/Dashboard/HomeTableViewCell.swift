//
//  HomeTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/24/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Cosmos

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet var homeImage: UIImageView!
    
    @IBOutlet var homePlace: UILabel!
    @IBOutlet var homeTitle: UILabel!
    
    @IBOutlet var fromyoulocation: UILabel!
    @IBOutlet var homedistance: UILabel!
    
    @IBOutlet var homeDateLabel: UILabel!
    
    @IBOutlet var cosmosView: CosmosView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
