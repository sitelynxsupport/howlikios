//
//  HomeViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/23/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Alamofire
import SDWebImage
import Cosmos
import NVActivityIndicatorView
import FontAwesome_swift
import FirebaseMessaging

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, CLLocationManagerDelegate,UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable  {
   
    @IBOutlet var cancelAction: UIBarButtonItem!
    
    @IBOutlet var searchbar: UISearchBar!
   
    
    @IBOutlet var catagorybuttonOutlet: UIBarButtonItem!
    @IBOutlet var homeTableview: UITableView!
    
    @IBOutlet var addedview: UIView!
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var array = [String]()
    var latitude = Double()
    var longitude = Double()
    var apikey = ""
    var buttonIndex = Int()
    var value = true
    var  homeelementArray = NSMutableArray()
    var homeCatogoryArray =  NSMutableArray()
    var homeCatogoryElementArray =  NSMutableArray()
    var Dashboard = ApiListViewcontroller()
    var isSearching = false
    var biz_titleArray = [countrynameClsss]()
    var biz_idArray = [countrynameClsss]()
    var biz_catogoryArray = [catogorycell]()
    var filteredobjects = [countrynameClsss]()
    var btnLeftMenu : UIButton = UIButton()
    var newview = UIView()
      var customview = UIView()
    
    var myCollectionView: UICollectionView!
    var catagoryCode = ""
    var bizid = ""
    var currentPage = 1
    var totalPage = Int()
    var addressString : String = ""
    var country_code = ""
    var last_page = Int()
    var language_code = ""
    var first = true
    var status  = ""
    var label = UILabel()
    var firstTime = false
    var theSearchTerm = ""
    let searchElementArray =  NSMutableArray()
    var currentSearchPage = 1
    var searchLastPage = 1
    var baseLoading = true
    
 let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        baseLoading = true
        self.homeTableview.isHidden = false
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
           self.apikey = UserDefaults.standard.string(forKey: "apikey")!
           self.language_code = UserDefaults.standard.string(forKey: "language_code")!
           //  self.latitude = UserDefaults.standard.double(forKey: "latitude")
            //   self.longitude = UserDefaults.standard.double(forKey: "longitude")
        }
     
 
        // For use in foreground
        self.locManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() && checkConnectionViewController.isConnectedToNetwork() {
            print("Connected")
            locManager.delegate = self
            locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locManager.startUpdatingLocation()
         
           
        }else{
               print("disConnected")
        }


        // Do any additional setup after loading the view.
        searchbar.delegate = self
        cancelAction.isEnabled = false
        cancelAction.tintColor = UIColor.clear
        let logo = UIImage(named: "howlik")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        let screenSize: CGRect = UIScreen.main.bounds
        self.newview = UIView(frame: CGRect(x: 8, y: 70, width: screenSize.width - 15 , height: screenSize.height/2 ))
        self.view.addSubview(newview)
       self.newview.backgroundColor = UIColor.red
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 100, height: 70)
 
        
 myCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: newview.frame.width , height: newview.frame.height ), collectionViewLayout: layout)
        myCollectionView.dataSource = self
        myCollectionView.delegate = self
        myCollectionView.register(UINib(nibName: "catogorycell", bundle: nil), forCellWithReuseIdentifier: "myIdentifier")
      
        newview.addSubview(myCollectionView)
        myCollectionView.backgroundColor = UIColor.white
        layout.scrollDirection = .horizontal
        myCollectionView.layer.shadowColor = UIColor.black.cgColor
        myCollectionView.layer.shadowOffset = CGSize(width:0,height: 2.0)
        myCollectionView.layer.shadowOpacity = 1
        myCollectionView.layer.shadowRadius = 1.0
        myCollectionView.clipsToBounds = false
        myCollectionView.layer.masksToBounds = false
        newview.isHidden = true
        SetBackBarButtonCustom()
        
          firstTime = true
        //create a new button
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
       
        btnLeftMenu.setImage(UIImage(named: "ic_menu_2x"), for: UIControlState())
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        if(!baseLoading){
        self.stopAnimating()
        }
    }
    
    
    
    @objc func onClcikBack()
    {
        //dismiss(animated: true, completion: nil)
        if firstTime == true{
    self.homeapiCalling(apikey:self.apikey,latitude:self.latitude,longitude:self.longitude,language_code:self.language_code)
            firstTime = false
        }
      
        
        if (value == true)
        {
           
            newview.isHidden = false
            myCollectionView.isHidden = false
            homeTableview.alpha = 0.3
            value = false
        }
        else
        {
            
            newview.isHidden = true
            myCollectionView.isHidden = true
            homeTableview.alpha = 1.0
            
            value = true
        }
        
        myCollectionView.reloadData()
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
    
        latitude = locValue.latitude
        longitude = locValue.longitude
        UserDefaults.standard.set(latitude, forKey: "deviceLatitude")
        UserDefaults.standard.set(longitude, forKey: "deviceLongitude")
        if first == true{
           getAddressFromLatLon(pdblLatitude: String(latitude), withLongitude: String(longitude))
      
        }
        first = false
    }
    override func viewDidAppear(_ animated: Bool) {
   self.tabBarItem.title = NSLocalizedString("Nearby", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        self.tabBarItem.title = "Nearby"
        myCollectionView.layer.masksToBounds = true
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
       
        let token = Messaging.messaging().fcmToken
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        print("FCM token: \(token ?? "")")
        print("^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        Messaging.messaging().subscribe(toTopic: "global")
        if token != nil{
        GcmapiCalling(apikey:apikey,device:"ios",token:token!)
        }
    
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    self.tabBarController?.tabBar.items?[0].title = NSLocalizedString("Nearby", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    //                    print(pm.country!)
                    //                    print(pm.locality!)
                    //                    print(pm.subLocality!)
                    //                    print(pm.thoroughfare!)
                    //                    print(pm.postalCode!)
                    //                    print(pm.subThoroughfare!)
                    
                    if pm.subLocality != nil {
                        self.addressString = self.addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        self.addressString = self.addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                        //self.locale(forfullCountryName : pm.country!)
                    self.country_code =   self.locale(for  : pm.country!)
                        UserDefaults.standard.set(self.country_code , forKey:"country_code")
                        
                        
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    
                    
               
                }
               self.homeapiCalling(apikey:self.apikey,latitude:self.latitude,longitude:self.longitude,language_code:self.language_code)
                self.catagoryapiCalling(apikey:self.apikey,latitude:self.latitude,longitude:self.longitude,category_id:self.catagoryCode,page : self.currentPage,country_code:self.country_code,language_code:self.language_code)
                
        })
        
        
    }
    private func locale(for fullCountryName : String) -> String {
        let locales : String = ""
        for localeCode in NSLocale.isoCountryCodes {
            let identifier = NSLocale(localeIdentifier: localeCode)
            let countryName = identifier.displayName(forKey: NSLocale.Key.countryCode, value: localeCode)
            if fullCountryName.lowercased() == countryName?.lowercased() {
                return localeCode
            }
        }
        return locales
    }
    func GcmapiCalling(apikey:String,device:String,token:String){
        let scriptUrl = Dashboard.UpdateGCMtoken
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&device=\(device)&token=\(token)"
        print("%%%%%%%%%%%%%%%%%%%%%%")
        print(postString)
        print("%%%%%%%%%%%%%%%%%%%%%%")
        
        
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
               // print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
       //  print(responseString)
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                   
                }
                
                
                DispatchQueue.main.async
                    {
                        if (self.status == "success") {
                            
                           self.stopActivityIndicator()
                            
                            
                        }else{
                            self.stopActivityIndicator()
                        }
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }

    func homeapiCalling(apikey:String,latitude:Double,longitude:Double,language_code:String){
    
          self.startactvityIndicator()
        self.homeCatogoryArray.removeAllObjects()
        let scriptUrl = Dashboard.dashboard
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"latitude": latitude,"longitude": longitude,language_code:language_code]
       
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                do{
                    
                     let response = JSON as! NSDictionary
                   // print(response)
                    
            if  response["category"] != nil
            {
                let categorys = response.object(forKey: "category")!
                for category in (categorys as! [[String:Any]])
                    {
                        let homeCatogory = catogaryClass()
                        let contry1 = category as NSDictionary
                        let name = contry1["name"] as! String
                        let icon = contry1["icon"] as! String
                        let code = contry1["code"] as! String
                        homeCatogory.catogoryName = name
                        homeCatogory.catogoryIcon = "http://www.howlik.com/" + icon
                        homeCatogory.catogoryCode =  code
                        self.homeCatogoryArray.add(homeCatogory)
                    }
             }
                    
                    
                    DispatchQueue.main.async
                        {
                           
                               self.stopActivityIndicator()
                            if self.homeCatogoryArray.count == 0 {
                            self.catagoryapiCalling(apikey:self.apikey,latitude:self.latitude,longitude:self.longitude,category_id:self.catagoryCode,page : self.currentPage,country_code:self.country_code,language_code:self.language_code)
                                
                            }
                            //   if (self.status == "success") {
                           /* for i in 0..<self.homeelementArray.count{
                                let homeElementfetch:countrynameClsss = self.homeelementArray.object(at: i) as! countrynameClsss
                                self.biz_titleArray.append(homeElementfetch)
                               
                                
                            }
                          
                            self.homeTableview.reloadData()
                            self.myCollectionView.reloadData()*/
                            self.myCollectionView.reloadData()
                            
                            
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    self.baseLoading = false;
                       self.stopActivityIndicator()
                   // print(errorGiven)
                    //print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                  //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })

        
        
        
        
        
        
    }
    
    
    func catagoryapiCalling(apikey:String,latitude:Double,longitude:Double,category_id:String,page : Int,country_code:String,language_code:String){
          self.startactvityIndicator()
        let scriptUrl = Dashboard.AllBusinessDetails
        let parameters: Parameters = ["apikey": apikey,"latitude": latitude,"longitude": longitude,"category_id": category_id,"page" : page,"country_code" : country_code,"language_code" : language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
             
                       let business = response.object(forKey: "business")!
                        let singleBusiness = business as! NSDictionary
                          if singleBusiness["current_page"] != nil {
                          let current_page = singleBusiness.object(forKey: "current_page")!
                        }
                        if singleBusiness["last_page"] != nil {
                            self.last_page = singleBusiness.object(forKey: "last_page")! as! NSInteger
                        }
                        
                        let userIds = singleBusiness.object(forKey: "data")!
                        for userId in (userIds as! [[String:Any]]){
                            var homeElement = countrynameClsss()
                            let contry1 = userId as NSDictionary
                            if  (contry1["biz_date"] as? String) != nil {
                                
                            let biz_date = contry1["biz_date"] as! String
                                homeElement.biz_date = biz_date
                            }
                            if  (contry1["biz_id"] as? String) != nil {
                                let biz_id = contry1["biz_id"] as! String
                                homeElement.biz_id = String(biz_id)
                            }
                             if  (contry1["biz_title"] as? String) != nil {
                            let biz_title = contry1["biz_title"] as! String
                                 homeElement.biz_title = biz_title
                            }
                             if  (contry1["city_title"] as? String) != nil {
                            let city_title = contry1["city_title"] as! String
                                  homeElement.city_title = city_title
                            }
                               if  (contry1["distance"] as? NSInteger) != nil {
                            let distance = contry1["distance"] as! NSInteger
                               
                                  homeElement.distance = String(describing: distance)
                            }
                            if  (contry1["rating"] as? NSNumber) != nil {
                            let rating = contry1["rating"] as! NSNumber
                                homeElement.rating = String(describing:rating)
                            }
                               if  (contry1["biz_image"] as? String) != nil {
                            let biz_image = contry1["biz_image"] as! String
                                homeElement.imageUrl = "http://www.howlik.com/" +  biz_image
                                // homeelementArray : NSMutableArray = NSMutableArray()
                                self.homeCatogoryElementArray.add(homeElement)
                                self.biz_titleArray.append(homeElement)
                            }
                            
                           
                          
                          
                            
                           
                            
                        }
                       /* let categorys = response.object(forKey: "category")!
                        
                        for category in (categorys as! [[String:Any]]){
                            var homeCatogory = catogaryClass()
                            let contry1 = category as NSDictionary
                            let name = contry1["name"] as! String
                            let icon = contry1["icon"] as! String
                            let code = contry1["code"] as! String
                            homeCatogory.catogoryName = name
                            homeCatogory.catogoryIcon = "http://www.howlik.com/" + icon
                            homeCatogory.catogoryCode =  code
                            self.homeCatogoryArray.add(homeCatogory)
                        }*/
                       
                        
                        
                        DispatchQueue.main.async
                            {
                                
                             if (self.homeCatogoryElementArray.count != 0) {
                               
                          self.homeTableview.isHidden = false
                                self.customview.isHidden = true
                                   self.stopActivityIndicator()
                                self.homeTableview.reloadData()
                                self.myCollectionView.reloadData()
                                if self.homeCatogoryElementArray.count == 0 {
                                    self.homeCatogoryElementArray.removeAllObjects()
                                    self.createAlertbox(text : NSLocalizedString("No business found! Reseting Catagory", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                    self.currentPage = 1
                                    self.catagoryCode = ""
                                self.catagoryapiCalling(apikey:self.apikey,latitude:latitude,longitude:longitude,category_id:self.catagoryCode,page:self.currentPage, country_code: country_code,language_code:language_code)
                                    self.homeTableview.reloadData()
                                    self.cancelAction.isEnabled = false
                                    self.cancelAction.tintColor = UIColor.clear
                                    self.btnLeftMenu.setImage(UIImage(named: "ic_menu_2x"), for: UIControlState())
                                    self.value = true
                                    self.btnLeftMenu.backgroundColor = UIColor.clear
                                }
                                
                             }else{
                               
                                self.homeTableview.isHidden = true
                                  self.customview.isHidden = false
                                self.createeventNothingview()
                                }
                            self.baseLoading = false
                               self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    print(errorGiven)
                       self.stopActivityIndicator()
                   // print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
    
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createAlertbox(text : String){
        let h = UIScreen.main.bounds.height
        label = UILabel.init(frame: CGRect(x: 10, y: h / 2, width: view.frame.width - 20, height: 50))
        view.addSubview(label)
        label.layer.cornerRadius = 10.0
        label.layer.masksToBounds = true
        //label.center = view.center
        // label.center.y = view.center.y - 70
        label.text = text
        label.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        UIView.animate(withDuration: 0.5, delay: 2.0, options: [], animations: {
            
            self.label.alpha = 0.0
            
        }) { (finished: Bool) in
            
            self.label.isHidden = true
        }
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      
//        filteredobjects.removeAll()
//        homeTableview.register(UINib(nibName: "catogorycell", bundle: nil), forCellReuseIdentifier: "myIdentifier")
//         filteredobjects = homeCatogoryElementArray as! [countrynameClsss]
//      // navigationItem.titleView = searchbar
//        isSearching = true
        searchbar.setShowsCancelButton(true, animated: true)
//        homeTableview.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
     //   navigationItem.titleView = nil
      //  searchbar.frame = CGRect( x: 16, y : 101, width : 248, height : 56 )
        isSearching = false
        currentSearchPage = 1
        searchElementArray.removeAllObjects()
        searchbar.text = ""
        searchbar.setShowsCancelButton(false, animated: true)
        
        // Remove focus from the search bar.
        searchbar.endEditing(true)
        
        homeTableview.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchElementArray.removeAllObjects()
        if(searchBar.text?.count ?? 0 > 0){
            theSearchTerm = searchBar.text ?? ""
            currentSearchPage = 1
            searchApiCalling(apikey: apikey, latitude: latitude, longitude: longitude, category_id: nil, page: currentSearchPage, country_code: country_code, language_code: language_code, searchQry: theSearchTerm)
            searchController.searchBar.resignFirstResponder()
            self.view.endEditing(true)
        }else{
            theSearchTerm = ""
        }
    //   biz_titleArray.removeAll()
//        if !isSearching {
//            isSearching = true
//            homeTableview.reloadData()
//        }
//           homeTableview.reloadData()
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//
//        filteredobjects = biz_titleArray.filter({ (text) -> Bool in
//
//            let tmp: NSString = text.biz_title as NSString
//            let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
//            return range.location != NSNotFound
//        })
//
//        if(filteredobjects.count == 0){
//            filteredobjects = homeCatogoryElementArray as! [countrynameClsss]
//            isSearching = false;
//
//
//        } else {
//
//            isSearching = true;
//           // searchbar.resignFirstResponder()
//        }
//
//
//        if ( searchText.isEmpty ) {
//            filteredobjects = homeCatogoryElementArray as! [countrynameClsss]
//             self.homeTableview.reloadData()
//        }
//
//        self.homeTableview.reloadData()
    }
    
    
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredobjects.count
        }
        return homeCatogoryElementArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
            cell.homeImage.layer.cornerRadius = 8.0
            cell.homeImage.clipsToBounds = true
            cell.selectionStyle = .none
            if isSearching
            {
                cell.homeTitle.text = filteredobjects[indexPath.row].biz_title
                cell.homePlace.text = filteredobjects[indexPath.row].city_title
                if (Double(filteredobjects[indexPath.row].distance)! <= 1000){
                    cell.homedistance.text = filteredobjects[indexPath.row].distance  + "M"
                }else{
                    let value  = Double(filteredobjects[indexPath.row].distance)! / 1000.0
                    let x = value.rounded(toPlaces: 2)
                    cell.homedistance.text = String(x) + "KM"
                }
            cell.homeImage.sd_setImage(with: URL(string: filteredobjects[indexPath.row].imageUrl), placeholderImage:
                UIImage(named: "no-image02"))
            cell.cosmosView.rating = Double(filteredobjects[indexPath.row].rating)!
            }else{
                let homeElement: countrynameClsss = homeCatogoryElementArray.object(at: indexPath.row) as! countrynameClsss
                cell.homeTitle.text = homeElement.biz_title
                cell.homePlace.text = homeElement.city_title
                if (Double(homeElement.distance)! <= 1000){
                    cell.homedistance.text = homeElement.distance + "M"
                }else{
                    let value  = Double(homeElement.distance)! / 1000.0
                    let x = value.rounded(toPlaces: 2)
                    cell.homedistance.text = String(x) + "KM"
                }
            bizid = homeElement.biz_id
            cell.homeImage.sd_setImage(with: URL(string: homeElement.imageUrl), placeholderImage:
            UIImage(named: "no-image02"))
            cell.cosmosView.rating = Double(homeElement.rating)!
        }
        return cell
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if isSearching{
                bizid = filteredobjects[indexPath.row].biz_id
            
     }else{
            let homeElement: countrynameClsss = homeCatogoryElementArray.object(at: indexPath.row) as! countrynameClsss
                bizid = homeElement.biz_id
            
    }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "singlebusnsview") as! singlebusinessviiewViewController
            vc.biz_id = bizid
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
            return 120.0;//Choose your custom row height
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = homeCatogoryElementArray.count - 1
        if lastItem ==  indexPath.row{
            if(isSearching){
                if currentSearchPage < searchLastPage{
                    currentSearchPage += 1
                        searchApiCalling(apikey: apikey, latitude: latitude, longitude: longitude, category_id: nil, page: currentSearchPage, country_code: country_code, language_code: language_code, searchQry: theSearchTerm)
                }
            }else{
                if currentPage < last_page{
                    currentPage += 1
                        catagoryapiCalling(apikey:self.apikey,latitude:latitude,longitude:longitude,category_id:catagoryCode,page :     currentPage,country_code:country_code,language_code:language_code)
                }
            }
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return homeCatogoryArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "myIdentifier", for: indexPath) as! catogorycell
         let homeElement: catogaryClass = homeCatogoryArray.object(at: indexPath.row) as! catogaryClass
         cell.catogoryImage.sd_setImage(with: URL(string: homeElement.catogoryIcon), placeholderImage:
            UIImage(named: "no-image02"))
         cell.catogoryLabel.text = homeElement.catogoryName
         cell.catogoryLabel.lineBreakMode = .byWordWrapping
         cell.catogoryLabel.numberOfLines = 2
        return cell
    }
    
    @IBAction func action(_ sender: Any) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 20
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        cancelAction.isEnabled = true
        cancelAction.tintColor = UIColor.white
        let homeElement: catogaryClass = homeCatogoryArray.object(at: indexPath.row) as! catogaryClass
        btnLeftMenu.sd_setImage(with: URL(string: homeElement.catogoryIcon), for: UIControlState.normal, placeholderImage:
            UIImage(named: "no-image02"))
       btnLeftMenu.backgroundColor = UIColor.white
       btnLeftMenu.layer.cornerRadius = btnLeftMenu.bounds.size.width/2;
       btnLeftMenu.layer.masksToBounds = true
       catagoryCode = homeElement.catogoryCode
       newview.isHidden = true
       myCollectionView.isHidden = true
       homeTableview.alpha = 1.0
       homeCatogoryElementArray.removeAllObjects(); catagoryapiCalling(apikey:self.apikey,latitude:latitude,longitude:longitude,category_id:catagoryCode,page:1, country_code: country_code,language_code:language_code)
    }
    @IBAction func cancelButton(_ sender: Any) {
       
     homeCatogoryElementArray.removeAllObjects()
        currentPage = 1
        catagoryCode = ""
   catagoryapiCalling(apikey:self.apikey,latitude:latitude,longitude:longitude,category_id:catagoryCode,page:currentPage, country_code: country_code,language_code:language_code)
        homeTableview.reloadData()
        cancelAction.isEnabled = false
        cancelAction.tintColor = UIColor.clear
        btnLeftMenu.setImage(UIImage(named: "ic_menu_2x"), for: UIControlState())
        value = true
        btnLeftMenu.backgroundColor = UIColor.clear
       
    }
    func createeventNothingview(){
        let screenSize: CGRect = UIScreen.main.bounds
        customview  = UIView(frame: CGRect(x: 0, y: 140, width: Int(screenSize.width) , height: Int(screenSize.height) ))
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:140,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text =  NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }

}
extension UISearchBar {
    func changeSearchBarColor(color: UIColor) {
        UIGraphicsBeginImageContext(self.frame.size)
        color.setFill()
        UIBezierPath(rect: self.frame).fill()
        let bgImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        self.setSearchFieldBackgroundImage(bgImage, for: .normal)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension HomeViewController {
    func searchApiCalling(apikey:String,latitude:Double,longitude:Double,category_id:String?,page : Int,country_code:String,language_code:String,searchQry:String){
        self.startactvityIndicator()
        let scriptUrl = Dashboard.AllBusinessDetails
        let parameters: Parameters = ["apikey": apikey,"latitude": latitude,"longitude": longitude,"category_id": category_id ?? "","page" : page,"country_code" : country_code,"language_code" : language_code,"q":searchQry]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        let response = JSON as! NSDictionary
                        
                        let business = response.object(forKey: "business")!
                        let singleBusiness = business as! NSDictionary
                      
                        
                        if singleBusiness["current_page"] != nil {
                            _ = singleBusiness.object(forKey: "current_page")!
                        }
                        if singleBusiness["last_page"] != nil {
                            self.searchLastPage = singleBusiness.object(forKey: "last_page")! as! NSInteger
                        }
                        
                        let userIds = singleBusiness.object(forKey: "data")!
                        for userId in (userIds as! [[String:Any]]){
                            let homeElement = countrynameClsss()
                            let contry1 = userId as NSDictionary
                            if  (contry1["biz_date"] as? String) != nil {
                                
                                let biz_date = contry1["biz_date"] as! String
                                homeElement.biz_date = biz_date
                            }
                            if  (contry1["biz_id"] as? String) != nil {
                                let biz_id = contry1["biz_id"] as! String
                                homeElement.biz_id = String(biz_id)
                            }
                            if  (contry1["biz_title"] as? String) != nil {
                                let biz_title = contry1["biz_title"] as! String
                                homeElement.biz_title = biz_title
                            }
                            if  (contry1["city_title"] as? String) != nil {
                                let city_title = contry1["city_title"] as! String
                                homeElement.city_title = city_title
                            }
                            if  (contry1["distance"] as? NSInteger) != nil {
                                let distance = contry1["distance"] as! NSInteger
                                
                                homeElement.distance = String(describing: distance)
                            }
                            if  (contry1["rating"] as? NSNumber) != nil {
                                let rating = contry1["rating"] as! NSNumber
                                homeElement.rating = String(describing:rating)
                            }
                            if  (contry1["biz_image"] as? String) != nil {
                                let biz_image = contry1["biz_image"] as! String
                                homeElement.imageUrl = "http://www.howlik.com/" +  biz_image
                                // homeelementArray : NSMutableArray = NSMutableArray()
                                self.searchElementArray.add(homeElement)
                                self.filteredobjects = self.searchElementArray as! [countrynameClsss]
                            }
                        }
                        
                        DispatchQueue.main.async
                            {
                                self.stopActivityIndicator()
                                if (self.homeCatogoryElementArray.count != 0) {
                                    self.isSearching = true
                                    self.homeTableview.isHidden = false
                                    self.customview.isHidden = true
                                    self.stopActivityIndicator()
                                    self.homeTableview.reloadData()
                                    
                                }else{
                                    
                                    self.homeTableview.isHidden = true
                                    self.customview.isHidden = false
                                    self.createeventNothingview()
                                }
                        }
                        
                    }
                    break
                case .failure(let errorGiven):
                    print(errorGiven)
                    self.stopActivityIndicator()
                    break
                }
            })
        
        
    }
}
