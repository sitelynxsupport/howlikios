//
//  EventGraphViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/14/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import  FontAwesome_swift
import Charts
import DLRadioButton

class EventGraphViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ChartViewDelegate {
var selectedIndex:NSIndexPath?
    @IBOutlet var last1mnthButton: DLRadioButton!
    
    @IBOutlet var selectTextfield: UITextField!
    @IBOutlet var last12mnthButton: DLRadioButton!
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    var apikey = ""
    var fieldArray = [String]()
    var valueArray = [String]()
    var EventArray = [selectBusinessClass]()
    var tableheightindicator = Int()
    let buisnessapiClass = ApiListViewcontroller()
    @IBOutlet var runkeeperSwitch4: DGRunkeeperSwitch!
    
    @IBOutlet var barchartView: BarChartView!
    @IBOutlet var selectBusinessButton: UIButton!
    var month = "1"
    var arr1 = [String]()
    var graph_type = "view"
    var eve_id = ""
      var customview = UIView()
    var label = NSLocalizedString("UserViews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectBusinessButton.isHidden = false
        self.barchartView.isHidden = false
        self.selectTextfield.isHidden = false
        self.runkeeperSwitch4.isHidden = false
        self.last1mnthButton.isHidden = false
        self.last12mnthButton.isHidden = false
        self.customview.isHidden = true
        // Do any additional setup after loading the view.
        runkeeperSwitch4.titles = [NSLocalizedString("UserViews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), NSLocalizedString("Tickets", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
        runkeeperSwitch4.backgroundColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        runkeeperSwitch4.selectedBackgroundColor = .white
        runkeeperSwitch4.titleColor = .white
        runkeeperSwitch4.selectedTitleColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        runkeeperSwitch4.titleFont = UIFont(name: "HelveticaNeue-Light", size: 13.0)
        runkeeperSwitch4.addTarget(self, action:#selector(self.buttonactionView(_:)), for:.valueChanged)
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            SelectEventapiCalling(apikey:apikey)
        }
        
        selectBusinessButton.backgroundColor = .clear
        selectBusinessButton.layer.cornerRadius = 5
        selectBusinessButton.layer.borderWidth = 0.5
        selectBusinessButton.layer.borderColor = UIColor.lightGray.cgColor
        last1mnthButton.isSelected = true
        last1mnthButton.iconColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        last1mnthButton.indicatorColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        SetBackBarButtonCustom()
        langSet()
        self.navigationItem.title = NSLocalizedString("EVENT GRAPH", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func langSet(){
        last1mnthButton.setTitle(NSLocalizedString("Last1Month", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
      last12mnthButton.setTitle(NSLocalizedString("Last12month", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        selectTextfield.placeholder = NSLocalizedString("Select an Event", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        if  UserDefaults.standard.string(forKey:"language_code") == "en"{
            last1mnthButton.contentHorizontalAlignment = .left
            last12mnthButton.contentHorizontalAlignment = .left
              selectBusinessButton.contentHorizontalAlignment = .left
            
            
        }else{
            last1mnthButton.contentHorizontalAlignment = .right
            last12mnthButton.contentHorizontalAlignment = .right
              selectBusinessButton.contentHorizontalAlignment = .right
        }
    }
    func SelectEventapiCalling(apikey:String){
        // self.startactvityIndicator()
        // inboxArray.removeAll()
        //selectedId.removeAll()
        let scriptUrl = buisnessapiClass.EventGraph
        
        let parameters: Parameters = ["apikey": apikey]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                         if response["events"] != nil{
                        let events = response.object(forKey: "events")!
                        for singleevents in (events as! [[String:Any]]){
                            let singlebusinessxElement = selectBusinessClass()
                            let contry = singleevents as NSDictionary
                            if  (contry["id"] as? String) != nil {
                                let id = contry["id"] as! String
                                singlebusinessxElement.id = id
                                
                            }
                            if  (contry["title"] as? String) != nil {
                                let title = contry["title"] as! String
                                singlebusinessxElement.title = title
                            }
                            singlebusinessxElement.isChecked = false
                            
                            self.EventArray.append(singlebusinessxElement)
                        }
                         }else{
                            self.selectBusinessButton.isHidden = true
                            self.barchartView.isHidden = true
                            self.selectTextfield.isHidden = true
                            self.runkeeperSwitch4.isHidden = true
                            self.last1mnthButton.isHidden = true
                            self.last12mnthButton.isHidden = true
                            self.createeventNothingview()
                        }
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                self.myTableView.reloadData()
                                //   self.bookingtable.separatorInset = .zero
                                print("&&&&&&&&&&&&&&&&&&&&&&")
                                //  print(self.bookingArray.count)
                                print("&&&&&&&&&&&&&&&&&&&&&&")
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    @objc func buttonactionView(_ sender: DGRunkeeperSwitch)
    {

        print("valueChanged: \(sender.selectedIndex)")
        if sender.selectedIndex == 0{
            if selectBusinessButton!.currentTitle == nil{
                firstalert(message:NSLocalizedString("Select an Event", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            }else{
                graph_type = "view"
                label =  NSLocalizedString("UserViews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                GraphapiCalling(apikey:apikey,eve_id:eve_id,month:month,graph_type:graph_type)
            }
        
        }else {
            if selectBusinessButton!.currentTitle == nil{
                firstalert(message: NSLocalizedString("Select an Event", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            }else{
                graph_type = "ticket"
                label =  NSLocalizedString("Tickets", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                GraphapiCalling(apikey:apikey,eve_id:eve_id,month:month,graph_type:graph_type)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createeventNothingview(){
        customview.isHidden = false
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func firstalert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func selectBusinessAction(_ sender: Any) {
        createTableview()
    }
    func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let height =  Int(tableviewHeight())
     
        // self.newview = UIView(frame: CGRect(x: 15, y: 8, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        if tableheightindicator == 0{
            self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
        }else{
            self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
            newview.center =  view.center
        }
        self.addview.addSubview(newview)
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
        myTableView.dataSource = self
        myTableView.delegate = self
        newview.addSubview(myTableView)
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label);
        let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 75, y:newview.frame.height - 45,width: 70  , height:  40));
        newview.addSubview(button);
        // button.backgroundColor = UIColor.green
        button.layer.cornerRadius = 3.0
        button.setTitle(NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        label.textColor = UIColor.black
        label.isHighlighted = true
        view1.backgroundColor  = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        myTableView.register(UINib(nibName: "selectBusinessXib", bundle: nil), forCellReuseIdentifier: "myIdentifier")
        label.text = NSLocalizedString("Select an Event", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        
        // self.myTableView.alwaysBounceVertical = false
        newview.backgroundColor = UIColor.white
        //newview.isHidden =  true
        //  myTableView.isHidden =  true
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
        //  newview.isHidden = false
        //   myTableView.isHidden = false
        myTableView.reloadData()
        //  textview.resignFirstResponder()
    }
    
    
    func tableviewHeight() -> Double{
        
        let screenSize: CGRect = UIScreen.main.bounds
        let maximumHeight = screenSize.height - 100
        if (Double(44.0)  *  Double(EventArray.count) >  Double(maximumHeight)){
            tableheightindicator = 0
            return Double(maximumHeight)
            
        }else{
         
            tableheightindicator = 1
            return(44.0  * Double(EventArray.count) + 55.0 + 50.0)
            
            
        }
        
    }
    @objc func okView(_ sender: UIButton)
    {
        addview.isHidden = true
        newview.isHidden = true
        myTableView.isHidden = true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return EventArray.count
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! selectBusinessXib
//        cell.businessSeletionButton.addTarget(self, action:#selector(self.moreVbhiew(_:)), for:.touchUpInside)
//        cell.businessSeletionButton.tag = indexPath.row
//          cell.businessSeletionButton.setTitle(EventArray[indexPath.row].title, for: UIControlState())
        cell.selectionStyle = .none
        cell.checkbutton.tag = indexPath.row
        cell.checkbutton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
        cell.businesstypeLabel.text = EventArray[indexPath.row].title
        let val = EventArray[indexPath.row].isChecked

        if(val == true){
            cell.checkbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
            cell.checkbutton.tintColor = UIColor.red

        }else{

            cell.checkbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
            cell.checkbutton.tintColor = UIColor.green
        }
        cell.checkbutton.isHidden = true
//
        
//                let val = EventArray[indexPath.row].isChecked
//
//                if(val == true){
//
//                    cell.businessSeletionButton.isSelected = true
//                    cell.businessSeletionButton.indicatorColor = UIColor.black
//                    cell.businessSeletionButton.iconColor = UIColor.black
//                }else{
//
//                    cell.businessSeletionButton.isSelected = false
//                    cell.businessSeletionButton.indicatorColor = UIColor.clear
//                    cell.businessSeletionButton.iconColor = UIColor.black
//                }
        
        
        return cell
        
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectBusinessButton.setTitle(EventArray[indexPath.row].title, for: UIControlState())
        selectTextfield.placeholder = ""
        eve_id = EventArray[indexPath.row].id
        self.runkeeperSwitch4.setSelectedIndex(0, animated: true)
        GraphapiCalling(apikey:apikey,eve_id:eve_id,month:"1",graph_type:graph_type)
        addview.isHidden = true
        newview.isHidden = true
        myTableView.isHidden = true
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    @objc func moreView(_ sender: UIButton)
    {
        //tickvalueArray.removeAll()
        if  EventArray[sender.tag].isChecked == false{
            EventArray[sender.tag].isChecked = true
            
            
        }else{
            EventArray[sender.tag].isChecked = false
        }
        
        
        myTableView.reloadData()
          eve_id =  EventArray[sender.tag].id
        addview.isHidden = true
         newview.isHidden = true
         myTableView.isHidden = true
    }
    @objc func moreVbhiew(_ sender: UIButton)
    {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = myTableView.cellForRow(at: indexPath) as! businessSelection
        if cell.businessSeletionButton.isSelected {
            cell.businessSeletionButton.isSelected = true
            cell.businessSeletionButton.indicatorColor = UIColor.black
            cell.businessSeletionButton.iconColor = UIColor.black
        } else {
            cell.businessSeletionButton.isSelected = false
            cell.businessSeletionButton.indicatorColor = UIColor.clear
            cell.businessSeletionButton.iconColor = UIColor.black
        }
         cell.businessSeletionButton.isSelected = !cell.businessSeletionButton.isSelected
       
        
        
        myTableView.reloadData()
        eve_id =  EventArray[sender.tag].id
        selectBusinessButton.setTitle(EventArray[sender.tag].title, for: UIControlState())
        selectTextfield.placeholder = ""
        eve_id = EventArray[sender.tag].id
        GraphapiCalling(apikey:apikey,eve_id:eve_id,month:"1",graph_type:graph_type)
        addview.isHidden = true
        newview.isHidden = true
        myTableView.isHidden = true
    }
   
    @IBAction func last1mnthAction(_ sender: Any) {
        if selectBusinessButton.currentTitle   == nil{
            firstalert(message:NSLocalizedString("Select an Event", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
        month = "1"
        GraphapiCalling(apikey:apikey,eve_id:eve_id,month:month,graph_type:graph_type)
        }
    }
    
    @IBAction func last2mnthAction(_ sender: Any) {
        if selectBusinessButton.currentTitle   == nil{
            firstalert(message:NSLocalizedString("Select an Event", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
        month = "12"
        GraphapiCalling(apikey:apikey,eve_id:eve_id,month:month,graph_type:graph_type)
        }
    }
    
    func GraphapiCalling(apikey:String,eve_id:String,month:String,graph_type:String){
        fieldArray.removeAll()
        valueArray.removeAll()
        let scriptUrl = buisnessapiClass.EventGraphMonthWiseView
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&eve_id=\(eve_id)&month=\(month)&graph_type=\(graph_type)"
        print(">>>>>>>>>")
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    print(convertedJsonIntoArray)
                    if (convertedJsonIntoArray["result"] as? NSArray) != nil {
                        let result  = convertedJsonIntoArray["result"] as! NSArray
                        print(result)
                        
                        for singleresult in  (result as! [[String:Any]]){
                            let contry1 = singleresult as NSDictionary
                            if (contry1["field"] as? String) != nil {
                                let field = contry1["field"] as! String
                                
                                self.fieldArray.append(String(describing: field))
                                
                            }
                            if (contry1["value"] as? String) != nil {
                                let value = contry1["value"] as! String
                                self.valueArray.append(value)
                                
                            }
                        }
                    }
                }
                
                
                DispatchQueue.main.async
                    {
                        self.setupView()
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    
    func setupView() {
        
   
        
        // Y - Axis Setup
        let yaxis = self.barchartView.leftAxis
        yaxis.spaceTop = 0.35
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = true
        yaxis.labelTextColor = UIColor.black
        yaxis.axisLineColor = UIColor.black
        self.barchartView.rightAxis.enabled = true
        let rightyaxis = self.barchartView.rightAxis
        rightyaxis.spaceTop = 0.35
        rightyaxis.axisMinimum = 0
        rightyaxis.drawGridLinesEnabled = true
        rightyaxis.labelTextColor = UIColor.black
        rightyaxis.axisLineColor = UIColor.black
        
        // X - Axis Setup
        let xaxis = self.barchartView.xAxis
        let formatter = CustomLabelsXAxisValueFormatter()//custom value formatter
        formatter.labels = self.fieldArray
        xaxis.valueFormatter = formatter
        barchartView.xAxis.granularityEnabled = true
        barchartView.xAxis.granularity = 1.0
        //        xaxis.drawGridLinesEnabled = true
        //        xaxis.labelPosition = .bothSided
        //        xaxis.labelTextColor = UIColor.black
        //        xaxis.centerAxisLabelsEnabled = true
        //        xaxis.axisLineColor = UIColor.black
        xaxis.granularityEnabled = true
        xaxis.enabled = true
        
        
        self.barchartView.delegate = self
        self.barchartView.noDataText = "You need to provide data for the chart."
        self.barchartView.noDataTextColor = UIColor.black
        self.barchartView.chartDescription?.textColor = UIColor.clear
        
        setChart()
    }
    
    func setChart() {
        self.barchartView.noDataText = "Loading...!!"
        var dataEntries: [BarChartDataEntry] = []
        
        
        for i in 0..<self.fieldArray.count {
            
            let dataEntry = BarChartDataEntry(x: Double(i) , y: Double(self.valueArray[i])!)
            dataEntries.append(dataEntry)
            
            
            
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: label)
        let dataSets: [BarChartDataSet] = [chartDataSet]
        let chartData = BarChartData(dataSets: dataSets)
        let barWidth = 0.6
        chartData.barWidth = barWidth
        self.barchartView.xAxis.axisMinimum = 0.0
        self.barchartView.xAxis.granularity = self.barchartView.xAxis.axisMaximum / Double(self.fieldArray.count)
        self.barchartView.data = chartData
        self.barchartView.notifyDataSetChanged()
        //  self.barchartView.setVisibleXRangeMaximum(4)
        self.barchartView.animate(yAxisDuration: 1.0, easingOption: .linear)
        chartData.setValueTextColor(UIColor.black)
    }
    
}

