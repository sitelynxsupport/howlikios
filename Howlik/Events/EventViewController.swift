//
//  EventViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/30/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import NVActivityIndicatorView
import FontAwesome_swift
import SCLAlertView
class EventViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NVActivityIndicatorViewable  {

    @IBOutlet var floatingButton: UIButton!
    @IBOutlet var mainview: UIView!
    
    @IBOutlet var switchView: DGRunkeeperSwitch!
    @IBOutlet var upcoming: UIButton!
    @IBOutlet var popular: UIButton!
    @IBOutlet var addnew: UIButton!
    var eventClass = ApiListViewcontroller()
    @IBOutlet var mycollectionView: UICollectionView!
      var dashImage = [String]()
    var apikey = ""
    var city_code = ""
      var country_code = ""
    var collectionIndex =  0
 var status = ""
         var buttonindex = Int()
    var customview = UIView()
       var popularElementArray =  NSMutableArray()
    let notingview = emailValidateViewController()
    @IBOutlet var eventView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
          dashImage = ["city","city","city","city","city","city","city","city","city"]

mycollectionView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        collectionIndex = 0
    
        
        
        switchView.titles = [NSLocalizedString("Upcoming", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Popular", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Myevents", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
            switchView.backgroundColor = UIColor.white
        
            switchView.selectedBackgroundColor = UIColor.black
            switchView.titleColor = .black
        switchView.titleFont = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
            switchView.selectedTitleColor = UIColor.white
        switchView.titleFont = UIFont(name: "HelveticaNeue-Light", size: 17.0)
          switchView.addTarget(self, action:#selector(self.buttonactionView(_:)), for:.valueChanged)
     
       
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        // self.navigationController?.navigationBar.barTintColor = UIColor(red: 255/255.0, green: 16/255.0, blue: 34/255.0, alpha: 0.0)
        self.navigationController?.navigationBar.barTintColor  = UIColor.red
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        //navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        // floatingButton.layer.cornerRadius = floatingButton.bounds.size.width/2;
        // floatingButton.isHidden = false
        floatingButton.layer.shadowColor = UIColor.lightGray.cgColor
        floatingButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        floatingButton.layer.masksToBounds = false
        floatingButton.layer.shadowRadius = 1.0
        floatingButton.layer.shadowOpacity = 0.5
        floatingButton.layer.cornerRadius = floatingButton.frame.width / 2
        
        mycollectionView.backgroundColor = UIColor.clear.withAlphaComponent(0)
          // Do any additional setup after loading the view.
       
    }
    override func viewDidAppear(_ animated: Bool) {
   self.navigationItem.title = NSLocalizedString("EVENTS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
         
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            
        }
        self.country_code = UserDefaults.standard.string(forKey: "country_code")!
        collectionIndex = 0
        if collectionIndex == 0{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 10, left: 12, bottom: 10, right: 12)
            //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            mycollectionView!.collectionViewLayout = layout
        }else if  collectionIndex == 1{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
            //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            mycollectionView!.collectionViewLayout = layout
        }
        if switchView.selectedIndex == 0 {
            collectionIndex = 0
            // popularElementArray.removeAllObjects()
            self.mycollectionView.isHidden = false
            self.customview.isHidden = true
            upcomingapiCalling(apikey:self.apikey,city_code:city_code,country_code:country_code)
        }else if switchView.selectedIndex == 1{
            self.mycollectionView.isHidden = false
            self.customview.isHidden = true
            collectionIndex = 0
            
            // popularElementArray.removeAllObjects()
            popularapiCalling(apikey:self.apikey,city_code:city_code,country_code:country_code)
        }else{
            self.mycollectionView.isHidden = false
            self.customview.isHidden = true
            collectionIndex =  1
            
            //   popularElementArray.removeAllObjects()
            OwnapiCalling(apikey:self.apikey,city_code:city_code)
        }
        

       
//        popularElementArray.removeAllObjects()
//        switchView.setSelectedIndex(0, animated: true)
//         upcomingapiCalling(apikey:self.apikey,city_code:city_code,country_code:country_code)
        

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tabBarController?.tabBar.items?[1].title = NSLocalizedString("Events", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 30, height: 30)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }

    @objc func buttonactionView(_ sender: DGRunkeeperSwitch)
    {
        
        print("valueChanged: \(sender.selectedIndex)")
        if sender.selectedIndex == 0{
            collectionIndex = 0
     // popularElementArray.removeAllObjects()
              self.mycollectionView.isHidden = false
            self.customview.isHidden = true
            upcomingapiCalling(apikey:self.apikey,city_code:city_code,country_code:country_code)
            
        }else if  sender.selectedIndex == 1{
              self.mycollectionView.isHidden = false
              self.customview.isHidden = true
            collectionIndex = 0

           // popularElementArray.removeAllObjects()
            popularapiCalling(apikey:self.apikey,city_code:city_code,country_code:country_code)
        }else {
              self.mycollectionView.isHidden = false
              self.customview.isHidden = true
           
            if apikey ==  ""{
                
                
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false // hide default button
                )
                let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                alert.addButton("OK", action: { // create button on alert
                
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                    let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                    self.present(navController, animated: true, completion: nil)
                })
                alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Eventsyouaddedwillappearhere.\nPleaseRegistertoaddEvents!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
               
            }else{
                collectionIndex =  1

             //   popularElementArray.removeAllObjects()
                OwnapiCalling(apikey:self.apikey,city_code:city_code)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return popularElementArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionIndex ==  0 {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! eventCollectionViewCell
        let popularElement:popularEventclass = self.popularElementArray.object(at: indexPath.row) as! popularEventclass
        
        cell.eventview.layer.cornerRadius = 10.0
        cell.eventImage.layer.cornerRadius = 10.0
        cell.eventImage.layer.masksToBounds = true
        cell.eventImage.sd_setImage(with: URL(string:popularElement.event_image1), placeholderImage:
            UIImage(named: "no-image02"))
        cell.eventname.text = popularElement.event_name
         cell.eventdate.text = popularElement.event_date
         cell.cityname.text = popularElement.city_name
        
        return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mycell", for: indexPath) as! myeventCollectionViewCell
                let popularElement:popularEventclass = self.popularElementArray.object(at: indexPath.row) as! popularEventclass
                cell.myeventImage.sd_setImage(with: URL(string:popularElement.event_image1), placeholderImage:UIImage(named: "no-image02"))
                cell.eventname.text = popularElement.event_name
                cell.eventdate.text = popularElement.event_date
                cell.eventcity.text = popularElement.city_name
                cell.eventenddate.text = popularElement.eventEnd_date
                cell.myeventLocationButton.setImage(UIImage(named: "book-iconlast"),for: UIControlState.normal)
          ///  cell.myeventLocationButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.calendarMinusO, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
                cell.myeventLocationButton.tintColor = UIColor.white
                cell.myeventEditButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.pencil, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
                cell.myeventEditButton.tintColor = UIColor.white
                cell.myeventEditButton.tag = indexPath.row;
                cell.myeventEditButton.addTarget(self, action:#selector(self.editbuttonPressed(_:)), for:.touchUpInside)
                cell.myeventDeleteButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.trash, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
                  cell.myeventDeleteButton.tag = indexPath.row;
                cell.myeventDeleteButton.addTarget(self, action:#selector(self.myeventDelete(_:)), for:.touchUpInside)
                cell.myeventDeleteButton.tintColor = UIColor.white
                cell.controller = self
                cell.myeventLocationButton.tag = indexPath.row;
                cell.myeventLocationButton.addTarget(self, action:#selector(self.getOwnedbookings(_:)), for:.touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

          if collectionIndex ==  0 {
           let popularElement:popularEventclass = self.popularElementArray.object(at: indexPath.row) as! popularEventclass
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "detailpopular") as! detaileventViewController
          
            vc.id = popularElement.id
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            if collectionIndex ==  0 {
        let width = (self.view.frame.size.width - 12 * 4) / 2//some width
        let height = (width * 3.6 / 3.5  )//ratio
        return CGSize(width: width, height: height);
            }else{
                let width = (self.view.frame.size.width - 12 * 2.5) / 2//some width
                let height = (width * 3.6 / 2.5  )//ratio
                return CGSize(width: width, height: height);
        }
                
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            if collectionIndex ==  0 {
        return 20
            }else{
               return 20
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        floatingButton.isHidden = false

//        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
//        {

//             floatingButton.isHidden = false
//        }
//        else
//        {
//              floatingButton.isHidden = true

//        }
    }
    @objc func editbuttonPressed(_ sender: UIButton) {
          let popularElement:popularEventclass = self.popularElementArray.object(at: sender.tag) as! popularEventclass
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "eventedit") as! eventEditViewController
        vc.id = popularElement.id
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }
    @objc func getOwnedbookings(_ sender: UIButton) {
        let popularElement:popularEventclass = self.popularElementArray.object(at: sender.tag) as! popularEventclass
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ownedeventbooking") as! OwnedEventBookingViewController
        vc.id = popularElement.id
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }

    @objc func myeventDelete(_ sender: UIButton) {
        if  collectionIndex ==  1{
             buttonindex = sender.tag
       let popularElement:popularEventclass = self.popularElementArray.object(at: sender.tag) as! popularEventclass
        let id  = popularElement.id
       
            
     
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false,
                shouldAutoDismiss: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton(NSLocalizedString("Yes", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")) {
                self.DeleteapiCalling(apikey:self.apikey,event_id:id)
                
                alertView.hideView()
                self.mycollectionView.reloadData()
            }
            alertView.addButton(NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")) {
                 self.resignFirstResponder()
                    alertView.hideView()
            }
         
            alertView.showSuccess(NSLocalizedString("Deleteforever", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:NSLocalizedString("Permanently delete this event?", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
   
            
        }
    }
    func DeleteapiCalling(apikey:String,event_id:String){
        notingview.startactvityIndicator()
        let scriptUrl = eventClass.DeleteEvent
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&event_id=\(event_id)"
       
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                  
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                  
                 
                }
                
                
                DispatchQueue.main.async
                    {
                        if (self.status == "success") {
                            self.stopActivityIndicator()
                            self.popularElementArray.removeObject(at: self.buttonindex)
                                self.mycollectionView.reloadData()
                           
                        }else{
                            self.stopActivityIndicator()
                        }
                        
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    func upcomingapiCalling(apikey:String,city_code:String,country_code:String){
          self.startactvityIndicator()
        self.popularElementArray.removeAllObjects()
        let scriptUrl = eventClass.UpcomingEventsList
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"city_code": city_code,"country_code": country_code]
      
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                       
                   
                       // if (response.object(forKey: "events")!) != nil {
                        if response["events"] != nil{
                            let events = response.object(forKey: "events")!
                         
                            for event in (events as! [[String:Any]]){
                                let popularElement = popularEventclass()
                                let contry1 = event as NSDictionary
                                if  (contry1["event_name"] as? String) != nil {
                                    let event_name = contry1["event_name"] as! String
                                    popularElement.event_name = event_name
                                }else{
                                    let event_name = "nil"
                                    popularElement.event_name = event_name
                                }
                                
                                if  (contry1["event_date"] as? String) != nil {
                                    let event_date = contry1["event_date"] as! String
                                  
                                  /*  let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "YYYY-MM-DD"
                                    let date = dateFormatter.date(from: event_date)
                                    dateFormatter.dateFormat = "DD-MMM-YYYY"
                                    let goodDate = dateFormatter.string(from: date!)*/
                                    popularElement.event_date = event_date
                                }else{
                                    let event_date = ""
                                    popularElement.event_date = event_date
                                }
                                if  (contry1["city_name"] as? String) != nil {
                                    let city_name = contry1["city_name"] as! String
                                    popularElement.city_name = city_name
                                }
                                else{
                                    let city_name = " "
                                    popularElement.city_name = city_name
                                }
                                if  (contry1["id"] as? String) != nil {
                                    let id = contry1["id"] as! String
                                    popularElement.id = String(id)
                                }
                                if  (contry1["event_image1"] as? String) != nil {
                                    let event_image1 = contry1["event_image1"] as! String
                                    popularElement.event_image1 = "http://www.howlik.com/" +  event_image1
                                }
                      
                            
                            self.popularElementArray.add(popularElement)
                            
                        }
        }
                            
                       
 
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                self.mycollectionView.reloadData()
                                    self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                        self.stopActivityIndicator()
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func popularapiCalling(apikey:String,city_code:String,country_code:String){
          self.startactvityIndicator()
         self.popularElementArray.removeAllObjects()
        let scriptUrl = eventClass.PopularEventsList
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"city_code": city_code,"country_code": country_code]
   
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        //  if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: retrivedResult, options: []) as? [String:Any]{
                     
                        if (response["events"]) != nil {
                            let events = response.object(forKey: "events")!
                        
                            for event in (events as! [[String:Any]]){
                                var popularElement = popularEventclass()
                                let contry1 = event as NSDictionary
                               if  (contry1["event_name"] as? String) != nil {
                                let event_name = contry1["event_name"] as! String
                                popularElement.event_name = event_name
                               }else{
                                let event_name = "nil"
                                popularElement.event_name = event_name
                                }
                               
                                if  (contry1["event_date"] as? String) != nil {
                                    let event_date = contry1["event_date"] as! String
                                   
                                    
                                     popularElement.event_date = event_date
                                }else{
                                    let event_date = ""
                                    popularElement.event_date = event_date
                                }
                                 if  (contry1["city_name"] as? String) != nil {
                                let city_name = contry1["city_name"] as! String
                                     popularElement.city_name = city_name
                                }
                                else{
                                    let city_name = ""
                                    popularElement.city_name = city_name
                                }
                                if  (contry1["id"] as? String) != nil {
                                    let id = contry1["id"] as! String
                                    popularElement.id = String(id)
                                }
                            if  (contry1["event_image1"] as? String) != nil {
                                let event_image1 = contry1["event_image1"] as! String
                                popularElement.event_image1 = "http://www.howlik.com/" +  event_image1
                            }
                         
                                self.popularElementArray.add(popularElement)
                                
                            }
                        }
                        
                        // self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                        /*  if (response.object(forKey: "business")!) != nil {
                         }
                         
                         for userId in (userIds as! [[String:Any]]){
                         var homeElement = countrynameClsss()
                         let contry1 = userId as NSDictionary
                         let biz_date = contry1["biz_date"] as! String
                         let biz_title = contry1["biz_title"] as! String
                         let city_title = contry1["city_title"] as! String
                         let distance = contry1["distance"] as! NSNumber
                         let rating = contry1["rating"] as! NSNumber
                         let biz_image = contry1["biz_image"] as! String
                         homeElement.biz_date = biz_date
                         homeElement.biz_title = biz_title
                         homeElement.city_title = city_title
                         homeElement.distance = String(describing: distance)
                         homeElement.rating = String(describing:rating)
                         homeElement.imageUrl = "http://www.howlik.com/" +  biz_image
                         // homeelementArray : NSMutableArray = NSMutableArray()
                         self.homeCatogoryElementArray.add(homeElement)
                         
                         }*/
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                    self.stopActivityIndicator()
                                //  if (self.status == "success") {
                                /*  for i in 0..<self.homeCatogoryElementArray.count{
                                 let homeElementfetch:countrynameClsss = self.homeCatogoryElementArray.object(at: i) as! countrynameClsss
                                 self.biz_titleArray.append(homeElementfetch)
                                 
                                 
                                 }*/
                                
                                
                                self.mycollectionView.reloadData()
                                
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                        self.stopActivityIndicator()
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func OwnapiCalling(apikey:String,city_code:String){
          self.startactvityIndicator()
         self.popularElementArray.removeAllObjects()
        let scriptUrl = eventClass.OwnEventsList
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"city_code": city_code]
    
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                       
                        
                    if (response["events"]) != nil {
                       
                    let events = response.object(forKey: "events")
                            if (events as AnyObject).count == 0 {
                             
                                  self.mycollectionView.isHidden = true
                              
                                    self.createeventNothingview()
                                  self.floatingButton.isHidden = false
                            }
                            else{
                                 self.mycollectionView.isHidden = false
                            for event in (events as! [[String:Any]]){
                                var popularElement = popularEventclass()
                                let contry1 = event as NSDictionary
                                if  (contry1["id"] as? String) != nil {
                                    let id = contry1["id"] as! String
                                    popularElement.id = String(id)
                                }else{
                                    let id = "nil"
                                    popularElement.id = id
                                }
                                if  (contry1["event_name"] as? String) != nil {
                                    let event_name = contry1["event_name"] as! String
                                    popularElement.event_name = event_name
                                }else{
                                    let event_name = "nil"
                                    popularElement.event_name = event_name
                                }
                                
                                if  (contry1["event_date"] as? String) != nil {
                                    let event_date = contry1["event_date"] as! String
                                 
                                  //  let dateFormatter = DateFormatter()
                                  //  dateFormatter.dateFormat = "YYYY-MM-DD"
                                 //   let date = dateFormatter.date(from: event_date)
                             //       dateFormatter.dateFormat = "DD-MMM-YYYY"
                                  //  let goodDate = dateFormatter.string(from: date!)
                                    popularElement.event_date = event_date
                                }else{
                                    let event_date = "nil"
                                    popularElement.event_date = event_date
                                    
                                }
                                if  (contry1["city_name"] as? String) != nil {
                                    let city_name = contry1["city_name"] as! String
                                    popularElement.city_name = city_name
                                }
                                else{
                                    let city_name = "nil"
                                    popularElement.city_name = city_name
                                }
                                if  (contry1["event_image1"] as? String) != nil {
                                    let event_image1 = contry1["event_image1"] as! String
                                    popularElement.event_image1 = "http://www.howlik.com/" +  event_image1
                                }
                                if  (contry1["eventEnd_date"] as? String) != nil {
                                    let eventEnd_date = contry1["eventEnd_date"] as! String
                                    popularElement.eventEnd_date = eventEnd_date
                                }
                               
                                self.popularElementArray.add(popularElement)
                                
                            }
                       
                           
                        }
                        }
                        
                  
                        DispatchQueue.main.async
                            {
                                //  if (self.status == "success") {
                                /*  for i in 0..<self.homeCatogoryElementArray.count{
                                 let homeElementfetch:countrynameClsss = self.homeCatogoryElementArray.object(at: i) as! countrynameClsss
                                 self.biz_titleArray.append(homeElementfetch)
                                 
                                 
                                 }*/
                                
                                    self.stopActivityIndicator()
                                self.mycollectionView.reloadData()
                                
                                
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                        self.stopActivityIndicator()
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createeventNothingview(){
        let screenSize: CGRect = UIScreen.main.bounds
     customview  = UIView(frame: CGRect(x: 0, y: 160, width: Int(screenSize.width) , height: Int(screenSize.height -  400) ))
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:100,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        //floatingButton
        
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text =  NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }

    
    @IBAction func floatingbuttonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "eventadding") as? eventAddingViewController
        self.navigationController?.pushViewController(vc!, animated: true)
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "eventadding") as! eventAddingViewController
//        let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
//        self.present(navController, animated: true, completion: nil)
    }
}
extension UICollectionView {
    func indexPathForView(view: AnyObject) -> IndexPath? {
        let originInCollectioView = self.convert(CGPoint.zero, from: (view as! UIView))
        return self.indexPathForItem(at: originInCollectioView) as IndexPath?
    }
}
