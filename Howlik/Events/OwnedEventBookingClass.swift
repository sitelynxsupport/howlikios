//
//  OwnedEventBookingClass.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/19/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class OwnedEventBookingClass: NSObject {
    var active = String()
    var event_date_Time = String()
    var event_name = String()
    var ticket_quantity = String()
}
