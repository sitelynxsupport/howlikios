//
//  OwnedEventBookingViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/19/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift

class OwnedEventBookingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet var OwnedeventBookingTable: UITableView!
    let Apiclass = ApiListViewcontroller()
    var OwnedeventBookingArray = [OwnedEventBookingClass]()
    var apikey = ""
    var country_code = ""
    var language_code = ""
    var event_date = ""
    var event_starttime = ""
     var id = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            OwnedEventBookingloadapiCalling(apikey:apikey,country_code:country_code,language_code:language_code,event_id:id)
        }
        SetBackBarButtonCustom()
        self.navigationItem.title = NSLocalizedString("My Events Bookings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func OwnedEventBookingloadapiCalling(apikey:String,country_code:String,language_code:String,event_id:String){
        // self.startactvityIndicator()
        self.OwnedeventBookingArray.removeAll()
        let scriptUrl = Apiclass.OwnedEventBookings
        
        let parameters: Parameters = ["apikey": apikey,"event_id": event_id,"country_code":country_code,"language_code":language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                      
                        let booking = response.object(forKey: "booking")!
                      for singlebooking in (booking as! [[String:Any]]){
                            let singlebookingElement = OwnedEventBookingClass()
                            let contry = singlebooking as NSDictionary
                            if  (contry["active"] as? String) != nil {
                                let active = contry["active"] as! String
                                singlebookingElement.active = active
                                
                            }
                            if  (contry["event_name"] as? String) != nil {
                                let event_name = contry["event_name"] as! String
                                singlebookingElement.event_name = event_name
                                
                            }
                            if  (contry["event_date"] as? String) != nil {
                                self.event_date = contry["event_date"] as! String
                                
                            }
                            if  (contry["event_starttime"] as? String) != nil {
                                self.event_starttime = contry["event_starttime"] as! String
                                singlebookingElement.event_date_Time = self.event_date + "-" + self.event_starttime
                                
                            }
                            if  (contry["ticket_quantity"] as? String) != nil {
                                let ticket_quantity = contry["ticket_quantity"] as! String
                                singlebookingElement.ticket_quantity = ticket_quantity
                                
                            }
                            self.OwnedeventBookingArray.append(singlebookingElement)
                        }
                        
                        
                        
                        DispatchQueue.main.async
                            {
                             self.OwnedeventBookingTable.reloadData()
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OwnedeventBookingArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! OwnedEventBookingTableViewCell
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        
        var yourViewBorder = CAShapeLayer()
        if  OwnedeventBookingArray[indexPath.row].active == "0"{
            cell.eventStatusLabel.text = NSLocalizedString("NOT ACTIVE", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            cell.eventStatusLabel.textColor = UIColor.red
            yourViewBorder.strokeColor = UIColor.red.cgColor
            
        }else{
            cell.eventStatusLabel.text = NSLocalizedString("ACTIVE", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            cell.eventStatusLabel.textColor = UIColor.green
            yourViewBorder.strokeColor = UIColor.green.cgColor
            
        }
        
        
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = cell.eventStatusLabel.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: cell.eventStatusLabel.bounds).cgPath
        cell.eventStatusLabel.layer.addSublayer(yourViewBorder)
        cell.eventNameLabel.text = OwnedeventBookingArray[indexPath.row].event_name
        cell.eventDateTimeLabel.text = OwnedeventBookingArray[indexPath.row].event_date_Time
        cell.eventTicketsLabel.text =  OwnedeventBookingArray[indexPath.row].ticket_quantity + NSLocalizedString("Tickets", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
        
    }
}
