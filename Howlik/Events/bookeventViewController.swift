//
//  bookeventViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/28/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import FontAwesome_swift
import TTGSnackbar
import SCLAlertView
class bookeventViewController: UIViewController,NVActivityIndicatorViewable,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate  {
    @IBOutlet var ticketsavailablelabel: UILabel!
    
    @IBOutlet var freeticketQuantityLabel: UILabel!
    @IBOutlet var freeticketQuantityText: UITextField!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var quantitylabel: UILabel!
    @IBOutlet var eventmage: UIImageView!
    
    @IBOutlet var priceOutlet: UITextField!
    @IBOutlet var totaltextfield: UITextField!
    @IBOutlet var quantitytextfield: UITextField!
    @IBOutlet var firstview: UIView!
    @IBOutlet var statuslabel: UILabel!
    @IBOutlet var titlelabel: UILabel!
    @IBOutlet var outerview: UIView!
    @IBOutlet var purchaseoutlet: UIButton!
    
    @IBOutlet var innerview: UIView!
    let buisnessapiClass = ApiListViewcontroller()
    var apikey = ""
    var country_code = ""
    var language_code = ""
    var lan_id = ""
    var event_id = ""
    var event_name = ""
    var event_image = ""
      var myPickerView : UIPickerView!
    var pricebook_Array = [String]()
    var ticket_type = ""
    var status = ""
    var usr_name = ""
    var usr_email = ""
    var usr_id = ""
    var currency_code = ""
    var val = false
    var price = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
         quantitytextfield.delegate = self
          freeticketQuantityText.delegate = self
        firstview.addBottomBorderWithColor(color: UIColor.black, width: 0.5)
        totaltextfield.setBorder()
        quantitytextfield.setBorder()
        freeticketQuantityText.setBorder()
        self.purchaseoutlet.isHidden = false
        // Do any additional setup after loading the view.
        pricebook_Array = ["1","2","3","4","5","6","7","8","9","10"]
        self.navigationItem.title = NSLocalizedString("Event Booking", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        // Do any additional setup after loading the view.
        
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.country_code = UserDefaults.standard.string(forKey: "country_code")!
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
                   self.usr_name = UserDefaults.standard.string(forKey: "name")!
                   self.usr_email = UserDefaults.standard.string(forKey: "email")!
                   self.usr_id = UserDefaults.standard.string(forKey: "senderid")!
    BookeventloadapiCalling(apikey:apikey,eve_id:event_id,country_code:country_code,language_code:language_code)
        }
        SetBackBarButtonCustom()
      purchaseoutlet.layer.cornerRadius = 10
        
     
        
        outerview.layer.shadowColor = UIColor.gray.cgColor
        outerview.layer.shadowOffset = CGSize(width: 3, height: 3)
        outerview.layer.shadowOpacity = 0.7
        outerview.layer.shadowRadius = 4.0
          outerview.layer.cornerRadius = 10
          innerview.layer.cornerRadius = 10
        priceOutlet.setBorder()
 languageSet()
 
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func languageSet(){
        ticketsavailablelabel.text = NSLocalizedString("Tickets available", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        quantitylabel.text = NSLocalizedString("Quantity", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           priceLabel.text = NSLocalizedString("Price", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        totalLabel.text = NSLocalizedString("Total", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
      
        purchaseoutlet.setTitle(NSLocalizedString("Purchase", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
       
    }
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        if ticket_type == "1"{
    
               freeticketQuantityText.inputView = self.myPickerView
        }else{
            quantitytextfield.inputView = self.myPickerView
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        //  toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.tintColor = UIColor.red
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title:  NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self, action: #selector(addgiftcardViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title:  NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self, action: #selector(addgiftcardViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
         if ticket_type == "1" {
             freeticketQuantityText.inputAccessoryView = toolBar
         }else{
        quantitytextfield.inputAccessoryView = toolBar
        }
       
        
    }
    @objc func doneClick() {
        quantitytextfield.resignFirstResponder()
         freeticketQuantityText.resignFirstResponder()
    }
    @objc func cancelClick() {
        quantitytextfield.resignFirstResponder()
         freeticketQuantityText.resignFirstResponder()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
          if ticket_type == "1" {
               self.pickUp(freeticketQuantityText)
          }else{
        self.pickUp(quantitytextfield)
        }
      
    }
    func BookeventloadapiCalling(apikey:String,eve_id:String,country_code:String,language_code:String){
       // self.startactvityIndicator()
        //personarray.removeAll()
        let scriptUrl = buisnessapiClass.EventBookingLoadPage
        
        let parameters: Parameters = ["apikey": apikey,"eve_id": event_id,"country_code":country_code,"language_code":language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                      
                        let currency = response.object(forKey: "currency")!
                       
                        for currenc in (currency as! [[String:Any]]){
                            let contry3 = currenc as NSDictionary
                         if  (contry3["currency_code"] as? String) != nil {
                            self.currency_code = contry3["currency_code"] as! String
                            
                            
                        }
                            
                        }
                 
                        let events = response.object(forKey: "events")!
                      
                        let country1 = events as! NSDictionary
                        if  (country1["ticket_type"] as? NSInteger) != nil {
                            self.ticket_type = String(country1["ticket_type"] as! NSInteger)
                            
                            
                        }
                        if  (country1["event_name"] as? String) != nil {
                            self.event_name = country1["event_name"] as! String
                           
                            
                        }
                        if  (country1["event_image1"] as? String) != nil {
                            let event_image1 = country1["event_image1"] as! String
                            self.event_image = "http://www.howlik.com/" +  event_image1
                        }
                     
                        
                        if  (country1["ticket_details"] as? NSDictionary) != nil {
                          let ticket_details = country1["ticket_details"] as! NSDictionary
                            if  (ticket_details["price"] as? String) != nil {
                                self.price = ticket_details["price"] as! String
                               
                                
                                
                            }
                           
                         
                        }
               
                        // }
                        
                        
                        DispatchQueue.main.async
                            {
                              
                                if self.ticket_type == "1"{
                                    self.quantitylabel.isHidden = true
                                    self.quantitytextfield.isHidden = true
                                    self.priceLabel.isHidden = true
                                      self.priceOutlet.isHidden = true
                                    self.totalLabel.isHidden = true
                                    self.totaltextfield.isHidden = true
                                    self.freeticketQuantityLabel.isHidden = false
                                      self.freeticketQuantityText.isHidden = false
                                    
                                    
                                }else{
                                 
                                    self.quantitylabel.isHidden = false
                                    self.quantitytextfield.isHidden = false
                                    self.priceLabel.isHidden = false
                                    self.priceOutlet.isHidden = false
                                    self.totalLabel.isHidden = false
                                    self.totaltextfield.isHidden = false
                                    self.freeticketQuantityText.isHidden = true
                                    self.freeticketQuantityLabel.isHidden = true
                                    self.priceOutlet.text = self.price
                                    let value =  (self.priceOutlet.text as! NSString).doubleValue * (self.quantitytextfield.text! as NSString).doubleValue
                                }
                              
                                self.stopActivityIndicator()
                                self.eventmage.sd_setImage(with: URL(string:self.event_image), placeholderImage:UIImage(named: "no-image02"))
                                self.titlelabel.text = self.event_name
                               
                             // self.totaltextfield.text = String(value)
                               
                      
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pricebook_Array.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pricebook_Array[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if ticket_type == "1" {
            freeticketQuantityText.text = pricebook_Array[row]
          
        }else{
        quantitytextfield.text = pricebook_Array[row]
        let value =  (self.priceOutlet.text! as NSString).doubleValue * (self.quantitytextfield.text! as NSString).doubleValue
        self.totaltextfield.text = String(value)
        }
    }
    

    @IBAction func purchaseAction(_ sender: Any) {
        if ticket_type == "1" {
            if (freeticketQuantityText.text?.isEmpty)!{
                
                alertsnack(message:NSLocalizedString("quantity is required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                
            }else{
                posttableapiCalling(apikey:apikey,eve_id:event_id,country_code:country_code,lan_id:self.language_code,usr_name:usr_name,usr_id:usr_id,usr_email:usr_email,curr_code:currency_code,ticket_quantity:freeticketQuantityText.text!,ticket_amount:freeticketQuantityText.text!,pay_now:"Purchase")
            }
        }else{
                if (quantitytextfield.text?.isEmpty)!{
                
                        alertsnack(message:NSLocalizedString("quantity is required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
         
                }else{
            
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "eventpaypal") as! eventpaypalViewController
                        vc.event_id = event_id
                        vc.currency_code = currency_code
                        vc.ticket_quantity = quantitytextfield.text!
                        vc.ticket_amount = totaltextfield.text!
                        let navController = UINavigationController(rootViewController: vc)
                        self.present(navController, animated: true, completion: nil)
                }
        }
    
    }
    func posttableapiCalling(apikey:String,eve_id:String,country_code:String,lan_id:String,usr_name:String,usr_id:String,usr_email:String,curr_code:String,ticket_quantity:String,ticket_amount:String,pay_now:String){
    //    self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.EventBookingPostPage1
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        let postString = "apikey=\(apikey)&eve_id=\(eve_id)&country_code=\(country_code)&lan_id=\(lan_id)&usr_id=\(usr_id)&usr_name=\(usr_name)&usr_email=\(usr_email)&curr_code=\(curr_code)&ticket_quantity=\(ticket_quantity)&ticket_amount=\(ticket_amount)&pay_now=\(pay_now)"
       
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                   
                   
                   
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.status == "success"{
                            // self.stopActivityIndicator()
                            
                            self.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton( NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.resignFirstResponder()
                               self.dismiss(animated: true, completion: nil)
                          
                            })
                            alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Successfully Booked!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                            
                            
                        }else{
                            self.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
        
    }
    func alertsnack(message: String){
        
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
}
extension UIImageView {
    func round(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
