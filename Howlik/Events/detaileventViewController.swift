//
//  detaileventViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/27/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FontAwesome_swift
import Alamofire
import GoogleMaps
import SCLAlertView
class detaileventViewController: UIViewController,NVActivityIndicatorViewable,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var tableview: UITableView!
    
    @IBOutlet var bookoutletheight: NSLayoutConstraint!
    @IBOutlet var bookoutlet: UIButton!
    var id = ""
    var apikey = ""
    
    var event_name = ""
    var event_date = ""
    
    var city_name = ""
    var event_image = ""
    
    var event_starttime = ""
    var eventEnd_date =  ""
     var event_endtime =  ""
    var longitude = ""
    var lattitude = ""
    var org_description = ""
    var country_code = ""
    var language_code = ""
    var ticket_type = ""
     var imageView = UIImageView()
    var price = ""
   //  var detailevent_Array = [detaileventclass]()
      let buisnessapiClass = ApiListViewcontroller()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     self.navigationController?.setNavigationBarHidden(true, animated: true)
        imageView.isUserInteractionEnabled = true
 bookoutlet.setTitle(NSLocalizedString("Book Now", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        
        }
        TableslotapiCalling(apikey:apikey,event_id:id)
        tableview.estimatedRowHeight = 15.0
        tableview.rowHeight = UITableViewAutomaticDimension
        
    }
    func TableslotapiCalling(apikey:String,event_id:String){
       // self.startactvityIndicator()
        //personarray.removeAll()
        let scriptUrl = buisnessapiClass.Eventdetailsbasedoneventid
        
        let parameters: Parameters = ["apikey": apikey,"event_id": event_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                       
                        
                        
                        let events = response.object(forKey: "events")!
                      
                       // for event in (events as! [[String:Any]]){
                       //     let popularElement = detaileventclass()
                        let contry1 = events as! NSDictionary
                            if  (contry1["event_name"] as? String) != nil {
                                self.event_name = contry1["event_name"] as! String
                               
                            }else{
                                let event_name = "nil"
                               
                            }
                            
                            if  (contry1["event_date"] as? String) != nil {
                                self.event_date = contry1["event_date"] as! String
                            }else{
                                self.event_date = ""
                                
                            }
                            if  (contry1["city_name"] as? String) != nil {
                                self.city_name = contry1["city_name"] as! String
                              
                            }
                            else{
                                self.city_name = "nil"
                           
                            }
                            if  (contry1["id"] as? String) != nil {
                                self.id = contry1["id"] as! String
                                
                            }
                            if  (contry1["event_image1"] as? String) != nil {
                                let event_image1 = contry1["event_image1"] as! String
                                self.event_image = "http://www.howlik.com/" +  event_image1
                            }
                            if  (contry1["event_starttime"] as? String) != nil {
                                self.event_starttime = contry1["event_starttime"] as! String
                               
                            }
                            if  (contry1["longitude"] as? String) != nil {
                                self.longitude = contry1["longitude"] as! String
                               
                            }
                            if  (contry1["latitude"] as? String) != nil {
                                self.lattitude = contry1["latitude"] as! String
                               
                            }
                        if  (contry1["eventEnd_date"] as? String) != nil {
                            self.eventEnd_date = contry1["eventEnd_date"] as! String
                            
                        }
                        if  (contry1["event_endtime"] as? String) != nil {
                            self.event_endtime = contry1["event_endtime"] as! String
                            
                        }
                        if  (contry1["about_event"] as? String) != nil {
                            self.org_description = contry1["about_event"] as! String
                            
                        }
                        if  (contry1["ticket_type"] as? String) != nil {
                            self.ticket_type = String(contry1["ticket_type"] as! String)
                          
                            
                        }
                        if  (contry1["ticket_price"] as? String) != nil {
                            self.price = contry1["ticket_price"] as! String
                            
                            
                        }
                            
                       // }
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                self.imageView.sd_setImage(with: URL(string:self.event_image), placeholderImage:UIImage(named: "no-image02"))
                                self.imageView.contentMode = .scaleAspectFill
                                //   parallaxHeaderView = imageView
                                
                                //setup bur view
                                //      imageView.blurView.setup(style: UIBlurEffectStyle.dark, alpha: 1).enable()
                                
                                self.tableview.parallaxHeader.view = self.imageView
                                self.self.tableview.parallaxHeader.height = 240
                                self.tableview.parallaxHeader.minimumHeight = 50
                                self.self.tableview.parallaxHeader.mode = .centerFill
                               
                                
                                let roundIcon = UIImageView(
                                    frame: CGRect(x: 0, y: 0, width: 100, height: 100)
                                )
                               
                                let btnLeftMenu: UIButton = UIButton()
                                
                                btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
                                btnLeftMenu.addTarget(self, action: #selector(self.onClcikBack), for: UIControlEvents.touchUpInside)
                                btnLeftMenu.frame = CGRect(x: 0, y: 10, width: 40, height: 40)
                                self.imageView.addSubview(btnLeftMenu)
                                self.tableview.parallaxHeader.view.addSubview(btnLeftMenu)
                                self.tableview.reloadData()
                                self.stopActivityIndicator()
                                //self.stopActivityIndicator()
                                
                                if self.ticket_type == "0"{
                                    //self.bookoutlet.isHidden = true
                                    self.bookoutletheight.constant = 0
                                }else if self.ticket_type == "1"{
                                   // self.bookoutlet.isHidden = true
                                     // self.bookoutletheight.constant = 0
                                    
                                    
                                    
                                    
                                    
                                    
                                    self.bookoutlet.isHidden = false
                                }else{
                                    self.bookoutlet.isHidden = false
                                }
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          if indexPath.section == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! detaileventTableViewCell
                cell.selectionStyle = .none
        cell.titlelabel?.text =   self.event_name
            if ticket_type == "0"{
                
        
        cell.likebutton.setImage(UIImage(named : "wallet"), for: UIControlState())
            }else if ticket_type == "1"{
                cell.likebutton.setTitle(NSLocalizedString("Free", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
                 cell.likebutton.setImage(UIImage(named : "wallet"), for: UIControlState())
            }else{
                cell.likebutton.setTitle(self.price, for: UIControlState())
                cell.likebutton.setImage(UIImage(named : "wallet"), for: UIControlState())
            }
        return cell
          }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath as IndexPath) as! eventdateTableViewCell
            //Starting
            cell.startlabel?.text =  NSLocalizedString("Starting", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") + " " + event_date + " " + NSLocalizedString("at", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") + " " + event_starttime
            cell.endlabel?.text =  NSLocalizedString("Ending", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") + " " + eventEnd_date + " " + NSLocalizedString("at", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") + " " + event_endtime
            return cell
          }else  if indexPath.section == 2{
             let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath as IndexPath) as! discriptionTableViewCell
              cell.discriptlabel?.text =   self.org_description
              return cell
          }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath as IndexPath) as! eventlocationTableViewCell
            let marker = GMSMarker()
            
            marker.position = CLLocationCoordinate2DMake((lattitude as NSString).doubleValue,(longitude as NSString).doubleValue)
            
            ///View for Marker
            let DynamicView = UIView(frame: CGRect(x:0, y:0, width:50, height:50))
            DynamicView.backgroundColor = UIColor.clear
            
            //Pin image view for Custom Marker
            let imageView = UIImageView()
            imageView.frame  = CGRect(x:0, y:0, width:50, height:20)
            // imageView.image = UIImage(named:"LocationPin")
            imageView.image = (UIImage.fontAwesomeIcon(name:  FontAwesome.mapMarker, textColor: UIColor.red,size: CGSize(width: 50, height: 20)))
            
            //Adding pin image to view for Custom Marker
            DynamicView.addSubview(imageView)
            
            UIGraphicsBeginImageContextWithOptions(DynamicView.frame.size, false, UIScreen.main.scale)
            DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let imageConverted: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            marker.icon = imageConverted
            marker.map = cell.mapview
            
            cell.mapview.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 11)
            return cell
        }
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          if indexPath.section == 0{
        return 92
          }else if  indexPath.section == 1{
             return 92
          }else if  indexPath.section == 2{
              return UITableViewAutomaticDimension
        }
         return 184
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
   
    @IBAction func bookeventAction(_ sender: Any) {
          if  UserDefaults.standard.string(forKey:"apikey") != nil{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "bookevent") as! bookeventViewController
            vc.event_id = id
            vc.ticket_type = ticket_type
            let navController = UINavigationController(rootViewController: vc)
            
            self.present(navController, animated: true, completion: nil)
        }else{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton("OK", action: { // create button on alert
             
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Please Register to book Event", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }
       

    }
    
}
