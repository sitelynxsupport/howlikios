//
//  discountsViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/3/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import FontAwesome_swift
import UITextViewPlaceholder
import TTGSnackbar
import SCLAlertView
class discountsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable,UITextViewDelegate {
    var id = ""
    var apikey = ""
      var val = false
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    var country_code = ""
    var language_code = ""
    var discountArray = [discountclass]()
    let buisnessapiClass = ApiListViewcontroller()
    
    @IBOutlet var offerHeadlineLabel: UILabel!
    @IBOutlet var offerTypeLabel: UILabel!
    var status = ""
    @IBOutlet var createoutlet: UIButton!
    @IBOutlet var offertextfield: UITextField!
   
    @IBOutlet var offerdetails: UITextView!
    @IBOutlet var discriptiontextfield: UITextField!
    @IBOutlet var amounttextfield: UITextField!
    @IBOutlet var offertypeoutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Discounts and Offers", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
        }
        // Do any additional setup after loading the view.
        createoutlet.backgroundColor = .clear
        createoutlet.layer.cornerRadius = 20
        createoutlet.layer.borderWidth = 1
        createoutlet.layer.borderColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
        createoutlet.setTitleColor(UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0), for: UIControlState())
        
        discriptiontextfield.setBorder()
        amounttextfield.setBorder()
        offertextfield.setBorder()
        offertypeoutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        amounttextfield.setLeftPaddingPoints(5.0)
        discriptiontextfield.setLeftPaddingPoints(5.0)

        offerdetails.layer.borderColor = UIColor.lightGray.cgColor
        offerdetails.layer.borderWidth = 0.5;
        offerdetails.backgroundColor = UIColor.clear
        offerdetails.layer.cornerRadius = 4
        offerdetails.layer.masksToBounds = false
        offerdetails.delegate = self
        
         setLanguage()
LoadcreateapiCalling(apikey:apikey,biz_id:id,country_code:country_code,language_code:language_code)
      
        
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func setLanguage(){
        amounttextfield.placeholder = NSLocalizedString("Amount or Percentage", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        discriptiontextfield.placeholder = NSLocalizedString("Description", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        offerdetails.placeholder = NSLocalizedString("Offerdetails", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        offerTypeLabel.text = NSLocalizedString("OfferType", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           offerHeadlineLabel.text = NSLocalizedString("Offer Headline", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        createoutlet.setTitle(NSLocalizedString("Create", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func LoadcreateapiCalling(apikey:String,biz_id:String,country_code:String,language_code:String){
        // self.startactvityIndicator()
        discountArray.removeAll()
        let scriptUrl = buisnessapiClass.LoadCreateOffer
        
        let parameters: Parameters = ["apikey": apikey,"biz_id": biz_id,"country_code":country_code,"language_code":language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        let types = response.object(forKey: "types")!
                        for type in (types as! [[String:Any]]){
                            let discountElement = discountclass()
                            let contry3 = type as NSDictionary
                            if  (contry3["title"] as? String) != nil {
                               let title = contry3["title"] as! String
                                discountElement.title = title
                               
                              

                            }
                            
                            if  (contry3["translation_of"] as? String) != nil {
                                let translation_of = contry3["translation_of"] as! String
                             
                                 discountElement.translation_of = translation_of
                               
                                
                            }
                            self.discountArray.append(discountElement)

                        }
                        
                        
                        
                        
                        // }
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                
                                self.myTableView.reloadData()
                                let offertitle = self.discountArray[0].title
                                self.offertypeoutlet.setTitle(self.self.discountArray[0].title, for: UIControlState())
                                
                        
                              
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }

  func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        addview.addSubview(newview)
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        //view1.backgroundColor  = UIColor.lightGray
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        label.text = NSLocalizedString("Select Location", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        addview.isHidden = false
        newview.isHidden =  false
        myTableView.isHidden =  false
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discountArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //cell.selectionStyle = .none
        // cell.separatorInset = .zero
        cell.textLabel?.text = discountArray[indexPath.row].title
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // personcount = numberarray[indexPath.row]
   offertypeoutlet.setTitle(discountArray[indexPath.row].title, for: UIControlState())
        newview.isHidden =  true
        addview.isHidden = true
        myTableView.isHidden =  true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    @IBAction func offeraction(_ sender: Any) {
           createTableview()
    }
    @IBAction func createAction(_ sender: Any) {
       if (offertypeoutlet!.currentTitle == nil){
            alertsnack(message: NSLocalizedString("Offer Type is required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        
        }else if amounttextfield!.text!.isEmpty{
            alertsnack(message:  NSLocalizedString("Amount is required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }
        else if discriptiontextfield!.text!.isEmpty{
            alertsnack(message:NSLocalizedString("Enter Discription", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        
       }else if offerdetails!.text!.isEmpty{
        alertsnack(message:NSLocalizedString("Enter Offer Details", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
       
        
       }else{
        postofferapiCalling(apikey:apikey,biz_id:id,offer_type:offertypeoutlet.currentTitle!,offer_percent:amounttextfield.text!,offer_content:offerdetails.text,offer_desc:discriptiontextfield.text!)
        }
    }
    func alertsnack(message: String){
        
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    func postofferapiCalling(apikey:String,biz_id:String,offer_type:String,offer_percent:String,offer_content:String,offer_desc:String){
        //    self.startactvityIndicator()
        let scriptUrl = buisnessapiClass.PostCreateOffer
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)&offer_type=\(offer_type)&offer_percent=\(offer_percent)&offer_content=\(offer_content)&offer_desc=\(offer_desc)"
       
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                   
                   self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                  
                    
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.status == "success"{
                            // self.stopActivityIndicator()
                            
                            self.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.resignFirstResponder()
                                self.dismiss(animated: true, completion: nil)

                            })
                            alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("SuccessfullyAdded", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                            
                            
                        }else{
                            self.stopActivityIndicator()
                            let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false // hide default button
                            )
                            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
        
    }
}
