//
//  eventAddingViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/1/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import DLRadioButton
import  Alamofire
import FontAwesome_swift
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import UITextViewPlaceholder
import IQKeyboardManagerSwift
import Photos
import TTGSnackbar
import SCLAlertView
import Tags

class eventAddingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,GMSPlacePickerViewControllerDelegate,UITextViewDelegate,UITextFieldDelegate,TagsDelegate,UISearchResultsUpdating,UISearchBarDelegate  {
    
    //,
    
  
   // fileprivate var tagsField = WSTagsField()
     fileprivate var layerBoundsObserver: NSKeyValueObservation?
    @IBOutlet var eventaddingOutlet: UIButton!
    
   // @IBOutlet fileprivate weak var tagsView: UIView!
    @IBOutlet var mapOutlet: UIButton!
    
    @IBOutlet var mapTextview: UITextView!
    
    @IBOutlet var paidTicketamountTextfield: UITextField!
    
    @IBOutlet var piadTicketcountTextfield: UITextField!
    
    @IBOutlet var freeticketTextfield: UITextField!
    
    @IBOutlet var eventdetailsTextfield: UITextView!
    @IBOutlet var organisationTextfield: UITextView!
    
    @IBOutlet var tagsView: TagsView!
    
    @IBOutlet var createcityOutlet: UIButton!
 
    
    @IBOutlet var countryaddingOutlet: UIButton!
    
    
    @IBOutlet var countryTextfield: UITextField!
    
    @IBOutlet var cityTextfield: UITextField!
    
    @IBOutlet var selectedimageview: UIImageView!
    @IBOutlet var uploadOutlet: UIButton!
       @IBOutlet internal var eventTitle: UITextField!
    
    @IBOutlet var eventtypeTextfield: UITextField!
    @IBOutlet var eventTypeOutletss: UIButton!
   
    
    
    @IBOutlet var squareOUtlet: UIButton!
   
    @IBOutlet var noneOutlet: DLRadioButton!
    
    @IBOutlet var freeTicketOutlet: DLRadioButton!
    
    @IBOutlet var privatepageOutlet: DLRadioButton!
    @IBOutlet var publicPageOutlet: DLRadioButton!
    @IBOutlet var paidTicketOutlet: DLRadioButton!
    
    @IBOutlet var AllowsLinkLabel: UILabel!
    
    @IBOutlet var CreateTicketLabel: UILabel!
    
    @IBOutlet var ListingPrivacyLabel: UILabel!
    var tagArray = [String]()
    var myTableView = UITableView()
    var newview = UIView()
     var eventClass = ApiListViewcontroller()
    var apikey = ""
    var language_code = ""
    var country_code = ""
    var country = ""
    var eventtypeArray = NSMutableArray()
     var createcountryArray = NSMutableArray()
    var buttonIndex = Int()
    var arr1  = ["1","2","3","$","5"]
    var cancelButton = UIBarButtonItem()
    var doneButton = UIBarButtonItem()
    var spaceButton = UIBarButtonItem()
     let toolBar = UIToolbar()
     var selectedIndex:NSIndexPath?
    var searchController:UISearchController!

    
    var isSearching = false
      var filteredobjects = [createeventClass]()
     var createTitleArry = [createeventClass]()
    var tableIndex = 0
     var createcityArray = [createeventClass]()
    var filteredcityobjects = [createeventClass]()
    var createcityTitleArry = [createeventClass]()
    let alert = emailValidateViewController()
   let imagePicker = UIImagePickerController()
    var countrytitleArray = [String]()
    var Placename = ""
    var Placeaddress = ""
    var placelattitude = Double()
    var placlongitude = Double()
   // var apikey = ""
    var  dashImage = ["city","city","city","city","city","city","city","city","city"]
    var cityid = ""
    var maptext = ""
    var status = ""
    var socialshare = ""
    var ticket_type = ""
    var ticket_count = ""
    var ticket_price = ""
    var event_privacy = ""
    var tickettypeFlag = 0
     var event_privacyFlag = 0
    var event_imageName = ""
    var selectedImage = UIImage()
    var event_type_id = ""
       var val = false
    var sourceType = Int()
    var cityvalue = 1
    var addview = UIView()
    @IBOutlet var startDateOutlet: UITextField!
    @IBOutlet var enddateOutlet: UITextField!
    
    @IBOutlet var heightcon: NSLayoutConstraint!
      var tablecatheightindicator =  Int()
     var flagImage = ["united_arab_emirates","bahrain","india","kuwait","oman","qatar","saudi_arabia"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tagArray.removeAll()
     // self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        freeticketTextfield.setLeftPaddingPoints(5.0)
        paidTicketamountTextfield.setLeftPaddingPoints(5.0)
        piadTicketcountTextfield.setLeftPaddingPoints(5.0)
        countryTextfield.setLeftPaddingPoints(5.0)
        cityTextfield.setLeftPaddingPoints(5.0)
        eventTitle.setLeftPaddingPoints(5.0)
        startDateOutlet.setLeftPaddingPoints(5.0)
        enddateOutlet.setLeftPaddingPoints(5.0)
          eventtypeTextfield.setLeftPaddingPoints(5.0)
        paidTicketamountTextfield.setBorder()
        piadTicketcountTextfield.setBorder()
        countryTextfield.setBorder()
        cityTextfield.setBorder()
        eventtypeTextfield.setBorder()
          eventTitle.setBorder()
          startDateOutlet.setBorder()
          enddateOutlet.setBorder()
        freeticketTextfield.setBorder()
        mapTextview.layer.borderColor = UIColor.lightGray.cgColor
        mapTextview.layer.borderWidth = 0.5;
        mapTextview.backgroundColor = UIColor.clear
        mapTextview.layer.cornerRadius = 4
        mapTextview.layer.masksToBounds = false
       
        
        eventdetailsTextfield.layer.borderWidth = 0.5;
        eventdetailsTextfield.backgroundColor = UIColor.clear
        eventdetailsTextfield.layer.cornerRadius = 4
        eventdetailsTextfield.layer.masksToBounds = false
        organisationTextfield.layer.borderColor = UIColor.lightGray.cgColor
     
        organisationTextfield.layer.borderWidth = 0.5;
        organisationTextfield.backgroundColor = UIColor.clear
        organisationTextfield.layer.cornerRadius = 4
        organisationTextfield.layer.masksToBounds = false
        eventaddingOutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        createcityOutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        countryaddingOutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        uploadOutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
           squareOUtlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
    
        eventaddingOutlet.layer.cornerRadius = 10 
     
               let screenSize: CGRect = UIScreen.main.bounds
       heightcon.constant = 0.0
        tagsView.isHidden = true
        noneOutlet.isSelected = true
        noneOutlet.indicatorColor = UIColor.black
        
        publicPageOutlet.isSelected = true
        publicPageOutlet.indicatorColor = UIColor.black
        ticket_type = "0"
        event_privacy = "0"
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        // Do any additional setup after loading the view.
        
        doneButton = UIBarButtonItem(title: NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self, action: #selector(eventAddingViewController.doneClick))
        spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        doneButton.tintColor = UIColor.green
          toolBar.isUserInteractionEnabled = true
        imagePicker.delegate = self
        mapTextview.delegate = self
        eventdetailsTextfield.delegate = self
         organisationTextfield.delegate = self
//         tagsField.frame = tagsView.bounds
//        tagsView.addSubview(tagsField)
//
//        tagsField.placeholder = "Enter a tag"
//        tagsField.returnKeyType = .send
//        tagsField.delimiter = ", "
//        tagsField.layer.borderWidth = 1.0
//        tagsField.layer.borderColor = UIColor.lightGray.cgColor
//        tagsField.layer.cornerRadius = 4.0
//        tagsField.layer.masksToBounds = false
//        tagsField.layoutMargins = UIEdgeInsets(top: 2, left: 6, bottom: 2, right: 6)
//        tagsField.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        tagsView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraint(NSLayoutConstraint(
            item: self.view,
            attribute: .leading,
            relatedBy: .equal,
            toItem: tagsView,
            attribute: .leading,
            multiplier: 1,
            constant: 0)
        )
        // cell.normalview.addSubview(tagsView)
        tagsView.paddingLeftRight = 6
        tagsView.paddingTopBottom = 4
        tagsView.marginLeftRight = 15
        
        tagsView.marginTopBottom = 4
        tagsView.removeAll()
        
        
        
        tagsView.delegate = self
        
        tagsView.tags = "Type your friend's Howlik Email"
        self.tagsView.layer.borderWidth = 0.5
        self.tagsView.layer.borderColor = UIColor.gray.cgColor
          self.tagsView.layer.cornerRadius = 4.0
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
        squareOUtlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
        languageSet()
        //textFieldEventss()
        uploadOutlet.layer.cornerRadius = 15.0
        selectedimageview.layer.masksToBounds=true
        selectedimageview.layer.borderWidth=0.5
        selectedimageview.layer.borderColor = UIColor.lightGray.cgColor
        selectedimageview.layer.cornerRadius = 4
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        //        self.view.addGestureRecognizer(tapGesture)
        newview.isHidden = true
        myTableView.isHidden = true
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.country_code = UserDefaults.standard.string(forKey: "country_code")!
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
createEventapiCalling(apikey:self.apikey,language_code:language_code,country_code:country_code)
        }else{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
              // action on click
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError(NSLocalizedString("ERROR", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:NSLocalizedString("Eventsyouaddedwillappearhere.\nPleaseRegistertoaddEvents!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "") )
            //
        }
        if  UserDefaults.standard.string(forKey:"language_code") == "en"{
            mapOutlet.contentHorizontalAlignment = .left
            createcityOutlet.contentHorizontalAlignment = .left
            countryaddingOutlet.contentHorizontalAlignment = .left
            eventTypeOutletss.contentHorizontalAlignment = .left
            
        }else{
            mapOutlet.contentHorizontalAlignment = .right
            createcityOutlet.contentHorizontalAlignment = .right
            countryaddingOutlet.contentHorizontalAlignment = .right
            eventTypeOutletss.contentHorizontalAlignment = .right
        }
     //
       
    }
    func languageSet(){
        self.navigationItem.title = NSLocalizedString("NEWEVENT", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        eventTitle.placeholder = NSLocalizedString("Eventtitle", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        eventtypeTextfield.placeholder = NSLocalizedString("EventType", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           startDateOutlet.placeholder = NSLocalizedString("StartDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           enddateOutlet.placeholder = NSLocalizedString("EndDate", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          countryTextfield.placeholder = NSLocalizedString("Country", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          cityTextfield.placeholder = NSLocalizedString("City", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          mapTextview.placeholder = NSLocalizedString("PinLocation", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        uploadOutlet.setTitle(  NSLocalizedString("UploadImage", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
         eventdetailsTextfield.placeholder = NSLocalizedString("EventDetails", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           organisationTextfield.placeholder = NSLocalizedString("OrganizationDetails", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        AllowsLinkLabel.text = NSLocalizedString("AllowLinksToFacebookTwitterAndinstagram", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        CreateTicketLabel.text = NSLocalizedString("CreateTicket", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        noneOutlet.setTitle(  NSLocalizedString("None", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
         freeTicketOutlet.setTitle(  NSLocalizedString("FreeTicket", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        paidTicketOutlet.setTitle(  NSLocalizedString("PaidTicket", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
         freeticketTextfield.placeholder = NSLocalizedString("Count", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        piadTicketcountTextfield.placeholder = NSLocalizedString("Count", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        paidTicketamountTextfield.placeholder = NSLocalizedString("Amount", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        ListingPrivacyLabel.text = NSLocalizedString("ListingPrivacy", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         publicPageOutlet.setTitle(  NSLocalizedString("PublicPage", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
          privatepageOutlet.setTitle(  NSLocalizedString("PrivatePage", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
         eventaddingOutlet.setTitle(  NSLocalizedString("Finish", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
      //  tagsField == nil
      //  dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
        
    }
  
    func tagsTouchAction(_ tagsView: TagsView, tagButton: TagButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        
        alertController.addAction(UIAlertAction(title: "Add email", style: .default, handler: { (_) in
            /// Next Insert UIAlertController
            let alertController = UIAlertController(title: nil, message: "Enter Email", preferredStyle: .alert)
            alertController.addTextField(configurationHandler: { (textField) in })
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alertController.addAction(UIAlertAction(title: "Add", style: .default, handler: { (_) in
                guard let text = alertController.textFields?.first?.text, text.count != 0 else{
                    return
                }
                /// Insert
                
                self.tagsView.insert(text, at: tagButton.index + 1)
                let value = tagsView.tagTextArray[0]
              
                if tagsView.tagTextArray[0].contains("Type your friend's Howlik Email"){
                    tagsView.remove(0)
                }
                
            }))
            self.present(alertController, animated: true, completion: nil)
        }))
        
        
        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            /// Remove
            self.tagsView.remove(tagButton)
            if tagsView.tagTextArray.count == 0{
                tagsView.tags = "Type your friend's Howlik Email"
            }
            
        }))
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    @IBAction func eventTypeAction(_ sender: Any) {
           tableIndex = 0
      eventtypeTextfield.placeholder = ""
         self.createTableview()
      

    }
   
    func createTableview(){
    
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let height =  Int(tableviewcatagoryHeight())
     
        
        
        if tablecatheightindicator == 0{
            self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
        }else{
            self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
            newview.center =  view.center
        }
        self.addview.addSubview(newview)
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 8, width: newview.frame.width, height: newview.frame.height))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        view1.backgroundColor  = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        // if buttonIndex == 0{
        myTableView.register(UINib(nibName: "eventTypecell", bundle: nil), forCellReuseIdentifier: "myIdentifierss")
        label.text =  NSLocalizedString("SelectCategory", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
        newview.backgroundColor = UIColor.white
        myTableView.reloadData()
        
    }
    func tableviewcatagoryHeight() -> Double{
        
        let screenSize: CGRect = UIScreen.main.bounds
        let maximumHeight = screenSize.height - 100
        if (Double(44.0)  *  Double(eventtypeArray.count) >  Double(maximumHeight)){
            tablecatheightindicator = 0
            return Double(maximumHeight)
            
        }else{
          
            tablecatheightindicator = 1
            return(44.0  * Double(eventtypeArray.count) + 55.0 + 50.0 )
            
            
        }
        
    }
    func createcountryTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        self.addview.addSubview(newview)
        newview.center = view.center
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
  
        label.text = NSLocalizedString("SelectCountry", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        myTableView.register(UINib(nibName: "profilecountryTableViewCell", bundle: nil), forCellReuseIdentifier: "myIdentifier")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
        let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 65, y:newview.frame.height - 45,width: 60  , height:  40));
        newview.addSubview(button);
        
        button.layer.cornerRadius = 3.0
        button.setTitle(NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        label.textColor = UIColor.black
        newview.backgroundColor = UIColor.white
        label.isHighlighted = true
        label.font = UIFont.boldSystemFont(ofSize: 20)
        myTableView.reloadData()
    }
    @objc func okView(_ sender: UIButton)
    {
        
        
        newview.isHidden = true
        addview.isHidden = true
        myTableView.isHidden = true
        
        
    }
   
    
    
    @IBAction func eventAddButton(_ sender: Any) {
       
        
    }
    override func viewDidAppear(_ animated: Bool) {
       
      
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func customtableview(){
        
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: Int(screenSize.height - 100) ))
        self.addview.addSubview(self.newview)
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height))
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "citycell")
        newview.addSubview(myTableView)
        myTableView.backgroundColor = UIColor.groupTableViewBackground
        newview.layer.cornerRadius = 10.0
        myTableView.layer.cornerRadius = 10.0
     
        
    }
    
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search here..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.searchBarStyle = UISearchBarStyle.prominent
        // Place the search bar view to the tableview headerview.
      //  myTableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.isUserInteractionEnabled = true
    }
//    deinit {
//        self.searchController.view.removeFromSuperview()
//    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
     
        isSearching = true
        myTableView.reloadData()
    }
    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//
//        isSearching = false
//
//        myTableView.reloadData()
//    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    
        myTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
      
        if !isSearching {
            isSearching = true
            myTableView.reloadData()
        }
        
        searchController.searchBar.resignFirstResponder()
    }
   
//    func updateSearchResults(for searchController: UISearchController) {
//        //        if tableIndex == 1 {
//        //        // If we haven't typed anything into the search bar then do not filter the results
//        //        if searchController.searchBar.text! == "" {
//        //            filteredobjects = createcountryArray as! [createeventClass]
//        //        } else {
//        //            // Filter the results
//        //
//        //            filteredobjects = createTitleArry.filter { $0.title.lowercased().contains(searchController.searchBar.text!.lowercased()) }
//        //        }
//        //        }else{
//        //  if searchController.searchBar.text! == "" {
//        //       filteredcityobjects = createcityArray
//        //  } else {
//        // Filter the results
//
//        filteredcityobjects = createcityArray.filter { $0.citytitle.lowercased().contains(searchController.searchBar.text!.lowercased()) }
//        //  }
//        // }
//        self.myTableView.reloadData()
//
//
//    }
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchString = searchController.searchBar.text
        
        // Filter the data array and get only those countries that match the search text.
        filteredcityobjects = createcityArray.filter({ (country) -> Bool in
            let countryText: NSString = country.citytitle as NSString
            
            return (countryText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        // Reload the tableview.
        myTableView.reloadData()
    }
    func createEventapiCalling(apikey:String,language_code:String,country_code:String){
   alert.startactvityIndicator()
        self.createcountryArray.removeAllObjects()
        self.eventtypeArray.removeAllObjects()
       // self.createcityArray.removeAll()
     
        let scriptUrl = eventClass.LoadCreateEvent
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code,"country_code": country_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        
                       
                    if (response["eventtype"] != nil) {
                     let events = response.object(forKey: "eventtype")!
                   
                        for event in (events as! [[String:Any]]){
                            let createElement = createeventClass()
                            let contry1 = event as NSDictionary
                            if  (contry1["name"] as? String) != nil {
                                let name = contry1["name"] as! String
                                createElement.name = name
                            }else{
                                let name = "nil"
                                createElement.name = name
                            }
                            if  (contry1["translation_of"] as? String) != nil {
                                let translation_of = contry1["translation_of"] as! String
                                createElement.translation_of = String(translation_of)
                            }else{
                                let translation_of = "nil"
                                createElement.translation_of =  translation_of
                            }
                              self.eventtypeArray.add(createElement)
                        }
                        }
                        if (response["countries"] != nil) {
                        let countries = response.object(forKey: "countries")!
                 
                        for countr in (countries as! [[String:Any]]){
                            let createCountry = createeventClass()
                            let contry1 = countr as NSDictionary
                            if  (contry1["code"] as? String) != nil {
                                let code = contry1["code"] as! String
                               createCountry.code = code
                            }else{
                                let code = "nil"
                                createCountry.code = code
                            }
                            if  (contry1["title"] as? String) != nil {
                                let title = contry1["title"] as! String
                                createCountry.title = title
                            }else{
                                let title = "nil"
                                createCountry.title =  title
                            }
                            self.createcountryArray.add(createCountry)
                            self.createTitleArry.append(createCountry)
                        }
                        }
                         if (response["cities"] != nil) {
                        let cities = response.object(forKey: "cities")!
                     
                        for citie in (cities as! [[String:Any]]){
                            let createCity = createeventClass()
                            let contry1 = citie as NSDictionary
                            if  (contry1["id"] as? String) != nil {
                                let id = contry1["id"] as! String
                                createCity.id = String(id)
                            }else{
                                let id = "nil"
                                createCity.id = id
                            }
                            if  (contry1["title"] as? String) != nil {
                                let title = contry1["title"] as! String
                                createCity.citytitle = title
                            }else{
                                let title = "nil"
                                createCity.citytitle =  title
                            }
                            self.createcityArray.append(createCity)
                            //self.createcityTitleArry.append(createCity)
                               
                        }
                        }
                        DispatchQueue.main.async
                            {
                                if self.cityvalue == 2{
                                    self.customtableview()
                                    self.configureSearchController()
                                    self.myTableView.reloadData()
                                    
                                   self.cityvalue = 1
                                }
                                
                               
                              //  self.myTableView.reloadData()
                                self.alert.stopActivityIndicator()
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                 //   self.stopActivityIndicator()
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableIndex == 0{
             return eventtypeArray.count
        }else if tableIndex == 1 {
//            if isSearching{
//                return filteredobjects.count
//            }
            return createcountryArray.count
            
        }else{
          
                if isSearching{
                    return filteredcityobjects.count
                }
                return createcityArray.count
                
           
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableIndex == 0{
          
        let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifierss", for: indexPath) as! eventTypecell
        let eventTypeclass : createeventClass = eventtypeArray.object(at: indexPath.row) as! createeventClass
                cell.selectionStyle = .none
        cell.eventLabel.text = eventTypeclass.name
            event_type_id = eventTypeclass.translation_of
            
        buttonIndex = indexPath.row
  
        return cell
        }else if tableIndex == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! profilecountryTableViewCell
            cell.selectionStyle = .none
             let eventCountryclass : createeventClass = createcountryArray.object(at: indexPath.row) as! createeventClass
                cell.selectionStyle = .none
//           // if isSearching{
//                cell.textLabel?.text = self.filteredobjects[indexPath.row].title
//                  country_code = self.filteredobjects[indexPath.row].code
//
//          }else{
                cell.countryname?.text = eventCountryclass.title
              cell.countryflag.image = UIImage(named:flagImage[indexPath.row])
                  country_code = eventCountryclass.code
        
           // }
            return cell
        }else {
            
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "citycell", for: indexPath)
                cell.selectionStyle = .none
       //     let eventCityclass : createeventClass = createcityArray.object(at: indexPath.row) as! createeventClass
            cell.backgroundColor = UIColor.white
            if isSearching{
                cell.textLabel?.text = self.filteredcityobjects[indexPath.row].citytitle
                
            }else{
                cell.textLabel?.text = createcityArray[indexPath.row].citytitle
            }
            return cell
        }
       
    }
    
 
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
     if tableIndex == 0{
         let eventTypeclass : createeventClass = eventtypeArray.object(at: indexPath.row) as! createeventClass
       
      
        eventTypeOutletss.setTitle(eventTypeclass.name, for: UIControlState())
     }else if tableIndex == 1{
         let eventCountryclass : createeventClass = createcountryArray.object(at: indexPath.row) as! createeventClass
        if isSearching{
        
          countryaddingOutlet.setTitle(self.filteredobjects[indexPath.row].title, for: UIControlState())
            country_code = self.filteredobjects[indexPath.row].code
            
        }else{
            
          countryaddingOutlet.setTitle(eventCountryclass.title, for: UIControlState())
            country_code = eventCountryclass.code
        }
     }
        else {
         //   let eventCityclass : createeventClass = createcityArray.object(at: indexPath.row) as! createeventClass
            if isSearching{
              
                cityid = self.filteredobjects[indexPath.row].id
                createcityOutlet.setTitle(self.filteredobjects[indexPath.row].citytitle, for: UIControlState())
               
            }else{
         
                     cityid = createcityArray[indexPath.row].id
               
                createcityOutlet.setTitle(createcityArray[indexPath.row].citytitle, for: UIControlState())
            }
    }
      addview.isHidden = true
        newview.isHidden = true
        myTableView.isHidden = true
        
        
    }

    @IBAction func startDateAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        startDateOutlet.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self,action:#selector(eventAddingViewController.canceldateClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        startDateOutlet.inputAccessoryView = toolBar
        
    }
    @objc func canceldateClick() {
        startDateOutlet.text = ""
        startDateOutlet.resignFirstResponder()
        
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        // Apply date format
        startDateOutlet.text = dateFormatter.string(from: sender.date)
        
    }
    @objc func doneClick() {
        
        startDateOutlet.resignFirstResponder()
        enddateOutlet.resignFirstResponder()
      
    }
  
    
    @IBAction func EnddateAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        enddateOutlet.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerTopValueChanged), for: UIControlEvents.valueChanged)
        cancelButton = UIBarButtonItem(title:  NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .plain, target: self,action:#selector(eventAddingViewController.canceltimeToClick))
        cancelButton.tintColor = UIColor.red
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        
        enddateOutlet.inputAccessoryView = toolBar
    }
    @objc func datePickerTopValueChanged(sender:UIDatePicker) {
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        // Apply date format
        enddateOutlet.text = dateFormatter.string(from: sender.date)
        
    }
    @objc func canceltimeToClick() {
        enddateOutlet.text = ""
        enddateOutlet.resignFirstResponder()
    }
    @IBAction func countryAddingAction(_ sender: Any) {
       countryTextfield.placeholder = ""
    tableIndex = 1
        createcountryTableview()
        myTableView.reloadData()
    }
    
   
   
    
  
  
  
    
    @IBAction func createcityAction(_ sender: Any) {
   
            cityvalue = 2
          tableIndex = 2
         cityTextfield.placeholder = ""
createEventapiCalling(apikey:self.apikey,language_code:language_code,country_code:country_code)
        
        if createcityArray.count == 0 {
            createEventapiCalling(apikey:self.apikey,language_code:language_code,country_code:country_code)
        }
////        }else{
////            createEventapiCalling(apikey:self.apikey,language_code:language_code,country_code:country_code)
//        }
      
      
    
       
        
    }
   
    
    
    
    @IBAction func mapAction(_ sender: Any) {
        mapTextview.placeholder = ""
        let  config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        
        present(placePicker, animated: true, completion: nil)
        placePicker.delegate = self
        
    }
 
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace)
    {
        
   
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
              mapTextview.placeholder = " "
        
     
        Placename = place.name
        Placeaddress = "\(String(describing: place.formattedAddress))"
        let total = place.name + "\n" + place.formattedAddress!
        placelattitude = place.coordinate.latitude
        placlongitude = place.coordinate.longitude
        //UserDefaults.standard.set(Placename, forKey: "Placename")
       // UserDefaults.standard.set(Placeaddress, forKey: "Placeaddress")
       // UserDefaults.standard.set(place.coordinate.latitude, forKey: "placelattitude")
      //  UserDefaults.standard.set(place.coordinate.longitude, forKey: "placelongitude")
           mapTextview.placeholder = " "
        mapOutlet.setTitle(total, for: UIControlState())
     
       
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
    
       
    }
    
    
    @IBAction func uploadImage(_ sender: Any) {

        showActionSheet()
    }
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            sourceType = 1
         //   let myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                      sourceType = 2
           // let myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func showActionSheet() {
     //   currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title:  NSLocalizedString("Gallery", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title:  NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }


     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                selectedimageview.image = image
                selectedImage = image
                selectedimageview.contentMode = .scaleAspectFit
            if let asset = PHAsset.fetchAssets(withALAssetURLs: [info[UIImagePickerControllerReferenceURL] as! URL],
                                               options: nil).firstObject {
              
                PHImageManager.default().requestImageData(for: asset, options: nil, resultHandler: { _, _, _, info in

                    if let fileName = (info?["PHImageFileURLKey"] as? NSURL)?.lastPathComponent {
                        self.self.event_imageName = fileName
                        //do sth with file name
                    }
                })
            }
           
            self.dismiss(animated: true, completion: nil)
        }
   
    }
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func eventaddingAction(_ sender: Any) {
    
        if tickettypeFlag == 0{
            ticket_count = ""
            ticket_price = ""
        }
        else if tickettypeFlag == 1{
              ticket_count = freeticketTextfield.text!
        }else  {
            ticket_count = piadTicketcountTextfield.text!
            ticket_price = paidTicketamountTextfield.text!
        }
        if event_privacyFlag == 0{
           
        }else{
            if tagsView.tagTextArray[0].contains("Type your friend's Howlik Email"){
                tagsView.remove(0)
                  publicPageOutlet.isSelected = true
            }
            
        }
        if eventTitle!.text!.isEmpty{
            alertsnack(message:  NSLocalizedString("EventTitleisrequired", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (eventTypeOutletss!.currentTitle == nil){
            alertsnack(message: NSLocalizedString("SelectEventType", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (countryaddingOutlet!.currentTitle == nil){
            alertsnack(message:  NSLocalizedString("Pleaseselectacountry", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (createcityOutlet!.currentTitle == nil){
            alertsnack(message: NSLocalizedString("Pleaseselectacity", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }
        else if (mapOutlet!.currentTitle == nil){
            alertsnack(message: NSLocalizedString("Pleaseselectalocation", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if organisationTextfield!.text!.isEmpty{
            alertsnack(message:  NSLocalizedString("EnteraOrganisationDetails", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }
        else if eventdetailsTextfield!.text!.isEmpty{
            alertsnack(message: NSLocalizedString("EnterEventDetails", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        } else if tickettypeFlag == 1 {
            if (freeticketTextfield.text?.isEmpty)!{
                alertsnack(message: NSLocalizedString("EnterFreeTicketCount", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            }else{
                apiCalling(apikey: apikey, event_title: eventTitle.text!, event_type: event_type_id, start_date: startDateOutlet.text!, end_date: enddateOutlet.text!, country_code: country_code, city_code: cityid, event_details: eventdetailsTextfield.text , organization_details: organisationTextfield.text, social_share: socialshare, ticket_type: ticket_type, ticket_count: ticket_count, ticket_price: ticket_price, event_privacy: event_privacy, visible_to: tagsView.tagTextArray, latitude: placelattitude, longitude: placlongitude,event_image:event_imageName)
            }
            
        }else if tickettypeFlag == 2{
            if paidTicketamountTextfield!.text!.isEmpty{
                alertsnack(message: NSLocalizedString("EnterTotalPrice", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                
            }else if piadTicketcountTextfield!.text!.isEmpty{
                alertsnack(message:  NSLocalizedString("EnterPaidTicketCount", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                
            }else{
            apiCalling(apikey: apikey, event_title: eventTitle.text!, event_type: event_type_id, start_date: startDateOutlet.text!, end_date: enddateOutlet.text!, country_code: country_code, city_code: cityid, event_details: eventdetailsTextfield.text , organization_details: organisationTextfield.text, social_share: socialshare, ticket_type: ticket_type, ticket_count: ticket_count, ticket_price: ticket_price, event_privacy: event_privacy, visible_to: tagArray, latitude: placelattitude, longitude: placlongitude,event_image:event_imageName)
            }
        }
       else if tickettypeFlag == 0{
            apiCalling(apikey: apikey, event_title: eventTitle.text!, event_type: event_type_id, start_date: startDateOutlet.text!, end_date: enddateOutlet.text!, country_code: country_code, city_code: cityid, event_details: eventdetailsTextfield.text , organization_details: organisationTextfield.text, social_share: socialshare, ticket_type: ticket_type, ticket_count: ticket_count, ticket_price: ticket_price, event_privacy: event_privacy, visible_to: tagArray, latitude: placelattitude, longitude: placlongitude,event_image:event_imageName)
        
        
        }
    

    }
    func alertsnack(message: String){
        
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    
    @IBAction func noneAction(_ sender: Any) {
        ticket_type = "0"
        noneOutlet.indicatorColor = UIColor.black
        freeTicketOutlet.indicatorColor = UIColor.clear
        paidTicketOutlet.indicatorColor = UIColor.clear
        freeticketTextfield.text = ""
        piadTicketcountTextfield.text = ""
        paidTicketamountTextfield.text = ""
        
        
    }
   
    @IBAction func freeticketAction(_ sender: Any) {
            ticket_type = "1"
        tickettypeFlag = 1
        freeTicketOutlet.indicatorColor = UIColor.black
        noneOutlet.indicatorColor = UIColor.clear
        paidTicketOutlet.indicatorColor = UIColor.clear
        piadTicketcountTextfield.text = ""
        paidTicketamountTextfield.text = ""
      
      
        
    }
    
    @IBAction func paidTicketAction(_ sender: Any) {
         ticket_type = "2"
         tickettypeFlag = 2
        paidTicketOutlet.indicatorColor = UIColor.black
        freeTicketOutlet.indicatorColor = UIColor.clear
        noneOutlet.indicatorColor = UIColor.clear
        freeticketTextfield.text = ""
        
        
    }
    
    @IBAction func publicpageAction(_ sender: Any) {
        //event_privacy = "Public Page"
        heightcon.constant = 0.0
        tagsView.isHidden = true
        event_privacyFlag = 0
            event_privacy = "0"
        publicPageOutlet.indicatorColor = UIColor.black
        privatepageOutlet.indicatorColor = UIColor.clear
    }
    
    @IBAction func privatepageAction(_ sender: Any) {
             tagsView.isHidden = false
        heightcon.constant = 80
      event_privacyFlag = 1
         event_privacy = "1"
        privatepageOutlet.indicatorColor = UIColor.black
        publicPageOutlet.indicatorColor = UIColor.clear
        if tagsView.tagTextArray.count == 0{
            tagsView.tags = "Type your friend's Howlik Email"
        }
       // heightconstraint.constant = 60
         //   tagsView.isHidden = false
    }
     func createJson(){
    
    }
  
    
    func apiCalling(apikey:String,event_title:String,event_type:String,start_date:String,end_date:String,country_code:String,city_code:String,event_details:String,organization_details:String,social_share:String,ticket_type:String,ticket_count:String,ticket_price:String,event_privacy:String,visible_to:[String],latitude:Double,longitude:Double,event_image:String){
       alert.startactvityIndicator()
    var createdArray = NSMutableArray()
    
    var bigDic: NSMutableDictionary = NSMutableDictionary()
    
    bigDic.setValue(tagsView.tagTextArray, forKey: "visible_to")
    bigDic.setValue(apikey, forKey: "apikey")
    bigDic.setValue(event_title, forKey: "event_title")
    
    bigDic.setValue(event_type, forKey: "event_type")
    bigDic.setValue(start_date, forKey: "start_date")
    bigDic.setValue(end_date, forKey: "end_date")
    
    bigDic.setValue(country_code, forKey: "country_code")
    bigDic.setValue(city_code, forKey: "city_code")
    bigDic.setValue(event_details, forKey: "event_details")
    
    bigDic.setValue(organization_details, forKey: "organization_details")
    bigDic.setValue(social_share, forKey: "social_share")
    bigDic.setValue(ticket_type, forKey: "ticket_type")
    
    bigDic.setValue(ticket_count, forKey: "ticket_count")
    bigDic.setValue(ticket_price, forKey: "ticket_price")
    bigDic.setValue(event_privacy, forKey: "event_privacy")
    
    
    bigDic.setValue(latitude, forKey: "latitude")
    bigDic.setValue(longitude, forKey: "longitude")
          bigDic.setValue(event_image, forKey: "event_image")
       
    do {
        
        
        
        //Convert to Data
        
        let jsonData = try! JSONSerialization.data(withJSONObject: bigDic, options: JSONSerialization.WritingOptions.prettyPrinted)
        
      
        
        //Convert back to string. Usually only do this for debugging
        
        if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
            
            //ethaann nink veenda string
            
            // json aayit kittum
            
        
            let scriptUrl = "https://www.howlik.com/api/create/event"
            let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
            let myUrl = URL(string: urlWithParams);
            var request = URLRequest(url:myUrl!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request)
            {
                data, response, error in
                
                if error != nil
                {
                    print("error=\(error)")
                    DispatchQueue.main.async
                        {
                            print("server down")
                    }
                    return
                }
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
                do{
                    
                    if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                        
                    {
                        
                        self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                   
                        
                    }
                    DispatchQueue.main.async
                        {
                           
                                
                           if self.status == "success"{
                                self.imageUploading(selectedImage:self.selectedImage)
                           
                            }
                            
                    }//dispatch ends
                    
                }//do ends
                catch let error as NSError {
                    print(error.localizedDescription)
                    
                }
                
            }
            
            
            
            
            task.resume()
            
            
        }
        
    }
    
    }
  

    
    
    
    
    
    
    
    
    
    
  
    func imageUploading(selectedImage:UIImage){
       
        let body =  [
            
            "apikey": "\(apikey)"]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImageJPEGRepresentation(selectedImage, 0.5)!, withName: "file", fileName: self.event_imageName, mimeType: "image/jpeg")
            for (key, value) in body {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: "https://www.howlik.com/api/upload/event/image"){
            (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                  
                })
                
                upload.responseString { response in
                   
                   // print(response)
                    //print(response.request!)  // original URL request
                   // print(response.response!) // URL response
                   // print(response.data!)     // server data
                    //print(response.result)   // result of response serialization
                    if let JSON = response.result.value {
                      
                        self.parseData(JSONData: response.data!)
                    }
                }
                //   self.stopActivityIndicator()
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
                self.alert.stopActivityIndicator()
                self.alert.Failurealert()
                //    self.stopActivityIndicator()
            }
            
            
        }
        
        
        //     }
        // }else{
        //      self.stopActivityIndicator()
        //  }
    }
    func parseData(JSONData: Data) {
        do {
            let readableJSON = try JSONSerialization.jsonObject(with: JSONData, options:.allowFragments) as! [String: Any]
            
            let status = readableJSON["status"] as! String
            
            
            if status == "success"{
                alert.stopActivityIndicator()
             Successalert(SubTitle: NSLocalizedString("SuccessfullyAdded", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                
                 

            }else{
            alert.stopActivityIndicator()
               Failurealert()
            }
        }
        catch {
            print(error)
        }
  
    }
    
    @IBAction func tickAction(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            socialshare = "0"
           // squareOUtlet.setImage(#imageLiteral(resourceName: "blank-check-box"), for: UIControlState.normal)
           squareOUtlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
           
        }else{
               sender.isSelected = true
          squareOUtlet.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
            socialshare = "1"
        }
    }
   
    
    func Failurealert(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
             self.navigationController?.popToRootViewController(animated: true)
        })
        
        alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
    }
    func Successalert(SubTitle:String){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
            self.resignFirstResponder()
              self.navigationController?.popToRootViewController(animated: true)
        })
        alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:SubTitle )
    }
    
    
}
//extension eventAddingViewController {
//    fileprivate func textFieldEventss() {
//        tagsField.onDidAddTag = { _, _ in
//            print("DidAddTag")
//        }
//
//        tagsField.onDidRemoveTag = { _, _ in
//            print("DidRemoveTag")
//        }
//
//        tagsField.onDidChangeText = { _, text in
//            print("onDidChangeText")
//            IQKeyboardManager.shared.enableAutoToolbar = false
//            IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
//            IQKeyboardManager.shared.shouldResignOnTouchOutside = true
//        }
//
//       // tagsField.onDidBeginEditing = { _ in
//       //     print("DidBeginEditing")
//       // }
//
//      //  tagsField.onDidEndEditing = { _ in
//        //    print("DidEndEditing")
//      //  }
//
//        tagsField.onDidChangeHeightTo = { _, height in
//            print("HeightTo \(height)")
//        }
//
//        tagsField.onDidSelectTagView = { _, tagView in
//            print("Select \(tagView)")
//
//        }
//
//        tagsField.onDidUnselectTagView = { _, tagView in
//            print("Unselect \(tagView)")
//        }
//    }
//}
extension eventAddingViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(eventAddingViewController.dismissKeyboards))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboards()
    {
        view.endEditing(true)
    }
}
