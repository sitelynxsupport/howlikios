//
//  eventCollectionViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/31/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class eventCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var eventname: UILabel!
    
    @IBOutlet var cityname: UILabel!
    @IBOutlet var eventdate: UILabel!
    @IBOutlet var eventview: UIView!
    @IBOutlet var eventImage: UIImageView!
}
