//
//  eventtypeTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/1/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import DLRadioButton

class eventTypecell: UITableViewCell {
    @IBOutlet var checkbutton: UIButton!
    
    @IBOutlet var radiobutton: DLRadioButton!
    @IBOutlet var eventLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
