//
//  eventdateTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/27/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class eventdateTableViewCell: UITableViewCell {

    @IBOutlet var dateandtimeLabel: UILabel!
    @IBOutlet var startlabel: UILabel!
    
    @IBOutlet var endlabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dateandtimeLabel.text = NSLocalizedString("Date and Time", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
