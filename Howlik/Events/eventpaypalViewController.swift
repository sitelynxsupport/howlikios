//
//  eventpaypalViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/29/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView
import FontAwesome_swift

class eventpaypalViewController: UIViewController,NVActivityIndicatorViewable,UIWebViewDelegate{

    @IBOutlet var webview: UIWebView!
    let buisnessapiClass = ApiListViewcontroller()
    var apikey = ""
    var country_code = ""
    var language_code = ""
    var event_id = ""
    var pricebook_Array = [String]()
    var ticket_type = ""
    var status = ""
    var usr_name = ""
    var usr_email = ""
    var usr_id = ""
    var currency_code = ""
    var ticket_quantity = ""
    var ticket_amount = ""
    var url = "http://www.howlik.com/eventpay/success.php"
    var url2 = "http://www.howlik.com/eventpay/cancel.php"
    var window = UIWindow()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.usr_name = UserDefaults.standard.string(forKey: "name")!
            self.usr_email = UserDefaults.standard.string(forKey: "email")!
            self.usr_id = UserDefaults.standard.string(forKey: "senderid")!
posttableapiCalling(apikey:apikey,eve_id:event_id,curr_code:currency_code,lan_id:"en",country_code:country_code,usr_id:usr_id,usr_name:usr_name,usr_email:usr_email,ticket_quantity:ticket_quantity,ticket_amount:ticket_amount,pay_now:"pay_now")
        }
        self.navigationItem.title = "ADD PAYPAL"
        //
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        webview.delegate = self
        SetBackBarButtonCustom()
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 40, height: 40)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
func posttableapiCalling(apikey:String,eve_id:String,curr_code:String,lan_id:String,country_code:String,usr_id:String,usr_name:String,usr_email:String,ticket_quantity:String,ticket_amount:String,pay_now:String){
      //  self.startactvityIndicator()
    let urlstring = buisnessapiClass.EventBookingPostPage2
    
        let url = URL(string: urlstring)!
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let query :[String:String] = ["apikey":apikey,"eve_id":eve_id,"curr_code":curr_code,"lan_id":lan_id,"country_code":country_code,"usr_id":usr_id,"usr_name":usr_name,"usr_email":usr_email,"ticket_quantity":ticket_quantity,"ticket_amount":ticket_amount,"pay_now":pay_now]
        let baseurl = URL(string:urlstring)!
        let postString = (baseurl.withQueries(query)!).query
        request.httpBody = postString?.data(using: .utf8)
        webview.loadRequest(request as URLRequest)
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading{
            return
        }
      //  self.stopActivityIndicator()
        if let text = webView.request?.url?.absoluteString{
         
            if text.contains("success.php"){
             
              
                Timer.scheduledTimer(withTimeInterval: 3, repeats: false){_ in
                    self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                }
               
                
             
            }
        }else if let text2 = webView.request?.url?.absoluteString{
                
     
            if text2.contains("cancel.php"){
              
                Timer.scheduledTimer(withTimeInterval: 3, repeats: false){_ in
                       self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    

}

