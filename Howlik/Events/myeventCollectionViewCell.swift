//
//  myeventCollectionViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/1/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class myeventCollectionViewCell: UICollectionViewCell {
     var controller : EventViewController?
    @IBOutlet var myeventEditButton: UIButton!
   
    @IBOutlet var myeventDeleteButton: UIButton!
    @IBOutlet var myeventLocationButton: UIButton!
    @IBOutlet var myview: UIView!
    @IBOutlet var myeventImage: UIImageView!
    @IBOutlet var eventname: UILabel!
    @IBOutlet var eventcity: UILabel!
    
    @IBOutlet var eventdate: UILabel!
    @IBOutlet var eventenddate: UILabel!
    func handleDelete(){
        controller?.delete(self)
    }
    
}
