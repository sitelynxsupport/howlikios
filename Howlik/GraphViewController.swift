//
//  GraphViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/11/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import  FontAwesome_swift
import Charts
import DLRadioButton

class GraphViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ChartViewDelegate {
    @IBOutlet var last1mnthButton: DLRadioButton!
    
    @IBOutlet var selectTextfield: UITextField!
    @IBOutlet var last12mnthButton: DLRadioButton!
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    var apikey = ""
    var alertvalue = Int()
    var fieldArray = [String]()
      var valueArray = [String]()
    var BusinessArray = [selectBusinessClass]()
    var tableheightindicator = Int()
    let buisnessapiClass = ApiListViewcontroller()
    @IBOutlet var runkeeperSwitch4: DGRunkeeperSwitch!
    
    @IBOutlet var barchartView: BarChartView!
    @IBOutlet var selectBusinessButton: UIButton!
    var month = "1"
    var arr1 = [String]()
    var graph_type = "view"
    var biz_id = ""
        var customview = UIView()
    var label = NSLocalizedString("UserViews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        self.selectBusinessButton.isHidden = false
        self.barchartView.isHidden = false
        self.selectTextfield.isHidden = false
        self.runkeeperSwitch4.isHidden = false
        self.last1mnthButton.isHidden = false
        self.last12mnthButton.isHidden = false
            self.customview.isHidden = true
       
         runkeeperSwitch4.titles = [NSLocalizedString("UserViews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), NSLocalizedString("GiftCards", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Ratings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
        runkeeperSwitch4.backgroundColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        runkeeperSwitch4.selectedBackgroundColor = .white
        runkeeperSwitch4.titleColor = .white
        runkeeperSwitch4.selectedTitleColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        runkeeperSwitch4.titleFont = UIFont(name: "HelveticaNeue-Light", size: 13.0)
        runkeeperSwitch4.addTarget(self, action:#selector(self.buttonactionView(_:)), for:.valueChanged)
        langSet()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            SelectBusinessapiCalling(apikey:apikey)
        }
       
        selectBusinessButton.backgroundColor = .clear
        selectBusinessButton.layer.cornerRadius = 5
        selectBusinessButton.layer.borderWidth = 0.5
        selectBusinessButton.layer.borderColor = UIColor.lightGray.cgColor
        last1mnthButton.isSelected = true
        last1mnthButton.iconColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        last1mnthButton.indicatorColor = UIColor(red: 90/255.0, green: 118/255.0, blue: 131/255.0, alpha: 1.0)
        SetBackBarButtonCustom()
    //    self.navigationItem.title = "GRAPH"
self.navigationItem.title = NSLocalizedString("GRAPH", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "hello")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
   
     
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func langSet(){
        last1mnthButton.setTitle(NSLocalizedString("Last1Month", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        last12mnthButton.setTitle(NSLocalizedString("Last12month", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        selectTextfield.placeholder = NSLocalizedString("SelectBusiness", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        if  UserDefaults.standard.string(forKey:"language_code") == "en"{
            last1mnthButton.contentHorizontalAlignment = .left
            last12mnthButton.contentHorizontalAlignment = .left
            selectBusinessButton.contentHorizontalAlignment = .left
            
            
        }else{
            last1mnthButton.contentHorizontalAlignment = .right
            last12mnthButton.contentHorizontalAlignment = .right
            selectBusinessButton.contentHorizontalAlignment = .right
        }
    }
    func SelectBusinessapiCalling(apikey:String){
    // self.startactvityIndicator()
   // inboxArray.removeAll()
    //selectedId.removeAll()
    let scriptUrl = buisnessapiClass.BusinessGraph
    
    let parameters: Parameters = ["apikey": apikey]
    
    Alamofire.request(scriptUrl, method: .get, parameters: parameters)
    .responseJSON(completionHandler: { (response) in
        switch response.result {
        case .success( let JSON):
            do{
        
                let response = JSON as! NSDictionary
                print(response)
                if response["business"] != nil{
             let business = response.object(forKey: "business")!
        for singlebusiness in (business as! [[String:Any]]){
                  let singlebusinessxElement = selectBusinessClass()
                    let contry = singlebusiness as NSDictionary
                    if  (contry["id"] as? String) != nil {
                        let id = contry["id"] as! String
                        singlebusinessxElement.id = id
    
                    }
                if  (contry["title"] as? String) != nil {
                    let title = contry["title"] as! String
                    singlebusinessxElement.title = title
                        }
            singlebusinessxElement.isChecked = false
        
            self.BusinessArray.append(singlebusinessxElement)
                }
                     }else{
                    self.selectBusinessButton.isHidden = true
                    self.barchartView.isHidden = true
                        self.selectTextfield.isHidden = true
                        self.runkeeperSwitch4.isHidden = true
                              self.last1mnthButton.isHidden = true
                       self.last12mnthButton.isHidden = true
                        self.createeventNothingview()
                    }
       
        
        
        
        DispatchQueue.main.async
        {
        
          self.myTableView.reloadData()
        //   self.bookingtable.separatorInset = .zero
            print("&&&&&&&&&&&&&&&&&&&&&&")
            //  print(self.bookingArray.count)
            print("&&&&&&&&&&&&&&&&&&&&&&")
        
        }
        
        }
        catch let error as NSError {
            print(error.localizedDescription)
        
        }
            break
            case .failure(let errorGiven):
        
                print(errorGiven)
                print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
            break
            }
        })
        
    
    }
    func createeventNothingview(){
        customview.isHidden = false
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    @objc func buttonactionView(_ sender: DGRunkeeperSwitch)
    {
      
        print("valueChanged: \(sender.selectedIndex)")
        if sender.selectedIndex == 0{
            if selectBusinessButton!.currentTitle == nil{
                firstalert(message: NSLocalizedString("SelectaBusiness", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            }else{
                graph_type = "view"
                label = NSLocalizedString("UserViews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                GraphapiCalling(apikey:apikey,biz_id:biz_id,month:month,graph_type:graph_type)
            }
            
         
        }else if  sender.selectedIndex == 1{
            if selectBusinessButton!.currentTitle == nil{
                firstalert(message: NSLocalizedString("SelectaBusiness", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            }else{
                graph_type = "gift"
                label =  NSLocalizedString("GiftCards", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                GraphapiCalling(apikey:apikey,biz_id:biz_id,month:month,graph_type:graph_type)
            }
        }else {
            if selectBusinessButton!.currentTitle == nil{
                firstalert(message: NSLocalizedString("SelectaBusiness", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                
            }else{
                graph_type = "review"
                 label = NSLocalizedString("Ratings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
                GraphapiCalling(apikey:apikey,biz_id:biz_id,month:month,graph_type:graph_type)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func firstalert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default)
        {
            UIAlertAction in
            NSLog("OK Pressed")
        //
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func selectBusinessAction(_ sender: Any) {
        createTableview()
    }
    func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let height =  Int(tableviewHeight())
        print("detected tap!")
        print("^^^^^^^^^^^^^^^^^^^^")
        print(height)
        print("detected tap!")
        print("^^^^^^^^^^^^^^^^^^^^")
        // self.newview = UIView(frame: CGRect(x: 15, y: 8, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        if tableheightindicator == 0{
            self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
        }else{
            self.newview = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: height ))
            newview.center =  view.center
        }
        self.addview.addSubview(newview)
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
        myTableView.dataSource = self
        myTableView.delegate = self
         newview.addSubview(myTableView)
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label);
        let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 75, y:newview.frame.height - 45,width: 70  , height:  40));
        newview.addSubview(button);
        // button.backgroundColor = UIColor.green
        button.layer.cornerRadius = 3.0
        button.setTitle(NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        label.textColor = UIColor.black
        label.isHighlighted = true
        view1.backgroundColor  = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        myTableView.register(UINib(nibName: "selectBusinessXib", bundle: nil), forCellReuseIdentifier: "myIdentifier")
        label.text = NSLocalizedString("SelectaBusiness", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
       
       // self.myTableView.alwaysBounceVertical = false
        newview.backgroundColor = UIColor.white
        //newview.isHidden =  true
      //  myTableView.isHidden =  true
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
     //  newview.isHidden = false
     //   myTableView.isHidden = false
        myTableView.reloadData()
        //  textview.resignFirstResponder()
    }


func tableviewHeight() -> Double{
    
    let screenSize: CGRect = UIScreen.main.bounds
    let maximumHeight = screenSize.height - 100
    if (Double(44.0)  *  Double(BusinessArray.count) >  Double(maximumHeight)){
        tableheightindicator = 0
        return Double(maximumHeight)

    }else{
        tableheightindicator = 1
        return(44.0  * Double(BusinessArray.count) + 55.0 + 50.0)


    }
    
}
    @objc func okView(_ sender: UIButton)
    {
        self.newview.isHidden = true
        self.addview.isHidden = true
        self.myTableView.isHidden = true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
            
            return BusinessArray.count
            
     
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! selectBusinessXib
            //cell.selectionStyle = .none
            // cell.backgroundColor = UIColor.lightGray
            cell.checkbutton.tag = indexPath.row
        //    cellIndex = indexPath.row
            cell.checkbutton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
            cell.businesstypeLabel.text = BusinessArray[indexPath.row].title
            let val = BusinessArray[indexPath.row].isChecked

            if(val == true){
                cell.checkbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.checkbutton.tintColor = UIColor.red

            }else{

                cell.checkbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.checkbutton.tintColor = UIColor.green
            }
        
              cell.checkbutton.isHidden = true
            return cell
       
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectBusinessButton.setTitle(BusinessArray[indexPath.row].title, for: UIControlState())
        selectTextfield.placeholder = ""
       biz_id = BusinessArray[indexPath.row].id
         self.runkeeperSwitch4.setSelectedIndex(0, animated: true)
GraphapiCalling(apikey:apikey,biz_id:biz_id,month:"1",graph_type:graph_type)
        addview.isHidden = true
            newview.isHidden = true
             myTableView.isHidden = true
        
       
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    @objc func moreView(_ sender: UIButton)
    {
        //tickvalueArray.removeAll()
        if  BusinessArray[sender.tag].isChecked == false{
            BusinessArray[sender.tag].isChecked = true
            
            
        }else{
            BusinessArray[sender.tag].isChecked = false
        }
        biz_id =  BusinessArray[sender.tag].id
        
        myTableView.reloadData()
        addview.isHidden = true
        newview.isHidden = true
        myTableView.isHidden = true
    }
    @IBAction func last1mnthAction(_ sender: Any) {
        if selectBusinessButton.currentTitle   == nil{
            firstalert(message:NSLocalizedString("SelectaBusiness", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
        month = "1"
        GraphapiCalling(apikey:apikey,biz_id:biz_id,month:month,graph_type:graph_type)
        }
    }
    
    @IBAction func last2mnthAction(_ sender: Any) {
        if selectBusinessButton.currentTitle   == nil{
            firstalert(message:NSLocalizedString("SelectaBusiness", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
          month = "12"
        GraphapiCalling(apikey:apikey,biz_id:biz_id,month:month,graph_type:graph_type)
        }
    }
    
    func GraphapiCalling(apikey:String,biz_id:String,month:String,graph_type:String){
        fieldArray.removeAll()
        valueArray.removeAll()
        let scriptUrl = buisnessapiClass.BusinessGraphMonthWiseView
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&biz_id=\(biz_id)&month=\(month)&graph_type=\(graph_type)"
        print(">>>>>>>>>")
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    print(convertedJsonIntoArray)
                    if (convertedJsonIntoArray["result"] as? NSArray) != nil {
                        let result  = convertedJsonIntoArray["result"] as! NSArray
                        print(result)
                      
                        for singleresult in  (result as! [[String:Any]]){
                            let contry1 = singleresult as NSDictionary
                            if (contry1["field"] as? String) != nil {
                                let field = contry1["field"] as! String

                                self.fieldArray.append(String(describing: field))
                                
                            }
                            if (contry1["value"] as? String) != nil {
                                let value = contry1["value"] as! String
                               self.valueArray.append(value)
                                
                            }
                        }
                    }
                }
                
                
                DispatchQueue.main.async
                    {
                        print("%%%%%%%CFVBCVH")
                        
                        print(self.valueArray.count)
                        print(self.fieldArray.count)
                       
////                        self.barchartView.rightAxis.enabled = false
////                        self.barchartView.legend.enabled = false
////                        self.barchartView.xAxis.labelPosition = .bottomInside
////                        self.barchartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.valueArray)
//                        var entries: [ChartDataEntry] = Array()
//
//                        for (i, value) in self.valueArray.enumerated()
//                        {
//                            entries.append(BarChartDataEntry(x: Double(i), y: Double(value)!))
//                        }
//                          print("%%%%%%%CFVBCVH")
//
//                        let ds1 = BarChartDataSet(values:entries, label: "Public Exam")
//
//                        //ds1.colors = [NSUIColor.red, NSUIColor.blue]
//                       // ds1.setType(passstring: self.fieldArray)
//                        let data = BarChartData()
//                        data.addDataSet(ds1)
//                        self.barchartView.data = data
//
//                        self.barchartView.gridBackgroundColor = NSUIColor.white
//                        //self.barchartView.chartDescription?.text = "Barchart Demo"
//                         self.barchartView.xAxis.valueFormatter = IndexAxisValueFormatter(values:self.fieldArray)
                        
                        self.setupView()
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    
    func setupView() {
        
//        //legend
//        let legend = self.barchartView.legend
//        legend.enabled = true
//        legend.horizontalAlignment = .right
//        legend.verticalAlignment = .bottom
//        legend.orientation = .vertical
//        legend.drawInside = true
//        legend.textColor = UIColor.black
        
        // Y - Axis Setup
        let yaxis = self.barchartView.leftAxis
        yaxis.spaceTop = 0.35
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = true
        yaxis.labelTextColor = UIColor.black
        yaxis.axisLineColor = UIColor.black
        self.barchartView.rightAxis.enabled = true
        let rightyaxis = self.barchartView.rightAxis
        rightyaxis.spaceTop = 0.35
        rightyaxis.axisMinimum = 0
        rightyaxis.drawGridLinesEnabled = true
        rightyaxis.labelTextColor = UIColor.black
        rightyaxis.axisLineColor = UIColor.black
        
        // X - Axis Setup
        let xaxis = self.barchartView.xAxis
        let formatter = CustomLabelsXAxisValueFormatter()//custom value formatter
        formatter.labels = self.fieldArray
        xaxis.valueFormatter = formatter
        barchartView.xAxis.granularityEnabled = true
        barchartView.xAxis.granularity = 1.0
//        xaxis.drawGridLinesEnabled = true
//        xaxis.labelPosition = .bothSided
//        xaxis.labelTextColor = UIColor.black
//        xaxis.centerAxisLabelsEnabled = true
//        xaxis.axisLineColor = UIColor.black
        xaxis.granularityEnabled = true
        xaxis.enabled = true
        
       
        self.barchartView.delegate = self
        self.barchartView.noDataText = "You need to provide data for the chart."
        self.barchartView.noDataTextColor = UIColor.black
        self.barchartView.chartDescription?.textColor = UIColor.clear
        
        setChart()
    }
    
    func setChart() {
        self.barchartView.noDataText = "Loading...!!"
        var dataEntries: [BarChartDataEntry] = []
        
        
        for i in 0..<self.fieldArray.count {
            
            let dataEntry = BarChartDataEntry(x: Double(i) , y: Double(self.valueArray[i])!)
            dataEntries.append(dataEntry)
            
       
            
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: label)
        let dataSets: [BarChartDataSet] = [chartDataSet]
        let chartData = BarChartData(dataSets: dataSets)
        let barWidth = 0.6
        chartData.barWidth = barWidth
        self.barchartView.xAxis.axisMinimum = 0.0
        self.barchartView.xAxis.granularity = self.barchartView.xAxis.axisMaximum / Double(self.fieldArray.count)
        self.barchartView.data = chartData
        self.barchartView.notifyDataSetChanged()
      //  self.barchartView.setVisibleXRangeMaximum(4)
        self.barchartView.animate(yAxisDuration: 1.0, easingOption: .linear)
        chartData.setValueTextColor(UIColor.black)
    }
    
}



class MyBarChartDataSet: BarChartDataSet {
    
    var type = [String]()
    func setType(passstring: [String]){
        type = passstring
    }
    override func color(atIndex index: Int) -> NSUIColor {
            return NSUIColor.red
       
    }
    
}





