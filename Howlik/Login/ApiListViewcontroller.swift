//
//  ApiListViewcontroller.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/22/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class ApiListViewcontroller: NSObject {
    let login = "https://www.howlik.com/api/login"
    let LoadRegister = "https://www.howlik.com/api/register/load"
    let PostRegister = "https://www.howlik.com/api/register"
    let ForgotPassword = "https://www.howlik.com/api/forgot"
    let dashboard = "https://www.howlik.com/api/dashboard"
    let AllBusinessDetails =  "https://www.howlik.com/api/business/all"
    let UpcomingEventsList = "https://www.howlik.com/api/events/upcome"
    let PopularEventsList = "https://www.howlik.com/api/events/popular"
    let OwnEventsList = "https://www.howlik.com/api/events/own"
    let LoadCreateEvent = "https://www.howlik.com/api/create/event/load"
    let PostCreateEvent = "https://www.howlik.com/api/create/event"
    let LoadProfile = "https://www.howlik.com/api/profile/load"
    let LoadUpdateEvent = "https://www.howlik.com/api/update/event/load"
    let PostUpdateEvent = "https://www.howlik.com/api/update/event"
    let OwnBusinessDetails = "https://www.howlik.com/api/business/own"
    let PendingBusinessList = "https://www.howlik.com/api/pending/business"
    let LoadCreateBusiness =  "https://www.howlik.com/api/create/business/load"
    let GenerateKeywordsfromCategory = "https://www.howlik.com/api/generate/keyword"
    let GenerateCityfromCountry = "https://www.howlik.com/api/generate/city"
    let LoadUpdateBusiness =  "https://www.howlik.com/api/update/business/load"
    let PostProfile = "https://www.howlik.com/api/profile"
    let PostUpdateBusinessHours = "https://www.howlik.com/api/update/business/hours"
    let PostUpdateBusinessInformations =  "https://www.howlik.com/api/update/business/infos"
    let PostUpdateBusinessBasicDetails = "https://www.howlik.com/api/update/business/basic"
    let PostUpdateBusinessGeneralDetails = "https://www.howlik.com/api/update/business"
    let SingleBusinessDetails   = "https://www.howlik.com/api/business/single"
    let  Postareviewforbusiness = "https://www.howlik.com/api/post/review"
    let LoadTimeReservation = "https://www.howlik.com/api/reservation/timeslot/load"
    let PostTimeSlotReservation = "https://www.howlik.com/api/reserve/timeslot"
    let LoadTableReservation = "https://www.howlik.com/api/reservation/load"
    let CheckTableAvailability = "https://www.howlik.com/api/reserve/table/check"
    let PostTableReservation = "https://www.howlik.com/api/reserve/table"
    let LoadCreateGiftCertificate = "https://www.howlik.com/api/create/certificate/load"
    let PostCreateGiftCertificate = "https://www.howlik.com/api/giftpay/index.php"
    let Eventdetailsbasedoneventid = "https://www.howlik.com/api/events/single"
    let EventBookingLoadPage = "https://www.howlik.com/api/events/book/load"
    let EventBookingPostPage1 = "https://www.howlik.com/api/events/book/post"
    let EventBookingPostPage2 = "https://www.howlik.com/eventpay/index.php"
    let DeleteEvent = "https://www.howlik.com/api/delete/event"
    let DeleteBusiness = "https://www.howlik.com/api/delete/business"
    let LoadCreateOffer = "https://www.howlik.com/api/create/offer/load"
    let PostCreateOffer = "https://www.howlik.com/api/create/offer"
    let OwnedBusinessBookings = "https://www.howlik.com/api/own/business/booking"
    let OtherBusinessBookings = "https://www.howlik.com/api/business/booking"
    let ListofallMessages = "https://www.howlik.com/api/messages/list"
    let DeleteMessages = "https://www.howlik.com/api/messages/delete"
    let SendMessages = "https://www.howlik.com/api/messages/send"
    let MailUserslist = "https://www.howlik.com/api/mail/compose"
    let ViewMessages = "https://www.howlik.com/api/messages/view"
    let ReplyMessages = "https://www.howlik.com/api/messages/reply"
    let BusinessGraph = "https://www.howlik.com/api/business/graph"
    let BusinessGraphMonthWiseView  = "https://www.howlik.com/api/business/graph/viewA"
    let EventGraph = "https://www.howlik.com/api/event/graph"
    let EventGraphMonthWiseView = "https://www.howlik.com/api/event/graph/viewA"
    let FriendsList = "https://www.howlik.com/api/friends"
    let ReviewList = "https://www.howlik.com/api/users/reviewlist"
    let  OtherEventBooking = "https://www.howlik.com/api/events/booking"
    let  ChangePassword = "https://www.howlik.com/api/password"
    let ActivityList = "https://www.howlik.com/api/users/activitylist"
    let InterestList = "https://www.howlik.com/api/users/intrestlist"
    let AllNotifications = "https://www.howlik.com/api/users/notifications_all"
    let Notifications = "https://www.howlik.com/api/notifications"
    let ManageFriends = "https://www.howlik.com/api/friends/manage"
    let OwnedEventBookings = "https://www.howlik.com/api/own/events/booking"
    let PostCreateBusiness = "https://www.howlik.com/api/create/business"
    let UploadBusinessImage = "https://www.howlik.com/api/upload/business/image"
    let ReportBusiness = "https://www.howlik.com/api/business/report"
    let AcceptRejectBookings = "https://www.howlik.com/api/business/status"
    let EnableDisableBusinessBooking = "https://www.howlik.com/api/biz/booking/status"
    let UpdateGCMtoken = "https://www.howlik.com/api/update_gcm"
    let InviteFriendstoHowlik = "https://www.howlik.com/api/invite/friends"
    let PostCreateGiftCertificateb = "https://www.howlik.com/api/giftpay/index.php"
    let SingleBusinessReviewListing = "https://www.howlik.com/api/business/reviews"
    let UserMadeReviewListing = "https://www.howlik.com/api/users/reviewlist"
    
    

    





  
















}
