//
//  LoginViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/20/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SCLAlertView
import TTGSnackbar


class LoginViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {
let loginURL = ApiListViewcontroller()
let emailvalidate = emailValidateViewController()
    var status = ""
     var status1 = ""
    var apikey = ""
    var value = ""
       var val = false
    var forgotpasswordview = forgotPasswordview()
    
    @IBOutlet var registernowoutlet: UIButton!
    @IBOutlet var forgotPasswordOutlet: UIButton!
    @IBOutlet var passwordlabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var skiploginOutlet: UIButton!
    
    @IBOutlet var howlikimage: UIImageView!
    
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var emailTextfield: UITextField!
    
    @IBOutlet var loginOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
  
        // Do any additional setup after loading the view.
        skiploginOutlet.layer.borderWidth = 1.0
          skiploginOutlet.layer.cornerRadius = 20
         skiploginOutlet.layer.borderColor = UIColor.red.cgColor
       // (red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0).cgColor
        skiploginOutlet.layer.masksToBounds = false
        howlikimage.backgroundColor = UIColor.clear
        let layer = CAGradientLayer()
        layer.frame = self.loginOutlet.bounds
        layer.startPoint = CGPoint(x:0, y:0)
        layer.endPoint = CGPoint(x:1.0, y:0.3)
       
        layer.colors = [UIColor.red.cgColor, UIColor.orange.cgColor]
        loginOutlet.layer.addSublayer(layer)
        loginOutlet.layer.cornerRadius = 20
        loginOutlet.layer.masksToBounds = true
        emailTextfield.setBottomBorder()
        passwordTextfield.setBottomBorder()
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        loginOutlet.backgroundColor =  UIColor.orange
        languageSet()
        
      
    }
//    override func viewDidAppear(_ animated: Bool) {
//        if  UserDefaults.standard.string(forKey:"apikey") != nil{
//            print("^^^^^^^^^^^^^^^^^^^")
//            
//            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
//            if let vcs = tabbarVC.viewControllers,
//                
//                let nc = vcs.first as? UINavigationController,
//                let pendingOverVC = nc.topViewController as? HomeViewController {
//                
//                
//            }
//            //    let navController = UINavigationController(rootViewController: vcs)
//            
//            self.present(tabbarVC, animated: false, completion: nil)
//            
//        }
//    }
   
       
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 30, height: 30)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func languageSet(){
        emailLabel.text = NSLocalizedString("Email ID", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          passwordlabel.text = NSLocalizedString("Password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        loginOutlet.setTitle(NSLocalizedString("LOGIN", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        skiploginOutlet.setTitle(NSLocalizedString("Skip Login", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        forgotPasswordOutlet.setTitle(NSLocalizedString("Forgot Password?", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
         registernowoutlet.setTitle(NSLocalizedString("Register Now", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
    }
    @IBAction func loginbuttonAction(_ sender: Any) {
        if (emailvalidate.isValidEmail(testStr:emailTextfield!.text! ) == 1) {
         
          
            emailvalidate.FailurealertWithSubtitle(SubTitle:NSLocalizedString("EnterEmail", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if(emailvalidate.isValidEmail(testStr:emailTextfield!.text! ) == 3){
            
             emailvalidate.FailurealertWithSubtitle(SubTitle: NSLocalizedString("Invalid Email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }
            
        else if (isValidPassword(password:passwordTextfield!.text!) == 1){
          
             emailvalidate.FailurealertWithSubtitle(SubTitle:NSLocalizedString("Please enter password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (isValidPassword(password:passwordTextfield!.text!) == 2){
            
               emailvalidate.FailurealertWithSubtitle(SubTitle:NSLocalizedString("Username or Password is incorrect!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
             self.startactvityIndicator()
            apiCalling(email:emailTextfield!.text!,password: passwordTextfield!.text!)
        }

        
        
    }
    func apiCalling(email:String,password:String){
        let scriptUrl = loginURL.login
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "email=\(email)&password=\(password)"
       
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
               
                   self.status1 = (convertedJsonIntoArray["status"] as! NSString) as String
                  
                    if self.status1 == "success"{
                         let apikey = (convertedJsonIntoArray["apikey"] as! NSString) as String
                          UserDefaults.standard.set(apikey, forKey: "apikey")
                        
                        let user = (convertedJsonIntoArray["user"] as! [String:Any])
                        let country = user as NSDictionary
                        if  (country["id"] as? NSInteger) != nil {
                        let id = (country["id"] as! NSInteger)
                           
                            self.value = String(id)
                            
                            UserDefaults.standard.set(self.value, forKey:"senderid")
                            
                        }
                         if  (country["email"] as? String) != nil {
                        let email = (country["email"] as! String)
                        UserDefaults.standard.set(email, forKey:"email")
                     
                    }
                    if  (country["name"] as? String) != nil {
                            let name = (country["name"] as! String)
                            UserDefaults.standard.set(name, forKey:"name")
                        
                    }
                        if  (country["user_type_id"] as? String) != nil {
                           
                            let userType = (country["user_type_id"] as! String)
                            UserDefaults.standard.set(userType, forKey:"userType")
                            
                        }
                      
                        
                    }
                   
                }
                
                
                DispatchQueue.main.async
                    {
                       if (self.status1 == "success") {
                       
                           self.stopActivityIndicator()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
                        if let vcs = tabbarVC.viewControllers,
                            
                            let nc = vcs.first as? UINavigationController,
                            let pendingOverVC = nc.topViewController as? HomeViewController {
                            
                            
                        }
                        //    let navController = UINavigationController(rootViewController: vcs)
                        
                        self.present(tabbarVC, animated: false, completion: nil)
                        
                            
                       }else{
                           self.stopActivityIndicator()
                        self.emailvalidate.Failurealert()
                        }
            
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    func isValidPassword(password:String) -> Int{
        if passwordTextfield.text!.isEmpty{
            return 1
        }else if passwordTextfield.text!.count < 6{
         
              return 2
        }else {
             return 0
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
    }
    
    @IBAction func ForgotPasswordAction(_ sender: Any) {
        createforgotView()
       
    }
    @objc  func CancelbuttonAction(sender: UIButton!) {
        forgotpasswordview.isHidden = true
        
    }
    @objc  func OKbuttonAction(sender: UIButton!) {
        if (emailvalidate.isValidEmail(testStr:emailTextfield!.text! ) == 1) {
      
            alertsnack(message: NSLocalizedString("EnterEmail", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
            
        }else if(emailvalidate.isValidEmail(testStr:emailTextfield!.text! ) == 3){
           
              alertsnack(message: NSLocalizedString("Invalid Email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
            
            ForgotapiCalling(email:forgotpasswordview.enterEmailTextfield.text!)
        }
        
    }
    func ForgotapiCalling(email:String){
        startactvityIndicator()
        let scriptUrl = loginURL.ForgotPassword
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "email=\(email)"
     
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                 
                     self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                    // print(self.status)
                    if self.status == "success"{
                        let url = convertedJsonIntoArray["url"] as!  String
                        
                    }
                }
                
                
                DispatchQueue.main.async
                    {
                        if (self.status == "success") {
                         self.stopActivityIndicator()
                             self.dismiss(animated: true, completion: nil)
                            
                       
                        }else{
                             self.stopActivityIndicator()
                             self.dismiss(animated: true, completion: nil)
                        }
                        
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    func createforgotView(){
        forgotpasswordview  = forgotPasswordview(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
        self.view.addSubview(forgotpasswordview)
        forgotpasswordview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        forgotpasswordview.lastView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        forgotpasswordview.forgotPasswordLabel.text = NSLocalizedString("Forgot Password?", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         forgotpasswordview.passwordrestLabel.text = NSLocalizedString("Password reset link sent to your registered Email-ID", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        forgotpasswordview.enterEmailTextfield.setBorder()
          forgotpasswordview.enterEmailTextfield.placeholder =  NSLocalizedString("Enter your registered Email-ID", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        forgotpasswordview.CancelOutlet.layer.cornerRadius = 15.0
        forgotpasswordview.okOutlet.layer.cornerRadius = 15.0
        forgotpasswordview.CancelOutlet.addTarget(self, action: #selector(CancelbuttonAction), for: .touchUpInside)
        forgotpasswordview.okOutlet.addTarget(self, action: #selector(OKbuttonAction), for: .touchUpInside)
        
    }
    
    @IBAction func skiploginAction(_ sender: Any) {
     
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
        if let vcs = tabbarVC.viewControllers,
            
            let nc = vcs.first as? UINavigationController,
            let pendingOverVC = nc.topViewController as? HomeViewController {
            
            
        }
        //    let navController = UINavigationController(rootViewController: vcs)
        
        self.present(tabbarVC, animated: false, completion: nil)
        

        
    }
    func alertsnack(message: String){
        
        //  let snackbar = TTGSnackbar(message:message, duration: .middle)
        //  snackbar.backgroundColor = UIColor.black
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = "Done"
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    
}
extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
extension UIView {
    func addshadow(top: Bool,
                   left: Bool,
                   bottom: Bool,
                   right: Bool,
                   shadowRadius: CGFloat = 2.0) {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1.0
        
        let path = UIBezierPath()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var viewWidth = self.frame.width
        var viewHeight = self.frame.height
        
        // here x, y, viewWidth, and viewHeight can be changed in
        // order to play around with the shadow paths.
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        // selecting top most point
        path.move(to: CGPoint(x: x, y: y))
        // Move to the Bottom Left Corner, this will cover left edges
        /*
         |☐
         */
        path.addLine(to: CGPoint(x: x, y: viewHeight))
        // Move to the Bottom Right Corner, this will cover bottom edge
        /*
         ☐
         -
         */
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        // Move to the Top Right Corner, this will cover right edge
        /*
         ☐|
         */
        path.addLine(to: CGPoint(x: viewWidth, y: y))
        // Move back to the initial point, this will cover the top edge
        /*
         _
         ☐
         */
        path.close()
        self.layer.shadowPath = path.cgPath
}
}
