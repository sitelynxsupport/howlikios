//
//  RegisterViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/20/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import DLRadioButton
import NVActivityIndicatorView
import TTGSnackbar
import FontAwesome_swift
import SCLAlertView


class RegisterViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, NVActivityIndicatorViewable{
    
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var femalelabel: UILabel!
    @IBOutlet var malelabel: UILabel!
    @IBOutlet var GenderLabel: UILabel!
    @IBOutlet var maleOutlet: DLRadioButton!
    @IBOutlet var femaleOutlet: DLRadioButton!
    
    @IBOutlet var individualbuttonOutlet: UIButton!
    
    @IBOutlet var companybuttonOutlet: UIButton!
    @IBOutlet var individualselectionview: UIView!
    
    @IBOutlet var loginOutlet: UIButton!
    @IBOutlet var companyselectionview: UIView!
    
    @IBOutlet var countryselectOutlet: UIButton!
    @IBOutlet var nameTextfield: UITextField!
    
    @IBOutlet var countryTextfield: UITextField!
    
    @IBOutlet var phoneTextfield: UITextField!
    
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var confirmTextfield: UITextField!
    
    @IBOutlet var registerbuttonOutlet: UIButton!
    
    var searchController: UISearchController!
     let LoadRegister  = ApiListViewcontroller()
    let PostRegister  = ApiListViewcontroller()
    
    var isSearching = false
    var myTableView = UITableView()
    var newview = UIView()
    var val = false
  
   // private var country = [String]()
    var buttonIndex = Int()
    var nameArray = [String]()
    var tidArry = [String]()
    var translationofArray = [String]()
    var usertyprIdArray = [String]()
    var usrTypenameArray = [String]()
    var countrycodeArray = [String]()
    var filteredobjects = [String]()
    var countrytitleArray = [String]()
    let emailvalidate = emailValidateViewController()
    var Gender = ""
    var usertypes = ""
    var status = ""
    var addview = UIView()
    var countrycode = ""
     var flagImage = ["kuwait","saudi_arabia","united_arab_emirates","bahrain","qatar","oman","india"]
    override func viewDidLoad() {
            super.viewDidLoad()
         //SetBackBarButtonCustom()
        let layer = CAGradientLayer()
        layer.frame = self.registerbuttonOutlet.bounds
        layer.startPoint = CGPoint(x:0, y:0)
        layer.endPoint = CGPoint(x:1.0, y:0.3)
        layer.colors = [UIColor.red.cgColor, UIColor.orange.cgColor]
        //set navigationbar transparent
       // UISearchResultsUpdating, UISearchBarDelegate,UISearchControllerDelegate,
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        // Set translucent. (Default value is already true, so this can be removed if desired.)
//.appearance().isTranslucent = true
       
      
        registerbuttonOutlet.layer.addSublayer(layer)
        registerbuttonOutlet.layer.cornerRadius = 20
        registerbuttonOutlet.backgroundColor =  UIColor.orange
        registerbuttonOutlet.layer.masksToBounds = true
  
        let layer1 = CAGradientLayer()
        layer1.frame = self.loginOutlet.bounds
        layer1.startPoint = CGPoint(x:0, y:0)
        layer1.endPoint = CGPoint(x:1.0, y:0.3)
        layer1.colors = [UIColor.red.cgColor, UIColor.orange.cgColor]
         loginOutlet.layer.addSublayer(layer1)
        loginOutlet.backgroundColor =  UIColor.orange
        loginOutlet.layer.cornerRadius = 20
        loginOutlet.layer.masksToBounds = true
       
        maleOutlet.isSelected = true
        maleOutlet.iconColor = UIColor.red
        maleOutlet.indicatorColor = UIColor.red
        Gender = "1"
            companyselectionview.isHidden = true
            nameTextfield.setBottomBorder()
            countryTextfield.setBottomBorder()
            phoneTextfield.setBottomBorder()
            emailTextfield.setBottomBorder()
            passwordTextfield.setBottomBorder()
            confirmTextfield.setBottomBorder()
        
        
        
        
//        let screenSize: CGRect = UIScreen.main.bounds
//        self.newview = UIView(frame: CGRect(x: 0, y: 60, width: screenSize.width, height: screenSize.height ))
//        self.view.addSubview(newview)
//        self.newview.backgroundColor = UIColor.red
//      //  self.newview.center = view.center
//
//        let tablescreenSize: CGRect = self.newview.bounds
//
//        let screenWidth = tablescreenSize.width
//        let screenHeight = tablescreenSize.height
//        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height))
//        myTableView.dataSource = self
//        myTableView.delegate = self
//
//        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
//        newview.addSubview(myTableView)
//        configureSearchController()
//        myTableView.tableHeaderView = searchController.searchBar
//        self.extendedLayoutIncludesOpaqueBars = true
//        //country = ["India","Africa","America"]
//          registerbuttonOutlet.backgroundColor =  UIColor.orange
//        newview.isHidden = true
        languageSet()
    }
    func languageSet(){
        
       
        individualbuttonOutlet.setTitle(NSLocalizedString("Individual", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
          companybuttonOutlet.setTitle(NSLocalizedString("Company", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
         nameLabel.text = NSLocalizedString("Name", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
      malelabel.text = NSLocalizedString("Male", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           femalelabel.text = NSLocalizedString("Female", bundle:  LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
           GenderLabel.text = NSLocalizedString("Gender", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         nameTextfield.placeholder = NSLocalizedString("Name", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        countryTextfield.placeholder = NSLocalizedString("Country", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        phoneTextfield.placeholder = NSLocalizedString("Phone", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        emailTextfield.placeholder = NSLocalizedString("Email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          passwordTextfield.placeholder = NSLocalizedString("Password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         confirmTextfield.placeholder = NSLocalizedString("Confirm Password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         loginOutlet.setTitle(NSLocalizedString("LOGIN", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
            registerbuttonOutlet.setTitle(NSLocalizedString("Register", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        if  UserDefaults.standard.string(forKey:"language_code") == "en"{
            maleOutlet.contentHorizontalAlignment = .left
            femaleOutlet.contentHorizontalAlignment = .left
            individualbuttonOutlet.contentHorizontalAlignment = .left
            companybuttonOutlet.contentHorizontalAlignment = .left
            countryselectOutlet.contentHorizontalAlignment = .left
            
        }else{
            maleOutlet.contentHorizontalAlignment = .right
            femaleOutlet.contentHorizontalAlignment = .right
            individualbuttonOutlet.contentHorizontalAlignment = .right
            companybuttonOutlet.contentHorizontalAlignment = .right
            countryselectOutlet.contentHorizontalAlignment = .right

        }
    }
    func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 130 ))
        self.addview.addSubview(newview)
        newview.center = view.center
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        // if buttonIndex == 0{
        label.text = NSLocalizedString("SelectCountry", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        myTableView.register(UINib(nibName: "profilecountryTableViewCell", bundle: nil), forCellReuseIdentifier: "myIdentifier")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
        let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 65, y:newview.frame.height - 45,width: 60  , height:  40));
        newview.addSubview(button);
        
        button.layer.cornerRadius = 3.0
        button.setTitle( NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        label.textColor = UIColor.black
        newview.backgroundColor = UIColor.white
        label.isHighlighted = true
        label.font = UIFont.boldSystemFont(ofSize: 20)
        myTableView.reloadData()
    }
    @objc func okView(_ sender: UIButton)
    {
        
        
        newview.isHidden = true
        addview.isHidden = true
        myTableView.isHidden = true
        //keywordoutlet.setTitle(tickValue, for: UIControlState())
        //  newview.isHidden = true
        
    }

    
    @IBAction func loginAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 30, height: 30)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func indidualbuttonAction(_ sender: UIButton) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {()
          self.companyselectionview.isHidden = true
        }, completion: nil)

        
        self.individualselectionview.isHidden = false
       individualbuttonOutlet.setTitleColor(UIColor(red: 246/255.0, green: 246/255.0, blue: 246/255.0, alpha: 0.0), for: UIControlState.selected)
   companybuttonOutlet.setTitleColor(UIColor.white, for: UIControlState.normal)
        usertypes = "3"
       // companyselectionview.isHidden = true
    }

    
    @IBAction func companyButtonAction(_ sender: UIButton) {
        companyselectionview.isHidden = false
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {()
            self.individualselectionview.isHidden = true
        }, completion: nil)
        companybuttonOutlet.setTitleColor(UIColor(red: 246/255.0, green: 246/255.0, blue: 246/255.0, alpha: 0.0), for: UIControlState.selected)
          individualbuttonOutlet.setTitleColor(UIColor.white, for: UIControlState.selected)
            usertypes = "2"
      
    }
    
    
    @IBAction func maleradiobutton(_ sender: Any) {
       
        maleOutlet.iconColor = UIColor.red
        maleOutlet.indicatorColor = UIColor.red
        femaleOutlet.iconColor = UIColor.black
        femaleOutlet.indicatorColor = UIColor.clear
        Gender = "1"
    
       
    }
    
    @IBAction func femaleradiobutton(_ sender: Any) {
        femaleOutlet.iconColor = UIColor.red
        femaleOutlet.indicatorColor = UIColor.red
        maleOutlet.iconColor = UIColor.black
        maleOutlet.indicatorColor = UIColor.clear
         Gender = "2"
        
    }
    
    
    @IBAction func registerAction(_ sender: Any) {
        if nameTextfield!.text!.isEmpty{
                alertsnack(message: NSLocalizedString("Enter your name", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        
            
        }else if (maleOutlet.isSelected == false) && (femaleOutlet.isSelected == false) {
            
        }else if phoneTextfield!.text!.isEmpty{
                 alertsnack(message: NSLocalizedString("Phone number required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
           
            
        }
        else if (emailvalidate.isValidEmail(testStr:emailTextfield!.text! ) == 1) {
          
              alertsnack(message: NSLocalizedString("EnterEmail", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
            
        }else if(emailvalidate.isValidEmail(testStr:emailTextfield!.text! ) == 3){
               alertsnack(message: NSLocalizedString("Invalid Email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
         
        }
            
        else if (isValidPassword(password:passwordTextfield!.text!) == 1){
             alertsnack(message: NSLocalizedString("Please enter password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
       
           
            
        }else if (isValidPassword(password:passwordTextfield!.text!) == 2){
         
                 alertsnack(message: NSLocalizedString("Use at least 6 characters!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }
        else if (isConfirmPassword(password:passwordTextfield!.text!) == 1){
            alertsnack(message: NSLocalizedString("Please enter password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (isConfirmPassword(password:passwordTextfield!.text!) == 2){
          
             alertsnack(message: NSLocalizedString("Password mismatch!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
             self.startactvityIndicator()
             apiCalling(country:countryTextfield!.text!,gender: Gender,name:nameTextfield!.text!,usertype:usertypes,phone: phoneTextfield!.text!,email:emailTextfield!.text!,password: passwordTextfield!.text!)
        }
        
    }
    
    func apiCalling(country:String,gender:String,name:String,usertype:String,phone:String,email:String,password:String){
        let scriptUrl = PostRegister.PostRegister
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "country=\(country)&gender=\(gender)&name=\(name)&usertype=\(usertype)&phone=\(phone)&email=\(email)&password=\(password)"
        
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
               
                     self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                  
                }
                
                
                DispatchQueue.main.async
                    {
                           if (self.status == "success") {
                         self.stopActivityIndicator()
                      
                            self.Successalert(SubTitle:"Successfully Registered")
                            
                           }else{
                             self.stopActivityIndicator()
                            self.emailvalidate.Failurealert()
                        }
                       
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
        
        
    }
    func alertsnack(message: String){
        
        //  let snackbar = TTGSnackbar(message:message, duration: .middle)
        //  snackbar.backgroundColor = UIColor.black
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = "Done"
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    func Successalert(SubTitle:String){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
            self.resignFirstResponder()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        })
        alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:SubTitle )
    }
//    func configureSearchController() {
//        // Initialize and perform a minimum configuration to the search controller.
//        searchController = UISearchController(searchResultsController: nil)
//        searchController.searchResultsUpdater = self
//        searchController.dimsBackgroundDuringPresentation = false
//        searchController.searchBar.placeholder = "Search here..."
//        searchController.searchBar.delegate = self
//        searchController.searchBar.sizeToFit()
//
//        // Place the search bar view to the tableview headerview.
//        myTableView.tableHeaderView = searchController.searchBar
//    }
//
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        isSearching = true
//
//        myTableView.reloadData()
//    }
//
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        isSearching = false
//
//        myTableView.reloadData()
//    }
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        if !isSearching {
//            isSearching = true
//
//            myTableView.reloadData()
//        }
//
//        searchController.searchBar.resignFirstResponder()
//    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if isSearching{
//            return filteredobjects.count
//        }
       
        return countrytitleArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
//        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//            cell.selectionStyle = .none
//        cell.backgroundColor = UIColor.white
//        if isSearching{
//            cell.textLabel?.text = self.filteredobjects[indexPath.row]
//
//        }else{
//            cell.textLabel?.text = countrytitleArray[indexPath.row]
//        }
          let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! profilecountryTableViewCell
        cell.countryname.text = countrytitleArray[indexPath.row]
        cell.countryflag.image = UIImage(named:flagImage[indexPath.row])
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryTextfield.text  = countrytitleArray[indexPath.row]
          countrycode  = countrycodeArray[indexPath.row]
        UserDefaults.standard.set(countrycode, forKey: "country_code")
        myTableView.isHidden = true
        addview.isHidden = true
        newview.isHidden = true
        
//        if isSearching{
//            countryTextfield.text = self.filteredobjects[indexPath.row]
//
//
//        }else{
//           countryTextfield.text  = countrytitleArray[indexPath.row]
//        }
//        newview.isHidden = true
//        myTableView.isHidden = true
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        navigationItem.rightBarButtonItem  = nil
        
    }
//    func updateSearchResults(for searchController: UISearchController) {
//
//        // If we haven't typed anything into the search bar then do not filter the results
//        if searchController.searchBar.text! == "" {
//            filteredobjects = countrytitleArray
//        } else {
//            // Filter the results
//
//            filteredobjects = countrytitleArray.filter { $0.lowercased().contains(searchController.searchBar.text!.lowercased()) }
//        }
//
//        self.myTableView.reloadData()
//
//    }
    @IBAction func countryTextfieldAction(_ sender: Any) {
          loadregistration()
     createTableview()
          myTableView.reloadData()
  
      
        
//        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = nil
//        navigationController?.navigationBar.barTintColor =  UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
//
//        self.navigationController?.navigationBar.isTranslucent = false
//        let add = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(addTapped))
//        navigationItem.rightBarButtonItem = add
//       navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
      
    }
 
//    @objc func addTapped(){
//
//                self.newview.isHidden = true
//          myTableView.isHidden = true
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//         navigationItem.rightBarButtonItem  = nil
//
//
//    }
    func loadregistration(){
             self.startactvityIndicator()
        self.countrycodeArray.removeAll()
         self.countrytitleArray.removeAll()
        let scriptUrl = LoadRegister.LoadRegister
        
        let myUrl = URL(string: scriptUrl);
        var request = URLRequest(url:myUrl!)
        
       
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                   
                    // self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                   
                       let genders = convertedJsonIntoArray["genders"] as! NSArray
                    for gender in  (genders as! [[String:Any]]){
                        let genderdic = gender as NSDictionary
                        
                        let name = genderdic["name"] as! String
                        let tid = genderdic["tid"] as! String
                        let translation_of = genderdic["translation_of"] as! String
                        self.self.nameArray.append(name)
                        self.tidArry.append(tid)
                        self.translationofArray.append(String(translation_of))
                    }
                    let usertypes = convertedJsonIntoArray["usertypes"] as! NSArray
                    for usertype in  (usertypes as! [[String:Any]]){
                        let usertypedic = usertype as NSDictionary
                        
                        let id = usertypedic["id"] as! NSInteger
                        let name = usertypedic["name"] as! String
                      
                        self.usrTypenameArray.append(name)
                        self.usertyprIdArray.append(String(id))
                       
                    }
                    let countries = convertedJsonIntoArray["countries"] as! NSArray
                    for countrie in  (countries as! [[String:Any]]){
                        let countriedic = countrie as NSDictionary
                        
                        let code = countriedic["code"] as! String
                        let title = countriedic["title"] as! String
                        
                        self.countrycodeArray.append(code)
                        self.countrytitleArray.append(title)
                        
                    }
                }
                
                
                DispatchQueue.main.async
                    {
                           if (self.status == "success") {
                            self.stopActivityIndicator()
                        
                            self.myTableView.reloadData()
                      
                        
                           }else{
                            self.stopActivityIndicator()
                        
                            self.myTableView.reloadData()
                          
                          
                            
                        }
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        

    }
    
    func isValidPassword(password:String) -> Int{
        if passwordTextfield.text!.isEmpty{
            return 1
        }else if passwordTextfield.text!.count < 6{
          
            return 2
        }else {
            return 0
        }
        
    }
    func isConfirmPassword(password:String) -> Int{
        if confirmTextfield.text!.isEmpty{
            return 1
        }else if (confirmTextfield.text! !=  passwordTextfield.text){
         
            return 2
        }else {
            return 0
        }
        
    }
  
}
