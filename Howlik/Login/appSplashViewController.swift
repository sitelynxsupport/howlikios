//
//  appSplashViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 6/26/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class appSplashViewController: UIViewController {
    var apikey = ""

    @IBOutlet var appLabel: UILabel!
     var gameTimer: Timer! //Ti
    override func viewDidLoad() {
        super.viewDidLoad()
        appLabel.text =  NSLocalizedString("Find Out What's Around You", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
      

 
        UIView.animate(withDuration: 1.0) {
            self.appLabel.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        }
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        gameTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(timeaction), userInfo: nil, repeats: false)
      
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func timeaction(){
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            print("^^^^^^^^^^^^^^^^^^^")
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
            if let vcs = tabbarVC.viewControllers,
                
                let nc = vcs.first as? UINavigationController,
                let pendingOverVC = nc.topViewController as? HomeViewController {
                
                
            }
            //    let navController = UINavigationController(rootViewController: vcs)
            
            self.present(tabbarVC, animated: false, completion: nil)
            
        }else{
            print("*************************")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "login") as! LoginViewController
            self.present(nextViewController, animated:true, completion:nil)

         //   let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
           // self.present(vc, animated: true, completion: nil)
        }
    }

}
