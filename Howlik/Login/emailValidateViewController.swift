//
//  emailValidateViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 1/22/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import SCLAlertView
import NVActivityIndicatorView

class emailValidateViewController: UIViewController,NVActivityIndicatorViewable {
    func isValidEmail(testStr:String) -> Int {
        if  testStr.isEmpty{
            return 1
        }else{
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
            if result == true{
                 return 2
            }else{
                  return 3
            }
       
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Failurealert(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                self.dismiss(animated: true, completion: nil)
        })
        
        alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
    }
    func Successalert(SubTitle:String){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
            self.resignFirstResponder()
            self.dismiss(animated: true, completion: nil)
        })
        alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:SubTitle )
    }
    func FailurealertWithSubtitle(SubTitle:String){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
        alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
            self.dismiss(animated: true, completion: nil)
        })
        
        alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: SubTitle)
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 30, height: 30)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    func createNothingview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let customview : UIView = UIView(frame: CGRect(x: 15, y: 80, width: Int(screenSize.width - 30) , height: Int(screenSize.height - 30) ))
        view.addSubview(customview)
          let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        customview.center = view.center
          let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = "Nothing to see Here"
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func createeventNothingview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let customview : UIView = UIView(frame: CGRect(x: 15, y: 65, width: Int(screenSize.width - 30) , height: Int(screenSize.height) ))
        view.addSubview(customview)
        customview.backgroundColor = UIColor.yellow
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = "Nothing to see Here"
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func skipAlert(SubTitle:String){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alert = SCLAlertView(appearance: appearance) // create alert with appearance
        alert.addButton("OK", action: { // create button on alert
            print("click click") // action on click
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        })
        alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: SubTitle)
    }

}
