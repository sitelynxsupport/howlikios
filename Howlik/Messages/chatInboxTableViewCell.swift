//
//  chatInboxTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/24/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class chatInboxTableViewCell: UITableViewCell {

    @IBOutlet weak var cellDelOutlet: UIButton!
    @IBOutlet weak var firstletterLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    @IBOutlet weak var celldeleteAction: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
