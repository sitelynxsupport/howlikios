//
//  chatSentViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/25/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift

class chatSentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var sentTable: UITableView!
 
      let ChatapiClass = ApiListViewcontroller()
    var selectedId = [String]()
    var colorarr1 = [UIColor]()
    var deleteIndex = [String]()
    var buttonIndex = Int()
    var ctid = Int()
    var val = ""
    var apikey = ""
    var status = ""
      var customview = UIView()
      var inboxArray = [inboxClass]()

    @IBOutlet weak var nomessageview: UIView!
    override func viewDidLoad() {
if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            ChatSentloadapiCalling(apikey:apikey)
        }
        SetBackBarButtonCustom()
        self.navigationItem.title =  NSLocalizedString("Send Message", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationControll/Users/shrishtiinformatics/Desktop/Howlik  copy 5/Howlik/Messages/fromMessageClass.swifter!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        // Do any additional setup after loading the view.
        self.navigationItem.rightBarButtonItem = nil
        colorarr1 = [UIColor(red: 156/255.0, green: 204/255.0, blue: 101/255.0, alpha: 1.0),
                     UIColor(red: 255/255.0, green: 167/255.0, blue: 38/255.0, alpha: 1.0),
                     UIColor(red: 120/255.0, green: 144/255.0, blue: 156/255.0, alpha: 1.0),
                     UIColor(red: 232/255.0, green: 78/255.0, blue: 64/255.0, alpha: 1.0),
                     UIColor(red: 236/255.0, green: 64/255.0, blue: 122/255.0, alpha: 1.0),
                     UIColor(red: 171/255.0, green: 71/255.0, blue: 188/255.0, alpha: 1.0),
                     UIColor(red: 126/255.0, green: 86/255.0, blue: 194/255.0, alpha: 1.0),
                     UIColor(red: 41/255.0, green: 182/255.0, blue: 246/255.0, alpha: 1.0),
                     UIColor(red: 255/255.0, green: 238/255.0, blue: 88/255.0, alpha: 1.0)]
        
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tabBarController?.tabBar.items?[1].title = NSLocalizedString("Sent", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }

    func ChatSentloadapiCalling(apikey:String){
        // self.startactvityIndicator()
      //  inboxArray.removeAll()
      //  selectedId.removeAll()
       let scriptUrl = ChatapiClass.ListofallMessages
        
        let parameters: Parameters = ["apikey": apikey]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                          if (response["sent"]) != nil {
                        let sent = response.object(forKey: "sent")!
                        if (sent as AnyObject).count == 0 {
                            self.sentTable.isHidden = true
                            self.createeventNothingview()
                            
                        }else{
                        for singlesent in (sent as! [[String:Any]]){
                              self.sentTable.isHidden = false
                            let sentElement = inboxClass()
                            let contry = singlesent as NSDictionary
                            if  (contry["datetime"] as? String) != nil {
                                let datetime = contry["datetime"] as! String
                                sentElement.datetime = datetime
                                
                            }
                            if  (contry["id"] as? String) != nil {
                                let id = contry["id"] as! String
                                sentElement.id = id
                            }
                            if  (contry["name"] as? String) != nil {
                                let name = contry["name"] as! String
                                sentElement.name = name
                                let letter = name.first
                                print("^^^^^^^^^^^^^")
                                print(letter)
                                print("^^^^^^^^^^^^^")
                                sentElement.firstletter = String(describing: letter!)
                            }
                            if  (contry["subject"] as? String) != nil {
                                let subject = contry["subject"] as! String
                                sentElement.subject = subject
                            }
                            
                            if  (contry["message"] as? String) != nil {
                                let message = contry["message"] as! String
                                sentElement.message = message
                            }
                          
                            self.inboxArray.append(sentElement)
                        }
                        
                        }
                        }
                        
                        DispatchQueue.main.async
                            {
                                
                          self.sentTable.reloadData()
                              
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createeventNothingview(){
        let screenSize: CGRect = UIScreen.main.bounds
        //     customview  = UIView(frame: CGRect(x: 0, y: 0, width: Int(screenSize.width) , height: Int(screenSize.height) ))
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inboxArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! chatSentTableViewCell
        cell.selectionStyle = .none
        cell.tonameLabel.text = inboxArray[indexPath.row].name
        cell.createdatLabel.text = inboxArray[indexPath.row].datetime
        cell.subjectLabel.text = inboxArray[indexPath.row].subject
        // cell.firstletterLabel.text = inboxArray[indexPath.row].firstletter
        cell.firstletterLabel.layer.cornerRadius = cell.firstletterLabel.frame.width/2
        cell.firstletterLabel.layer.masksToBounds = true
        let randomSample = colorarr1.sample()
        cell.firstletterLabel.backgroundColor = randomSample
        cell.firstletterLabel.text =  inboxArray[indexPath.row].firstletter.uppercased()
        cell.celldelAction.tag = indexPath.row;
        cell.celldelAction.addTarget(self, action:#selector(self.buttonView(_:)), for:.touchUpInside)
        cell.cellDelOutlet.tag = indexPath.row;
        cell.cellDelOutlet.addTarget(self, action:#selector(self.buttondelView(_:)), for:.touchUpInside)
        cell.cellDelOutlet.setImage(#imageLiteral(resourceName: "check circle1"), for: .normal)
        if deleteIndex.contains(String(describing: indexPath.row)){
            
            cell.cellDelOutlet.isHidden = false
            cell.firstletterLabel.isHidden = true
            cell.celldelAction.isHidden = true
            
            
        }else{
            
            cell.cellDelOutlet.isHidden = true
            cell.firstletterLabel.isHidden = false
            cell.celldelAction.isHidden = false
            
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "chatsent") as! sentChatViewController
       vc.toname = inboxArray[indexPath.row].name
        vc.dateTime = inboxArray[indexPath.row].datetime
        vc.subject = inboxArray[indexPath.row].subject
        vc.content = inboxArray[indexPath.row].message
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }
    @objc func buttonView(_ sender: UIButton)
    {
        let cell = sender.superview?.superview as! chatSentTableViewCell
        cell.firstletterLabel.isHidden = true
        cell.celldelAction.isHidden = true
        cell.cellDelOutlet.isHidden = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash,target: self, action: #selector(deleteAction))
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        buttonIndex =  sender.tag
        self.val = inboxArray[buttonIndex].id
        
        if  selectedId.contains(val){
        }else{
            self.selectedId.append(self.val)
            self.deleteIndex.append(String(self.buttonIndex))
        }
        
        
        
        
    }
    @objc func buttondelView(_ sender: UIButton)
    {
     
        let cell = sender.superview?.superview as! chatSentTableViewCell
        buttonIndex =  sender.tag
        cell.cellDelOutlet.isHidden = true
        cell.firstletterLabel.isHidden = false
        cell.celldelAction.isHidden = false
        
        
        self.val = inboxArray[buttonIndex].id
        if selectedId.contains(val){
            let j = selectedId.index(of: val)
            selectedId.remove(at: j!)
            deleteIndex.remove(at: j!)
            
        }
        
        
        
        if selectedId.count == 0{
            self.navigationItem.setHidesBackButton(true, animated: false)
            self.navigationItem.rightBarButtonItem = nil
        }
        
  
        
    }
    @objc func deleteAction(){
        var createdArray = NSMutableArray()
        var bigDic: NSMutableDictionary = NSMutableDictionary()
        for i in 0..<selectedId.count
        {
            createdArray.add(selectedId[i])
        }
        bigDic.setValue(createdArray, forKey: "msg_id")
        bigDic.setValue(apikey, forKey: "apikey")
        print(bigDic)
        
        do {
            
            //Convert to Data
            
            let jsonData = try! JSONSerialization.data(withJSONObject: bigDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            // print(jsonData)
            
            
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8)
            {
                
                
                
                print(JSONString)
                let scriptUrl = ChatapiClass.DeleteMessages
                let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
                let myUrl = URL(string: urlWithParams);
                var request = URLRequest(url:myUrl!)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpBody = jsonData
                let task = URLSession.shared.dataTask(with: request)
                {
                    data, response, error in
                    if error != nil
                    {
                        print("error=\(error)")
                        DispatchQueue.main.async
                            {
                                print("server down")
                        }
                        return
                    }
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("################################")
                    print(responseString)
                    do{
                        if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                        {
                            self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                            print(self.status)
                        }
                        DispatchQueue.main.async
                            {
                                if (self.status == "success")
                                {
                                    
                                    self.ChatSentloadapiCalling(apikey:self.apikey)
                                    
                                }
                                else{
                                    
                                    
                                }
                                
                                
                        }//dispatch ends
                        
                    }//do ends
                    catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }
                
                
                
                
                task.resume()
            }
        }
    }
  
}
