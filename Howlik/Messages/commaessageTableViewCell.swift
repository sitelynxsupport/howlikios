//
//  commaessageTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/4/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class commaessageTableViewCell: UITableViewCell {
    @IBOutlet var subjectTextfield: UITextField!
    
    @IBOutlet var contentgTextview: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       subjectTextfield.setBottomBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
