//
//  composeListViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 6/6/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Alamofire
class composeListViewController: UIViewController,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var searchbar: UISearchBar!
    
    @IBOutlet var doneOutlet: UIButton!
    @IBOutlet var composelistTable: UITableView!
    let ComposeapiClass = ApiListViewcontroller()
    var composeArray = [composeClass]()
    var filterArray = [composeClass]()
    var apikey = ""
    var  senderid = ""
     var emailArray = [String]()
     var idArray = [String]()
    var selectedArray = [String]()
    var wichcell = Int()
    var email = ""
   var shouldShowSearchResults = false
    var searchCount = 1
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            ChatComposeloadapiCalling(apikey:apikey)
            
        }
        searchbar.delegate = self
        searchbar.placeholder = "Enter email.."
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        shouldShowSearchResults = true
//        composelistTable.reloadData()
//    }
//
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        shouldShowSearchResults = false
//        composelistTable.reloadData()
//    }
//
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        composelistTable.reloadData()
//
//        if !shouldShowSearchResults {
//            shouldShowSearchResults = true
//            composelistTable.reloadData()
//        }
//        composelistTable.reloadData()
//        searchbar.resignFirstResponder()
//    }
//    func updateSearchResults(for searchController: UISearchController) {
//
//        let searchString = searchController.searchBar.text
//
//        // Filter the data array and get only those countries that match the search text.
//        filterArray = composeArray.filter({ (country) -> Bool in
//            let countryText: NSString = country.email as NSString
//
//            return (countryText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
//        })
//
//        // Reload the tableview.
//        composelistTable.reloadData()
//    }
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       shouldShowSearchResults = true
        searchBar.setShowsCancelButton(true, animated: true)
          filterArray = composeArray
        composelistTable.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       // shouldShowSearchResults = false
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        
        // Remove focus from the search bar.
        searchBar.endEditing(true)
        
        composelistTable.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            
            composelistTable.reloadData()
        }
        
        searchbar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterArray = composeArray.filter({ (text) -> Bool in
            
            let tmp: NSString = text.email as NSString
            let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
      
//        if(filterArray.count == 0){
//            shouldShowSearchResults = false;
//          //  tableViewHeaders.removeAll()
//           // tableViewSource.removeAll()
//        } else {
//
//            shouldShowSearchResults = true;
//          // searchbar.resignFirstResponder()
//        }
        
        if (searchbar.text?.isEmpty)!{
            filterArray = composeArray
               searchBar.setShowsCancelButton(false, animated: true)
            searchbar.resignFirstResponder()
        }
       
        self.composelistTable.reloadData()
    }
    
    
    
    
    
    
    
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    
    func ChatComposeloadapiCalling(apikey:String){
        // self.startactvityIndicator()
        composeArray.removeAll()
        emailArray.removeAll()
        filterArray.removeAll()
        let scriptUrl = ComposeapiClass.MailUserslist
        
        let parameters: Parameters = ["apikey": apikey]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                        let users = response.object(forKey: "users")!
                        for user in (users as! [[String:Any]]){
                            let composeElement = composeClass()
                            let contry = user as NSDictionary
                            if  (contry["email"] as? String) != nil {
                                self.email = contry["email"] as! String
                                composeElement.email = self.email
                                
                                
                            }
                            if  (contry["id"] as? NSInteger) != nil {
                                let id = contry["id"] as! NSInteger
                                composeElement.id = String(id)
                            }
                            if self.selectedArray.count != 0{
                            if self.selectedArray.contains(self.email){
                                   composeElement.isChecked = true
                           
                            }else{
                                   composeElement.isChecked = false
                            }
                            }else{
                                 composeElement.isChecked = false
                            }
                            self.composeArray.append(composeElement)
                        }
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                self.composelistTable.reloadData()
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
       
            return 1
      
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchCount != 0{
            if(shouldShowSearchResults){
                return filterArray.count
            } else {
                return composeArray.count
            }
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
      
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! comListTableViewCell
            if(shouldShowSearchResults){
                cell.tickLabel.text = filterArray[indexPath.row].email
                cell.tickButton.tag = indexPath.row
                cell.alltickButton.tag = indexPath.row
                cell.alltickButton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
                let  val = filterArray[indexPath.row].isChecked
                wichcell = 1
                
                if(val == true){
                    cell.tickButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                    cell.tickButton.tintColor = UIColor.red
                    
                }else{
                    
                    cell.tickButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                    cell.tickButton.tintColor = UIColor.green
                }
            } else {
                cell.tickLabel.text = composeArray[indexPath.row].email
                cell.tickButton.tag = indexPath.row
                  cell.alltickButton.tag = indexPath.row
                cell.alltickButton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
                let val = composeArray[indexPath.row].isChecked
                wichcell = 2
                if(val == true){
                    cell.tickButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                    cell.tickButton.tintColor = UIColor.red
                    
                }else{
                    
                    cell.tickButton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                    cell.tickButton.tintColor = UIColor.green
                }
            }
            return cell
        
        
    
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
            return 44.0
      }
    
    @objc func moreView(_ sender: UIButton)
    {
        //tickvalueArray.removeAll()
        if(shouldShowSearchResults){
            if  filterArray[sender.tag].isChecked == false{
                filterArray[sender.tag].isChecked = true
                
                
            }else{
                filterArray[sender.tag].isChecked = false
            }
            
        }else{
            if  composeArray[sender.tag].isChecked == false{
                composeArray[sender.tag].isChecked = true
                
                
            }else{
                composeArray[sender.tag].isChecked = false
            }
        }
          searchbar.resignFirstResponder()
        composelistTable.reloadData()
    }
    
    
    
    
    
    
    
    @IBAction func doneAction(_ sender: Any) {
        emailArray.removeAll()
        if(shouldShowSearchResults){
            //  tickValue = ""
            for i in 0..<filterArray.count{
                if  filterArray[i].isChecked == true{
                    self.emailArray.append(filterArray[i].email)
                    self.idArray.append(filterArray[i].id)
                   
                    
                }else{
                    if emailArray.contains(filterArray[i].email){
                    let index = emailArray.index(of: filterArray[i].email)
                    self.emailArray.remove(at: index!)
                    self.idArray.remove(at: index!)
                    }
                }
            }
            
            
        }else{
            for i in 0..<composeArray.count{
                
                if  composeArray[i].isChecked == true{
                  
                    self.emailArray.append(composeArray[i].email)
                        self.idArray.append(composeArray[i].id)
                    
                }else{
                    if emailArray.contains(composeArray[i].email){
                        let index = emailArray.index(of: composeArray[i].email)
                        self.emailArray.remove(at: index!)
                        self.idArray.remove(at: index!)
                    }
                }
            }
        }
        self.composelistTable.reloadData()
        UserDefaults.standard.set(emailArray, forKey: "emailArray")
         UserDefaults.standard.set(idArray, forKey: "idArray")
         self.dismiss(animated: true, completion: nil)
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            print(emailArray)
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
       
    }
    
    
}
