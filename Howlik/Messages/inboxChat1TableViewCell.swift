//
//  inboxChat1TableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class inboxChat1TableViewCell: UITableViewCell {
    @IBOutlet weak var parentimage: UIImageView!
    
    @IBOutlet weak var chat1Label: UILabel!
    @IBOutlet weak var createdlabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
