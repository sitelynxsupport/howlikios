//
//  inboxchat2TableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class inboxchat2TableViewCell: UITableViewCell {
    @IBOutlet weak var teacherImage: UIImageView!
    @IBOutlet weak var createdTeacherLabel: UILabel!
    
    @IBOutlet weak var Chat2Label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
