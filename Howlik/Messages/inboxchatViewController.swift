//
//  inboxchatViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import FontAwesome_swift
import SCLAlertView
class inboxchatViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

   var id = ""
   var subject = ""
    var created = ""
    var toid = ""
    var Toname = ""
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet var navigationview: UIView!
    @IBOutlet weak var chatTable: UITableView!
    
 
    @IBOutlet weak var fromNameLabel: UILabel!
    
    @IBOutlet weak var createdLabel: UILabel!
    
    let ChatapiClass = ApiListViewcontroller()
    var apikey = ""
    var inboxfromArray = [fromMessageClass]()
    var inboxtoArray = [toMessageClass]()
    var senderid = ""
    var from_ids = ""
    var from = ""
    var createdAt = ""
   var rplyView = replyView()
   var addview = UIView()
    var newview = UIView()
    var status = ""
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("InboxMessage", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
         navigationview.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
     
        SetBackBarButtonCustom()
        fromNameLabel.text = subject
             createdLabel.text = created
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.senderid = UserDefaults.standard.string(forKey: "senderid")!
            
            ViewChatInboxloadapiCalling(apikey:apikey,msg_id:id)
        }

      //  rplyView = UINib(nibName: "replyView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
  //  rplyView.frame = view.frame
       
        self.tabBarController?.tabBar.isHidden = true

    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func createTableview(){

      rplyView  = replyView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
        self.view.addSubview(rplyView)
         rplyView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        rplyView.lastview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        rplyView.Contentview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        rplyView.contentLabel.layer.borderWidth = 0.5
        rplyView.contentLabel.layer.cornerRadius = 5
        rplyView.contentLabel.layer.borderColor = UIColor.gray.cgColor
        rplyView.contentLabel.layer.masksToBounds = false
        rplyView.tolabel.layer.borderWidth = 0.5
        rplyView.tolabel.layer.cornerRadius = 5
        rplyView.tolabel.layer.borderColor = UIColor.gray.cgColor
        rplyView.tolabel.layer.masksToBounds = false
        rplyView.tolabel.text = Toname
        rplyView.okOutet.addTarget(self, action: #selector(sendbuttonAction), for: .touchUpInside)
       rplyView.cancelOut.addTarget(self, action: #selector(cancelbuttonAction), for: .touchUpInside)
        // rplyView.backgroundColor = UIColor.red
    }
    @objc  func sendbuttonAction(sender: UIButton!) {
       
        let dic = NSMutableDictionary()
        // var emailarray = NSMutableArray()
        
        dic.setValue(id, forKey: "msg_id")
        dic.setValue(apikey, forKey: "apikey")
        dic.setValue(toid, forKey: "to")
        dic.setValue( rplyView.contentLabel.text, forKey: "message")
          dic.setValue( subject, forKey: "subject")
        do {
            
            
            
            //Convert to Data
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            // print(jsonData)
            
            //Convert back to string. Usually only do this for debugging
            
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                
                //ethaann nink veenda string
                
                // json aayit kittum
                print("***************************")
                print(JSONString)
                print("***************************")
                let scriptUrl = ChatapiClass.ReplyMessages
                let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
                let myUrl = URL(string: urlWithParams);
                var request = URLRequest(url:myUrl!)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpBody = jsonData
                let task = URLSession.shared.dataTask(with: request)
                {
                    data, response, error in
                    
                    if error != nil
                    {
                        print("error=\(error)")
                        DispatchQueue.main.async
                            {
                                print("server down")
                        }
                        return
                    }
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("################################")
                    print(responseString)
                    do{
                        
                        if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                            
                        {
                            
                            self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                            print(self.status)
                            
                        }
                        DispatchQueue.main.async
                            {
                                
                                
                               
                                if self.status == "success"{
                                    
                                    let appearance = SCLAlertView.SCLAppearance(
                                        showCloseButton: false // hide default button
                                    )
                                    let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                                    alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                        self.resignFirstResponder()
                                        self.dismiss(animated: true, completion: nil)
                                    })
                                    alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle:NSLocalizedString("Message Sent", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                  
                                }else{
                                    let appearance = SCLAlertView.SCLAppearance(
                                        showCloseButton: false // hide default button
                                    )
                                    let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                                    alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                        
                                    })
                                    alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("  OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                  
                                }
                                
                                
                        }//dispatch ends
                        
                    }//do ends
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    
                }
                
                
                
                //  let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                // print("################################")
                //  print(responseString)
                
                //   }
                
                task.resume()
                
                
            }
            
        }
        
        
       
    }
    @objc  func cancelbuttonAction(sender: UIButton!) {
         self.dismiss(animated: true, completion: nil)
        
    }
    func ViewChatInboxloadapiCalling(apikey:String,msg_id:String){
        // self.startactvityIndicator()
        inboxfromArray.removeAll()
      inboxtoArray.removeAll()
        let scriptUrl = ChatapiClass.ViewMessages
        
        let parameters: Parameters = ["apikey": apikey,"msg_id": msg_id]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                       let message = response.object(forKey: "message")!
                        for singlemessage in (message as! [[String:Any]]){
                            let messageElement = fromMessageClass()
                            let messagetoElement = toMessageClass()
                            let contry = singlemessage as NSDictionary
                            if  (contry["from_id"] as? String) != nil {
                                self.from_ids = contry["from_id"] as! String
                                 messageElement.from_id = self.from_ids
                            }
                         //   if self.from_ids == self.senderid{
                            
                            if  (contry["datetime"] as? String) != nil {
                                let datetime = contry["datetime"] as! String
                                messageElement.datetime = datetime

                            }
                            if  (contry["reply"] as? String) != nil {
                                let reply = contry["reply"] as! String
                                messageElement.reply = reply
                                
                            }
                            if  (contry["to_id"] as? String) != nil {
                                let to_id = contry["to_id"] as! String
                                messageElement.to_id = to_id
                            }
                            if  (contry["username"] as? String) != nil {
                                let username = contry["username"] as! String
                                messageElement.username = username
                            }
                            if  (contry["userphoto"] as? String) != nil {
                                let userphoto = contry["userphoto"] as! String
                                messageElement.userphoto = userphoto
                            }
                                self.inboxfromArray.append(messageElement)

                           
                        }
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                
                             
                                print("&&&&&&&&&&&&&&&&&&&&&&")
                               self.chatTable.reloadData()
                                print(self.inboxtoArray.count)
                                print(self.inboxfromArray.count)
                                print("&&&&&&&&&&&&&&&&&&&&&&")
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
   
    func alert(message: String, title: String ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return inboxfromArray.count
        
      
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = inboxfromArray[indexPath.row].from_id
        if type == senderid{
        let cell : inboxChat1TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! inboxChat1TableViewCell
            let username =  inboxfromArray[indexPath.row].username
             let datetime =  inboxfromArray[indexPath.row].datetime
            cell.createdlabel.text = username + ":" + datetime
            cell.parentimage.sd_setImage(with: URL(string:inboxfromArray[indexPath.row].userphoto), placeholderImage: UIImage(named: "placeholder.png"))
            cell.parentimage.layer.cornerRadius = cell.parentimage.frame.size.width/2
            cell.parentimage.clipsToBounds = true
            cell.chat1Label.text = inboxfromArray[indexPath.row].reply
                    cell.chat1Label.numberOfLines = 0
                cell.chat1Label.lineBreakMode = .byWordWrapping
            
            cell.chat1Label.layer.masksToBounds = true
            cell.chat1Label.layer.cornerRadius = 5.0

        return cell
        
    }
        else{
            let cell : inboxchat2TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell2")! as! inboxchat2TableViewCell
            let username =  inboxfromArray[indexPath.row].username
            let datetime =  inboxfromArray[indexPath.row].datetime
            cell.createdTeacherLabel.text = username + ":" + datetime
            cell.teacherImage.sd_setImage(with: URL(string:inboxfromArray[indexPath.row].userphoto), placeholderImage: UIImage(named: "placeholder.png"))
            cell.Chat2Label.layer.cornerRadius = 5.0
            cell.teacherImage.layer.cornerRadius = cell.teacherImage.frame.size.width/2
            cell.teacherImage.clipsToBounds = true
            cell.Chat2Label.text = inboxfromArray[indexPath.row].reply
            cell.Chat2Label.numberOfLines = 0
            cell.Chat2Label.lineBreakMode = .byWordWrapping
            toid = inboxfromArray[indexPath.row].from_id
            Toname = inboxfromArray[indexPath.row].username
            print(Toname)
            return cell
        }
        
 
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
  
    
    
    func alertAction(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        // Create the actions
        let okAction = UIAlertAction(title: NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "parentdashboard") as! parentdashboardViewController
//            let navController = UINavigationController(rootViewController: vc)
//            self.present(navController, animated: true, completion: nil)
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
   

    @IBAction func replyAction(_ sender: Any) {
        createTableview()
    }
}
