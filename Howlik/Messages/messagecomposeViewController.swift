//
//  messagecomposeViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/25/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift
import WSTagsField
import IQKeyboardManagerSwift
import Tags
import TTGSnackbar

class messagecomposeViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UISearchResultsUpdating,UISearchBarDelegate
 {
 var val = false
    var height = Double()
    var subject  = ""
    var content = ""
    var searchController: UISearchController!
    var shouldShowSearchResults = false
    var mytableViewY = 0
    var cellHeight = 50.0
    var cellmaximumHeight = 200.0
    @IBOutlet var composeTable: UITableView!
    let ComposeapiClass = ApiListViewcontroller()
    var composeArray = [composeClass]()
      var filterArray = [composeClass]()
    var apikey = ""
    var  senderid = ""
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    var emailArray = [String]()
     var idArray = [String]()
    var activityView = emailValidateViewController()
  var status = ""
      var searchActive : Bool = false
    var array = ["jfdigigfgutiugtruyit"]
   var wichcell = Int()
    var first  = 1
    
    @IBOutlet var toTextfield: UITextField!

    @IBOutlet weak var markallOutlet: UIButton!
    
    @IBOutlet weak var myview: UIView!
    @IBOutlet weak var subjectText: UITextField!
    @IBOutlet weak var contentText: UITextView!
    
    @IBOutlet weak var sendOutlet: UIButton!
    @IBOutlet weak var toText: UITextField!
   //let tagsView = TagsView()
    
  
    //let tagsField = WSTagsField()
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
   
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        // Do any additional setup after loading the view.
      
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
         
            print(apikey)
            ChatComposeloadapiCalling(apikey:apikey)
            
        }
       UserDefaults.standard.set("", forKey: "emailArray")
        UserDefaults.standard.set("", forKey: "idArray")
       first = 1
//        UserDefaults.standard.set("", forKey: "emailArray")

//        tagsField.delegate = self
//        tagsField.cornerRadius = 3.0
//        tagsField.spaceBetweenLines = 10
//        tagsField.spaceBetweenTags = 10
//
//
//        tagsField.layoutMargins = UIEdgeInsets(top: 2, left: 6, bottom: 2, right: 6)
//        tagsField.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10) ///old padding
//        tagsField.placeholder = "Enter a tag"
//        tagsField.placeholderColor = .lightGray
//        tagsField.placeholderAlwaysVisible = false
//        tagsField.backgroundColor = .white
//        tagsField.returnKeyType = .next
//        tagsField.delimiter = ""
//        tagsField.maxHeight = 100
//        textFieldEventss()
       
       
    }
 
    override func viewDidLayoutSubviews() {
    //   textview.layer.masksToBounds = true
       //  adjustUITextViewHeight(arg : textview)
        
    }
    override func viewDidAppear(_ animated: Bool) {
   
       
        if  UserDefaults.standard.array(forKey:"emailArray") != nil{
            self.emailArray = UserDefaults.standard.array(forKey: "emailArray") as! [String]
            self.idArray = UserDefaults.standard.array(forKey: "idArray") as! [String]
            composeTable.reloadData()
        }
        if emailArray.count == 0{
            first = 1
            composeTable.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tabBarController?.tabBar.items?[2].title = NSLocalizedString("Compose", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }

    @objc func labelTapped() {
        print("labelTapped tapped")
        self.createTableview()
        self.myTableView.reloadData()
     //   self.tagsField.resignFirstResponder()
    }
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search here..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        // Place the search bar view to the tableview headerview.
        myTableView.tableHeaderView = searchController.searchBar
    }
   
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        shouldShowSearchResults = true
        myTableView.reloadData()
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        myTableView.reloadData()
    }
    
  
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
          myTableView.reloadData()
       
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            myTableView.reloadData()
        }
        myTableView.reloadData()
        searchController.searchBar.resignFirstResponder()
    }
    func updateSearchResults(for searchController: UISearchController) {
    
        let searchString = searchController.searchBar.text
        
        // Filter the data array and get only those countries that match the search text.
        filterArray = composeArray.filter({ (country) -> Bool in
            let countryText: NSString = country.email as NSString
            
            return (countryText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        // Reload the tableview.
        myTableView.reloadData()
    }
  
  
   
   
    @IBAction func emailButtonAction(_ sender: Any) {
        self.newview.isHidden = false
          self.myTableView.isHidden = false
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func ChatComposeloadapiCalling(apikey:String){
        // self.startactvityIndicator()
       composeArray.removeAll()
         emailArray.removeAll()
           filterArray.removeAll()
        let scriptUrl = ComposeapiClass.MailUserslist
        
        let parameters: Parameters = ["apikey": apikey]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                        if (response["users"]) != nil {

                    let users = response.object(forKey: "users")!
                 for user in (users as! [[String:Any]]){
                            let composeElement = composeClass()
                            let contry = user as NSDictionary
                            if  (contry["email"] as? String) != nil {
                                let email = contry["email"] as! String
                                composeElement.email = email
                                
                            }
                            if  (contry["id"] as? NSInteger) != nil {
                                let id = contry["id"] as! NSInteger
                                composeElement.id = String(id)
                            }
                                composeElement.isChecked = false
                            self.composeArray.append(composeElement)
                        }
                        
                        }
                        
                        DispatchQueue.main.async
                            {
                      self.myTableView.reloadData()
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        self.newview = UIView(frame: CGRect(x: 0, y: 60 , width: Int(screenSize.width)  , height: Int(screenSize.height - 140) ))
        view.addSubview(newview)
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
       // view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        //view1.backgroundColor  = UIColor.lightGray
        label.font = UIFont.boldSystemFont(ofSize: 20)
      //  self.myTableView.tableHeaderView = view1
       
    myTableView.register(UINib(nibName: "composeTableview", bundle: nil), forCellReuseIdentifier: "cell")
        label.text =  NSLocalizedString("Select Location", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        self.configureSearchController()
        let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 65, y:newview.frame.height - 45,width: 60  , height:  40));
        newview.addSubview(button);
        // button.backgroundColor = UIColor.green
        button.layer.cornerRadius = 3.0
        button.setTitle( NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        
    newview.addSubview(myTableView)
    self.myTableView.alwaysBounceVertical = false
    addview.isHidden = false
    newview.isHidden =  false
    myTableView.isHidden =  false
        myTableView.reloadData()
   
    }
 func numberOfSections(in tableView: UITableView) -> Int {
    if tableView == myTableView{
         return 1
    }else{
          return 2
    }
   
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if tableView == myTableView{
        if(shouldShowSearchResults){
            return filterArray.count
        } else {
            return composeArray.count
        }
         }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
           if tableView == myTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! composeTableview
        if(shouldShowSearchResults){
            cell.emaillabel.text = filterArray[indexPath.row].email
            cell.selectionbutton.tag = indexPath.row
             cell.selectionbutton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
           let  val = filterArray[indexPath.row].isChecked
            wichcell = 1
            
            if(val == true){
                cell.selectionbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.selectionbutton.tintColor = UIColor.red
                
            }else{
                
                cell.selectionbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.selectionbutton.tintColor = UIColor.green
            }
        } else {
              cell.emaillabel.text = composeArray[indexPath.row].email
                 cell.selectionbutton.tag = indexPath.row
             cell.selectionbutton.addTarget(self, action:#selector(self.moreView(_:)), for:.touchUpInside)
             let val = composeArray[indexPath.row].isChecked
             wichcell = 2
            if(val == true){
                cell.selectionbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.checkCircle, textColor: UIColor.red, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.selectionbutton.tintColor = UIColor.red
                
            }else{
                
                cell.selectionbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.circleO, textColor: UIColor.green, size: CGSize(width: 50, height: 50)), for: UIControlState.normal)
                cell.selectionbutton.tintColor = UIColor.green
            }
        }
        return cell
           }else{
            if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellone", for: indexPath) as! TagTableViewCell
                cell.selectionStyle  = .none
                  cell.tagsView.translatesAutoresizingMaskIntoConstraints = false
                self.view.addConstraint(NSLayoutConstraint(
                    item: self.view,
                    attribute: .leading,
                    relatedBy: .equal,
                    toItem: cell.tagsView,
                    attribute: .leading,
                    multiplier: 1,
                    constant: 0)
                )
               // cell.normalview.addSubview(tagsView)
                cell.tagsView.paddingLeftRight = 6
                cell.tagsView.paddingTopBottom = 4
                cell.tagsView.marginLeftRight = 15
                cell.tagsView.marginTopBottom = 4
                cell.tagsView.removeAll()
             //
                if first == 1{
             cellHeight = 60
                      cell.tagsView.tags = "click here"
                   first = 2
                }else{
                      cellHeight =  Double(30.0) + Double(cell.tagsView.height)
                      cell.tagsView.append(contentsOf:emailArray)
                }
              
              
            
              cell.tagsView.lastTag = "+"
                cell.tagsView.delegate = self
               
//                tagsField.frame = cell.tagview.bounds
//                cell.tagview.addSubview(tagsField)
//                tagsField.addSubview(cell.tagviewButton)
//                cell.tagviewButton.tag = indexPath.row
//                cell.tagviewButton.addTarget(self, action:#selector(self.tagView(_:)), for:.touchUpInside)
//                let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(labelTapped))
//                cell.tagview.addGestureRecognizer(gestureRecognizer)
           
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "celltwo", for: indexPath) as! commaessageTableViewCell
                cell.selectionStyle  = .none
                cell.separatorInset = .zero
              
                subject = cell.subjectTextfield.text!
                content = cell.contentgTextview.text
                return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == composeTable{
            if indexPath.section == 0{
                if cellHeight <= cellmaximumHeight{
                    print("%%%%%%%%%%%%%%cell%%%%%%%%%%%%%%")
                    print(cellHeight)
                    print("%%%%%%%%%%%%%cell%%%%%%%%%%%%%%%")
                    if first == 1{
                return CGFloat(cellHeight)
                    }else{
                          return CGFloat(cellHeight + 20)
                    }
                }else{
                    return 200.0
                }
            }
        return 200.0
        }else{
            return 44.0
        }
    }
    @objc func okView(_ sender: UIButton)
    {
        
        if(shouldShowSearchResults){
      //  tickValue = ""
        for i in 0..<filterArray.count{
             print("outside))*******************")
            if  filterArray[i].isChecked == true{
                     print("intsdfdfide))*******************")
                //tagsField.addTag(filterArray[i].email)
              //  emailButton.setTitle((emailButton.currentTitle)! + "," + filterArray[i].email, for: UIControlState())
                self.array.append(filterArray[i].email)
                self.makeTagsString()
                
            }else{
               // tagsField.removeTag(filterArray[i].email)
            }
        }
      
        }else{
            for i in 0..<composeArray.count{
               
                if  composeArray[i].isChecked == true{
                //    cell.tagsView.append(composeArray[i].email)
                        print("inside))*******************")
                   
                     //tagsField.addTag(composeArray[i].email)
                    // emailArray.append(composeArray[i].id)
                }else{
                   // tagsField.removeTag(composeArray[i].email)
                }
            }
        }
       
        addview.isHidden = true
        newview.isHidden = true
        self.myTableView.isHidden = true
    
  self.composeTable.reloadData()
    }
    @objc func moreView(_ sender: UIButton)
    {
        //tickvalueArray.removeAll()
         if(shouldShowSearchResults){
            if  filterArray[sender.tag].isChecked == false{
                filterArray[sender.tag].isChecked = true
            
            
            }else{
                filterArray[sender.tag].isChecked = false
            }
        
            }else{
            if  composeArray[sender.tag].isChecked == false{
                composeArray[sender.tag].isChecked = true
                
                
            }else{
                composeArray[sender.tag].isChecked = false
            }
        }
        myTableView.reloadData()
    }
    @objc func tagView(_ sender: UIButton)
        
    {
        //
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
    //    tagsField.resignFirstResponder()
        return true
    }
//    fileprivate func textFieldEventss() {
//                tagsField.onDidAddTag = { _, _ in
//                    print("DidAddTag")
//                    self.addview.isHidden = true
//                    self.newview.isHidden = true
//                    self.myTableView.isHidden = true
//
//                }
//
//                tagsField.onDidChangeText = { _, text in
//                    print("onDidChangeText")
//        }
//                tagsField.onDidChangeHeightTo = { _, height in
//                    print("HeightTo \(height)")
//                    self.cellHeight = Double(height)
//                     self.composeTable.reloadSections([0,0], with: UITableViewRowAnimation.none)
//                    self.mytableViewY = (Int(height/1))
//                    self.composeTable.reloadData()
//                }
//
//                tagsField.onDidSelectTagView = { _, tagView in
//                    print("Select \(tagView)")
//
//
//                }
//
//                tagsField.onDidUnselectTagView = { _, tagView in
//                    print("Unselect \(tagView)")
//                }
//            }
    
    @IBAction func sendAction(_ sender: UIButton) {
       
        print(composeArray.count)
          print(subject)
          print(content)
      
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = composeTable.cellForRow(at: indexPath) as! commaessageTableViewCell
        cell.subjectTextfield.resignFirstResponder()
          cell.contentgTextview.resignFirstResponder()
        let dic = NSMutableDictionary()
        if idArray.count == 0{
                print("empty tag")
             alertsnack(message: NSLocalizedString("Please select atleast one recipient", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (cell.subjectTextfield.text?.isEmpty)!{
            print("empty subject")
            alertsnack(message: NSLocalizedString("Subject required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (cell.contentgTextview.text?.isEmpty)!{
                    print("empty content")
             alertsnack(message: NSLocalizedString("Content required", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }else{
      
       activityView.startactvityIndicator()
          dic.setValue(idArray, forKey: "user_ids")
          dic.setValue(cell.subjectTextfield.text, forKey: "subject")
          dic.setValue(cell.contentgTextview.text, forKey: "message")
          dic.setValue(apikey, forKey: "apikey")
        do {
            
            
            
            //Convert to Data
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            // print(jsonData)
            
            //Convert back to string. Usually only do this for debugging
            
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                
                //ethaann nink veenda string
                
                // json aayit kittum
                print("***************************")
                print(JSONString)
                print("***************************")
                let scriptUrl = ComposeapiClass.SendMessages
                let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
                let myUrl = URL(string: urlWithParams);
                var request = URLRequest(url:myUrl!)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpBody = jsonData
                let task = URLSession.shared.dataTask(with: request)
                {
                    data, response, error in
                    
                    if error != nil
                    {
                        print("error=\(error)")
                        DispatchQueue.main.async
                            {
                                print("server down")
                        }
                        return
                    }
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("################################")
                    print(responseString)
                    do{
                        
                        if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                            
                        {
                            
                           self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                            print(self.status)
                            
                        }
                        DispatchQueue.main.async
                            {
                                
                                
                                if self.status == "success"{
                                    self.activityView.stopActivityIndicator()
                                    self.activityView.Successalert(SubTitle:NSLocalizedString("Message Sent", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                    self.dismiss(animated: true, completion: nil)
                                    
                                    
                                }else{
                                    self.activityView.stopActivityIndicator()
                                    self.activityView.Failurealert()
                                    self.dismiss(animated: true, completion: nil)
                                }
                                

                                
                                
                        }//dispatch ends
                        
                    }//do ends
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    
                }
                
                
                
                //  let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                // print("################################")
                //  print(responseString)
                
                //   }
                
                task.resume()
                
                
            }
            
        }
        }
    }
  
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    private func makeTagsString(){
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = composeTable.cellForRow(at: indexPath) as! TagTableViewCell
        cell.tagsLabel.text = cell.tagsView.tagTextArray
            .reduce("tags:\n", { (result, str) -> String in
                return "\(result)\(str),"
            })
    }
    func tagsTouchAction(_ tagsView: TagsView, tagButton: TagButton) {
      
       // self.createTableview()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "emaillist") as! composeListViewController
        vc.selectedArray = emailArray
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }
    func tagsChangeHeight(_ tagsView: TagsView, height: CGFloat) {
        let indexPath = IndexPath(row: 0, section: 0)
       // let cell = composeTable.cellForRow(at: indexPath) as! TagTableViewCell
          print("##########################################")
        print(height)
          print("##########################################")
    //height
        //cell.tagsView.frame.height = Int(height)
        //
        
    //    self.cellHeight = Double(height)
       // self.composeTable.reloadSections([0,0], with: UITableViewRowAnimation.none)
     //   self.mytableViewY = (Int(height/1))
      //  self.composeTable.reloadData()
        self.cellHeight = Double(height)
        self.composeTable.reloadSections([0,0], with: UITableViewRowAnimation.none)
       // self.mytableViewY = (Int(height/1))
     //   self.composeTable.reloadData()
        
    }
    func alertsnack(message: String){
        
        //  let snackbar = TTGSnackbar(message:message, duration: .middle)
        //  snackbar.backgroundColor = UIColor.black
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = "Done"
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    

}
extension UIView {
    func setBottomviewBorder() {
       // self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
extension messagecomposeViewController: TagsDelegate{
    
//
//    func tagsTouchAction(_ tagsView: TagsView, tagButton: TagButton) {
//        print("##########################################")
//
//                    let alertController = UIAlertController(title: nil, message: "Custom Tag", preferredStyle: .alert)
//                    alertController.addTextField(configurationHandler: { (textField) in })
//                    alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                    alertController.addAction(UIAlertAction(title: "Append", style: .default, handler: { (_) in
//                        guard let text = alertController.textFields?.first?.text, text.count != 0 else{
//                            return
//                        }
//                        /// Append
//                        let button = TagButton()
//                        button.setTitle(text, for: .normal)
//                        let options = ButtonOptions(
//                            layerColor: UIColor.brown,
//                            layerRadius: 10,
//                            layerWidth: 2,
//                            tagTitleColor: UIColor(white: 89/255, alpha: 1),
//                            tagFont: UIFont.boldSystemFont(ofSize: 15),
//                            tagBackgroundColor: UIColor.white)
//                        button.setEntity(options)
//                       tagsView.append(button)
//                        self.makeTagsString()
//                    }))
//                    self.present(alertController, animated: true, completion: nil)
//        /// Update & Delete ActionSheet UIAlertController
//        //        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        //        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        //
//        //        alertController.addAction(UIAlertAction(title: "Update", style: .default, handler: { (_) in
//        //
//        //            /// Update UIAlertController
//        //            let alertController = UIAlertController(title: nil, message: "Update Tag", preferredStyle: .alert)
//        //            alertController.addTextField(configurationHandler: { (textField) in
//        //                textField.text = tagButton.currentTitle
//        //            })
//        //            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        //            alertController.addAction(UIAlertAction(title: "Update", style: .default, handler: { (_) in
//        //                guard let text = alertController.textFields?.first?.text, text.count != 0 else{
//        //                    return
//        //                }
//        //                /// Update
//        //                self.tagView.update(text, at: tagButton.index)
//        //                self.makeTagsString()
//        //            }))
//        //            self.present(alertController, animated: true, completion: nil)
//        //
//        //        }))
//        //
//        //
//        //        alertController.addAction(UIAlertAction(title: "PrevInsert", style: .default, handler: { (_) in
//        //            /// Prev Insert UIAlertController
//        //            let alertController = UIAlertController(title: nil, message: "Prev Insert Tag", preferredStyle: .alert)
//        //            alertController.addTextField(configurationHandler: { (textField) in })
//        //            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        //            alertController.addAction(UIAlertAction(title: "Insert", style: .default, handler: { (_) in
//        //                guard let text = alertController.textFields?.first?.text, text.count != 0 else{
//        //                    return
//        //                }
//        //                /// Insert
//        //                self.tagView.insert(text, at: tagButton.index)
//        //                self.makeTagsString()
//        //            }))
//        //            self.present(alertController, animated: true, completion: nil)
//        //        }))
//        //
//        //
//        //        alertController.addAction(UIAlertAction(title: "NextInsert", style: .default, handler: { (_) in
//        //            /// Next Insert UIAlertController
//        //            let alertController = UIAlertController(title: nil, message: "Next Insert Tag", preferredStyle: .alert)
//        //            alertController.addTextField(configurationHandler: { (textField) in })
//        //            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        //            alertController.addAction(UIAlertAction(title: "Insert", style: .default, handler: { (_) in
//        //                guard let text = alertController.textFields?.first?.text, text.count != 0 else{
//        //                    return
//        //                }
//        //                /// Insert
//        //                self.tagView.insert(text, at: tagButton.index + 1)
//        //                self.makeTagsString()
//        //            }))
//        //            self.present(alertController, animated: true, completion: nil)
//        //        }))
//        //
//        //
//        //        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
//        //            /// Remove
//        //            self.tagView.remove(tagButton)
//        //            self.makeTagsString()
//        //        }))
//        //
//        //        self.present(alertController, animated: true, completion: nil)
//    }
    
    // Last Tag Touch Action
    func tagsLastTagAction(_ tagsView: TagsView, tagButton: TagButton) {
        
    }
    
    // TagsView Change Height
   
}
