//
//  sentChatViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/10/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import SDWebImage
import FontAwesome_swift
class sentChatViewController: UIViewController {
    var toname = ""
    var dateTime  = ""
    var subject = ""
    var content = ""
    var completecontent = ""
      var photo = ""
    var label = UILabel()
    
    
    
  
    
    @IBOutlet weak var navigationview: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var myDetails: UILabel!
    @IBOutlet weak var student_Image: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title =  NSLocalizedString("Send Message", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        SetBackBarButtonCustom()
       navigationview.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
            toLabel.text =  toname
       student_Image.layer.cornerRadius = student_Image.frame.size.width/2
       student_Image.clipsToBounds = true
        student_Image.image = UIImage(named:"placeholder")
        let full_name = UserDefaults.standard.string(forKey: "name")!
       
            let name = full_name + "(" + "Me" + ")" + "," + dateTime
            myDetails.numberOfLines = 0
            myDetails.lineBreakMode = .byWordWrapping
            myDetails.text = name
     
        let completesubject = "Subject" + ":" + subject
        self.completecontent = completesubject + "\n" + " Content" + ":" + content
        contentLabel.numberOfLines = 0
        contentLabel.lineBreakMode = .byWordWrapping
       // contentLabel.layer.shadowColor = UIColor.black.cgColor
       // contentLabel.layer.shadowRadius = 0.5
       // contentLabel.layer.shadowOpacity = 0.5
        
       contentLabel.text = self.completecontent
        contentLabel.layer.cornerRadius = 5.0
        contentLabel.layer.masksToBounds = true
        
    }
  
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}



