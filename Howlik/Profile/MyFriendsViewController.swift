//
//  findfriendsViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/15/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import SCLAlertView

class MyFriendsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var friendsTableview: UITableView!
    var apikey = ""
    var status = ""
    var user_id = ""
    var sendMessageView = sendMessageview()
    var myFriendsArray = [myFriendsClass]()
    var useridArray = [String]()
var buisnessapiClass = ApiListViewcontroller()
    let activityview = emailValidateViewController()
       var customview = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
     
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            self.user_id = UserDefaults.standard.string(forKey: "senderid")!
            
           FriendlistapiCalling(apikey:apikey,user_id:user_id)
        }
        SetBackBarButtonCustom()
        self.navigationItem.title = NSLocalizedString("My Friends", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        // Do any additional setup after loading the view.
           self.friendsTableview.isHidden = true
        
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func FriendlistapiCalling(apikey:String,user_id:String){
         self.activityview.startactvityIndicator()
        myFriendsArray.removeAll()
        let scriptUrl = buisnessapiClass.FriendsList
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&user_id=\(user_id)"
     
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
               
                    if (convertedJsonIntoArray["friend"] as? NSArray) != nil {
                        let friend  = convertedJsonIntoArray["friend"] as! NSArray
                      
                       
                        for singlefriend in  (friend as! [[String:Any]]){
                            let myfriendElement = myFriendsClass()
                                self.friendsTableview.isHidden = false
                            let contry1 = singlefriend as NSDictionary
                            if (contry1["country"] as? String) != nil {
                                let country = contry1["country"] as! String
                                myfriendElement.country = country
                            }
                            if (contry1["name"] as? String) != nil {
                                let name = contry1["name"] as! String
                                myfriendElement.name = name
                                
                            }
                            if (contry1["photo"] as? String) != nil {
                                let photo = contry1["photo"] as! String
                                myfriendElement.photo = photo
                            }
                            if (contry1["friend_id"] as? String) != nil {
                                let friend_id = contry1["friend_id"] as! String
                                myfriendElement.friend_id = friend_id
                            }
                            self.myFriendsArray.append(myfriendElement)
                            }
                       
                    }
                }
                
                
                DispatchQueue.main.async
                    {
                        self.activityview.stopActivityIndicator()
                        if self.myFriendsArray.count == 0{
                                self.friendsTableview.isHidden = true
                                self.createeventNothingview()
                            
                        }else{
                            self.friendsTableview.isHidden = false
                            self.friendsTableview.reloadData()
                        }
                       
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    func createeventNothingview(){
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myFriendsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! myFriendsTableViewCell
        cell.selectionStyle = .none
        cell.name.text = myFriendsArray[indexPath.row].name
        cell.country.text = myFriendsArray[indexPath.row].country
        cell.messageOutlet.tag = indexPath.row;
        cell.photo.sd_setImage(with: URL(string:myFriendsArray[indexPath.row].photo), placeholderImage:
            UIImage(named: "no-image02"))
        cell.photo.layer.cornerRadius = cell.photo.frame.size.width/2
        cell.photo.clipsToBounds = true
        cell.messageOutlet.addTarget(self, action:#selector(self.messageActionView(_:)), for:.touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    @objc func messageActionView(_ sender: UIButton)
    {
        createTableview()
    }
    func createTableview(){
        
        sendMessageView  = sendMessageview(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
        self.view.addSubview(sendMessageView)
        sendMessageView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        sendMessageView.lastview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
       // sendMessageView.Contentview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        sendMessageView.contentLabel.layer.borderWidth = 0.5
        sendMessageView.contentLabel.layer.cornerRadius = 5
        sendMessageView.contentLabel.layer.borderColor = UIColor.gray.cgColor
        sendMessageView.contentLabel.layer.masksToBounds = false
        sendMessageView.subjectTextfield.setBorder()
        
      //  sendMessageView.tolabel.text = Toname
        
        sendMessageView.okOutet.addTarget(self, action: #selector(sendbuttonAction), for: .touchUpInside)
        sendMessageView.cancelOut.addTarget(self, action: #selector(cancelbuttonAction), for: .touchUpInside)
        // rplyView.backgroundColor = UIColor.red
    }
    @objc  func sendbuttonAction(sender: UIButton!) {
        useridArray.removeAll()
        useridArray.append(myFriendsArray[sender.tag].friend_id)
       
        
        let dic = NSMutableDictionary()
        dic.setValue(useridArray, forKey: "user_ids")
        dic.setValue(sendMessageView.subjectTextfield.text, forKey: "subject")
        dic.setValue(sendMessageView.contentLabel.text, forKey: "message")
        dic.setValue(apikey, forKey: "apikey")
        do {
            
            
            
            //Convert to Data
            
            let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            // print(jsonData)
            
            //Convert back to string. Usually only do this for debugging
            
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                
                //ethaann nink veenda string
                
                // json aayit kittum
              
                let scriptUrl = buisnessapiClass.SendMessages
                let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
                let myUrl = URL(string: urlWithParams);
                var request = URLRequest(url:myUrl!)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpBody = jsonData
                let task = URLSession.shared.dataTask(with: request)
                {
                    data, response, error in
                    
                    if error != nil
                    {
                        print("error=\(error)")
                        DispatchQueue.main.async
                            {
                                print("server down")
                        }
                        return
                    }
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    do{
                        
                        if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                            
                        {
                            
                            self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                         
                            
                        }
                        DispatchQueue.main.async
                            {
                                
                                
                                 if self.status == "success"{
                                    let appearance = SCLAlertView.SCLAppearance(
                                        showCloseButton: false // hide default button
                                    )
                                    let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                                    alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                        self.resignFirstResponder()
                                        self.dismiss(animated: true, completion: nil)
                                    })
                                    alert.showSuccess(NSLocalizedString("SUCCESS", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Message Sent", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                    
                                 }else{
                                    let appearance = SCLAlertView.SCLAppearance(
                                        showCloseButton: false // hide default button
                                    )
                                    let alert = SCLAlertView(appearance: appearance) // create alert with appearance
                                    alert.addButton(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), action: { // create button on alert
                                        
                                    })
                                    alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("OopsSomethingwentwrong.Tryagain!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                }
                                
                                
                                
                                
                        }//dispatch ends
                        
                    }//do ends
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    
                }
                
                
                
              
                
                task.resume()
                
                
            }
            
        }
        
   
}
    @objc  func cancelbuttonAction(sender: UIButton!) {
        sendMessageView.isHidden = true
     
    }
        
}
