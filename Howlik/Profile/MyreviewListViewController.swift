//
//  reviewListViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/15/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift

class MyreviewListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var reviewTable: UITableView!
    let reviewApiclass = ApiListViewcontroller()
    var reviewArray = [myreviewClass]()
    var apikey = ""
    var img_url = ""
    var customview = UIView()
      let activityview = emailValidateViewController()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            ReviewListloadapiCalling(apikey:apikey)
        }
        SetBackBarButtonCustom()
     
        self.navigationItem.title =  NSLocalizedString("Reviews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func ReviewListloadapiCalling(apikey:String){
        activityview.startactvityIndicator()
       reviewArray.removeAll()
        //selectedId.removeAll()
        let scriptUrl = reviewApiclass.UserMadeReviewListing
        
        let parameters: Parameters = ["apikey": apikey]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
               
                        if response["img_url"] != nil{
                        self.img_url = response.object(forKey: "img_url")! as! String
                        }
                          if response["reviews"] != nil{
                        let reviews = response.object(forKey: "reviews")!
                        //if (reviews as AnyObject).count == 0 {
                            self.reviewTable.isHidden = false
                        for singlereviews in (reviews as! [[String:Any]]){
                             let singlereviewsElement = myreviewClass()
                            let contry = singlereviews as NSDictionary
                            if  (contry["created_at"] as? String) != nil {
                                let created_at = contry["created_at"] as! String
                                singlereviewsElement.created_at = created_at
                                
                            }
                            if  (contry["review"] as? String) != nil {
                                let review = contry["review"] as! String
                                singlereviewsElement.review = review
                            }
                            if  (contry["user_name"] as? String) != nil {
                                let user_name = contry["user_name"] as! String
                                singlereviewsElement.user_name = user_name
                           
                            }
                            self.reviewArray.append(singlereviewsElement)
                        }
                        
                          }else{
                            self.reviewTable.isHidden = true
                            self.createeventNothingview()
                        }
                        
                        DispatchQueue.main.async
                            {
                                
                               self.reviewTable.reloadData()
                                self.activityview.stopActivityIndicator()
                               
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createeventNothingview(){
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! myreviewTableViewCell
        cell.selectionStyle = .none
        cell.name.text = reviewArray[indexPath.row].user_name
        cell.dateLabel.text = reviewArray[indexPath.row].created_at
        cell.reviewLabel.text = reviewArray[indexPath.row].review
        cell.photo.sd_setImage(with: URL(string:self.img_url), placeholderImage:
            UIImage(named: "no-image02"))
        cell.photo.layer.cornerRadius = cell.photo.frame.size.width/2
        cell.photo.clipsToBounds = true
   
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }

}
