//
//  SettingsView.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/16/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class SettingsView: UIView {
    @IBOutlet var newPasswordTextfield: UITextField!
    
    @IBOutlet var changepasswordLabel: UILabel!
    @IBOutlet var oldPasswordtextfield: UITextField!
    
    @IBOutlet var confirmpasswordTextfield: UITextField!
    
    @IBOutlet var cancelOutlet: UIButton!
    
    @IBOutlet var lastView: UIView!
    @IBOutlet var okOutlet: UIButton!
    override init(frame: CGRect){ // for using custom viw in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
        
        commonInit()
    }
    private func commonInit(){
        //  we are going to do stuff here
        //        Bundle.main.loadNibNamed("replyView", owner: self, options: nil)
        //        addSubview(replyView)
        //        replyView.frame = self.bounds
        //        replyView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        let nibView = Bundle.main.loadNibNamed("SettingsView", owner: self, options: nil)![0] as! UIView
        nibView.frame = self.bounds;
        self.addSubview(nibView)
    }
    
}
