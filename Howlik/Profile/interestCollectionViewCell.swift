//
//  interestCollectionViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class interestCollectionViewCell: UICollectionViewCell {
    @IBOutlet var intTitleLabel: UILabel!
    
    @IBOutlet var intImageLabel: UIImageView!
}
