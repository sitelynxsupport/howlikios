//
//  myActivityTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class myActivityTableViewCell: UITableViewCell {
    @IBOutlet var imagview: UIImageView!
    
    @IBOutlet var activityNameLabel: UILabel!
    
    @IBOutlet var ActivityImageLabel: UIImageView!
    @IBOutlet var activityDescriptionLabel: UILabel!
    @IBOutlet var activityDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
