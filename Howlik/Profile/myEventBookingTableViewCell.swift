//
//  myEventBookingTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/16/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class myEventBookingTableViewCell: UITableViewCell {
    @IBOutlet var eventStatusLabel: UILabel!
    
    @IBOutlet var myview: UIView!
    @IBOutlet var eventTicketsLabel: UILabel!
    @IBOutlet var eventDateTimeLabel: UILabel!
    
    @IBOutlet var eventNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
