//
//  myFriendsTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/15/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class myFriendsTableViewCell: UITableViewCell {

    @IBOutlet var country: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var photo: UIImageView!
    
    @IBOutlet var messageOutlet: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
