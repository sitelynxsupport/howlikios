//
//  interestViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Alamofire
import SDWebImage
class myInterestViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var apikey = ""
    var language_code = ""
    let Apiclass = ApiListViewcontroller()
    var myInterestArray = [interestClass]()
     let activityview = emailValidateViewController()
      var customview = UIView()
    
    @IBOutlet var interestCollectionview: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            self.language_code = UserDefaults.standard.string(forKey: "language_code")!
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            InterestloadapiCalling(apikey:apikey,lang:language_code)
        }
    
        SetBackBarButtonCustom()
        self.navigationItem.title = NSLocalizedString("My Interests", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)

    }
    func InterestloadapiCalling(apikey:String,lang:String){
        activityview.startactvityIndicator()
        self.myInterestArray.removeAll()
        let scriptUrl = Apiclass.InterestList
        
        let parameters: Parameters = ["apikey": apikey,"lang": lang]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                     
                        if response["user_interest"] != nil{
                            self.interestCollectionview.isHidden = false
                        
                        let user_interest = response.object(forKey: "user_interest")!
                     for singleuser_interest in (user_interest as! [[String:Any]]){
                            let singleInterestElement = interestClass()
                            let contry = singleuser_interest as NSDictionary
                            if  (contry["int_title"] as? String) != nil {
                                let int_title = contry["int_title"] as! String
                                singleInterestElement.int_title = int_title
                                
                            }
                        
                            if  (contry["int_img"] as? String) != nil {
                                let int_img = contry["int_img"] as! String
                                singleInterestElement.int_img = "https://www.howlik.com/" + int_img
                                
                            }
                            self.myInterestArray.append(singleInterestElement)
                        }
                        }else{
                            self.interestCollectionview.isHidden = true
                            self.createeventNothingview()
                        }
                        
                        DispatchQueue.main.async
                            {
                                self.interestCollectionview.reloadData()
                                self.activityview.stopActivityIndicator()
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createeventNothingview(){
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myInterestArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! interestCollectionViewCell
     
        cell.intImageLabel.sd_setImage(with: URL(string:myInterestArray[indexPath.row].int_img),
                                   placeholderImage:UIImage(named: "no-image02"))
        cell.intImageLabel.layer.cornerRadius = cell.intImageLabel.frame.size.width/2
        cell.intImageLabel.clipsToBounds = true
        cell.intTitleLabel.text = myInterestArray[indexPath.row].int_title
      
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        
        return 5.0
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 12 * 4) / 3//some width
        let height = (width * 3.6 / 2.5  )//ratio
        return CGSize(width: width, height: height);
    }

}
