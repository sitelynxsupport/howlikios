//
//  myactivityViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Alamofire
import SDWebImage
class myactivityViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
@IBOutlet var myactivityTable: UITableView!
     var apikey = ""
    let Apiclass = ApiListViewcontroller()
    var myActivityArray = [myActivityClass]()
     let activityview = emailValidateViewController()
      var customview = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
            ActivityloadapiCalling(apikey:apikey)
        }
        myactivityTable.estimatedRowHeight = 44.0
        myactivityTable.rowHeight = UITableViewAutomaticDimension
        SetBackBarButtonCustom()
        self.navigationItem.title = NSLocalizedString("My Activities", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func ActivityloadapiCalling(apikey:String){
       activityview.startactvityIndicator()
      self.myActivityArray.removeAll()
     
        let scriptUrl = Apiclass.ActivityList
        
        let parameters: Parameters = ["apikey": apikey]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                       
                        
                        if response["activity"] != nil{
                             self.myactivityTable.isHidden = false
                        let activity = response.object(forKey: "activity")!
                        for singleactivity in (activity as! [[String:Any]]){
                            let singleActivityElement = myActivityClass()
                            let contry = singleactivity as NSDictionary
                            if  (contry["event_name"] as? String) != nil {
                                let event_name = contry["event_name"] as! String
                                singleActivityElement.event_name = event_name
                                
                            }
                            if  (contry["created_at"] as? String) != nil {
                                let created_at = contry["created_at"] as! String
                                singleActivityElement.created_at = created_at
                                
                            }
                          
                            if  (contry["about_event"] as? String) != nil {
                                let about_event = contry["about_event"] as! String
                                singleActivityElement.about_event = about_event
                                
                            }
                            if  (contry["event_image"] as? String) != nil {
                                let event_image = contry["event_image"] as! String
                                singleActivityElement.event_image = event_image
                                
                            }
                            if  (contry["event_image1"] as? String) != nil {
                                let event_image1 = contry["event_image1"] as! String
                                singleActivityElement.event_image1 = "https://www.howlik.com/" + event_image1
                                
                            }
                            self.myActivityArray.append(singleActivityElement)
                        }
                        
                        }else{
                            self.myactivityTable.isHidden = true
                            self.createeventNothingview()
                        }
                        
                        DispatchQueue.main.async
                            {
                                self.myactivityTable.reloadData()
                                self.activityview.stopActivityIndicator()
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createeventNothingview(){
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myActivityArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! myActivityTableViewCell
        cell.imagview.sd_setImage(with: URL(string:myActivityArray[indexPath.row].event_image), placeholderImage:
            UIImage(named: "no-image02"))
        cell.imagview.layer.cornerRadius = cell.imagview.frame.size.width/2
        cell.imagview.clipsToBounds = true
        cell.activityNameLabel.text = myActivityArray[indexPath.row].event_name
        cell.activityDateLabel.text = myActivityArray[indexPath.row].created_at
              cell.activityDescriptionLabel.text = myActivityArray[indexPath.row].about_event
        cell.ActivityImageLabel.sd_setImage(with: URL(string:myActivityArray[indexPath.row].event_image1), placeholderImage:
            UIImage(named: "no-image02"))
        cell.selectionStyle = .none
        cell.separatorInset = .zero
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  

}
