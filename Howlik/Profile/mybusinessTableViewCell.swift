//
//  mybusinessTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/18/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class mybusinessTableViewCell: UITableViewCell {

    @IBOutlet var mainview: UIView!
    @IBOutlet var bookingtypelabel: UILabel!
    @IBOutlet var namelabel: UILabel!
    
    @IBOutlet var approvedImage: UIImageView!
    @IBOutlet var approvedbutton: UIButton!
    @IBOutlet var bookingdate: UILabel!
    @IBOutlet var bookingtime: UILabel!
    @IBOutlet var peoplecount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.mainview.backgroundColor = UIColor.clear
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
