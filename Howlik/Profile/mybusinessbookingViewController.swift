//
//  mybusinessbookingViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/18/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Alamofire

class mybusinessbookingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var bookingtable: UITableView!
    var apikey = ""
var language_code = ""
      var customview = UIView()
let buisnessapiClass = ApiListViewcontroller()
    var bookingArray = [otherbusnessbookingclass]()
       let activityview = emailValidateViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        SetBackBarButtonCustom()
        self.navigationItem.title = NSLocalizedString("MybusinessBooking", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        bookingtable.separatorInset = .zero
      //  self.bookingtable.separatorStyle = UITableViewCellSeparatorStyle.none
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
      //  SetBackBarButtonCustom()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
                  self.language_code = UserDefaults.standard.string(forKey: "language_code")!
             BookeventloadapiCalling(apikey:apikey,language_code:language_code)
        }
        // Do any additional setup after loading the view.
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookingArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! mybusinessTableViewCell
        cell.selectionStyle = .none
        cell.separatorInset = .zero
      


        cell.namelabel.text = bookingArray[indexPath.row].title
        cell.bookingtypelabel.text = bookingArray[indexPath.row].type_name
        cell.peoplecount.text =  bookingArray[indexPath.row].sr_people + "People"
        cell.bookingtime.text = bookingArray[indexPath.row].sr_time
        cell.bookingdate.text = bookingArray[indexPath.row].sr_date
       //approved =0 means waiting
        //approved =1 means accepted
        //approved =1 means rejected
          cell.approvedImage.layer.cornerRadius = (  cell.approvedImage.frame.width / 2) //instead of let radius = CGRectGetWidth(self.frame) / 2
         cell.approvedImage.layer.masksToBounds = true
         cell.approvedbutton.layer.cornerRadius = 10.0
        if  bookingArray[indexPath.row].approved == "0"{
            cell.approvedbutton.setTitle(NSLocalizedString("WAITING", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
            cell.approvedbutton.setTitleColor(UIColor(red: 224/255.0, green: 147/255.0, blue: 8/255.0, alpha: 1.0), for: UIControlState())
            cell.approvedImage.image = UIImage(named :"book_waiting" )
          
        }
        else if  bookingArray[indexPath.row].approved == "1"{
            cell.approvedbutton.setTitle(NSLocalizedString("ACCEPTED", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
             cell.approvedbutton.setTitleColor(UIColor.green, for: UIControlState())
              cell.approvedImage.image = UIImage(named :"book_accept" )
        }else{
            cell.approvedbutton.setTitle(NSLocalizedString("DECLINED", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
               cell.approvedbutton.setTitleColor(UIColor.red, for: UIControlState())
               cell.approvedImage.image = UIImage(named :"book_rejected" )
            
            
        }
                cell.mainview.layer.shadowColor = UIColor.gray.cgColor
                cell.mainview.layer.shadowOpacity = 1
                cell.mainview.layer.shadowOffset = CGSize.zero
                cell.mainview.layer.shadowRadius = 2
                cell.mainview.backgroundColor = UIColor.groupTableViewBackground
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    func BookeventloadapiCalling(apikey:String,language_code:String){
       activityview.startactvityIndicator()
        bookingArray.removeAll()
        let scriptUrl = buisnessapiClass.OtherBusinessBookings
        
        let parameters: Parameters = ["apikey": apikey,"language_code":language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                      
                        if (response["booking"]) != nil {
                     let booking = response.object(forKey: "booking")!
                        if (booking as AnyObject).count == 0{

                            self.bookingtable.isHidden = true
                            self.createeventNothingview()
                        }else{
                        for book in (booking as! [[String:Any]]){
                             self.bookingtable.isHidden = false
                           let bookElement = otherbusnessbookingclass()
                            let contry3 = book as NSDictionary
                            if  (contry3["title"] as? String) != nil {
                                let name = contry3["title"] as! String
                               bookElement.title = name
                                
                            }
                            if  (contry3["type_name"] as? String) != nil {
                                let type_name = contry3["type_name"] as! String
                              bookElement.sr_date = type_name
                            }
                            if  (contry3["approved"] as? String) != nil {
                                let approved = contry3["approved"] as! String
                                bookElement.approved = approved
                            }
                            if  (contry3["extraInfo"] as? NSDictionary) != nil {
                                let extraInfo = contry3["extraInfo"] as! NSDictionary
                                
                               
                                 let sr_people = extraInfo["sr_people"] as! String
                                bookElement.sr_people = sr_people

                               
                                 let sr_time = extraInfo["sr_time"] as! String
                                  bookElement.sr_time = sr_time
                                
                                 let sr_date = extraInfo["sr_date"] as! String
                                bookElement.sr_date = sr_date
                                
                            
                            }
                            
                            if  (contry3["book_type"] as? String) != nil {
                            let type_name = contry3["type_name"] as! String
                                bookElement.type_name = type_name

                            }
                            if  (contry3["approved"] as? String) != nil {
                                let approved = contry3["approved"] as! String
                               bookElement.approved = approved
                            }
                          self.bookingArray.append(bookElement)
                        }
                        
                        }
                        }
                        
                        DispatchQueue.main.async
                            {
                             
                                self.bookingtable.reloadData()
                                self.bookingtable.separatorInset = .zero
                                self.activityview.stopActivityIndicator()
                              
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createeventNothingview(){
        let screenSize: CGRect = UIScreen.main.bounds
        customview  = UIView(frame: CGRect(x: 0, y: 140, width: Int(screenSize.width) , height: Int(screenSize.height) ))
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:140,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }

    
}
