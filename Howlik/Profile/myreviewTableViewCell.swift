//
//  myreviewTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/16/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class myreviewTableViewCell: UITableViewCell {
    @IBOutlet var reviewLabel: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var photo: UIImageView!
    
    @IBOutlet var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
