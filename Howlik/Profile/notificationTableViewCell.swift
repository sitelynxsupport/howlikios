//
//  notificationTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/17/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class notificationTableViewCell: UITableViewCell {
    
    @IBOutlet var myview: UIView!
    
    @IBOutlet var notificationDescriptionLabel: UILabel!
    @IBOutlet var notifictiontitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
