//
//  profTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/8/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class profTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var mainview: UIView!
    
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
