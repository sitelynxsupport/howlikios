//
//  profdetEditViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/2/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import FontAwesome_swift
import DLRadioButton
import Alamofire
import NVActivityIndicatorView

class profdetEditViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
    
    @IBOutlet var updateOutlet: UIButton!
    @IBOutlet var aboutLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var GenderLabel: UILabel!
    @IBOutlet var countryOutletbutton: UIButton!
    @IBOutlet var femaleOutlet: DLRadioButton!
    @IBOutlet var maleOutlet: DLRadioButton!
    var name = ""
    var email = ""
    var phone = ""
    var countryname = ""
    var countrycode = ""
    let profileClass = ApiListViewcontroller()
     let activityview = emailValidateViewController()
    var apikey = ""
    var language_code = ""
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    var gender_id = ""
    var status = ""
    var finalcode = ""
    var swiftArray = [String]()
    var swiftcodeArray = [String]()
      var flagImage = ["united_arab_emirates","bahrain","india","kuwait","oman","qatar","saudi_arabia"]
    var Gender = Int()
  
    @IBOutlet var aboutOutlet: UITextField!
    @IBOutlet var emailOutlet: UITextField!
    @IBOutlet var phoneOutlet: UITextField!
    @IBOutlet var countryOutlet: UITextField!
    @IBOutlet var nameoutlet: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("EditProfile", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        SetBackBarButtonCustom()
       

        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
              LoadprofileapiCalling(apikey:apikey,language_code:language_code)
        }
      
         aboutOutlet.setBorder()
        emailOutlet.setBorder()
        phoneOutlet.setBorder()
        countryOutlet.setBorder()
        nameoutlet.setBorder()
        aboutOutlet.setLeftPaddingPoints(5.0)
        emailOutlet.setLeftPaddingPoints(5.0)
        phoneOutlet.setLeftPaddingPoints(5.0)
        nameoutlet.setLeftPaddingPoints(5.0)
        aboutOutlet.setLeftPaddingPoints(5.0)
         countryOutletbutton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        setLanguage()
        if  UserDefaults.standard.string(forKey:"language_code") == "en"{
            countryOutletbutton.contentHorizontalAlignment = .left
        }else{
            countryOutletbutton.contentHorizontalAlignment = .right
        }
        // Do any additional setup after loading the view.
    }
    func setLanguage(){
        GenderLabel.text =  NSLocalizedString("Gender", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        nameLabel.text =  NSLocalizedString("Name", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        countryLabel.text = NSLocalizedString("Country", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        phoneLabel.text =  NSLocalizedString("Phone", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        emailLabel.text =  NSLocalizedString("Email", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        aboutLabel.text =  NSLocalizedString("About", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        updateOutlet.setTitle( NSLocalizedString("Update", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        maleOutlet.setTitle(NSLocalizedString("Male", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        femaleOutlet.setTitle(NSLocalizedString("Female", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        //  btnLeftMenu.setImage(UIImage(named: "arrow-3"), for: UIControlState())
        btnLeftMenu.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.chevronLeft, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
        btnLeftMenu.addTarget(self, action: #selector(onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //  btnLeftMenu.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func onClcikBack()
    {
        dismiss(animated: true, completion: nil)
        
    }
    func LoadprofileapiCalling(apikey:String,language_code:String){
     self.activityview.startactvityIndicator()
        let scriptUrl = profileClass.LoadProfile
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code]
     
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                       
                     
                        let profiles = response.object(forKey: "profile")!
  
                      
                        let contry1 = profiles as! NSDictionary
                        if  (contry1["name"] as? String) != nil {
                            self.name = contry1["name"] as! String
                            
                        }else{
                            self.name = "nil"
                        }
                        
                        if  (contry1["phone"] as? String) != nil {
                            self.phone = contry1["phone"] as! String
                            
                            
                        }else{
                            self.phone = "nil"
                            
                            
                        }
                        if  (contry1["email"] as? String) != nil {
                            self.email = contry1["email"] as! String
                            
                        }
                        else{
                            self.email = "nil"
                            
                        }
                        
                        if  (contry1["country_code"] as? String) != nil {
                            self.countrycode = contry1["country_code"] as! String
                         
                           
                        }
                        
                        
                        if  (contry1["gender_id"] as? String) != nil {
                            self.gender_id = contry1["gender_id"] as! String
                            
                        }
                        
                          let countries = response.object(forKey: "countries")!
                        
                        for country in (countries as! [[String :Any]]){
                            if  (country["code"] as? String) != nil {
                                let code = country["code"] as! String
                                self.swiftcodeArray.append(code)
                                if code == self.countrycode{
                                       self.finalcode = code
                                    self.countryname = country["title"] as! String
                                   
                                }
                            }
                            if  (country["title"] as? String) != nil {
                                let title = country["title"] as! String
                                self.swiftArray.append(title)
                                
                            }
                        }
                     
                        
                        DispatchQueue.main.async
                            {
                               
                                self.nameoutlet?.text = self.name
                                self.emailOutlet?.text = self.email
                                if(self.emailOutlet.hasText){
                                self.emailOutlet.isUserInteractionEnabled = false
                                }
                                self.phoneOutlet?.text = self.phone
                                self.countryOutletbutton.setTitle(self.countryname, for: UIControlState())
                                if self.gender_id == "1"{
                                        self.maleOutlet.isSelected = true
                                    self.maleOutlet.indicatorColor = UIColor.red
                                    self.maleOutlet.iconColor = UIColor.red
                                    self.Gender = 1
                                   
                                }else{
                                     self.femaleOutlet.isSelected = true
                                     self.femaleOutlet.indicatorColor = UIColor.red
                                    self.femaleOutlet.iconColor = UIColor.red
                                   self.Gender = 2
                                }
                              self.activityview.stopActivityIndicator()
                                self.myTableView.reloadData()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func UpdateprofileapiCalling(apikey:String,country:String,gender:Int,phone:String,email:String,name:String){
         self.activityview.startactvityIndicator()
        let scriptUrl = profileClass.PostProfile
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&country=\(country)&gender=\(gender)&phone=\(phone)&email=\(email)&name=\(name)"
      
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                  
                    if self.status == "success"{
                     
                    }
                }
                
                
                DispatchQueue.main.async
                    {
                        if (self.status == "success") {
                            self.activityview.stopActivityIndicator()
                             self.activityview.Successalert(SubTitle: NSLocalizedString("Updated", bundle:LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                             self.dismiss(animated: true, completion: nil)
                           
                         
                        }else{
                             self.activityview.stopActivityIndicator()
                            self.activityview.Failurealert()
                        }
                        
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
 func createTableview(){
     let screenSize: CGRect = UIScreen.main.bounds
    let window = UIApplication.shared.keyWindow!
    addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
    window.addSubview(addview)
    
    addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
     self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
     self.addview.addSubview(newview)
    newview.center = view.center
     //  self.newview.backgroundColor = UIColor.blue
     self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
     myTableView.dataSource = self
     myTableView.delegate = self
     let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
     let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
     view1.addSubview(label)
     label.textColor = UIColor.black
     label.isHighlighted = true
     label.font = UIFont.boldSystemFont(ofSize: 20)
     self.myTableView.tableHeaderView = view1
     // if buttonIndex == 0{
     label.text = " Select Country"
     myTableView.register(UINib(nibName: "profilecountryTableViewCell", bundle: nil), forCellReuseIdentifier: "myIdentifier")
     newview.addSubview(myTableView)
     self.myTableView.alwaysBounceVertical = false
     newview.isHidden =  true
     myTableView.isHidden =  true
     newview.layer.cornerRadius = 10;
     newview.layer.masksToBounds = true
     view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
     view1.layer.masksToBounds = true
    let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 65, y:newview.frame.height - 45,width: 60  , height:  40));
    newview.addSubview(button);
  
    button.layer.cornerRadius = 3.0
    button.setTitle("Done", for: UIControlState.normal)
    button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
    button.setTitleColor(UIColor.black, for: UIControlState.normal)
    button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
    label.textColor = UIColor.black
    newview.backgroundColor = UIColor.white
    label.isHighlighted = true
    label.font = UIFont.boldSystemFont(ofSize: 20)
     }
    @objc func okView(_ sender: UIButton)
    {
        
        
        newview.isHidden = true
        addview.isHidden = true
        myTableView.isHidden = true
        //keywordoutlet.setTitle(tickValue, for: UIControlState())
      //  newview.isHidden = true
        
    }
     @IBAction func maleAction(_ sender: Any) {
        maleOutlet.isSelected = true
        maleOutlet.iconColor = UIColor.red
        maleOutlet.indicatorColor = UIColor.red
        Gender = 1
        
     }
 
    @IBAction func femaleAction(_ sender: Any) {
        femaleOutlet.isSelected = true
        femaleOutlet.iconColor = UIColor.red
        femaleOutlet.indicatorColor = UIColor.red
        Gender = 2
        
    }
    
    @IBAction func countryAction(_ sender: Any) {
      createTableview()
        addview.isHidden = false
        newview.isHidden = false
        myTableView.isHidden = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
            return swiftArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! profilecountryTableViewCell
            cell.selectionStyle = .none
            cell.countryname.text = swiftArray[indexPath.row]
        cell.countryflag.image = UIImage(named:flagImage[indexPath.row])
    
            return cell
            
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryOutletbutton.setTitle(swiftArray[indexPath.row], for: UIControlState())
        finalcode = swiftcodeArray[indexPath.row]
        newview.isHidden = true
        addview.isHidden = true
        myTableView.isHidden = true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
  

    @IBAction func updateAction(_ sender: Any) {
        UpdateprofileapiCalling(apikey:apikey,country: finalcode,gender:Gender,phone:phoneOutlet.text!,email:emailOutlet.text!,name:nameoutlet.text!)
    }
}
