//
//  profilecountryTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/2/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class profilecountryTableViewCell: UITableViewCell {
    
    @IBOutlet var countryflag: UIImageView!
    
    @IBOutlet var countryname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
