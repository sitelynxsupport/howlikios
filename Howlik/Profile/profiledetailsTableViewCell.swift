//
//  profiledetailsTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/7/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class profiledetailsTableViewCell: UITableViewCell {
    @IBOutlet var imageLabel: UIImageView!
    
    @IBOutlet var smallview: UIView!
    @IBOutlet var itemLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
