//
//  profileloadViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/6/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Alamofire
import ParallaxHeader
import SnapKit
import SDWebImage
import NVActivityIndicatorView
import FontAwesome_swift
import SCLAlertView
import TTGSnackbar


class profileloadViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,NVActivityIndicatorViewable {
    @IBOutlet var tableview: UITableView!
      @IBOutlet var addedview: UIView!
     @IBOutlet var addedImge: UIImageView!
    @IBOutlet var mainview: UIView!
    @IBOutlet var imgview: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var editbutton: UIButton!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
     var settingsview = SettingsView()
    let profileClass = ApiListViewcontroller()
    var apikey = ""
    var language_code = ""
 let imagePicker = UIImagePickerController()
    var name = ""
    var email  = ""
    var phone = ""
    var photo = ""
    var country = ""
    var profilephoto = ""
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    let alert = emailValidateViewController()
    var val = false
       var myNotificationArray = [notificationClass]()
     var customview = UIView()
    var  dashImage = ["my_review","my_business_booking","my_event_bookings","settings","my_activity","my_interest","my_friends","my_notification","my_more"]
  //   var  dashName = ["Reviews","My business bookings","My event bookings","Settings","Activity","Interest","Friends","Notifications","More"]
    
     var  dashName = [NSLocalizedString("Reviews", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("MybusinessBooking", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("MyEventsBookings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Settings", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Activity", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Interest", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Friends", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Notifications", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("More", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
    var selectedImage = UIImage()
    var status = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title =  NSLocalizedString("Profile", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        
        // self.navigationController?.navigationBar.barTintColor  = UIColor.red
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       editbutton.setImage(UIImage.fontAwesomeIcon(name: FontAwesome.pencil, textColor: UIColor.white, size: CGSize(width: 40, height: 40)), for: UIControlState.normal)
       
        // Do any additional setup after loading the view.
      editbutton.layer.cornerRadius = editbutton.bounds.size.width / 2.0
      //  myTableView.estimatedRowHeight = 44.0
      //  myTableView.rowHeight = UITableViewAutomaticDimension
    
        // cell.homeImage.clipsToBounds = true
    }
    func startactvityIndicator(){
        // let color1 = UIColor.red
        let size = CGSize(width: 30, height: 30)
        //startAnimating(size, message: "Loading...",color: color1)
        startAnimating(size, message: "Loading...")
        
    }
    func stopActivityIndicator(){
        
        self.stopAnimating()
    }
    
    @IBAction func editAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "profdetedit") as! profdetEditViewController
        let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
        self.present(navController, animated: true, completion: nil)
     
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tabBarController?.tabBar.items?[2].title = NSLocalizedString("Profile", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
          
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
               self.language_code = UserDefaults.standard.string(forKey: "language_code")!
            LoadprofileapiCalling(apikey:apikey,language_code:language_code)
        }else{
            
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton("OK", action: { // create button on alert
               
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError(NSLocalizedString("ERROR", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), subTitle: NSLocalizedString("Please  Register to create profile!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
        }
        
     
    
 //  let image = UIImage(named: "city")
        
       
       // roundButton.snp.makeConstraints { make in
      //      make.center.equalToSuperview()
      //      make.width.height.equalTo(60)
       // }
    }
    @objc func buttonAction(sender: UIButton!){
 
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func LoadprofileapiCalling(apikey:String,language_code:String){
     self.startactvityIndicator()
        let scriptUrl = profileClass.LoadProfile
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code]
     
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        //  if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: retrivedResult, options: []) as? [String:Any]{
                     
                        if response["profile"] != nil {
                        let profiles = response.object(forKey: "profile")!
                      
                       // for profile in (profiles as! [[String:Any]]){
                            //var popularElement = popularEventclass()
                        let contry1 = profiles as! NSDictionary
                            if  (contry1["name"] as? String) != nil {
                                self.name = contry1["name"] as! String
                               
                            }else{
                                self.name = "nil"
                            }
                            
                            if  (contry1["phone"] as? String) != nil {
                                self.phone = contry1["phone"] as! String
                                
                               
                            }else{
                                self.phone = "nil"
                              
                                
                            }
                            if  (contry1["email"] as? String) != nil {
                                self.email = contry1["email"] as! String
                              
                            }
                            else{
                                self.email = "nil"
                                
                            }
                            if  (contry1["cover"] as? String) != nil {
                                let cover = contry1["cover"] as! String
                                self.photo = "http://www.howlik.com/" +  cover
                            }
                            if  (contry1["photo"] as? String) != nil {
                                let photo = contry1["photo"] as! String
                                self.profilephoto = "http://www.howlik.com/" +  photo
                            }
                            if  (contry1["country_code"] as? String) != nil {
                                self.country = contry1["country_code"] as! String
                                
                            }
                       
                        }
                        //  }
                        
                        // self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                self.imgview.layer.cornerRadius = self.imgview.bounds.size.width / 2.0
                                self.imgview.clipsToBounds = true
                                self.mainview.roundCorners(corners: [.topLeft,.topRight], radius: 5.0)
                                self.nameLabel?.text = self.name
                                
                                self.emailLabel?.text = self.email
                               
                                self.numberLabel?.text = self.phone
                              
                                self.countryLabel?.text = self.country
                                
                                self.self.imgview.sd_setImage(with: URL(string:self.profilephoto), placeholderImage:UIImage(named: "no-image02"))
                                 self.self.addedImge.sd_setImage(with: URL(string:self.photo), placeholderImage:UIImage(named: "no-image02"))
                                
                             let imageView = UIImageView()
                                //imageView.image = image
                                imageView.sd_setImage(with: URL(string:self.photo), placeholderImage:UIImage(named: "no-image02"))
                                imageView.contentMode = .scaleAspectFill
                                //   parallaxHeaderView = imageView
                                
                                //setup bur view
                          //      imageView.blurView.setup(style: UIBlurEffectStyle.dark, alpha: 1).enable()
                                
                                self.tableview.parallaxHeader.view = imageView
                                self.self.tableview.parallaxHeader.height = 100
                                self.tableview.parallaxHeader.minimumHeight = 70
                                self.self.tableview.parallaxHeader.mode = .centerFill
                                
                                let roundIcon = UIImageView(
                                    frame: CGRect(x: 0, y: 0, width: 100, height: 100)
                                )
                             
                              
                                self.tableview.reloadData()
                                self.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
               
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == myTableView{
         return myNotificationArray.count
        }else{
             return dashName.count
        }
      
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 if tableView == myTableView{
    let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! notificationTableViewCell
    cell.selectionStyle = .none
    cell.separatorInset = .zero
  cell.notifictiontitleLabel.text = myNotificationArray[indexPath.row].type
      cell.notificationDescriptionLabel.text = myNotificationArray[indexPath.row].title
    cell.myview.layer.cornerRadius = 8.0
    cell.backgroundColor = UIColor.black
            return cell
      }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as!
            profiledetailsTableViewCell
            cell.selectionStyle = .none
           cell.imageLabel.layer.cornerRadius = cell.imageLabel.bounds.size.width / 2.0
          cell.imageLabel.clipsToBounds = true
            cell.imageLabel?.image = UIImage(named:dashImage[indexPath.row])
            cell.itemLabel.text = dashName[indexPath.row]
            return cell
      }
   
        }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == myTableView{
            
        }else{
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "myreview") as! MyreviewListViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        
        }else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "mybuisbook") as! mybusinessbookingViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)

           
        }else if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "myeventbooking") as! myEventBookingViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        }else if indexPath.row == 3 {
            createTableview()
            
        }else if indexPath.row == 4 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "myactivity") as! myactivityViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        }else if indexPath.row == 5 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "myinterest") as! myInterestViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
           
        }else if indexPath.row == 6 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "myfriends") as! MyFriendsViewController
            let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
            self.present(navController, animated: true, completion: nil)
        }else if indexPath.row == 7 {
                 NotificationloadapiCalling(apikey:apikey,language_code:self.language_code)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
            tabbarVC.selectedIndex = 4
           // if let vcs = tabbarVC.viewControllers,
                
                
                //let nc = vcs.first as? UINavigationController,
               // let pendingOverVC = nc.topViewController as? parentinboxViewController {
                //
                //
          //  }
            //let navController = UINavigationController(rootViewController: vcs)
            
            self.present(tabbarVC, animated: false, completion: nil)
                
            }
            
        }
    }
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
   if tableView == myTableView{
   
            return UITableViewAutomaticDimension
 
   }else{
        return 60
        }
        
    }
    
    
    @IBAction func uploadImage(_ sender: Any) {
        //        imagePicker.allowsEditing = false
        //        imagePicker.sourceType = .photoLibrary
        //        imagePicker.delegate = self
        //        present(imagePicker, animated: true, completion: nil)
        showActionSheet()
    }
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            //   let myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            // let myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func createTableview(){
        
        settingsview  = SettingsView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
        self.view.addSubview(settingsview)
        settingsview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        settingsview.lastView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // sendMessageView.Contentview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        settingsview.oldPasswordtextfield.setBottomBorder()
            settingsview.newPasswordTextfield.setBottomBorder()
        settingsview.confirmpasswordTextfield.setBottomBorder()
          settingsview.changepasswordLabel.text = NSLocalizedString("Change password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        settingsview.oldPasswordtextfield.placeholder = NSLocalizedString("Old Password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        settingsview.newPasswordTextfield.placeholder = NSLocalizedString("New password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
          settingsview.confirmpasswordTextfield.placeholder = NSLocalizedString("Confirm Password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        settingsview.okOutlet.setTitle(NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
          settingsview.cancelOutlet.setTitle(NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        
        settingsview.okOutlet.addTarget(self, action: #selector(sendbuttonAction), for: .touchUpInside)
        settingsview.cancelOutlet.addTarget(self, action: #selector(cancelbuttonAction), for: .touchUpInside)
        // rplyView.backgroundColor = UIColor.red
         settingsview.oldPasswordtextfield.becomeFirstResponder()
    }
    @objc  func sendbuttonAction(sender: UIButton!) {
        if (settingsview.oldPasswordtextfield.text?.isEmpty)!{
             alertsnack(message:NSLocalizedString("Old Password required!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (settingsview.newPasswordTextfield.text?.isEmpty)!{
            alertsnack(message:NSLocalizedString("Type your new Password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if ((settingsview.newPasswordTextfield.text?.count)! < 6){
            alertsnack(message:NSLocalizedString("Use at least 6 characters!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (settingsview.confirmpasswordTextfield.text?.isEmpty)!{
               alertsnack(message:NSLocalizedString("Confirm your new Password", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else if (settingsview.newPasswordTextfield.text != settingsview.confirmpasswordTextfield.text){
              alertsnack(message:NSLocalizedString("Password mismatch!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
            
        }else{
                ChangePasswordapiCalling(apikey:apikey,old_password: settingsview.oldPasswordtextfield.text!,new_password:  settingsview.confirmpasswordTextfield.text!)
        }
    }
    @objc  func cancelbuttonAction(sender: UIButton!) {
        settingsview.isHidden = true
        
    }
    func alertsnack(message: String){
        
        if val == false{
            self.val = true
            let loadingSnackbar = TTGSnackbar(message: "\n"+message+"\n", duration: .forever)
            loadingSnackbar.actionText = NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            loadingSnackbar.cornerRadius = 8
            loadingSnackbar.leftMargin = 8
            loadingSnackbar.rightMargin = 8
            loadingSnackbar.bottomMargin = 8
            loadingSnackbar.backgroundColor = UIColor.black
            loadingSnackbar.animationType = .slideFromRightToLeft
            //  loadingSnackbar.show()
            
            loadingSnackbar.actionBlock = { (snackbar) in NSLog("Click Yes !")
                
                loadingSnackbar.dismiss()
                self.val = false
            }
            loadingSnackbar.show()
        }
    }
    func showActionSheet() {
        //   currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //selectedimageview.image = image
            selectedImage = image
            noteUploading(selectedImage:selectedImage)
          //  selectedimageview.contentMode = .scaleAspectFit
            self.dismiss(animated: true, completion: nil)
        }
    }
    private func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func noteUploading(selectedImage:UIImage){
     //   if connectionViewController.isConnectedToNetwork(){
       self.startactvityIndicator()
       
                let body =  [
                    
                    "apikey": "\(apikey)"]
        
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(UIImageJPEGRepresentation(selectedImage, 0.5)!, withName: "file", fileName: "file", mimeType: "image/jpeg")
                    for (key, value) in body {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                }, to: "http://www.howlik.com/api/upload/profile/picture"){
                    (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (Progress) in
                         //   print("Upload Progress: \(Progress.fractionCompleted)")
                        })
                        
                        upload.responseString { response in
                         
                            //print(response)
                          //  print(response.request!)  // original URL request
                           /// print(response.response!) // URL response
                           // print(response.data!)     // server data
                           // print(response.result)   // result of response serialization
                            if let JSON = response.result.value {
                               // print("JSON: \(JSON)")
                              //  self.stopActivityIndicator()
                                self.LoadprofileapiCalling(apikey:self.apikey,language_code:self.language_code)
                            }
                        }
                   self.stopActivityIndicator()
                        
                    case .failure(let encodingError):
                        //self.delegate?.showFailAlert()
                        print(encodingError)
                   self.stopActivityIndicator()
                    }
                    
                    
                }
                
                
       //     }
       // }else{
      //
      //  }
    }
    func ChangePasswordapiCalling(apikey:String,old_password:String,new_password:String){
        self.startactvityIndicator()
        let scriptUrl = profileClass.ChangePassword
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&old_password=\(old_password)&new_password=\(new_password)"

        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                return
            }
            
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                  
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                 
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.status == "success"{
                            self.stopActivityIndicator()
                            self.alert.Successalert(SubTitle: NSLocalizedString("Your password for Howlik changed!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                            self.settingsview.isHidden = true
                            
                        }else{
                              self.stopActivityIndicator()
                           self.alert.Failurealert()
                            self.settingsview.isHidden = true
                        }
                        
                        
                        
                        
                        
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
    
    func NotificationloadapiCalling(apikey:String,language_code:String){
        // self.startactvityIndicator()
        self.myNotificationArray.removeAll()
        
        let scriptUrl = profileClass.AllNotifications
        
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                    
                        
                    if  (response["notifications"]) != nil
                    {
                        let notifications = response.object(forKey: "notifications")!
                        
                     
                        for singlenotifications in (notifications as! [[String:Any]]){
                          
                            let singleNotificationElement = notificationClass()
                            let contry = singlenotifications as NSDictionary
                            if  (contry["type"] as? String) != nil {
                                let type = contry["type"] as! String
                                singleNotificationElement.type = type
                                
                            }
                        if  (contry["title"] as? String) != nil {
                            let title = contry["title"] as! String
                            singleNotificationElement.title = title
                            
                        }
                        
                            self.myNotificationArray.append(singleNotificationElement)
                        }
               
                        }
                        
                        
                        DispatchQueue.main.async
                            {
                                
                                if ((self.myNotificationArray).count == 0){
                                    self.myTableView.isHidden = true
                                       self.newview.isHidden = true
                                    self.tableview.isHidden = true
                                    self.newview.backgroundColor = UIColor.black
                                    self.createeventNothingview()
                                }else{
                                    
                                self.tableview.isHidden = false
                                self.myTableView.isHidden = false
                                self.createNotificationTableview()
                                 self.myTableView.reloadData()
                                let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                                swipeDown.direction = UISwipeGestureRecognizerDirection.down
                                self.view.addGestureRecognizer(swipeDown)
                                }
                                
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            let screenSize: CGRect = UIScreen.main.bounds
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right: break
               
            case UISwipeGestureRecognizerDirection.down:
               
                UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  // self.newview = UIView(frame: CGRect(x: 0, y: self.view.frame.height , width: screenSize.width , height: screenSize.height ))
                    self.newview.center.y = screenSize.height + 300
                  //  self.mainView.center.y = self.view.frame.height - self.mainView.frame.height/2
                })
               
                //self.newview.isHidden = true
            case UISwipeGestureRecognizerDirection.left: break
               
            case UISwipeGestureRecognizerDirection.up: break
              
            default:
                break
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        let screenSize: CGRect = UIScreen.main.bounds
         self.newview.center.y = screenSize.height + 300
        self.tableview.isHidden = false
      self.customview.isHidden = true
        //self.myTableView.isHidden = true
    }
   
    func createNotificationTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        self.newview = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width , height: screenSize.height  ))
        self.view.addSubview(newview)
        newview.backgroundColor = UIColor.black
        let view1: UIView = UIView(frame: CGRect(x:0, y:20,width: newview.frame.width , height: 65));
         self.newview.addSubview(view1)
       
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:15,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textAlignment = NSTextAlignment.center
    self.myTableView = UITableView(frame: CGRect(x: 0, y: 80, width: newview.frame.width, height: newview.frame.height - 80))
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0); //values
        label.textColor = UIColor.white
        label.isHighlighted = true
        view1.backgroundColor  = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 20)
        myTableView.register(UINib(nibName: "notificationTableviewCell", bundle: nil), forCellReuseIdentifier: "myIdentifier")
        label.text = NSLocalizedString("Slide down", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        newview.addSubview(myTableView)
       myTableView.backgroundColor = UIColor.black
        self.myTableView.separatorStyle = .none
      
        self.myTableView.alwaysBounceVertical = false
        myTableView.estimatedRowHeight = 44.0
        myTableView.rowHeight = UITableViewAutomaticDimension

     //   self.myTableView.reloadData()
        
       
    }
    func createeventNothingview(){
       
        customview.isHidden = false
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    }

extension UIView {
    
    private struct AssociatedKeys {
        static var descriptiveName = "AssociatedKeys.DescriptiveName.blurView"
    }
    
    private (set) var blurView: BlurView {
        get {
            if let blurView = objc_getAssociatedObject(
                self,
                &AssociatedKeys.descriptiveName
                ) as? BlurView {
                return blurView
            }
            self.blurView = BlurView(to: self)
            return self.blurView
        }
        set(blurView) {
            objc_setAssociatedObject(
                self,
                &AssociatedKeys.descriptiveName,
                blurView,
                .OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }
    
    class BlurView {
        
        private var superview: UIView
        private var blur: UIVisualEffectView?
        private var editing: Bool = false
        private (set) var blurContentView: UIView?
        private (set) var vibrancyContentView: UIView?
        
        var animationDuration: TimeInterval = 0.1
        
        /**
         * Blur style. After it is changed all subviews on
         * blurContentView & vibrancyContentView will be deleted.
         */
        var style: UIBlurEffectStyle = .light {
            didSet {
                guard oldValue != style,
                    !editing else { return }
                applyBlurEffect()
            }
        }
        /**
         * Alpha component of view. It can be changed freely.
         */
        var alpha: CGFloat = 0 {
            didSet {
                guard !editing else { return }
                if blur == nil {
                    applyBlurEffect()
                }
                let alpha = self.alpha
                UIView.animate(withDuration: animationDuration) {
                    self.blur?.alpha = alpha
                }
            }
        }
        
        init(to view: UIView) {
            self.superview = view
        }
        
        func setup(style: UIBlurEffectStyle, alpha: CGFloat) -> Self {
            self.editing = true
            
            self.style = style
            self.alpha = alpha
            
            self.editing = false
            
            return self
        }
        
        func enable(isHidden: Bool = false) {
            if blur == nil {
                applyBlurEffect()
            }
            
            self.blur?.isHidden = isHidden
        }
        
        private func applyBlurEffect() {
            blur?.removeFromSuperview()
            
            applyBlurEffect(
                style: style,
                blurAlpha: alpha
            )
        }
        
        private func applyBlurEffect(style: UIBlurEffectStyle,
                                     blurAlpha: CGFloat) {
            superview.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: style)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            
            let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
            let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
            blurEffectView.contentView.addSubview(vibrancyView)
            
            blurEffectView.alpha = blurAlpha
            
            superview.insertSubview(blurEffectView, at: 0)
            
            blurEffectView.addAlignedConstrains()
            vibrancyView.addAlignedConstrains()
            
            self.blur = blurEffectView
            self.blurContentView = blurEffectView.contentView
            self.vibrancyContentView = vibrancyView.contentView
        }
    }
    
    private func addAlignedConstrains() {
        translatesAutoresizingMaskIntoConstraints = false
        addAlignConstraintToSuperview(attribute: NSLayoutAttribute.top)
        addAlignConstraintToSuperview(attribute: NSLayoutAttribute.leading)
        addAlignConstraintToSuperview(attribute: NSLayoutAttribute.trailing)
        addAlignConstraintToSuperview(attribute: NSLayoutAttribute.bottom)
    }
    
    private func addAlignConstraintToSuperview(attribute: NSLayoutAttribute) {
        superview?.addConstraint(
            NSLayoutConstraint(
                item: self,
                attribute: attribute,
                relatedBy: NSLayoutRelation.equal,
                toItem: superview,
                attribute: attribute,
                multiplier: 1,
                constant: 0
            )
        )
    }
    
    
}

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
