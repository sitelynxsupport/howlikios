//
//  howlikUsersViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 6/7/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Tags
class howlikUsersViewController: UIViewController,TagsDelegate {
    @IBOutlet var lastview: UIView!
    @IBOutlet var sendOutlet: UIButton!
    
    
    @IBOutlet var mainview: UIView!
    @IBOutlet var cancelOutlet: UIButton!
    @IBOutlet var enteremailLabel: UILabel!
    
    @IBOutlet var tagsView: TagsView!
    var emailArray = [String]()
    var idArray = [String]()
    var first  = 1
    var apikey = ""
    var status = ""
    let apiClass = ApiListViewcontroller()
    let activityview = emailValidateViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
         
        }
        UserDefaults.standard.set("", forKey: "emailArray")
        UserDefaults.standard.set("", forKey: "idArray")
       first = 1
        // Do any additional setup after loading the view.
        mainview.layer.shadowColor = UIColor.white.cgColor
        mainview.layer.shadowOpacity = 1
        mainview.layer.shadowOffset = CGSize.zero
        mainview.layer.shadowRadius = 2.0
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        let img = UIImage()
        navigationController?.navigationBar.shadowImage = img
        navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 0.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
         self.navigationItem.title = NSLocalizedString("FindFriends", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        cancelOutlet.setTitle(NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        sendOutlet.setTitle(NSLocalizedString("Send", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState())
        
    }
    override func viewDidAppear(_ animated: Bool) {
     
        enteremailLabel.text =  NSLocalizedString("Enter Email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        sendOutlet.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        
        sendOutlet.layer.cornerRadius = 12
        sendOutlet.layer.borderWidth = 1.0;
        cancelOutlet.backgroundColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        cancelOutlet.layer.cornerRadius = 12
        cancelOutlet.layer.borderWidth = 1.0;
        tagsView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraint(NSLayoutConstraint(
            item: self.view,
            attribute: .leading,
            relatedBy: .equal,
            toItem: tagsView,
            attribute: .leading,
            multiplier: 1,
            constant: 0)
        )
        // cell.normalview.addSubview(tagsView)
    tagsView.paddingLeftRight = 6
    tagsView.paddingTopBottom = 4
    tagsView.marginLeftRight = 15
        
    tagsView.marginTopBottom = 4
    tagsView.removeAll()
        
       
  
        tagsView.delegate = self
     
            tagsView.tags = "Type your friend's Howlik Email"

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tagsTouchAction(_ tagsView: TagsView, tagButton: TagButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel, handler: nil))
        
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Add email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (_) in
            /// Next Insert UIAlertController
            let alertController = UIAlertController(title: nil, message: NSLocalizedString("EnterEmail", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: .alert)
            alertController.addTextField(configurationHandler: { (textField) in })
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel, handler: nil))
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Add", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (_) in
                guard let text = alertController.textFields?.first?.text, text.count != 0 else{
                    return
                }
                /// Insert
               
                self.tagsView.insert(text, at: tagButton.index + 1)
                let value = tagsView.tagTextArray[0]
                print("^^^^^^^^^^^^^^^^^^^^")
                print(value)
                   print("^^^^^^^^^^^^^^^^^^^^")
                if tagsView.tagTextArray[0].contains("Type your friend's Howlik Email"){
                    tagsView.remove(0)
                }
             
            }))
            self.present(alertController, animated: true, completion: nil)
        }))
        
        
        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            /// Remove
            self.tagsView.remove(tagButton)
            if tagsView.tagTextArray.count == 0{
                tagsView.tags = NSLocalizedString("Type your friend's Howlik Email", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
            }
         
        }))
        
        self.present(alertController, animated: true, completion: nil)
        

    }
    
   
    @IBAction func cancelAction(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    @IBAction func sendAction(_ sender: Any) {
        activityview.startactvityIndicator()
        let dic = NSMutableDictionary()
        // var emailarray = NSMutableArray()

        dic.setValue(tagsView.tagTextArray, forKey: "emails")
        dic.setValue(apikey, forKey: "apikey")
        do {



            //Convert to Data

            let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)

            // print(jsonData)

            //Convert back to string. Usually only do this for debugging

            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {

                //ethaann nink veenda string

                // json aayit kittum
                print("***************************")
                print(JSONString)
                print("***************************")
                let scriptUrl = apiClass.InviteFriendstoHowlik
                let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
                let myUrl = URL(string: urlWithParams);
                var request = URLRequest(url:myUrl!)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpBody = jsonData
                let task = URLSession.shared.dataTask(with: request)
                {
                    data, response, error in

                    if error != nil
                    {
                        print("error=\(error)")
                        DispatchQueue.main.async
                            {
                                print("server down")
                        }
                        return
                    }
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("################################")
                    print(responseString)
                    do{

                        if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]

                        {

                            self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                            print(self.status)

                        }
                        DispatchQueue.main.async
                            {


                                 if self.status == "success"{
                                    self.activityview.stopActivityIndicator()
                                   self.activityview.Successalert(SubTitle:NSLocalizedString("Invitations will be send", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                                    self.dismiss(animated: true, completion: nil)
                                    
                                
                                 }else{
                                    self.activityview.stopActivityIndicator()
                                    self.activityview.Failurealert()
                                       self.dismiss(animated: true, completion: nil)
                                 }


                        }//dispatch ends

                    }//do ends
                    catch let error as NSError {
                        print(error.localizedDescription)

                    }

                }



                //  let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                // print("################################")
                //  print(responseString)

                //   }

                task.resume()


            }

        }
    }
}
