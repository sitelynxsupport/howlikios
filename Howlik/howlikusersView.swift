//
//  howlikusersView.swift
//  Howlik
//
//  Created by Shrishti Informatics on 6/7/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import Tags

class howlikusersView: UIView {

    @IBOutlet var lastview: UIView!
    @IBOutlet var sendOutlet: UIButton!
    @IBOutlet var enteremailLabel: UILabel!
    
    @IBOutlet var tagsView: TagsView!
    override init(frame: CGRect){ // for using custom viw in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
        
        commonInit()
    }
    private func commonInit(){
        //  we are going to do stuff here
        let nibView = Bundle.main.loadNibNamed("howlikusersView", owner: self, options: nil)![0] as! UIView
        nibView.frame = self.bounds;
        self.addSubview(nibView)
    }

    
}
