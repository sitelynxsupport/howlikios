//
//  languageView.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/30/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import DLRadioButton
class languageView: UIView {

    @IBOutlet var englishButton: DLRadioButton!
    
    @IBOutlet var arabicButton: DLRadioButton!
    
    @IBOutlet var englishLabel: UILabel!
    
    @IBOutlet var arabicLabel: UILabel!
    
    @IBOutlet var cancelButton: UIButton!
    
    @IBOutlet var lastview: UIView!
    @IBOutlet var myview: UIView!
    @IBOutlet var okOutlet: UIButton!
    override init(frame: CGRect){ // for using custom viw in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
        
        commonInit()
    }
    private func commonInit(){
        //  we are going to do stuff here
        let nibView = Bundle.main.loadNibNamed("languageView", owner: self, options: nil)![0] as! UIView
        nibView.frame = self.bounds;
        self.addSubview(nibView)
    }

}
