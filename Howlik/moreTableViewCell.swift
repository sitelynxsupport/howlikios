//
//  moreTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/24/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class moreTableViewCell: UITableViewCell {

    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var titlelabel: UILabel!
    @IBOutlet var imageview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
