//
//  moreViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 4/24/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SCLAlertView


class moreViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet var moreTable: UITableView!
    var more_Array = [String]()
     var more_ImageArray = [String]()
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var array = [String]()
    var latitude = Double()
    var longitude = Double()
      var addressString : String = ""
    var language_code = ""
    let apiClass = ApiListViewcontroller()
     let activityview = emailValidateViewController()
    var changeCountryArray = [changeCountryClass]()
    var myTableView = UITableView()
    var newview = UIView()
    var addview = UIView()
    var mynotiTableView = UITableView()
    var notinewview = UIView()
    var notiaddview = UIView()
    var apikey = ""
    var status = ""
      var NotificationArray = [morenotificationClass]()
    var countrycode = ""
    var bundle = Bundle()
     var flagImage = ["united_arab_emirates","bahrain","india","kuwait","oman","qatar","saudi_arabia"]
     var languageview = languageView()
        var howlikUsersview = howlikusersView()
    
    var language = 1
    var statusValue = Int()
    var customview = UIView()
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
    //  self.navigationController?.setNavigationBarHidden(true, animated: true)
      
        // Do any additional setup after loading the view.
       
        if defaults.string(forKey: "userType") == "3" {
            more_Array = [NSLocalizedString("Currentlocation", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("ChangeCountry", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Language", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Notification", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Message", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("FindFriends", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("EventGraph", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Log Out", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
            more_ImageArray = ["add_business_ico","language_ico","myorders_ico","notification_ico","messages_ico","friends_ico","event_ico","log_out_ico"]
            
        }else{
            more_Array = [NSLocalizedString("Currentlocation", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("ChangeCountry", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Language", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Notification", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Message", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("FindFriends", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("BusinessGraph", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("EventGraph", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""),NSLocalizedString("Log Out", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")]
            more_ImageArray = ["add_business_ico","language_ico","myorders_ico","notification_ico","messages_ico","friends_ico","business_ico","event_ico","log_out_ico"]
        }
       
       
        moreTable.layer.cornerRadius = 5.0
        moreTable.estimatedRowHeight = 55.0
        moreTable.rowHeight = UITableViewAutomaticDimension
        if  UserDefaults.standard.string(forKey:"apikey") != nil{
            
            self.apikey = UserDefaults.standard.string(forKey: "apikey")!
               self.language_code = UserDefaults.standard.string(forKey: "language_code")!
            if checkConnectionViewController.isConnectedToNetwork(){
            getcurrentLoction()
            getAddressFromLatLon(pdblLatitude: String(latitude), withLongitude: String(longitude))
            }else{
                print("No connection")
            }
        }else{
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false // hide default button
            )
            let alert = SCLAlertView(appearance: appearance) // create alert with appearance
            alert.addButton("OK", action: { // create button on alert
                print("click click") // action on click
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterViewController
                let navController = UINavigationController(rootViewController: vc) // Creating a navigation controller with resultController at the root of the navigation stack
                self.present(navController, animated: true, completion: nil)
            })
            alert.showError("ERROR", subTitle: "Please Register to view More!")
        }
       
        moreTable.isHidden = true
        
      

        
    }
   
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tabBarController?.tabBar.items?[4].title = NSLocalizedString("More", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func getcurrentLoction(){
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            //   print(currentLocation.coordinate.latitude)
            //print(currentLocation.coordinate.longitude)
            latitude = currentLocation.coordinate.latitude
            longitude =  currentLocation.coordinate.longitude
            
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        activityview.startactvityIndicator()
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
//                    print(pm.country!)
//                    print(pm.locality!)
//                    print(pm.subLocality!)
//                    print(pm.thoroughfare!)
//                    print(pm.postalCode!)
//                    print(pm.subThoroughfare!)
                  
                    if pm.subLocality != nil {
                        self.addressString = self.addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        self.addressString = self.addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                        //self.locale(forfullCountryName : pm.country!)
                       let value =   self.locale(for  : pm.country!)
                       

                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                 
                    
                    self.moreTable.isHidden = false
                    self.moreTable.reloadData()
                    self.activityview.stopActivityIndicator()
                }
        })
      
        
    }
    private func locale(for fullCountryName : String) -> String {
        let locales : String = ""
        for localeCode in NSLocale.isoCountryCodes {
            let identifier = NSLocale(localeIdentifier: localeCode)
            let countryName = identifier.displayName(forKey: NSLocale.Key.countryCode, value: localeCode)
            if fullCountryName.lowercased() == countryName?.lowercased() {
                return localeCode
            }
        }
        return locales
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
      
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == myTableView{
            return changeCountryArray.count
        }else if tableView == mynotiTableView{
            return NotificationArray.count
        }else{
              return more_Array.count
        }
      
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == myTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myIdentifier", for: indexPath) as! profilecountryTableViewCell
            cell.selectionStyle = .none
            cell.countryname.text = changeCountryArray[indexPath.row].title
            cell.countryflag.image = UIImage(named:flagImage[indexPath.row])
            
            return cell
        }else if tableView == mynotiTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "mynotiIdentifier", for: indexPath) as! morenotificationTableviewCell
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            cell.asciinameLabel.text = NotificationArray[indexPath.row].asciiname
            cell.nameLabel.text = NotificationArray[indexPath.row].name  +  NSLocalizedString("  Sent you a friend request!", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
       
            cell.myview.layer.cornerRadius = 8.0
            cell.backgroundColor = UIColor.black
            cell.myview.backgroundColor = UIColor.white
            cell.rejectOutlet.tag = indexPath.row
            cell.rejectOutlet.addTarget(self, action:#selector(self.rejectView(_:)), for:.touchUpInside)
            cell.rejectOutlet.layer.cornerRadius = 8.0
            cell.acceptOutlet.tag = indexPath.row
             cell.acceptOutlet.addTarget(self, action:#selector(self.acceptView(_:)), for:.touchUpInside)
            cell.acceptOutlet.layer.cornerRadius = 8.0
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! moreTableViewCell
        if indexPath.row == 0{
            cell.addressLabel.text = addressString
        }else if indexPath.row == 1{
            cell.addressLabel.text = countrycode
        }else{
            cell.addressLabel.text = ""
        }
        cell.selectionStyle = .none
        cell.imageview.image = UIImage(named:more_ImageArray[indexPath.row])
            cell.titlelabel.text = more_Array[indexPath.row]
        return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if tableView == myTableView{
        countrycode = changeCountryArray[indexPath.row].code
            UserDefaults.standard.set(countrycode, forKey: "country_code")
            myTableView.isHidden = true
            addview.isHidden = true
            newview.isHidden = true
            moreTable.reloadData()
         }else if tableView == mynotiTableView{
            
            
         }else{
        
        if indexPath.row == 0{
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "currentaddress") as! currentAddressViewController
//            let navController = UINavigationController(rootViewController: vc)
//            self.present(navController, animated: true, completion: nil)
            
        }else if indexPath.row == 1 {
           
           LoadprofileapiCalling(apikey:apikey,language_code:language_code)
        }else if indexPath.row == 2 {
//            let alertController = UIAlertController(title: NSLocalizedString("ChangeLanguage", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), message: "", preferredStyle: .alert)
//          //
//            let oneAction = UIAlertAction(title: "English", style: .default) { _ in
//                LanguageManager.sharedInstance.setLocale("en")
//                UserDefaults.standard.set("en", forKey:"language_code")
//
//                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
//                if let vcs = tabbarVC.viewControllers,
//
//                    let nc = vcs.first as? UINavigationController,
//                    let pendingOverVC = nc.topViewController as? HomeViewController {
//
//
//                }
//
//
//                self.present(tabbarVC, animated: false, completion: nil)
//            }
//            let twoAction = UIAlertAction(title: "Arabic", style: .default) { _ in
//                 LanguageManager.sharedInstance.setLocale("ar")
//                   UserDefaults.standard.set("ar", forKey:"language_code")
//
//                UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
//                if let vcs = tabbarVC.viewControllers,
//
//                    let nc = vcs.first as? UINavigationController,
//                    let pendingOverVC = nc.topViewController as? HomeViewController {
//
//
//                }
//                //    let navController = UINavigationController(rootViewController: vcs)
//
//                self.present(tabbarVC, animated: false, completion: nil)
//            }
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
//
//            alertController.addAction(oneAction)
//            alertController.addAction(twoAction)
//            alertController.addAction(cancelAction)
//            self.present(alertController, animated: true)
             createlanguageview()
        }else if indexPath.row == 3 {
            NotificationloadapiCalling(apikey:apikey)
           
            
        }else if indexPath.row == 4 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabbarVC = storyboard.instantiateViewController(withIdentifier: "messagetab") as! UITabBarController
            if let vcs = tabbarVC.viewControllers,
                
                let nc = vcs.first as? UINavigationController,
                let pendingOverVC = nc.topViewController as? chatInboxViewController {
                
                
            }
            //    let navController = UINavigationController(rootViewController: vcs)
            
            self.present(tabbarVC, animated: false, completion: nil)
            
        }else if indexPath.row == 5 {
              ActionSheet()
            
        }else if  indexPath.row == 6 {
            
            if defaults.string(forKey: "userType") == "3" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "eventgraph") as! EventGraphViewController
                let navController = UINavigationController(rootViewController: vc)
                self.present(navController, animated: true, completion: nil)
                return
            }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "businessgraph") as! GraphViewController
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
            
        }else if indexPath.row == 7 {
            if defaults.string(forKey: "userType") == "3" {
                let dialogMessage = UIAlertController(title:  NSLocalizedString("Log Out", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), message: NSLocalizedString("Are you sure you want to exit??", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: .alert)
                //
                // Create OK button with action handler
                let ok = UIAlertAction(title: NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (action) -> Void in
                    print("Ok button tapped")
                    UserDefaults.standard.removeObject(forKey: "apikey")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
                    self.present(vc, animated: true, completion: nil)
                })
                
                // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel) { (action) -> Void in
                    print("Cancel button tapped")
                }
                
                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)
                
                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)
                
                
                return
            }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "eventgraph") as! EventGraphViewController
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
            
        }else {
            let dialogMessage = UIAlertController(title:  NSLocalizedString("Log Out", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), message: NSLocalizedString("Are you sure you want to exit??", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: .alert)
            //
            // Create OK button with action handler
            let ok = UIAlertAction(title: NSLocalizedString("OK", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (action) -> Void in
                print("Ok button tapped")
                UserDefaults.standard.removeObject(forKey: "apikey")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "login") as! LoginViewController
                self.present(vc, animated: true, completion: nil)
            })
            
            // Create Cancel button with action handlder
            let cancel = UIAlertAction(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel) { (action) -> Void in
                print("Cancel button tapped")
            }
            
            //Add OK and Cancel button to dialog message
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            
            // Present dialog message to user
            self.present(dialogMessage, animated: true, completion: nil)
            
   
        }


        }

        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == myTableView{
            return 55
        }else if tableView == mynotiTableView{
            
            return UITableViewAutomaticDimension
        }else{
        return UITableViewAutomaticDimension
        }
    }
    func ActionSheet(){
       //
        let alertController = UIAlertController(title: "Action Sheet", message:  NSLocalizedString("Whatwouldyouliketodo?", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), preferredStyle: .actionSheet)
      
        let SocialButton = UIAlertAction(title: NSLocalizedString("SocialMedia", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            self.SharethroughSocialmedia()
        })
        let howlikFriends = UIAlertAction(title: NSLocalizedString("HowlikUsers", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "howlikusers") as! howlikUsersViewController
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
        })
       
        let cancelButton = UIAlertAction(title: NSLocalizedString("Cancel", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        alertController.addAction(SocialButton)
        alertController.addAction(howlikFriends)
        alertController.addAction(cancelButton)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
    func SharethroughSocialmedia(){
        //Hey I am using this awesome app for exploring local business
        let text = NSLocalizedString("Hey I am using this awesome app for exploring local business", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    func LoadprofileapiCalling(apikey:String,language_code:String){
      activityview.startactvityIndicator()
        self.changeCountryArray.removeAll()
        let scriptUrl = apiClass.LoadProfile
        //let myUrl = URL(string: scriptUrl);
        let parameters: Parameters = ["apikey": apikey,"language_code": language_code]
        print("%%%%%%%%%%%%%%%%%%%%%%%%%")
        print(parameters)
          print("%%%%%%%%%%%%%%%%%%%%%%%%%")
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print("%%%%%%%%%%%%%%%%%%%%%%%")
                        print(response)
                            print("%%%%%%%%%%%%%%%%%%%%%%%")
                        if (response["countries"]) != nil {
                        let countries = response.object(forKey: "countries")!
                        print(countries)
                        for country in (countries as! [[String :Any]]){
                            let coundryElement = changeCountryClass()
                            if  (country["code"] as? String) != nil {
                                let code = country["code"] as! String
                                coundryElement.code = code
                               
                            }
                            if  (country["title"] as? String) != nil {
                                let title = country["title"] as! String
                                coundryElement.title = title
                            }
                            self.changeCountryArray.append(coundryElement)
                        }
                        }
                        
                        DispatchQueue.main.async
                            {
                                self.createTableview()
                                self.activityview.stopActivityIndicator()
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    func createTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        let window = UIApplication.shared.keyWindow!
        addview = UIView(frame: CGRect(x: window.frame.origin.x  , y: window.frame.origin.y   , width: window.frame.width  , height: window.frame.height))
        window.addSubview(addview)
        
        addview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.newview = UIView(frame: CGRect(x: 15, y: 80, width: screenSize.width - 30 , height: screenSize.height - 100 ))
        self.addview.addSubview(newview)
        newview.center = view.center
        //  self.newview.backgroundColor = UIColor.blue
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: newview.frame.width, height: newview.frame.height - 50))
        myTableView.dataSource = self
        myTableView.delegate = self
        let view1: UIView = UIView(frame: CGRect(x:0, y:0,width: newview.frame.width , height: 55));
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:10,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textColor = UIColor.black
        label.isHighlighted = true
        label.font = UIFont.boldSystemFont(ofSize: 20)
        self.myTableView.tableHeaderView = view1
        // if buttonIndex == 0{
        label.text = NSLocalizedString("SelectCountry", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        myTableView.register(UINib(nibName: "profilecountryTableViewCell", bundle: nil), forCellReuseIdentifier: "myIdentifier")
        newview.addSubview(myTableView)
        self.myTableView.alwaysBounceVertical = false
        newview.layer.cornerRadius = 10;
        newview.layer.masksToBounds = true
        view1.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        view1.layer.masksToBounds = true
        let button: UIButton = UIButton.init(frame: CGRect(x:newview.frame.width - 65, y:newview.frame.height - 45,width: 60  , height:  40));
        newview.addSubview(button);
        
        button.layer.cornerRadius = 3.0
        button.setTitle( NSLocalizedString("Done", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.black, for: UIControlState.normal)
        button.addTarget(self, action:#selector(self.okView(_:)), for:.touchUpInside)
        label.textColor = UIColor.black
        newview.backgroundColor = UIColor.white
        label.isHighlighted = true
        label.font = UIFont.boldSystemFont(ofSize: 20)
        myTableView.reloadData()
    }
    @objc func okView(_ sender: UIButton)
    {
        
        
        newview.isHidden = true
        addview.isHidden = true
        myTableView.isHidden = true
        //keywordoutlet.setTitle(tickValue, for: UIControlState())
        //  newview.isHidden = true
        
    }
    func NotificationloadapiCalling(apikey:String){
        activityview.startactvityIndicator()
        self.NotificationArray.removeAll()
        
        let scriptUrl = apiClass.Notifications
        
        let parameters: Parameters = ["apikey": apikey]
        
        Alamofire.request(scriptUrl, method: .get, parameters: parameters)
            .responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success( let JSON):
                    do{
                        
                        let response = JSON as! NSDictionary
                        print(response)
                        
                          if (response["notify"]) != nil {
                        let notify = response.object(forKey: "notify")!
                        for singlenotify in (notify as! [[String:Any]]){
                            let singleNotificationElement = morenotificationClass()
                            let contry = singlenotify as NSDictionary
                            if  (contry["asciiname"] as? String) != nil {
                                let asciiname = contry["asciiname"] as! String
                                singleNotificationElement.asciiname = asciiname

                            }
                            if  (contry["name"] as? String) != nil {
                                let name = contry["name"] as! String
                                singleNotificationElement.name = name

                            }
                            if  (contry["id"] as? String) != nil {
                                let request_id = contry["id"] as! String
                                singleNotificationElement.request_id = request_id
                                 print(request_id)
                                print("&&&&&&&&&&&&&&&&")
                            }

                            self.NotificationArray.append(singleNotificationElement)
                           
                        }
                        }
                        
                        
                        DispatchQueue.main.async
                            {
                                 self.activityview.stopActivityIndicator()
                                if ((self.NotificationArray).count == 0){
                                    self.myTableView.isHidden = true
                                    self.newview.isHidden = true
                                    self.moreTable.isHidden = true
                                    self.newview.backgroundColor = UIColor.red
                                    self.createeventNothingview()
                                }else{
                                self.createNotificationTableview()
                                self.mynotiTableView.reloadData()
                                let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                                swipeDown.direction = UISwipeGestureRecognizerDirection.down
                                self.view.addGestureRecognizer(swipeDown)
                                }
                              
                                
                        }
                        
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        
                    }
                    break
                case .failure(let errorGiven):
                    
                    print(errorGiven)
                    print(String(data: response.data!, encoding: String.Encoding.utf8) ?? "")
                    //  failure(APICaller.parseErrorAndGiveMessage(givenError: errorGiven as NSError))
                    break
                }
            })
        
        
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            let screenSize: CGRect = UIScreen.main.bounds
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                UIView.transition(with: view, duration: 1.0, options: .transitionCrossDissolve, animations: {
                    // self.newview = UIView(frame: CGRect(x: 0, y: self.view.frame.height , width: screenSize.width , height: screenSize.height ))
                    self.notinewview.center.y = screenSize.height + 300
                    //  self.mainView.center.y = self.view.frame.height - self.mainView.frame.height/2
                })
                
            //self.newview.isHidden = true
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        let screenSize: CGRect = UIScreen.main.bounds
        self.newview.center.y = screenSize.height + 300
        self.moreTable.isHidden = false
        self.customview.isHidden = true
        //self.myTableView.isHidden = true
    }
    func createNotificationTableview(){
        let screenSize: CGRect = UIScreen.main.bounds
        self.notinewview = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width , height: screenSize.height ))
        self.view.addSubview(notinewview)
        notinewview.backgroundColor = UIColor.black
        let view1: UIView = UIView(frame: CGRect(x:0, y:20,width: notinewview.frame.width , height: 65));
        self.notinewview.addSubview(view1)
        
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:15,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.textAlignment = NSTextAlignment.center
        self.mynotiTableView = UITableView(frame: CGRect(x: 0, y: 125, width: notinewview.frame.width, height: notinewview.frame.height))
        mynotiTableView.dataSource = self
        mynotiTableView.delegate = self
        
        label.textColor = UIColor.white
        label.isHighlighted = true
        view1.backgroundColor  = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 20)
        mynotiTableView.register(UINib(nibName: "morenotificationTableviewCell", bundle: nil), forCellReuseIdentifier: "mynotiIdentifier")
    label.text = NSLocalizedString("Slidedown", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        notinewview.addSubview(mynotiTableView)
        mynotiTableView.backgroundColor = UIColor.black
        self.mynotiTableView.separatorStyle = .none
        
        self.mynotiTableView.alwaysBounceVertical = false
        mynotiTableView.estimatedRowHeight = 44.0
        mynotiTableView.rowHeight = UITableViewAutomaticDimension
        mynotiTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0); //values
     //   self.mynotiTableView.reloadData()
        
        
    }
    func createeventNothingview(){
        customview.isHidden = false
        customview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(customview)
        let view1: UIView = UIView(frame: CGRect(x:0, y:customview.frame.height/2,width: customview.frame.width , height: 55));
        customview.addSubview(view1)
        let label: UILabel = UILabel.init(frame: CGRect(x:10, y:0,width: view1.frame.width - 20 , height: 30));
        view1.addSubview(label)
        label.text = NSLocalizedString("Nothing to see here", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.textColor = UIColor.gray
    }
    func createlanguageview(){
       
        
        languageview  = languageView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
        self.view.addSubview(languageview)
        languageview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        languageview.lastview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        languageview.myview.layer.cornerRadius = 8.0
        languageview.englishLabel.text =  NSLocalizedString("English", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        languageview.arabicLabel.text =  NSLocalizedString("Arabic", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
        languageview.okOutlet.layer.borderColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
           languageview.okOutlet.layer.cornerRadius = 12
       languageview.okOutlet.layer.borderWidth = 1.0;
        languageview.cancelButton.layer.borderColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
        languageview.cancelButton.layer.cornerRadius = 12
        languageview.cancelButton.layer.borderWidth = 1.0;
         languageview.englishButton.isSelected = true
         languageview.englishButton.iconColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
         languageview.englishButton.indicatorColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        if UserDefaults.standard.string(forKey: "language_code") == "en"{
            languageview.englishButton.isSelected = true
        }else{
            languageview.arabicButton.isSelected = true
        }
        languageview.englishButton.addTarget(self, action: #selector(englishButtonAction), for: .touchUpInside)
        languageview.arabicButton.addTarget(self, action: #selector(arabicButtonAction), for: .touchUpInside)
        languageview.okOutlet.addTarget(self, action: #selector(sendbuttonAction), for: .touchUpInside)
        languageview.cancelButton.addTarget(self, action: #selector(cancellanbuttonAction), for: .touchUpInside)
        
    }
    @objc func englishButtonAction(_ sender: UIButton)
    {
         languageview.englishButton.isSelected = true
          languageview.englishButton.iconColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
          languageview.englishButton.indicatorColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
       languageview.arabicButton.iconColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
         languageview.arabicButton.indicatorColor = UIColor.clear
        language = 1
    }
    
    @objc func arabicButtonAction(_ sender: UIButton)
    {
        languageview.arabicButton.isSelected = true
        languageview.arabicButton.iconColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        languageview.arabicButton.indicatorColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        languageview.englishButton.iconColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0)
        languageview.englishButton.indicatorColor = UIColor.clear
        language = 2
    }
    @objc func sendbuttonAction(_ sender: UIButton)
     {
        if language == 1{
            LanguageManager.sharedInstance.setLocale("en")
            UserDefaults.standard.set("en", forKey:"language_code")
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
            if let vcs = tabbarVC.viewControllers,
                
                let nc = vcs.first as? UINavigationController,
                let pendingOverVC = nc.topViewController as? HomeViewController {
                
                
            }
            
            
            self.present(tabbarVC, animated: false, completion: nil)
        }else{
            LanguageManager.sharedInstance.setLocale("ar")
            UserDefaults.standard.set("ar", forKey:"language_code")
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabbarVC = storyboard.instantiateViewController(withIdentifier: "hometab") as! UITabBarController
            if let vcs = tabbarVC.viewControllers,
                
                let nc = vcs.first as? UINavigationController,
                let pendingOverVC = nc.topViewController as? HomeViewController {
                
                
            }
            //    let navController = UINavigationController(rootViewController: vcs)
            
            self.present(tabbarVC, animated: false, completion: nil)
        }
   
        
    }
    @objc func cancellanbuttonAction(_ sender: UIButton)
    {
        languageview.isHidden = true
    }
    
    
    @objc func acceptView(_ sender: UIButton)
    {
    requestacceptorrejectapiCalling(apikey:apikey,request_id:NotificationArray[sender.tag].request_id,status: "Accepted")
        NotificationArray.remove(at: sender.tag)
        self.mynotiTableView.reloadData()
    }
    @objc func rejectView(_ sender: UIButton)
    {
        requestacceptorrejectapiCalling(apikey:apikey,request_id:NotificationArray[sender.tag].request_id,status: "Declined")
        NotificationArray.remove(at: sender.tag)
         self.mynotiTableView.reloadData()
    }
    func requestacceptorrejectapiCalling(apikey:String,request_id:String,status: String){
        activityview.startactvityIndicator()
        let scriptUrl = apiClass.ManageFriends
        
        let urlWithParams = scriptUrl + "?UUID=\(NSUUID().uuidString)"
        
        let myUrl = URL(string: urlWithParams);
        
        var request = URLRequest(url:myUrl!)
        
        let postString = "apikey=\(apikey)&request_id=\(request_id)&status=\(status)"
        print(">>>>>>>>>")
        print(postString)
        request.httpBody = postString.data(using: .utf8)
        
        request.httpMethod = "POST"
        let task = URLSession.shared.dataTask(with: request)
        {
            data, response, error in
            
            if error != nil
            {
                print("error=\(error)")
                DispatchQueue.main.async
                    {
                        print("server down")
                }
                
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("################################")
            print(responseString)
            do{
                
                if let convertedJsonIntoArray = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                    
                {
                    print(convertedJsonIntoArray)
                    self.status = (convertedJsonIntoArray["status"] as! NSString) as String
                    print(self.status)
                   
                    
                }
                
                
                DispatchQueue.main.async
                    {
                        if self.status == "success"{
                        if self.statusValue == 1 {
                            self.activityview.Successalert(SubTitle:NSLocalizedString("Accepted", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                        }else{
                            self.activityview.FailurealertWithSubtitle(SubTitle: NSLocalizedString("Declined", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: ""))
                            
                        
                        }
                        }else{
                            self.activityview.Failurealert()
                        }
                        
                        self.activityview.stopActivityIndicator()
                }
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
                
            }
            
            
        }
        task.resume()
        
    }
//    func createhowlikusersview(){
//
//        howlikUsersview  = howlikusersView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height))
//        self.view.addSubview(howlikUsersview)
//        howlikUsersview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        howlikUsersview.lastview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        howlikUsersview.enteremailLabel.text =  NSLocalizedString("English", bundle: LanguageManager.sharedInstance.getCurrentBundle(), comment: "")
//        howlikUsersview.sendOutlet.layer.borderColor = UIColor(red: 224/255.0, green: 45/255.0, blue: 93/255.0, alpha: 1.0).cgColor
//        howlikUsersview.sendOutlet.layer.cornerRadius = 12
//        howlikUsersview.sendOutlet.layer.borderWidth = 1.0;
//        howlikUsersview.tagsView.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addConstraint(NSLayoutConstraint(
//            item: self.view,
//            attribute: .leading,
//            relatedBy: .equal,
//            toItem: howlikUsersview.tagsView,
//            attribute: .leading,
//            multiplier: 1,
//            constant: 0)
//        )
//        // cell.normalview.addSubview(tagsView)
//        howlikUsersview.tagsView.paddingLeftRight = 6
//        howlikUsersview.tagsView.paddingTopBottom = 4
//        howlikUsersview.tagsView.marginLeftRight = 15
//
//        howlikUsersview.tagsView.marginTopBottom = 4
//        howlikUsersview.tagsView.removeAll()
//
//        if first == 1{
//
//               howlikUsersview.tagsView.tags = "click here"
//            first = 2
//        }else{
//
//            howlikUsersview.tagsView.append(contentsOf:emailArray)
//        }
//        howlikUsersview.tagsView.lastTag = "+"
//     //   howlikUsersview.tagsView.delegate = self
//
//
//    }
    
  
}
