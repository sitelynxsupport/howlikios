//
//  morenotificationTableviewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/18/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class morenotificationTableviewCell: UITableViewCell {
    @IBOutlet var myview: UIView!
    
    @IBOutlet var asciinameLabel: UILabel!
    @IBOutlet var rejectOutlet: UIButton!
    @IBOutlet var acceptOutlet: UIButton!
    
    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
