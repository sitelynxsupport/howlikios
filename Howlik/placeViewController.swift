//
//  placeViewController.swift
//  Howlik
//
//  Created by Shrishti Informatics on 2/5/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import GoogleMaps

class placeViewController: UIViewController,GMSPlacePickerViewControllerDelegate {
var Placename = ""
var Placeaddress = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
            let  config = GMSPlacePickerConfig(viewport: nil)
            let placePicker = GMSPlacePickerViewController(config: config)
        
        present(placePicker, animated: true, completion: nil)
        placePicker.delegate = self
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        let  config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        present(placePicker, animated: true, completion: nil)
        placePicker.delegate = self
    }
    
    @IBAction func place(_ sender: Any) {
    
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace)
    {
        
        print("&&&&&&&&&%%%%%%%%%%%%%%%%%%%%%%")
        // Dismiss the place picker, as it cannot dismiss itself.
       viewController.dismiss(animated: true, completion: nil)
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        UserDefaults.standard.set(Placename, forKey: "Placename")
        UserDefaults.standard.set(Placeaddress, forKey: "Placeaddress")
        UserDefaults.standard.set(place.coordinate.latitude, forKey: "placelattitude")
        UserDefaults.standard.set(place.coordinate.longitude, forKey: "placelongitude")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    
}
