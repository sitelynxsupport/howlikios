//
//  replyView.swift
//  Howlik
//
//  Created by Shrishti Informatics on 5/9/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class replyView: UIView {

   
    @IBOutlet var toTextlabel: UILabel!
    
    @IBOutlet var firstLabel: UILabel!
    
    @IBOutlet var cancelOut: UIButton!
    @IBOutlet var okOutet: UIButton!
    @IBOutlet var contentLabel: UITextView!
  
    @IBOutlet var tolabel: UILabel!
    @IBOutlet var Contentview: UIView!
    
    @IBOutlet var lastview: UIView!
    override init(frame: CGRect){ // for using custom viw in code
        super.init(frame: frame)
       commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
        
        commonInit()
    }
    private func commonInit(){
       //  we are going to do stuff here
        let nibView = Bundle.main.loadNibNamed("replyView", owner: self, options: nil)![0] as! UIView
        nibView.frame = self.bounds;
        self.addSubview(nibView)
    }

}
