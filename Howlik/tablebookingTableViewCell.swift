//
//  tablebookingTableViewCell.swift
//  Howlik
//
//  Created by Shrishti Informatics on 3/6/18.
//  Copyright © 2018 Shrishti Informatics. All rights reserved.
//

import UIKit

class tablebookingTableViewCell: UITableViewCell {

    @IBOutlet var deleteOutlet: UIButton!
    @IBOutlet var tablelabel: UILabel!
    @IBOutlet var slotlabel: UILabel!
    @IBOutlet var availabilitylabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
